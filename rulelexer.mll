(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

{
open Ruleparser

exception Eof

let rec replace_escquote' q qlen idx result =
    if idx = qlen then result
    else if idx + 2 < qlen then
        let front = String.sub q 0 2 in
        if front = "\\'" then
            replace_escquote' q qlen (idx + 2) (result ^ "'")
        else
            let firstchar = String.sub q idx 1 in
            replace_escquote' q qlen (idx + 1) (result ^ firstchar)
    else
        let firstchar = String.sub q idx 1 in
        replace_escquote' q qlen (idx + 1) (result ^ firstchar)

let replace_escquote q =
    let qlen = String.length q in
    replace_escquote' q qlen 0 ""
}

rule token = parse
    [' ' '\t' '\n']         { token lexbuf }
  | "#" [^ '\n']*           { token lexbuf }
  | "begin"                 { BLOCK_START }
  | "end"                   { BLOCK_END }
  | '/'                     { FWDSLASH }
  | '\\'                    { BWDSLASH }
  | '('                     { LPAREN }
  | ')'                     { RPAREN }
  | '<'                     { LDIR }
  | '>'                     { RDIR }
  | '&'                     { CONJ }
  | '!'                     { UNARY }
  | ":-"                    { DEFN }
  | ':'                     { COLON }
  | ','                     { COMMA }
  | ';'                     { EOS }
  | '='                     { EQUAL }
  | '{'                     { LBRC }
  | '}'                     { RBRC }
  | "@attr"                 { CMD_ATTR }
  | "@add_cats"             { CMD_ADDCATS }
  | "@transform"            { CMD_TRANSFORM }
  | "@uncertain"            { CMD_UNCERTAIN }
  | "@no_enum"              { CMD_NO_ENUM }
  | "@leftpunc"             { CMD_LEFTPUNC }
  | "@rightpunc"            { CMD_RIGHTPUNC }
  | "@left_serial"          { CMD_LEFTSER }
  | "@right_serial"         { CMD_RIGHTSER }
  | "@enum_only_unc"        { CMD_ENUM_ONLY_UNC }
  | "@enum_all"             { CMD_ENUM_ALL }
  | "@accept"               { CMD_ACCEPT }
  | "@typology"             { CMD_TYPOLOGY }
  | "@dmv"                  { CMD_DMV }
  | "@evg"                  { CMD_EVG }
  | "@ghg"                  { CMD_GHG }
  | "@none"                 { CMD_NONE }
  | "@arbitrary"            { CMD_ARBITRARY }
  | "@enable_split_head"    { CMD_ENABLE_SPLIT_HEAD }
  | "@disable_split_head"   { CMD_DISABLE_SPLIT_HEAD }
  | ['A'-'Z' 'a'-'z' '0'-'9' '_' '-' '.' '?' '+' '$']+ as value
                            { ID(value) }
  | '[' ['A'-'Z' 'a'-'z' '0'-'9' '_' '-' '.' '?' '+' '$']* ']' as value
                            { CATMACRO(value) }
  | ''' ([^ ''' '\n'] | "\\'")* ''' as quote
                            { let q = String.sub quote 1 (String.length quote - 2) in
                              QUOTE(replace_escquote q) }
  | eof                     { raise Eof }

type disp_t =
    Guage of int
  | Number of int
  | Percentage of int
  | NumberNoMax
  | WindMill of int
  | GuageWithWindMill of int * int
  | GuageWithNumber of int
  | GuageWithPercentage of int
  | PercentageWithWindMill of int * int
  | StaticInfix
val add : disp_t -> string -> string -> unit
val discard : unit -> unit
val inc : unit -> unit
val display : unit -> unit
val print : string -> unit
val clearline : unit -> unit
val redisplay : unit -> unit
val go_quiet : unit -> unit

open Printf

type disp_t =
    Guage of int
  | Number of int
  | Percentage of int
  | NumberNoMax
  | WindMill of int
  | GuageWithWindMill of int * int
  | GuageWithNumber of int
  | GuageWithPercentage of int
  | PercentageWithWindMill of int * int
  | StaticInfix

module Scale = struct

    type t = {
        mutable indstk  : int ref list;
        mutable dispstk : disp_t list;
        mutable cntstk  : int ref list;
        mutable oldstrs : string ref list;
        mutable prfstks : string list;
        mutable sfxstks : string list;
    }

    let guagelen = 40.0

    let create () = 
    {
        indstk  = [];
        dispstk = [];
        cntstk  = [];
        oldstrs = [];
        prfstks = [];
        sfxstks = [];
    }

    let bsp n =
        fprintf stderr "%s[K" (String.make n '\x08');
        flush stderr

    let length scale =
        List.length scale.indstk

    let peek_top scale =
        (
            List.hd scale.indstk, List.hd scale.dispstk, 
            List.hd scale.cntstk, List.hd scale.oldstrs,
            List.hd scale.prfstks, List.hd scale.sfxstks
        )

    let pop scale =
        scale.indstk <- List.tl scale.indstk;
        scale.dispstk <- List.tl scale.dispstk;
        scale.cntstk <- List.tl scale.cntstk;
        scale.oldstrs <- List.tl scale.oldstrs;
        scale.prfstks <- List.tl scale.prfstks;
        scale.sfxstks <- List.tl scale.sfxstks

    let push scale disp cnt prefix suffix =
        scale.indstk <- ref 0 :: scale.indstk;
        scale.dispstk <- disp :: scale.dispstk;
        scale.cntstk <- ref cnt :: scale.cntstk;
        scale.oldstrs <- ref "" :: scale.oldstrs;
        scale.prfstks <- prefix :: scale.prfstks;
        scale.sfxstks <- suffix :: scale.sfxstks

    let gen_guage cnt max =
        let scl = float_of_int cnt *. guagelen /. float_of_int max in
        let bar = String.make (int_of_float scl) '=' in
        "|" ^ bar ^ ">"

    let gen_windmill cnt friction =
        let millno = (cnt / friction) mod 4 in
        String.sub "|/-\\" millno 1

    let gen_number cnt max =
        sprintf "%d/%d" cnt max

    let gen_number_nomax cnt =
        sprintf "%d" cnt

    let gen_percentage cnt max =
        let percentage = 100.0 *. (float_of_int cnt) /. (float_of_int max) in
        sprintf "%6.2f%%" percentage

    let show_meter ?(force=false) ind disp cnt oldstr prefix suffix =
        let inner = match disp with
            Guage max -> gen_guage !cnt max
          | Number max -> gen_number !cnt max
          | Percentage max -> gen_percentage !cnt max
          | NumberNoMax -> gen_number_nomax !cnt
          | WindMill friction -> gen_windmill !cnt friction
          | GuageWithWindMill (max, friction) ->
                (gen_guage !cnt max) ^ " " ^ (gen_windmill !cnt friction)
          | GuageWithNumber max ->
                (gen_guage !cnt max) ^ " " ^ (gen_number !cnt max)
          | GuageWithPercentage max ->
                (gen_guage !cnt max) ^ " " ^ (gen_percentage !cnt max)
          | PercentageWithWindMill (max, friction) ->
                (gen_percentage !cnt max) ^ " "
                ^ (gen_windmill !cnt friction) 
          | StaticInfix -> "" in
        let str = prefix ^ inner ^ suffix in
        if str <> !oldstr || force then begin
            bsp !ind;
            let len = String.length str in
            ind := len;
            fprintf stderr "%s" str;
            flush stderr;
            oldstr := str
        end else ()

    let add scale disp prefix suffix =
        push scale disp 0 prefix suffix

    let discard scale =
        if length scale > 0 then
            let (ind, _, _, _, _, _) = peek_top scale in
            bsp !ind;
            pop scale
        else ()

    let clearline scale =
        List.iter (fun ind -> bsp !ind) scale.indstk

    let inc scale =
        if length scale > 0 then
            let (_, _, cnt, _, _, _) = peek_top scale in
            cnt := !cnt + 1
        else ()

    let display scale =
        if length scale > 0 then
            let (ind, disp, cnt, oldstr, prefix, suffix) = peek_top scale in
            show_meter ind disp cnt oldstr prefix suffix
        else ()

    let redisplay scale =
        for i = List.length scale.indstk - 1 downto 0 do
            let oldstr = List.nth scale.oldstrs i in
            fprintf stderr "%s" !oldstr;
            flush stderr
        done

end

let scale = Scale.create ()

let verbose = ref true

let go_quiet () =
    verbose := false

let add disp prefix suffix =
    if !verbose then
        Scale.add scale disp prefix suffix
    else ()

let discard () =
    if !verbose then
        Scale.discard scale
    else ()

let inc () =
    if !verbose then
        Scale.inc scale
    else ()

let display () =
    if !verbose then
        Scale.display scale
    else ()

let print text =
    if !verbose then begin
        fprintf stderr "%s" text;
        flush stderr
    end else ()

let clearline () =
    if !verbose then
        Scale.clearline scale
    else ()

let redisplay () =
    if !verbose then
        Scale.redisplay scale
    else ()

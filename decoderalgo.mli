module Viterbi :
sig
    type syncat_t = Cg.Syncat.t
    type genmodel_t = Deriv.DerivGenModel.t
    type chart_t = Chart.Chart.t
    type tree_t = Tree.Tree.t
    type t
    val create : genmodel_t -> chart_t -> syncat_t -> float -> float -> float -> t
    val compute_viterbi : t -> unit
    val find_max : t -> tree_t * float
    val compute : genmodel_t -> chart_t -> syncat_t -> float -> float -> float -> tree_t * float
end

module MaxExpViterbi :
sig
    type syncat_t = Cg.Syncat.t
    type genmodel_t = Deriv.DerivGenModel.t
    type chart_t = Chart.Chart.t
    type tree_t = Tree.Tree.t
    type t
    val create : genmodel_t -> chart_t -> syncat_t -> float -> float -> float -> t
    val compute_viterbi : t -> unit
    val find_max : t -> tree_t * float
    val compute : genmodel_t -> chart_t -> syncat_t -> float -> float -> float -> tree_t * float
end

module MinBayesRisk :
sig
    type syncat_t = Cg.Syncat.t
    type genmodel_t = Deriv.DerivGenModel.t
    type chart_t = Chart.Chart.t
    type tree_t = Tree.Tree.t
    type t
    val create : genmodel_t -> chart_t -> syncat_t -> float -> float -> float -> t
    val compute_viterbi : t -> unit
    val find_min : t -> tree_t * float
    val compute : genmodel_t -> chart_t -> syncat_t -> float -> float -> float -> tree_t * float
end

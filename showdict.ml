open Dict
open Dictloader

open Printf

let showdict fname =
    printf "\nDictionary: %s\n\n" fname;
    let dict = DictLoader.load fname true in
    printf "%s\n" (Dict.to_string dict)

let main =
    let fnames = ref [] in
    let args = [
        ("--seedlvl",
            Arg.Int Utils.set_seedlvl,
            "  Set the seed level (default = 100)")
    ]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg = 
        "\nLoad the dictionary from the specified files and display the result of each one.\n\n"
        ^ (sprintf "Usage: %s dict1 dict2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    List.iter showdict !fnames

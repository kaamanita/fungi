open Dep
open Conlldep
open Printf

let showdepdb fname =
    let id = ref 0 in
    let depstrs = ConllDep.from_file fname in
    printf "\nDependency database: %s\n\n" fname;
    List.iter
        (fun depstr ->
            incr id;
            printf "\n# Dependency structure %d\n" !id;
            printf "%s\n" (DepStruct.to_string depstr))
        depstrs

let showdepdb_conll fname =
    let id = ref 0 in
    let content = ConllDep.compile fname in
    printf "\nDependency database: %s\n\n" fname;
    List.iter
        (fun entries ->
            incr id;
            printf "\n# Dependency structure %d\n" !id;
            printf "%s\n" (ConllDep.entries_to_conll entries))
        content

let main =
    let fnames = ref [] in
    let args = []
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg =
        "\nDisplay all dependency structures read from the specified CoNLL2006-formatted databases.\n\n"
        ^ (sprintf "Usage: %s depdb1 depdb2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    List.iter showdepdb_conll !fnames

(** Chart Database. *)

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

(** Chart Database. This module is a packed-chart-parameterized 
    sequential hash database. *)
module ChartDb :
sig

    (** Key: chart ID. *)
    type key_t = int

    (** Value: packed chart *)
    type value_t = Chart.Chart.t

    (** Chart database. *)
    type t

    (** [create fname] creates a chart database with a file name 
        [fname]. *)
    val create : string -> t

    (** [add cdb chart] adds a packed chart [chart] to a chart database
        [cdb]. *)
    val add : t -> value_t -> unit

    (** [copy cdb] copies a chart database. *)
    val copy : t -> t

    (** [find cdb id] finds a packed chart with a chart ID [id]. *)
    val find : t -> key_t -> value_t

    (** [find_all cdb id] finds all packed charts with a chart ID [id]. *)
    val find_all : t -> key_t -> value_t list

    (** [mem cdb id] checks if a chart ID [id] is in a chart database
        [cdb]. *)
    val mem : t -> key_t -> bool

    (** [remove cdb id] removes a packed chart identified by its ID [id]
        from a chart database [cdb]. *)
    val remove : t -> key_t -> unit

    (** [replace cdb id chart] replaces a packed chart [chart] to the
        chart entry [id] in a chart database [cdb]. *)
    val replace : t -> key_t -> value_t -> unit

    (** [iter fn cdb] iterates a function [fn] over a chart database 
        [cdb]. *)
    val iter : (key_t -> value_t -> unit) -> t -> unit

    (** [fold fn cdb initval] folds a function [fn] over a chart
        database [cdb] with an initial value [initval]. *)
    val fold : (key_t -> value_t -> 'a -> 'a) -> t -> 'a -> 'a

    (** [length cdb] finds the total number of entries in a chart
        database [cdb].  *)
    val length : t -> int

    (** [from_file fname] loads a chart database from a file [fname]. *)
    val from_file : string -> t

    (** [to_file cdb fname] stores a chart database [cdb] into a file
        [fname]. *)
    val to_file : t -> string -> unit

end

(** Chart Database Wrapper. *)
module ChartDbWrapper :
sig

    (** Key: chart ID. *)
    type key_t = int

    (** Value: packed chart. *)
    type value_t = Chart.Chart.t

    (** Sequential chart database. *)
    type seqhdb_t = ChartDb.t

    (** Chart database wrapper. *)
    type t

    (** [create cdbs] creates a chart database wrapper from a list of
        chart databases [cdbs]. *)
    val create : seqhdb_t list -> t

    (** [from_files fnames] creates a chart database wrapper from a list
        of file names [fnames]. *)
    val from_files : string list -> t

    (** [get_nodbs dbw] finds the total number of chart databases in
        a chart database wrapper [dbw]. *)
    val get_nodbs : t -> int

    (** [get_noitems dbw] finds the total number of packed charts in
        a chart database wrapper [dbw]. *)
    val get_noitems : t -> int

    (** [nth dbw i] gets the [i]-th packed chart in a chart database
        wrapper [dbw]. *)
    val nth : t -> int -> seqhdb_t

    (** [calc_relidx dbw i] computes the relative index of the [i]-th
        packed chart in a chart database wrapper [dbw]. The result is
        in the form of (chart database ID, local chart ID). *)
    val calc_relidx : t -> int -> int * int

    (** [mem dbw i] checks if there is the [i]-th packed chart in a
        chart database wrapper [dbw]. *)
    val mem : t -> int -> bool

    (** [find dbw i] finds the [i]-th packed chart in a chart database
        wrapper [dbw]. *)
    val find : t -> int -> value_t

    (** [find_all dbw i] finds all [i]-th packed charts in a chart
        database wrapper [dbw]. *)
    val find_all : t -> int -> value_t list

    (** [length dbw] finds the total number of packed charts in a chart
        database wrapper [dbw]. *)
    val length : t -> int

    (** [replace dbw i chart] replaces the [i]-th packed chart in a
        chart database wrapper [dbw] with another packed chart [chart].*)
    val replace : t -> int -> value_t -> unit

    (** [iter fn dbw] iterates a function [fn] over a chart database
        wrapper [dbw]. *)
    val iter : (key_t -> value_t -> unit) -> t -> unit

end

open Eval
open Printf

let main =
    let title = ref ""
    and topno = ref 20
    and goldbrks = ref []
    and golddeps = ref []
    and conlldeps = ref []
    and treedbs = ref []
    and disphist = ref false in
    let args = [
        ("--quiet", Arg.Unit Scale.go_quiet, "  Quiet mode (default = false)");
        ("--history",
            Arg.Set disphist,
            "  Also display the history (corrects, overgenerations, and undergenerations) (default = no)");
        ("--title",
            Arg.Set_string title,
            "[title]   Set the title of the experiment (default = empty)");
        ("--topno",
            Arg.Set_int topno,
            "[int]   Set the number of top correct/error entries (default = 20)");
        ("--goldbrks",
            Arg.String (fun gb -> goldbrks := !goldbrks @ [gb]),
            "[goldbrks.txt]   Specify a gold-standard bracket file (can be specified more than once)");
        ("--golddeps",
            Arg.String (fun gd -> golddeps := !golddeps @ [gd]),
            "[golddeps.txt]   Specify a gold-standard dependency file (can be specified more than once)");
        ("--conlldeps",
            Arg.String (fun cd -> conlldeps := !conlldeps @ [cd]),
            "[conlldeps.txt]   Specify a gold-standard CoNLL2006-formatted dependency file (can be specified more than once)")
    ]
    and fn_anon = (fun treedbfname -> treedbs := !treedbs @ [treedbfname])
    and usagemsg =
        "\nEvaluate the parse results in the tested files with the gold standard.\n\n"
        ^ (sprintf "Usage: %s [options] treedb1 treedb2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    if List.length !goldbrks > 0 && List.length !golddeps > 0 then begin
        let eval = Evaluator.create !title !topno !goldbrks !golddeps !treedbs in
        Evaluator.run eval;
        printf "<Scores>\n%s\n\n" (Evaluator.scores_to_string eval);
        if !disphist then
            printf "%s\n" (Evaluator.history_to_string eval)
        else ()
    end;
    if List.length !conlldeps > 0 then begin
        (* Printf.printf "eval is being created.\n"; *)
        let eval = ConllEvaluator.create !title !topno !conlldeps !treedbs in
        (* Printf.printf "eval is being run.\n"; *)
        ConllEvaluator.run eval;
        (* Printf.printf "eval shows the scores.\n"; *)
        if !disphist then
            printf "%s\n" (ConllEvaluator.history_to_string eval)
        else
            printf "%s\n" (ConllEvaluator.scores_to_string eval)
    end

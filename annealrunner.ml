open Cg
open Chart
open Dict
open Cky
open Chartdb
open Prob
open Deriv
open Probinit
open Models
open Inout
open Em
open Anneal
open Printf
open Utils

type inoutmode_t = StandardInOut | VBInOut

type ann_t = NoAnn | DetAnn | SkewedDetAnn | StructAnn

type annmode_t = 
    NoAnnealing
  | DetAnnealing of float * float
  | SkewedDetAnnealing of float * float
  | StructAnnealing of float * float * float

module AnnealRunner = struct

    type syncat_t = Syncat.t

    let score_prior 
            deriv dict start defprior knownderivprior startderivprior maxslash =
        let catconst = get_catconst () in
        let prems = Deriv.get_prems deriv
        (* and consq = Deriv.get_consq deriv *) in
        let syncat = Deriv.get_syncat prems in
        if catconst > 0.0 then 
            if not (Syncat.is_bo_cat syncat) then
                catconst *. (Syncat.slashno syncat +. 1.0)
            else catconst
        else if catconst < 0.0 then
            if not (Syncat.is_bo_cat syncat) then
                -1.0 *. catconst *. (maxslash -. Syncat.slashno syncat +. 1.0)
            else -1.0 *. catconst *. (maxslash +. 1.0)
        (*
        else if Syncat.equal_ign_attrs (Deriv.get_syncat prems) start then
            startderivprior
        *)
        else if not (Syncat.is_bo_cat syncat) then
            knownderivprior
        else defprior
    
    let init_ruleprior 
            n epsilon dict start defprior knownderivprior startderivprior 
            ruleprob maxslash =
        let ruleprior = Models.create_pcfg n epsilon in
        DerivGenModel.iter
            ruleprob
            (fun deriv prob ->
                let prior = score_prior deriv dict start 
                    defprior knownderivprior startderivprior maxslash in
                DerivGenModel.set_prob ruleprior deriv prior);
        ruleprior
    
    let init_probs 
            wrapper n epsilon probflags dict start 
            defprior knownderivprior startderivprior =
        let ruleprob = Models.create_model n epsilon probflags
        and rulebias = Models.create_model n epsilon probflags in
        let maxslash = ProbInit.learn ruleprob rulebias 
            (ChartDbWrapper.find wrapper) 
            (ChartDbWrapper.get_noitems wrapper) in
        let ruleprior = init_ruleprior 
            n epsilon dict start defprior knownderivprior startderivprior 
            ruleprob maxslash in
        (ruleprob, rulebias, ruleprior)
    
    let run_anneal
            inoutmode babystep annmode rulebias 
            n diffthr rndthr epsilon ruleprob ruleprior start
            get_chart nocharts =
        let compute_inout = match inoutmode with
            StandardInOut -> 
                if not babystep then StandardInOutAlgo.compute
                else StandardBabystepInOutAlgo.compute
          | VBInOut -> 
                if not babystep then VBInOutAlgo.compute 
                else VBBabystepInOutAlgo.compute in
        match annmode with
            NoAnnealing -> 
                let initparam = EmParam.create rndthr in
                EmAnnealer.compute
                    n diffthr rndthr epsilon ruleprob ruleprior 
                    start get_chart nocharts compute_inout initparam
          | DetAnnealing (inittemp, epoch) -> 
                (* finaltemp = 1.0 always *)
                let initparam = DetParam.create inittemp epoch in
                DetAnnealer.compute 
                    n diffthr rndthr epsilon ruleprob ruleprior 
                    start get_chart nocharts compute_inout initparam
          | SkewedDetAnnealing (inittemp, epoch) -> 
                (* finaltemp = 1.0 always *)
                let initparam = SkewedDetParam.create 
                    inittemp epoch rulebias in
                SkewedDetAnnealer.compute
                    n diffthr rndthr epsilon ruleprob ruleprior 
                    start get_chart nocharts compute_inout initparam
          | StructAnnealing (initdelta, epoch, finaldelta) -> 
                let initparam = StructParam.create 
                    initdelta epoch finaldelta in
                StructAnnealer.compute
                    n diffthr rndthr epsilon ruleprob ruleprior 
                    start get_chart nocharts compute_inout initparam
    
    let train 
            compute n diffthr rndthr epsilon ruleprob ruleprior 
            start wrapper =
        let get_chart = ChartDbWrapper.find wrapper 
        and nocharts = ChartDbWrapper.length wrapper in
        compute 
            n diffthr rndthr epsilon 
            ruleprob ruleprior start get_chart nocharts (* ;
        ChartDbWrapper.close wrapper *)
    
    let save_prob ruleprob ofname =
        DerivGenModel.to_file ruleprob ofname
    
    let compute 
            probflags inoutmode babystep annmode diffthr rndthr epsilon
            start dict defprior knownderivprior startderivprior leapfrogthr
            fnames ofname =
        let n = 1000 in
        let wrapper = (* ChartDbWrapper.opendbs fnames [Mdb.ReadOnly] in *)
                      ChartDbWrapper.from_files fnames in
        let (ruleprob, rulebias, ruleprior) = init_probs 
            wrapper n epsilon probflags dict start 
            defprior knownderivprior startderivprior in
        if babystep then Em.leapfrogthr := leapfrogthr
        else ();
        Utils.set_save_prob 
            (fun () -> save_prob ruleprob ofname);
        train (run_anneal inoutmode babystep annmode rulebias) 
            n diffthr rndthr epsilon ruleprob ruleprior start wrapper;
        save_prob ruleprob ofname;
        flush stdout

end

let set_probflags probflags str =
    let probflag = match str with
        "roleemis" -> [RoleEmis]
      | "hockenmaier" -> [JuliaEmis]
      | "lexemis" -> [LexEmis]
      | "heademis" -> [HeadEmis]
      | "model0" -> []
      | "model1" -> [RoleEmis]
      | "model2" -> [JuliaEmis]
      | "model3" -> [RoleEmis; LexEmis]
      | "model4" -> [JuliaEmis; HeadEmis]
      | "model5" -> [RoleEmis; LexEmis; JuliaEmis; HeadEmis]
      | _ -> failwith "set_probflags" in
    probflags := union probflag !probflags

let set_inoutmode inoutmode str =
    let m = match str with
        "standard" -> StandardInOut
      | "vb" -> VBInOut
      | _ -> failwith "set_inoutmode" in
    inoutmode := m

let set_annmode annmode str =
    let m = match str with
        "none" -> NoAnn
      | "da" -> DetAnn
      | "sda" -> SkewedDetAnn
      | "sa" -> StructAnn
      | _ -> failwith "set_annmode" in
    annmode := m

let set_start start str =
    let lexbuf = Lexing.from_string str in
    let syncat = Ruleparser.syncat Rulelexer.token lexbuf in
    start := syncat

let conv_annparam annmode initparam epoch finalparam =
    match annmode with
        NoAnn -> NoAnnealing
      | DetAnn -> DetAnnealing (initparam, epoch)
      | SkewedDetAnn -> SkewedDetAnnealing (initparam, epoch)
      | StructAnn -> StructAnnealing (initparam, epoch, finalparam)

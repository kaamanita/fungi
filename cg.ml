(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Printf
open Avm
open Utils

module Syncat = struct

    type avm_t = Avm.t

    type depdir_t = Left | Unary | Coor | Right | LeftSerial | RightSerial

    type deptag_t = 
        NoDep 
      | LeftDep 
      | RightDep 
      | LeftToRightDep 
      | RightToLeftDep

    type sertag_t = NoSer | LeftSer | RightSer

    type t = {
        syncat  : elem_t;
        is_conj : bool;
        deptag  : deptag_t;
        serial  : sertag_t;
        ftrs    : avm_t;
    }

    and elem_t =
        Atom of string
      | Bwd of slash_t
      | Fwd of slash_t

    and slash_t = {
        consq  : t;
        prems  : t;
        depdir : depdir_t;
    }

    let conn_cat = {
        syncat  = Atom "&";
        is_conj = false;
        deptag  = NoDep;
        serial  = NoSer;
        ftrs    = Avm.create_empty ();
    }

    let conn_cat_depbank = {
        syncat  = Atom "&&";
        is_conj = false;
        deptag  = NoDep;
        serial  = NoSer;
        ftrs    = Avm.create_empty ();
    }

    let conn_cat_truedep = {
        syncat  = Atom "&&&";
        is_conj = false;
        deptag  = NoDep;
        serial  = NoSer;
        ftrs    = Avm.create_empty ();
    }

    let conn_cat_rev = {
        syncat  = Atom "&&&&";
        is_conj = false;
        deptag  = NoDep;
        serial  = NoSer;
        ftrs    = Avm.create_empty ();
    }

    let bo_cat deptag = {
        syncat  = Atom "*";
        is_conj = false;
        deptag  = deptag;
        serial  = NoSer;
        ftrs    = Avm.create_empty ();
    }

    let leftpunc_cat deptag = {
        syncat  = Atom "<:";
        is_conj = false;
        deptag  = deptag;
        serial  = NoSer;
        ftrs    = Avm.create_empty ();
    }

    let rightpunc_cat deptag = {
        syncat  = Atom ":>";
        is_conj = false;
        deptag  = deptag;
        serial  = NoSer;
        ftrs    = Avm.create_empty ();
    }

    let root_cat = {
        syncat  = Atom "ROOT";
        is_conj = false;
        deptag  = NoDep;
        serial  = NoSer;
        ftrs    = Avm.create_empty ();
    }

    let create_atom ?(is_conj=false) ?(deptag=NoDep) 
            ?(ftrs=Avm.create_empty ()) value = 
    {
        syncat  = Atom value;
        is_conj = is_conj;
        deptag  = deptag;
        serial  = NoSer;
        ftrs    = ftrs;
    }

    let create_bwd ?(is_conj=false) ?(deptag=NoDep) 
            ?(ftrs=Avm.create_empty ()) consq prems depdir = 
    {
        syncat  = Bwd {
            consq  = consq; 
            prems  = prems; 
            depdir = depdir;
        };
        is_conj = is_conj;
        deptag  = deptag;
        serial  = NoSer;
        ftrs    = ftrs;
    }

    let create_fwd ?(is_conj=false) ?(deptag=NoDep) 
            ?(ftrs=Avm.create_empty ()) consq prems depdir = 
    {
        syncat  = Fwd {
            consq  = consq; 
            prems  = prems; 
            depdir = depdir;
        };
        is_conj = is_conj;
        deptag  = deptag;
        serial  = NoSer;
        ftrs    = ftrs;
    }

    let create ?(serial : sertag_t = NoSer) syncat is_conj deptag ftrs = {
        syncat  = syncat;
        is_conj = is_conj;
        deptag  = deptag;
        serial  = serial;
        ftrs    = ftrs;
    }

    let copy ?(ftrs : avm_t = Avm.create []) (cat : t) : t = {
        syncat = cat.syncat;
        is_conj = cat.is_conj;
        deptag = cat.deptag;
        serial = cat.serial;
        ftrs = Avm.replace cat.ftrs ftrs;
    }

    let get_syncat cat =
        cat.syncat

    let is_conj cat = cat.is_conj

    let is_serial cat =
        match cat.serial with
            NoSer -> false
          | _ -> true

    let get_deptag cat =
        cat.deptag

    let get_serial cat =
        cat.serial

    let get_ftrs cat =
        cat.ftrs

    let is_atom cat =
        match cat.syncat with
            Atom _ -> true
          | _ -> false

    let is_fwd cat =
        match cat.syncat with
            Fwd _ -> true
          | _ -> false

    let is_bwd cat =
        match cat.syncat with
            Bwd _ -> true
          | _ -> false

    let get_value cat =
        match cat.syncat with
            Atom value -> value
          | _ -> failwith "get_value"

    let get_consq cat =
        match cat.syncat with
            Fwd slash | Bwd slash ->
                slash.consq
          | _ -> failwith "get_conseq"

    let get_prems cat =
        match cat.syncat with
            Fwd slash | Bwd slash ->
                slash.prems
          | _ -> failwith "get_premise"

    let get_depdir cat =
        match cat.syncat with
            Fwd slash | Bwd slash ->
                slash.depdir
          | _ -> failwith "get_depdir"

    let depdir_to_string depdir =
        match depdir with
            Left -> "<"
          | Right -> ">"
          | Unary -> "!"
          | Coor -> "&"
          | LeftSerial -> "L"
          | RightSerial -> "R"

    let ord_depdir depdir =
        match depdir with
            Left -> 0
          | Unary -> 1
          | Coor -> 2
          | Right -> 3
          | LeftSerial -> 4
          | RightSerial -> 5

    let deptag_to_string deptag =
        match deptag with
            NoDep -> "N"
          | LeftDep -> "L"
          | RightDep -> "R"
          | LeftToRightDep -> "LR"
          | RightToLeftDep -> "RL"

    let ord_deptag deptag =
        match deptag with
            NoDep -> 0
          | LeftDep -> 1
          | RightDep -> 2
          | LeftToRightDep -> 3
          | RightToLeftDep -> 4

    let ord_sertag serial =
        match serial with
            NoSer -> 0
          | LeftSer -> 1
          | RightSer -> 2

    let sertag_to_string sertag =
        match sertag with
            NoSer -> ""
          | LeftSer -> "<S"
          | RightSer -> ">S"

    let ord_ftrs ftrs =
        Avm.hash ftrs

    let rec to_string cat =
        let result1 =
            if not cat.is_conj then 
                to_string_syncat cat.syncat cat.ftrs
            else 
                sprintf "[%s]&" (to_string_syncat cat.syncat cat.ftrs)
        and result2 = sertag_to_string cat.serial in
        result1 ^ result2

    and to_string_paren cat =
        if not cat.is_conj then begin
            match cat.syncat with
                Atom value -> value
              | _ -> 
                    sprintf "(%s)" (to_string_syncat cat.syncat cat.ftrs)
        end else sprintf "[%s]&" (to_string_syncat cat.syncat cat.ftrs)

    and to_string_syncat cat ftrs =
        match cat with
            Atom value -> 
                sprintf "%s%s" value (Avm.to_string_cat ftrs)
          | Bwd slash ->
                sprintf "%s\\%s%s%s" (to_string slash.consq) 
                    (depdir_to_string slash.depdir) 
                    (Avm.to_string_cat ftrs)
                    (to_string_paren slash.prems)
          | Fwd slash ->
                sprintf "%s/%s%s%s" (to_string slash.consq) 
                    (depdir_to_string slash.depdir) 
                    (Avm.to_string_cat ftrs)
                    (to_string_paren slash.prems)

    and to_string_syncat_paren cat ftrs =
        match cat with
            Atom value -> value
          | Bwd _ | Fwd _ ->
                sprintf "(%s)" (to_string_syncat cat ftrs)

    let rec equal cat1 cat2 =
        equal_syncat cat1.syncat cat2.syncat
        && cat1.is_conj = cat2.is_conj
        && cat1.deptag = cat2.deptag
        && cat1.serial = cat2.serial
        && Avm.equal cat1.ftrs cat2.ftrs

    and equal_syncat cat1 cat2 =
        match cat1, cat2 with
            Atom v1, Atom v2 -> v1 = v2
          | Bwd slash1, Bwd slash2
          | Fwd slash1, Fwd slash2 ->
                equal slash1.consq slash2.consq 
                    && equal slash1.prems slash2.prems 
                    && slash1.depdir = slash2.depdir
          | _, _ -> false

    let is_leftpunc_cat cat =
        equal_syncat cat.syncat (Atom "<:")

    let is_rightpunc_cat cat =
        equal_syncat cat.syncat (Atom ":>")

    let is_punc_cat cat =   
        is_leftpunc_cat cat || is_rightpunc_cat cat

    let is_bo_cat cat =
        equal_syncat cat.syncat (Atom "*") || is_punc_cat cat 

    let rec equal_ign_attrs cat1 cat2 =
        equal_syncat_ign_attrs cat1.syncat cat2.syncat

    and equal_syncat_ign_attrs cat1 cat2 =
        match cat1, cat2 with
            Atom v1, Atom v2 -> v1 = v2
          | Bwd slash1, Bwd slash2 
          | Fwd slash1, Fwd slash2 ->
                equal_ign_attrs slash1.consq slash2.consq 
                    && equal_ign_attrs slash1.prems slash2.prems 
                    && slash1.depdir = slash2.depdir
          | _, _ -> false

    let rec hash cat =
        Hashtbl.hash cat.is_conj + Hashtbl.hash cat.deptag + Hashtbl.hash cat.serial
            + hash_syncat cat.syncat + Avm.hash cat.ftrs

    and hash_syncat cat =
        match cat with
            Atom value -> Hashtbl.hash value
          | Fwd slash ->
                hash slash.consq + hash slash.prems 
                    + Hashtbl.hash slash.depdir + Hashtbl.hash Right
          | Bwd slash ->
                hash slash.consq + hash slash.prems 
                    + Hashtbl.hash slash.depdir + Hashtbl.hash Left

    let rec compare cat1 cat2 =
        let comp_syncat = compare_syncat cat1.syncat cat2.syncat in
        if comp_syncat <> 0 then comp_syncat else
        let comp_isconj = 
            match cat1.is_conj, cat2.is_conj with
                false, false | true, true -> 0
              | false, true -> 1
              | true, false -> -1 in
        if comp_isconj <> 0 then comp_isconj else
        let comp_deptag = compare_deptag cat1.deptag cat2.deptag in
        if comp_deptag <> 0 then comp_deptag else
        let comp_sertag = compare_sertag cat1.serial cat2.serial in
        if comp_sertag <> 0 then comp_sertag else
        compare_ftrs cat1.ftrs cat2.ftrs
    
    and compare_syncat cat1 cat2 =
        match cat1, cat2 with
            Atom _, Fwd _ | Atom _, Bwd _ | Bwd _, Fwd _ -> 1
          | Fwd _, Atom _ | Bwd _, Atom _ | Fwd _, Bwd _ -> -1
          | Atom value1, Atom value2 -> 
                Stdlib.compare value1 value2
          | Fwd slash1, Fwd slash2 
          | Bwd slash1, Bwd slash2 ->
                compare_slash slash1 slash2

    and compare_slash slash1 slash2 =
        let comp_consq = compare slash1.consq slash2.consq in
        if comp_consq <> 0 then comp_consq
        else
            let comp_prems = compare slash1.prems slash2.prems in
            if comp_prems <> 0 then comp_prems
            else 
                (ord_depdir slash1.depdir) - (ord_depdir slash2.depdir)

    and compare_deptag deptag1 deptag2 =
        Stdlib.compare (ord_deptag deptag1) (ord_deptag deptag2)

    and compare_sertag sertag1 sertag2 =
        Stdlib.compare (ord_sertag sertag1) (ord_sertag sertag2)

    and compare_ftrs ftrs1 ftrs2 =
        Stdlib.compare (ord_ftrs ftrs1) (ord_ftrs ftrs2)

    let rec slashno cat =
        match cat.syncat with
            Atom _ -> 0.0
          | Fwd slash | Bwd slash -> 
                1.0 +. slashno slash.consq +. slashno slash.prems

    let rec score ?(depth = 0.0) const cat =
        if const = 1.0 then 1.0
        else
            match cat.syncat with
                Atom _ -> const ** depth
              | Fwd slash | Bwd slash ->
                    let inner = 
                        score ~depth:(depth +. 1.0) const slash.consq
                    and arg = 
                        score ~depth:0.0 const slash.prems in
                    inner *. arg

    let penalize_category const cat =
        score (10.0 ** const) cat

    let rec cat_prior ?(depth = 1.0) const cat =
        if const = 0.0 then 0.0
        else
            match cat.syncat with
                Atom _ -> const *. depth
              | Fwd slash | Bwd slash ->
                    let inner = 
                        cat_prior ~depth:(depth +. 1.0) const slash.consq
                    and arg = 
                        cat_prior ~depth:1.0 const slash.prems in
                    inner +. arg

end

let is_split_head_enabled = ref false

module CG = struct
   
    type syncat_t = Syncat.t

    type depdir_t = Syncat.depdir_t

    type transf_t = (syncat_t * syncat_t list) list

    exception Combination_failure
    
    let rec transform_inner (rules : transf_t) cat result =
        let result' = ref result in
        let ftrs_cat = Syncat.get_ftrs cat in
        List.iter
            (fun (syncat, newcats) ->
                let ftrs_syncat = Syncat.get_ftrs syncat in
                if Syncat.equal_ign_attrs syncat cat 
                    && Avm.subsumes ftrs_syncat ftrs_cat then 
                begin
                    let deptag = Syncat.get_deptag syncat in
                    let newcats = List.map 
                        (fun c -> 
                            let ftrs = Syncat.get_ftrs c in
                            let newftrs = Avm.replace ftrs_cat ftrs in
                            Syncat.create (Syncat.get_syncat c) 
                                false deptag newftrs)
                        newcats in
                    let newcats' = diff newcats !result' in
                    result' := union newcats' !result';
                    List.iter
                        (fun newcat' ->
                            result' := 
                                transform_inner rules newcat' !result')
                        newcats'
                end else ())
            rules;
        !result'

    let transform (rules : transf_t) cat =
        transform_inner rules cat []

    let enable_split_head () = is_split_head_enabled := true
    
    let disable_split_head () = is_split_head_enabled := false

    let gen_deptag_nocond _ _ =
        Syncat.NoDep

    let gen_deptag_cond olddeptag depdir =
        match olddeptag, depdir with
            Syncat.NoDep, Syncat.Right -> Syncat.RightDep
          | Syncat.RightDep, Syncat.Right -> Syncat.RightDep
          | Syncat.LeftDep, Syncat.Right -> Syncat.LeftToRightDep
          | Syncat.LeftToRightDep, Syncat.Right -> Syncat.LeftToRightDep
          | Syncat.NoDep, Syncat.Left -> Syncat.LeftDep
          | Syncat.LeftDep, Syncat.Left -> Syncat.LeftDep
          | Syncat.RightDep, Syncat.Left -> Syncat.RightToLeftDep
          | Syncat.RightToLeftDep, Syncat.Left -> Syncat.RightToLeftDep
          | _, _ -> raise Combination_failure

    let gen_deptag olddeptag depdir deptagcond_temp_enabled =
        if !is_split_head_enabled || deptagcond_temp_enabled then
            gen_deptag_cond olddeptag depdir
        else
            gen_deptag_nocond olddeptag depdir

    let is_ftrs_subsume depdir ftrs1 ftrs2 =
        match depdir with
            Syncat.Left | Syncat.LeftSerial -> Avm.subsumes ftrs2 ftrs1
          | Syncat.Right | Syncat.RightSerial -> Avm.subsumes ftrs1 ftrs2
          | Syncat.Unary | Syncat.Coor -> raise Combination_failure

    let combine_ftrs depdir ftrs1 ftrs2 =
        match depdir with
            Syncat.Left | Syncat.LeftSerial -> Avm.combine ftrs2 ftrs1
          | Syncat.Right | Syncat.RightSerial -> Avm.combine ftrs1 ftrs2
          | Syncat.Unary | Syncat.Coor -> raise Combination_failure

    let funcapp cat1 cat2 =
        let results = ref [] in
        let deptagcond_temp_enabled = ref false in
        let ftrs1 = Syncat.get_ftrs cat1
        and ftrs2 = Syncat.get_ftrs cat2 in
        if Syncat.is_fwd cat1 && not (Syncat.is_serial cat1)
                && Syncat.equal_ign_attrs (Syncat.get_prems cat1) cat2 
                && is_ftrs_subsume (Syncat.get_depdir cat1) ftrs1 ftrs2 
        then
            let ftrs = combine_ftrs (Syncat.get_depdir cat1) ftrs1 ftrs2 in
            let result = 
                Syncat.get_consq cat1, Syncat.get_depdir cat1, ftrs in
            results := result :: !results
        else if Syncat.is_bwd cat2 && not (Syncat.is_serial cat2)
                && Syncat.equal_ign_attrs (Syncat.get_prems cat2) cat1 
                && is_ftrs_subsume (Syncat.get_depdir cat2) ftrs1 ftrs2 
        then
            let ftrs = combine_ftrs (Syncat.get_depdir cat2) ftrs1 ftrs2 in
            let result = 
                Syncat.get_consq cat2, Syncat.get_depdir cat2, ftrs in
            results := result :: !results
        else if (Syncat.is_punc_cat cat1 || Syncat.is_punc_cat cat2) then 
        begin
            if Syncat.is_punc_cat cat1 && Syncat.is_punc_cat cat2 then ()
            else begin
                if Syncat.is_leftpunc_cat cat1 
                        && not (Syncat.equal cat2 Syncat.root_cat) then
                    results := (cat2, Syncat.Right, ftrs2) :: !results
                else ();
                if Syncat.is_rightpunc_cat cat2 
                        && not (Syncat.equal cat1 Syncat.root_cat) then
                    results := (cat1, Syncat.Left, ftrs1) :: !results
                else ()
            end
        end else if Syncat.is_bo_cat cat1 
                || Syncat.is_bo_cat cat2 then begin
            let result1 = 
                Syncat.bo_cat (Syncat.get_deptag cat1), Syncat.Left, ftrs1
            and result2 = 
                Syncat.bo_cat (Syncat.get_deptag cat2), Syncat.Right, ftrs2 
            in
            results := result1 :: !results;
            results := result2 :: !results;
            deptagcond_temp_enabled := true
        end else raise Combination_failure;
        let results' = ref [] in
        List.iter
            (fun (oldconsq, depdir, ftrs) ->
                if Syncat.equal_ign_attrs oldconsq Syncat.root_cat then
                    deptagcond_temp_enabled := false
                else ();
                let syncat = Syncat.get_syncat oldconsq in
                let isconj = Syncat.is_conj oldconsq in
                let olddeptag = 
                    match depdir with
                        Syncat.Left | Syncat.LeftSerial -> Syncat.get_deptag cat1
                      | Syncat.Right | Syncat.RightSerial -> Syncat.get_deptag cat2
                      | Syncat.Coor | Syncat.Unary -> 
                            failwith "funcapp" in
                try
                    let deptag = gen_deptag 
                        olddeptag depdir !deptagcond_temp_enabled in
                    let consq = 
                        Syncat.create syncat isconj deptag ftrs in
                    results' := (consq, depdir) :: !results'
                with Combination_failure -> ())
            !results;
        !results'
   
    let conjoin cat1 cat2 cat3 =
        if Syncat.equal_ign_attrs cat1 cat3 
                && (Syncat.equal cat2 Syncat.conn_cat 
                    || Syncat.equal cat2 Syncat.conn_cat_depbank
                    || Syncat.equal cat2 Syncat.conn_cat_truedep
                    || Syncat.equal cat2 Syncat.conn_cat_rev)
                && not (Syncat.is_conj cat1) then
            let syncat1 = Syncat.get_syncat cat1 in
            let ftrs = Syncat.get_ftrs cat1 in
            Syncat.create syncat1 true Syncat.LeftDep ftrs
        else raise Combination_failure

    let conjoin_opt cat1 cat3 =
        if Syncat.equal_ign_attrs cat1 cat3 
                && not (Syncat.is_conj cat1) then
            let syncat1 = Syncat.get_syncat cat1 in
            let ftrs = Syncat.get_ftrs cat1 in
            Syncat.create syncat1 true Syncat.LeftDep ftrs
        else raise Combination_failure

    let conjoin_opt_depbank cat1 cat3 =
        if Syncat.equal_ign_attrs cat1 cat3 
                && not (Syncat.is_conj cat1) then
            let syncat1 = Syncat.get_syncat Syncat.conn_cat_depbank in
            let ftrs = Syncat.get_ftrs cat1 in
            Syncat.create syncat1 true Syncat.LeftDep ftrs
        else raise Combination_failure

    let left_serialize cat1 cat2 =
        if Syncat.equal_ign_attrs cat1 cat2
                && (cat1.serial = Syncat.NoSer || cat1.serial = Syncat.LeftSer)
                && not (Syncat.is_conj cat1)
                && cat2.serial = Syncat.NoSer then
            let syncat1 = Syncat.get_syncat cat1 in
            let ftrs = Syncat.get_ftrs cat1 in
            Syncat.create syncat1 ~serial:LeftSer false Syncat.LeftDep ftrs
        else raise Combination_failure

    let right_serialize cat1 cat2 =
        if Syncat.equal_ign_attrs cat1 cat2
                && (cat2.serial = Syncat.NoSer || cat2.serial = Syncat.RightSer)
                && not (Syncat.is_conj cat1)
                && cat1.serial = Syncat.NoSer then
            let syncat1 = Syncat.get_syncat cat2 in
            let ftrs = Syncat.get_ftrs cat2 in
            Syncat.create syncat1 ~serial:RightSer false Syncat.RightDep ftrs
        else raise Combination_failure

end

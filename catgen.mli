(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

module Catgen :
sig
    type depscheme_t = HeadOutwardScheme | DepbankScheme
    type word_order_t =
        SVOI
      | SVIO
      | SIVO
      | ISVO
      | SOVI
      | SOIV
      | SIOV
      | ISOV
      | VSOI
      | VSIO
      | VISO
      | IVSO
      | OVSI
      | OVIS
      | OIVS
      | IOVS
      | OSVI
      | OSIV
      | OISV
      | IOSV
      | VOSI
      | VOIS
      | VIOS
      | IVOS
    val all_word_orders : word_order_t list
    type v_order_t = VerbMedial | VerbFinal | VerbInitial
    type so_order_t = SubjObj | ObjSubj
    type iobj_order_t = IobjOutLeft | IobjOutRight | IobjInLeft | IobjInRight
    val interpret_word_order :
      word_order_t -> v_order_t * so_order_t * iobj_order_t
    type vicomp_order_t = SVC | SCV | CSV | VSC | VCS | CVS
    val all_vicomp_orders : vicomp_order_t list
    type vtcomp_order_t =
        SVOC
      | SVCO
      | SCVO
      | CSVO
      | SOVC
      | SOCV
      | SCOV
      | CSOV
      | VSOC
      | VSCO
      | VCSO
      | CVSO
      | OVSC
      | OVCS
      | OCVS
      | COVS
      | OSVC
      | OSCV
      | OCSV
      | COSV
      | VOSC
      | VOCS
      | VCOS
      | CVOS
    val all_vtcomp_orders : vtcomp_order_t list
    type objcomp_order_t = ObjComp | CompObj
    type comp_order_t = CompOutLeft | CompOutRight | CompInLeft | CompInRight
    val interpret_vicomp_order : vicomp_order_t -> v_order_t * comp_order_t
    val interpret_vtcomp_order :
      vtcomp_order_t -> v_order_t * objcomp_order_t * comp_order_t
    type adj_order_t = NounAdj | AdjNoun
    val all_adj_orders : adj_order_t list
    type adv_order_t = VpAdv | AdvVp
    val all_adv_orders : adv_order_t list
    type advadj_order_t = AdvAdj | AdjAdv
    val all_advadj_orders : advadj_order_t list
    type modal_order_t = ModalVp | VpModal
    val all_modal_orders : modal_order_t list
    type part_order_t = PartVp | VpPart
    val all_part_orders : part_order_t list
    type adposition_t = Preposition | Postposition
    val all_adpositions : adposition_t list
    type nmod_order_t = NpNmod | NmodNp
    val all_nmod_orders : nmod_order_t list
    type vmod_order_t = VpVmod | VmodVp
    val all_vmod_orders : vmod_order_t list
    type poss_order_t =
        OwnerPossOwnee
      | OwneePossOwner
      | OwnerOwneePoss
      | OwneeOwnerPoss
      | PossOwnerOwnee
      | PossOwneeOwner
    val all_poss_orders : poss_order_t list
    type relpro_order_t = RelproComp | CompRelpro
    val all_relpro_orders : relpro_order_t list
    type relcls_order_t = NpRelcls | RelclsNp
    val all_relcls_orders : relcls_order_t list
    type subcls_order_t =
        ConjMainSubcls
      | MainConjSubcls
      | MainSubclsConj
      | ConjSubclsMain
      | SubclsConjMain
      | SubclsMainConj
    val all_subcls_orders : subcls_order_t list
    type npgerund_order_t = NpGerund | GerundNp
    val all_npgerund_orders : npgerund_order_t list
    type vpgerund_order_t = VpGerund | GerundVp
    val all_vpgerund_orders : vpgerund_order_t list
    type infvp_order_t = InfVp | VpInf
    val all_infvp_orders : infvp_order_t list
    type npnom_order_t = NomNp | NpNom
    val all_npnom_orders : npnom_order_t list
    type vpnom_order_t = NomVp | VpNom
    val all_vpnom_orders : vpnom_order_t list
    type smod_order_t = SmodSent | SentSmod
    val all_smod_orders : smod_order_t list
    type nump_order_t = NumCl | ClNum
    val all_nump_orders : nump_order_t list
    type nump_position_t = NumpAsAdj | NumpAsAdv | NumpAsNmod | NumpAsVmod
    val all_nump_positions : nump_position_t list
    type drop_t = SubjDrop | ObjDrop | IobjDrop
    type neg_order_t =
        NegV
      | VNeg
      | NegAdj
      | AdjNeg
      | NegAdv
      | AdvNeg
      | NegNmod
      | NmodNeg
      | NegVmod
      | VmodNeg
    val all_neg_orders : neg_order_t list
    type verb_decl_t = VDecl_by_subj | VDecl_by_obj | VDecl_by_iobj
    val all_verb_decls : verb_decl_t list
    type noun_verb_agr_t =
        NVAgr_by_gender
      | NVAgr_by_number
      | NVAgr_by_person
    val all_noun_verb_agrs : noun_verb_agr_t list
    type adj_noun_agr_t = ANAgr_by_gender | ANAgr_by_number | ANAgr_by_case
    val all_adj_noun_agrs : adj_noun_agr_t list
    type modal_infl_t = Modal_infl | Modal_req_infv
    type macroexp_t = Macro.MacroExpander.t
    type dict_t = Dict.Dict.t
    type t
    val create : macroexp_t -> dict_t -> t
    val change_depscheme : t -> depscheme_t -> unit
    val set_freewordorder : t -> bool -> unit
    val set_infl : t -> bool -> unit
    val add_verb_decl : t -> verb_decl_t -> unit
    val add_noun_verb_agr : t -> noun_verb_agr_t -> unit
    val add_adj_noun_agr : t -> adj_noun_agr_t -> unit
    val add_gender : t -> string -> unit
    val add_person : t -> string -> unit
    val add_number : t -> string -> unit
    val add_case_arg : t -> string -> unit
    val add_case_apposarg : t -> string -> unit
    val add_case_adj : t -> string -> unit
    val add_word_order : t -> word_order_t -> unit
    val allow_all_word_orders : t -> unit
    val add_vicomp_order : t -> vicomp_order_t -> unit
    val allow_all_vicomp_orders : t -> unit
    val add_vtcomp_order : t -> vtcomp_order_t -> unit
    val allow_all_vtcomp_orders : t -> unit
    val add_adj_order : t -> adj_order_t -> unit
    val allow_all_adj_orders : t -> unit
    val add_adv_order : t -> adv_order_t -> unit
    val allow_all_adv_orders : t -> unit
    val add_advadj_order : t -> advadj_order_t -> unit
    val allow_all_advadj_orders : t -> unit
    val add_modal_order : t -> modal_order_t -> unit
    val allow_all_modal_orders : t -> unit
    val add_modal_infl : t -> modal_infl_t -> unit
    val add_part_order : t -> part_order_t -> unit
    val allow_all_part_orders : t -> unit
    val add_adposition : t -> adposition_t -> unit
    val allow_all_adpositions : t -> unit
    val add_nmod_order : t -> nmod_order_t -> unit
    val allow_all_nmod_orders : t -> unit
    val add_vmod_order : t -> vmod_order_t -> unit
    val allow_all_vmod_orders : t -> unit
    val add_poss_order : t -> poss_order_t -> unit
    val allow_all_poss_orders : t -> unit
    val add_relpro_order : t -> relpro_order_t -> unit
    val allow_all_relpro_orders : t -> unit
    val add_relpro_infl : t -> adj_noun_agr_t -> unit
    val add_relcls_order : t -> relcls_order_t -> unit
    val allow_all_relcls_orders : t -> unit
    val add_subcls_order : t -> subcls_order_t -> unit
    val allow_all_subcls_orders : t -> unit
    val add_npgerund_order : t -> npgerund_order_t -> unit
    val allow_all_npgerund_orders : t -> unit
    val add_npgerund_infl : t -> adj_noun_agr_t -> unit
    val add_vpgerund_order : t -> vpgerund_order_t -> unit
    val allow_all_vpgerund_orders : t -> unit
    val add_infvp_order : t -> infvp_order_t -> unit
    val allow_all_infvp_orders : t -> unit
    val add_npnom_order : t -> npnom_order_t -> unit
    val allow_all_npnom_orders : t -> unit
    val add_vpnom_order : t -> vpnom_order_t -> unit
    val allow_all_vpnom_orders : t -> unit
    val add_smod_order : t -> smod_order_t -> unit
    val allow_all_smod_orders : t -> unit
    val add_nump_order : t -> nump_order_t -> unit
    val allow_all_nump_orders : t -> unit
    val add_nump_infl : t -> adj_noun_agr_t -> unit
    val add_nump_position : t -> nump_position_t -> unit
    val allow_all_nump_positions : t -> unit
    val add_neg_order : t -> neg_order_t -> unit
    val allow_all_neg_orders : t -> unit
    val enable_copula : t -> unit
    val disable_copula : t -> unit
    val enable_gerund_as_np : t -> unit
    val disable_gerund_as_np : t -> unit
    val enable_gerund_as_nmod : t -> unit
    val disable_gerund_as_nmod : t -> unit
    val enable_gerund_as_vmod : t -> unit
    val disable_gerund_as_vmod : t -> unit
    val enable_shift : t -> unit
    val disable_shift : t -> unit
    val add_drop : t -> drop_t -> unit
    val disable_drops : t -> unit
    val allow_all_drops : t -> unit
    val allows_subjdrop : t -> bool
    val allows_objdrop : t -> bool
    val allows_iobjdrop : t -> bool

    val set_head_adj : t -> bool -> unit
    val set_head_nmod : t -> bool -> unit
    val set_head_verb : t -> bool -> unit
    val set_head_copula : t -> bool -> unit
    val set_head_vmod : t -> bool -> unit
    val set_head_gmod : t -> bool -> unit
    val set_head_smod : t -> bool -> unit
    val set_head_adv : t -> bool -> unit
    val set_head_neg : t -> bool -> unit
    val set_head_modal : t -> bool -> unit
    val set_head_adpos : t -> bool -> unit
    val set_head_relpro : t -> bool -> unit
    val set_head_part : t -> bool -> unit
    val set_head_subconj_smod : t -> bool -> unit
    val set_head_subconj_phr : t -> bool -> unit
    val set_head_poss_nmod : t -> bool -> unit
    val set_head_poss_phr : t -> bool -> unit
    val set_head_inf : t -> bool -> unit
    val set_head_npnom : t -> bool -> unit
    val set_head_vpnom : t -> bool -> unit
    val set_head_cl : t -> bool -> unit

    val s : Cg.Syncat.t
    val np : Cg.Syncat.t
    val conj : Cg.Syncat.t
    val conj_depbank : Cg.Syncat.t
    val num : Cg.Syncat.t
    val gen_avm :
      ?is_infl:bool ->
      string -> string -> string -> string -> string -> Avm.Avm.t
    val enum_np_ftrs : ?is_infl:bool -> t -> Avm.Avm.t list
    val enum_s_ftrs : ?is_infl:bool -> t -> Avm.Avm.t list
    val gen_adjavms : ?is_infl:bool -> t -> Avm.Avm.t list
    val gen_adjs : t -> Cg.Syncat.t list
    val gen_nmods : t -> Cg.Syncat.t list
    val gen_nmods_for_gerund : t -> Cg.Syncat.t list
    val gen_nmods_for_relpro : t -> Cg.Syncat.t list
    val gen_vis : t -> v_order_t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_vis_with_prems :
      t -> v_order_t -> Cg.Syncat.t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_vts : t -> v_order_t -> Cg.Syncat.t list -> Cg.Syncat.t list
    val gen_vts_with_comp :
      t -> v_order_t -> Cg.Syncat.t list -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_vds :
      t -> v_order_t -> iobj_order_t -> Cg.Syncat.t list -> Cg.Syncat.t list
    val gen_vcompsents : t -> 'a -> Cg.Syncat.t list
    val gen_vicomps : t -> vicomp_order_t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_vicomps_rplc_vi :
      t -> vicomp_order_t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_vtcomps : t -> vtcomp_order_t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_vtcomps_rplc_vi :
      t -> vtcomp_order_t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_verbs_with_consq : t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_verbs_with_consq_vis : t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_verbs_with_consq_vts : t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_verbs_with_consq_vds : t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_verbs_with_consq_vicomps : t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_verbs_with_consq_vtcomps : t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_verbs : t -> Cg.Syncat.t list
    val gen_verbs_vis : t -> Cg.Syncat.t list
    val gen_verbs_vts : t -> Cg.Syncat.t list
    val gen_verbs_vds : t -> Cg.Syncat.t list
    val gen_verbs_vicomps : t -> Cg.Syncat.t list
    val gen_verbs_vtcomps : t -> Cg.Syncat.t list
    val gen_verbs_vcomps : t -> Cg.Syncat.t list
    val gen_vps_with_consq : t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_vps : t -> Cg.Syncat.t list
    val gen_vmods : t -> Cg.Syncat.t list
    val gen_vmods_for_gerund : t -> Cg.Syncat.t list
    val gen_gmods : t -> Cg.Syncat.t list
    val gen_smods : t -> Cg.Syncat.t list
    val gen_smods_final : t -> Cg.Syncat.t list
    val gen_advs : t -> Cg.Syncat.t list
    val gen_negs : t -> Cg.Syncat.t list
    val gen_modals : t -> Cg.Syncat.t list
    val gen_gerunds_with_consq : t -> Cg.Syncat.t -> Cg.Syncat.t list
    val gen_gerund_nouns : t -> Cg.Syncat.t list
    val gen_gerund_nmods : t -> Cg.Syncat.t list
    val gen_gerund_vmods : t -> Cg.Syncat.t list
    val gen_gerunds : t -> Cg.Syncat.t list
    val gen_pps : t -> Cg.Syncat.t list
    val gen_adpositions : t -> Cg.Syncat.t list
    val gen_prepositions : t -> Cg.Syncat.t list
    val gen_postpositions : t -> Cg.Syncat.t list
    val gen_relpros : t -> Cg.Syncat.t list
    val gen_copulas : t -> Cg.Syncat.t list
    val gen_parts : t -> Cg.Syncat.t list
    val gen_subconjs : t -> Cg.Syncat.t list
    val gen_posses : t -> Cg.Syncat.t list
    val gen_infs : t -> Cg.Syncat.t list
    val gen_npnoms : t -> Cg.Syncat.t list
    val gen_vpnoms : t -> Cg.Syncat.t list
    val gen_cls_adj : t -> Cg.Syncat.t list
    val gen_cls_adv : t -> Cg.Syncat.t list
    val gen_cls_nmod : t -> Cg.Syncat.t list
    val gen_cls_vmod : ?is_infl:bool -> t -> Cg.Syncat.t list
    val gen_cls : ?is_infl:bool -> t -> Cg.Syncat.t list
    val gen_np_to_varg : t -> unit
    val gen_np_to_adj : t -> unit
    val catset_to_string : Cg.Syncat.t list -> string
    val test : t -> unit
    val run : t -> unit
end

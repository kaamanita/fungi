open Dep
open Tree
open Treedb
open Printf

let ubracket_to_string ubracket =
    let (begpos, endpos) = ubracket in
    sprintf "(%d, %d)" begpos endpos

let ubrackets_to_string ubrackets =
    sprintf "[%s]"
        (String.concat ", " 
            (List.map ubracket_to_string ubrackets))

let bracket_to_string bracket =
    let ((begpos, endpos), head) = bracket in
    sprintf "(%d, %d, %s)" begpos endpos head

let brackets_to_string brackets =
    sprintf "[%s]"
        (String.concat ", " 
            (List.map bracket_to_string brackets))

let showtreedb showtree showdep showdepstr showdepstrcat showprob showubrackets showbrackets highlight fname =
    (* printf "\nTree database: %s\n\n" fname; *)
    let treedb = (* TreeDb.opendb fname [Mdb.ReadOnly] in *)
        TreeDb.from_file fname in
    let comment = showtree || showdep in
    TreeDb.iter
        (fun id treemonad ->
            match treemonad with
                Some (tree, prob, depstr, words, enum) -> 
                begin
                    let enum_str = if enum then " (enum)" else "" in
                    if showtree || showdep || showdepstr || showdepstrcat || showubrackets || showbrackets || showprob then
                        printf "# Tree %d%s\n" (id + 1) enum_str
                    else ();
                    if showdepstr then
                        printf "%s\n" (Tree.pp_depstruct tree words ~highlight:highlight ~showcat:false ~comment:comment)
                    else ();
                    if showdepstrcat then
                        printf "%s\n" (Tree.pp_depstruct tree words ~highlight:highlight ~showcat:true ~comment:comment)
                    else ();
                    if showtree then
                        printf "%s\n" (Tree.pp tree)
                    else ();
                    if showdep then
                        printf "%s\n" (Tree.to_conll tree words)
                    else ();
                    if showubrackets then
                        printf "<Unlabeled Brackets>\n%s\n"
                            (ubrackets_to_string (Tree.to_ubrackets tree))
                    else ();
                    if showbrackets then
                        printf "<Labeled Brackets>\n%s\n"
                            (brackets_to_string (Tree.to_brackets tree))
                    else ();
                    if showprob then
                        printf "<Probability>\nexp(%e)\n" prob
                    else ();
                    printf "\n"
                end
              | None ->
                begin
                    printf "<Tree %d>\nno parses\n\n" id
                end)
        treedb (* ;
    TreeDb.close treedb *)

let main =
    let fnames = ref [] in
    let showtree = ref false
    and showdep = ref false 
    and showdepstr = ref false 
    and showdepstrcat = ref false 
    and showprob = ref false 
    and showubrackets = ref false
    and showbrackets = ref false 
    and highlight = ref true in
    let args = [
        ("--tree", 
            Arg.Set showtree, 
            "  Also display tree structures");
        ("--dep", 
            Arg.Set showdep, 
            "  Also display dependency structures");
        ("--depstr", 
            Arg.Set showdepstr, 
            "  Also display dependency structures graphically");
        ("--depstrcat", 
            Arg.Set showdepstrcat, 
            "  Also display dependency structures graphically");
        ("--prob",
            Arg.Set showprob, 
            "  Also display the probability of each tree");
        ("--nohighlight",
            Arg.Clear highlight,
            "  Disable highlight colors");
        ("--ubrackets",
            Arg.Set showubrackets, 
            "  Also display unlabeled brackets of each tree");
        ("--brackets",
            Arg.Set showbrackets, 
            "  Also display labeled brackets of each tree")]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg = 
        "\nDisplay all trees in the specified tree databases.\n\n"
        ^ (sprintf "Usage: %s [options] treedb1 treedb2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    if not (!showtree || !showdep || !showdepstr || !showdepstrcat || !showprob || !showubrackets || !showbrackets) then
        showtree := true
    else ();
    List.iter (showtreedb !showtree !showdep !showdepstr !showdepstrcat !showprob !showubrackets !showbrackets !highlight) !fnames

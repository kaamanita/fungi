(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Cg
open Macro
open Dict
open Utils
open Printf
open Avm

let collect_words fnames =
    let words = ref [] in
    List.iter
        (fun fname ->
            let fhdl = open_in fname in
            try
                while true do
                    let line = input_line fhdl in
                    let sent = only_tags (split line) in
                    words := union !words (list_to_set sent)
                done
            with End_of_file -> close_in fhdl)
        fnames;
    !words

module Dmv = struct

    type macroexp_t = MacroExpander.t

    type dict_t = Dict.t

    type t = {
        macroexp    : macroexp_t;
        dict        : dict_t;
    }

    let create macroexp dict = {
        macroexp = macroexp;
        dict = dict;
    }

    let w = Syncat.create_atom "w"

    let wr = Syncat.create_atom "wr"

    let wl = Syncat.create_atom "wl"

    let wrl = Syncat.create_atom "wrl"

    let wlr = Syncat.create_atom "wlr"

    let s = Syncat.create_atom "s"

    let run dmv fnames =
        MacroExpander.add_cats dmv.macroexp "[word]" [w];
        MacroExpander.add_cats dmv.macroexp "[s]" [s];
        Dict.add_unarydrv dmv.dict w wr;
        (* Dict.add_unarydrv dmv.dict w wrl; *)
        Dict.add_unarydrv dmv.dict w wl;
        (* Dict.add_unarydrv dmv.dict w wlr; *)
        (* Dict.add_unarydrv dmv.dict w s; *)
        Dict.add_unarydrv dmv.dict w (Syncat.create_fwd wr s Syncat.Left);
        Dict.add_unarydrv dmv.dict w (Syncat.create_bwd wl s Syncat.Right);
        Dict.add_unarydrv dmv.dict wr (Syncat.create_fwd wr s Syncat.Left);
        Dict.add_unarydrv dmv.dict wr wrl;
        (* Dict.add_unarydrv dmv.dict wr s; *)
        Dict.add_unarydrv dmv.dict wl (Syncat.create_bwd wl s Syncat.Right);
        Dict.add_unarydrv dmv.dict wl wlr;
        (* Dict.add_unarydrv dmv.dict wl s; *)
        Dict.add_unarydrv dmv.dict wrl (Syncat.create_bwd wrl s Syncat.Right);
        Dict.add_unarydrv dmv.dict wlr (Syncat.create_fwd wlr s Syncat.Left);
        Dict.add_unarydrv dmv.dict wrl s;
        Dict.add_unarydrv dmv.dict wlr s;
        Dict.add_cat dmv.dict w;
        Dict.add_cat dmv.dict wr;
        Dict.add_cat dmv.dict wl;
        Dict.add_cat dmv.dict wrl;
        Dict.add_cat dmv.dict wlr;
        Dict.add_cat dmv.dict s;
        Dict.add_accept dmv.dict s;
        let words = collect_words fnames in
        List.iter
            (fun word -> 
                Dict.add_entry dmv.dict word w)
            words

end

module Evg = struct

    type macroexp_t = MacroExpander.t

    type dict_t = Dict.t

    type t = {
        macroexp : macroexp_t;
        dict     : dict_t;
    }

    let create macroexp dict = {
        macroexp = macroexp;
        dict = dict;
    }

    let w = Syncat.create_atom "w"

    let wr0 = Syncat.create_atom "wr0"
    let wr1 = Syncat.create_atom "wr1"

    let wrl0 = Syncat.create_atom "wrl0"
    let wrl1 = Syncat.create_atom "wrl1"

    let s = Syncat.create_atom "s"

    let run dmv fnames =
        CG.enable_split_head ();
        MacroExpander.add_cats dmv.macroexp "[word]" [w];
        MacroExpander.add_cats dmv.macroexp "[s]" [s];
        Dict.add_unarydrv dmv.dict w wr0;
        (* Dict.add_unarydrv dmv.dict w wrl0;
        Dict.add_unarydrv dmv.dict w s; *)
        Dict.add_unarydrv dmv.dict wr0 (Syncat.create_fwd wr1 s Syncat.Left);
        Dict.add_unarydrv dmv.dict wr1 (Syncat.create_fwd wr1 s Syncat.Left);
        Dict.add_unarydrv dmv.dict wr0 wrl0;
        (* Dict.add_unarydrv dmv.dict wr0 s; *)
        Dict.add_unarydrv dmv.dict wr1 wrl0;
        (* Dict.add_unarydrv dmv.dict wr1 s; *)
        Dict.add_unarydrv dmv.dict wrl0 (Syncat.create_bwd wrl1 s Syncat.Right);
        Dict.add_unarydrv dmv.dict wrl1 (Syncat.create_bwd wrl1 s Syncat.Right);
        Dict.add_unarydrv dmv.dict wrl0 s;
        Dict.add_unarydrv dmv.dict wrl1 s;
        Dict.add_cat dmv.dict w;
        Dict.add_cat dmv.dict wr0;
        Dict.add_cat dmv.dict wr1;
        Dict.add_cat dmv.dict wrl0;
        Dict.add_cat dmv.dict wrl1;
        Dict.add_cat dmv.dict s;
        Dict.add_accept dmv.dict s;
        let words = collect_words fnames in
        List.iter
            (fun word -> 
                Dict.add_entry dmv.dict word w)
            words

end

module LimitedEvg = struct

    type macroexp_t = MacroExpander.t

    type dict_t = Dict.t

    type t = {
        macroexp       : macroexp_t;
        dict           : dict_t;
        mutable noargs : int;
    }

    let create ?(noargs=4) macroexp dict = {
        macroexp = macroexp;
        dict = dict;
        noargs = noargs;
    }

    let set_noargs evg noargs =
        evg.noargs <- noargs
    
    let w = Syncat.create_atom "w"
    
    let wr i j = 
        Syncat.create_atom (Printf.sprintf "wr%d%d" i j)

    let wrl i j = 
        Syncat.create_atom (Printf.sprintf "wrl%d%d" i j)

    let s = Syncat.create_atom "s"

    let create_syncat head leftno rightno =
        let result = ref head in
        for i = 1 to leftno do
            result := Syncat.create_bwd !result s Syncat.Right
        done;
        for i = 1 to rightno do
            result := Syncat.create_fwd !result s Syncat.Left
        done;
        !result

    let run evg fnames =
        MacroExpander.add_cats evg.macroexp "[word]" [w];
        MacroExpander.add_cats evg.macroexp "[s]" [s];
        (* Dict.add_unarydrv evg.dict w (wr 0 0); *)
        for len = 0 to evg.noargs do
            for leftno = 0 to (evg.noargs - len) do
                let rightno = len - leftno in
                Dict.add_unarydrv evg.dict w 
                    (create_syncat s leftno rightno)
            done
        done;
        let words = collect_words fnames in
        Dict.add_cat evg.dict w;
        List.iter
            (fun word -> 
                Dict.add_entry evg.dict word w)
            words

end

module Ghg = struct

    type macroexp_t = MacroExpander.t

    type dict_t = Dict.t

    type branchpref_t =
        LeftThenRight
      | RightThenLeft
      | LeftOnly
      | RightOnly
      | NoFixedOrder

    type t = {
        macroexp           : macroexp_t;
        dict               : dict_t;
        mutable noargs     : int;
        mutable varg       : branchpref_t list;
        mutable causearg   : branchpref_t list;
        mutable adjarg     : branchpref_t list;
        mutable advarg     : branchpref_t list;
        mutable linkarg    : branchpref_t list;
        mutable transfarg  : branchpref_t list;
        mutable seriesarg  : branchpref_t list;
    }

    let create macroexp dict = {
        macroexp = macroexp;
        dict = dict;
        noargs = 3;
        varg = [];
        causearg = [];
        adjarg = [];
        advarg = [];
        linkarg = [];
        transfarg = [];
        seriesarg = [];
    }

    let set_noargs ghg noargs =
        ghg.noargs <- noargs

    let set_varg ghg varg =
        if not (List.mem varg ghg.varg) then
            ghg.varg <- varg :: ghg.varg
        else ()

    let set_causearg ghg varg =
        if not (List.mem varg ghg.causearg) then
            ghg.causearg <- varg :: ghg.causearg
        else ()

    let set_adjarg ghg adjarg =
        if not (List.mem adjarg ghg.adjarg) then
            ghg.adjarg <- adjarg :: ghg.adjarg
        else ()

    let set_advarg ghg advarg =
        if not (List.mem advarg ghg.advarg) then
            ghg.advarg <- advarg :: ghg.advarg
        else ()

    let set_linkarg ghg linkarg =
        if not (List.mem linkarg ghg.linkarg) then
            ghg.linkarg <- linkarg :: ghg.linkarg
        else ()

    let set_transfarg ghg transfarg =
        if not (List.mem transfarg ghg.transfarg) then
            ghg.transfarg <- transfarg :: ghg.transfarg
        else ()

    let set_seriesarg ghg seriesarg =
        if not (List.mem seriesarg ghg.seriesarg) then
            ghg.seriesarg <- seriesarg :: ghg.seriesarg
        else ()

    let inst = Syncat.create_atom "i"

    let modatt = Syncat.create_atom "m"

    let adjmod = Syncat.create_atom "j"

    let advmod = Syncat.create_atom "d"

    let link = Syncat.create_atom "l"

    let adpos = Syncat.create_atom "p"

    let pp = Syncat.create_atom "pp"

    let subord = Syncat.create_atom "o"

    let relativizer = Syncat.create_atom "r"

    let transf = Syncat.create_atom "t"

    let nominalizer = Syncat.create_atom "nz"

    let verbalizer = Syncat.create_atom "vz"

    let act argno totalno = 
        if argno = 0 && totalno = 0 then Syncat.create_atom "a"
        else Syncat.create_atom (Printf.sprintf "a_%d_%d" argno totalno)

    let act_l argno total =
        Syncat.create_atom (Printf.sprintf "al_%d_%d" argno total)

    let act_r argno total =
        Syncat.create_atom (Printf.sprintf "ar_%d_%d" argno total)

    let act_lr argno total =
        Syncat.create_atom (Printf.sprintf "alr_%d_%d" argno total)

    let act_rl argno total =
        Syncat.create_atom (Printf.sprintf "arl_%d_%d" argno total)

    let causative = Syncat.create_atom "c"

    let sent = Syncat.create_atom "s"

    let np = Syncat.create_atom "np"

    let gen_verb_leftthenright ghg =
        let acts = ref [] in
        for totalno = 1 to ghg.noargs do
            Dict.add_unarydrv ghg.dict (act 0 0) (act_l 0 totalno);
            acts := (act_l 0 totalno) :: !acts;
            for leftno = 0 to totalno do
                for k = 0 to leftno - 1 do
                    let v = Syncat.create_fwd (act_l (k + 1) totalno) (act_l k totalno) Syncat.Right in
                    acts := (act_l (k + 1) totalno) :: !acts;
                    Dict.add_unarydrv ghg.dict inst v
                done;
                Dict.add_unarydrv ghg.dict (act_l leftno totalno) (act_lr leftno totalno);
                acts := (act_lr leftno totalno) :: !acts;
                for k = leftno to totalno - 1 do
                    let v = Syncat.create_bwd (act_lr (k + 1) totalno) (act_lr k totalno) Syncat.Left in
                    acts := (act_lr (k + 1) totalno) :: !acts;
                    Dict.add_unarydrv ghg.dict inst v
                done;
            done;
            Dict.add_unarydrv ghg.dict (act_lr totalno totalno) sent
        done;
        !acts

    let gen_verb_rightthenleft ghg =
        let acts = ref [] in
        for totalno = 1 to ghg.noargs do
            Dict.add_unarydrv ghg.dict (act 0 0) (act_r 0 totalno);
            acts := (act_r 0 totalno) :: !acts;
            for rightno = 0 to totalno do
                for k = 0 to rightno - 1 do
                    let v = Syncat.create_bwd (act_r (k + 1) totalno) (act_r k totalno) Syncat.Left in
                    acts := (act_r (k + 1) totalno) :: !acts;
                    Dict.add_unarydrv ghg.dict inst v
                done;
                Dict.add_unarydrv ghg.dict (act_r rightno totalno) (act_rl rightno totalno);
                acts := (act_rl rightno totalno) :: !acts;
                for k = rightno to totalno - 1 do
                    let v = Syncat.create_fwd (act_rl (k + 1) totalno) (act_rl k totalno) Syncat.Right in
                    acts := (act_rl (k + 1) totalno) :: !acts;
                    Dict.add_unarydrv ghg.dict inst v
                done;
            done;
            Dict.add_unarydrv ghg.dict (act_rl totalno totalno) sent
        done;
        !acts

    let gen_verb_leftonly ghg =
        let acts = ref [] in
        for totalno = 1 to ghg.noargs do
            Dict.add_unarydrv ghg.dict (act 0 0) (act_rl 0 totalno);
            acts := (act_rl 0 totalno) :: !acts;
            for k = 0 to totalno - 1 do
                let v = Syncat.create_fwd (act_rl (k + 1) totalno) (act_rl k totalno) Syncat.Right in
                acts := (act_rl (k + 1) totalno) :: !acts;
                Dict.add_unarydrv ghg.dict inst v
            done;
            Dict.add_unarydrv ghg.dict (act_rl totalno totalno) sent
        done;
        !acts

    let gen_verb_rightonly ghg =
        let acts = ref [] in
        for totalno = 1 to ghg.noargs do
            Dict.add_unarydrv ghg.dict (act 0 0) (act_lr 0 totalno);
            acts := (act_lr 0 totalno) :: !acts;
            for k = 0 to totalno - 1 do
                let v = Syncat.create_bwd (act_lr (k + 1) totalno) (act_lr k totalno) Syncat.Left in
                acts := (act_lr (k + 1) totalno) :: !acts;
                Dict.add_unarydrv ghg.dict inst v
            done;
            Dict.add_unarydrv ghg.dict (act_lr totalno totalno) sent
        done;
        !acts

    let gen_verb_nofixedorder ghg =
        let acts = ref [] in
        for totalno = 1 to ghg.noargs do
            Dict.add_unarydrv ghg.dict (act 0 0) (act 0 totalno);
            for i = 0 to totalno - 1 do
                let v1 = Syncat.create_fwd (act (i + 1) totalno) (act i totalno) Syncat.Right
                and v2 = Syncat.create_bwd (act (i + 1) totalno) (act i totalno) Syncat.Left in
                Dict.add_unarydrv ghg.dict inst v1; 
                Dict.add_unarydrv ghg.dict inst v2;
                acts := (act (i + 1) totalno) :: !acts
            done;
            Dict.add_unarydrv ghg.dict (act totalno totalno) sent
        done;
        !acts

    let gen_verb_system ghg =
        let acts = ref [] in
        MacroExpander.add_cats ghg.macroexp "[instance]" [inst];
        MacroExpander.add_cats ghg.macroexp "[action]" [act 0 0];
        Dict.add_unarydrv ghg.dict (act 0 0) sent;
        Dict.add_unarydrv ghg.dict inst np;
        acts := (act 0 0) :: !acts;
        List.iter
            (fun varg ->
                let vs = match varg with
                    LeftThenRight -> gen_verb_leftthenright ghg
                  | RightThenLeft -> gen_verb_rightthenleft ghg
                  | LeftOnly -> gen_verb_leftonly ghg
                  | RightOnly -> gen_verb_rightonly ghg
                  | NoFixedOrder -> gen_verb_nofixedorder ghg in
                acts := !acts @ vs) 
            ghg.varg;
        !acts

    let gen_causative_inst ghg head causearg =
        match causearg with
            LeftThenRight | LeftOnly -> begin
                let h = Syncat.create_bwd head head Syncat.Right in
                Dict.add_unarydrv ghg.dict causative h;
                [h]
            end
          | RightThenLeft | RightOnly -> begin
                let h = Syncat.create_fwd head head Syncat.Left in
                Dict.add_unarydrv ghg.dict causative h;
                [h]
            end
          | NoFixedOrder -> begin
                let h1 = Syncat.create_bwd head head Syncat.Right
                and h2 = Syncat.create_fwd head head Syncat.Left in
                Dict.add_unarydrv ghg.dict causative h1;
                Dict.add_unarydrv ghg.dict causative h2;
                [h1; h2]
            end

    let gen_causative_system ghg acts =
        let causatives = ref [] in
        MacroExpander.add_cats ghg.macroexp "[causative]" [causative];
        Utils.cartiter
            (fun head modarg -> 
                causatives := !causatives @ gen_causative_inst ghg head modarg) 
            acts ghg.causearg;
        !causatives

    let is_act cat =
        Syncat.is_atom cat
            && ((Syncat.get_value cat).[0] = 'a')

    let add_unary_mod ghg head h =
        if Syncat.equal head inst then
            Dict.add_unarydrv ghg.dict adjmod h
        else ();
        if is_act head then
            Dict.add_unarydrv ghg.dict advmod h
        else ();
        Dict.add_unarydrv ghg.dict modatt h

    let gen_mod_inst ghg head modarg =
        match modarg with
            LeftThenRight | LeftOnly -> begin
                let h = Syncat.create_fwd head head Syncat.Right in
                add_unary_mod ghg head h;
                [h]
            end
          | RightThenLeft | RightOnly -> begin
                let h = Syncat.create_bwd head head Syncat.Left in
                add_unary_mod ghg head h;
                [h]
            end
          | NoFixedOrder -> begin
                let h1 = Syncat.create_fwd head head Syncat.Right
                and h2 = Syncat.create_bwd head head Syncat.Left in
                add_unary_mod ghg head h1;
                add_unary_mod ghg head h2;
                [h1; h2]
            end

    let gen_mod_system ghg acts =
        let mods = ref [] in
        MacroExpander.add_cats ghg.macroexp "[modifier]" [modatt];
        MacroExpander.add_cats ghg.macroexp "[adjective]" [adjmod];
        MacroExpander.add_cats ghg.macroexp "[adverb]" [advmod];
        let adjs = ref [] in
        List.iter
            (fun adjarg ->
                adjs := !adjs @ gen_mod_inst ghg inst adjarg)
            ghg.adjarg;
        mods := !mods @ !adjs;
        let heads = !adjs @ acts in
        Utils.cartiter
            (fun head advarg -> 
                mods := !mods @ gen_mod_inst ghg head advarg) 
            heads ghg.advarg;
        (!adjs, !mods)

    let add_unary_link ghg head comp outer =
        if Syncat.equal comp inst then
            Dict.add_unarydrv ghg.dict adpos outer
        else ();
        if is_act comp then
            if Syncat.equal head adjmod then
                Dict.add_unarydrv ghg.dict relativizer outer
            else
                Dict.add_unarydrv ghg.dict subord outer
        else ();
        Dict.add_unarydrv ghg.dict link outer

    let gen_link_inst ghg head comp linkarg =
        match linkarg with
            LeftThenRight | LeftOnly -> begin
                let outer = Syncat.create_bwd head comp Syncat.Right in
                add_unary_link ghg head comp outer;
                [outer]
            end
          | RightThenLeft | RightOnly -> begin
                let outer = Syncat.create_fwd head comp Syncat.Left in
                add_unary_link ghg head comp outer;
                [outer]
            end
          | NoFixedOrder -> begin
                let outer1 = Syncat.create_bwd head comp Syncat.Right in
                let outer2 = Syncat.create_fwd head comp Syncat.Left in
                add_unary_link ghg head comp outer1;
                add_unary_link ghg head comp outer2;
                [outer1; outer2]
            end

    let gen_link_system ghg acts mods =
        let links = ref [] in
        MacroExpander.add_cats ghg.macroexp "[link]" [link];
        MacroExpander.add_cats ghg.macroexp "[adposition]" [adpos];
        MacroExpander.add_cats ghg.macroexp "[subordconj]" [subord];
        MacroExpander.add_cats ghg.macroexp "[relativizer]" [relativizer];
        let heads = [adjmod; advmod] in
        let comps = inst :: acts @ mods in
        List.iter
            (fun linkarg ->
                Utils.cartiter
                    (fun head comp ->
                        links := !links @ gen_link_inst ghg head comp linkarg)
                    heads comps)
            ghg.linkarg;
        !links

    let add_unary_transf ghg head comp outer =
        if Syncat.equal head inst && is_act comp then
            Dict.add_unarydrv ghg.dict nominalizer outer
        else ();
        if is_act head && Syncat.equal comp inst then
            Dict.add_unarydrv ghg.dict verbalizer outer
        else ();
        Dict.add_unarydrv ghg.dict transf outer

    let gen_transf_inst ghg head comp transfarg =
        match transfarg with
            LeftThenRight | LeftOnly -> begin
                let outer = Syncat.create_bwd head comp Syncat.Right in
                add_unary_transf ghg head comp outer;
                [outer]
            end
          | RightThenLeft | RightOnly -> begin
                let outer = Syncat.create_fwd head comp Syncat.Left in
                add_unary_transf ghg head comp outer;
                [outer]
            end
          | NoFixedOrder -> begin
                let outer1 = Syncat.create_bwd head comp Syncat.Right in
                let outer2 = Syncat.create_fwd head comp Syncat.Left in
                add_unary_transf ghg head comp outer1;
                add_unary_transf ghg head comp outer2;
                [outer1; outer2]
            end

    let gen_transf_system ghg acts mods =
        let transfs = ref [] in
        MacroExpander.add_cats ghg.macroexp "[transf]" [transf];
        let heads = [inst; act 0 0] in
        let comps = inst :: acts @ mods in
        List.iter
            (fun transfarg ->
                Utils.cartiter
                    (fun head comp ->
                        transfs := !transfs @ gen_transf_inst ghg head comp transfarg)
                    heads comps)
            ghg.transfarg;
        !transfs

    let gen_series_inst ghg head seriesarg =
        match seriesarg with
            LeftThenRight | LeftOnly -> begin
                let outer = Syncat.create_fwd head head Syncat.Right in
                Dict.add_unarydrv ghg.dict head outer;
                [outer]
            end
          | RightThenLeft | RightOnly -> begin
                let outer = Syncat.create_bwd head head Syncat.Left in
                Dict.add_unarydrv ghg.dict head outer;
                [outer]
            end
          | NoFixedOrder -> begin
                let outer1 = Syncat.create_fwd head head Syncat.Right in
                let outer2 = Syncat.create_bwd head head Syncat.Left in
                Dict.add_unarydrv ghg.dict head outer1;
                Dict.add_unarydrv ghg.dict head outer2;
                [outer1; outer2]
            end

    let gen_series_system ghg =
        let series = ref [] in
        let cats = [sent] in
        Utils.cartiter
            (fun cat seriesarg ->
                series := !series @ gen_series_inst ghg cat seriesarg)
            cats ghg.seriesarg;
        !series

    let gen_coor_system ghg =
        MacroExpander.add_cats ghg.macroexp "[leftpunc]" [Syncat.leftpunc_cat Syncat.NoDep];
        MacroExpander.add_cats ghg.macroexp "[rightpunc]" [Syncat.rightpunc_cat Syncat.NoDep];
        [
            Syncat.conn_cat; 
            Syncat.conn_cat_depbank; 
            Syncat.leftpunc_cat Syncat.NoDep; 
            Syncat.rightpunc_cat Syncat.NoDep
        ]

    let run ghg =
        let acts = gen_verb_system ghg in
        let _ = gen_causative_system ghg acts in
        let (adjs, mods) = gen_mod_system ghg acts in
        let _ = gen_link_system ghg acts adjs in
        let _ = gen_transf_system ghg acts mods in
        let _ = gen_series_system ghg in
        let _ = gen_coor_system ghg in
        Dict.add_cat ghg.dict inst;
        Dict.add_cat ghg.dict (act 0 0);
        Dict.add_cat ghg.dict modatt;
        Dict.add_cat ghg.dict link;
        Dict.add_cat ghg.dict transf;
        Dict.add_accept ghg.dict sent;
        Dict.add_accept ghg.dict np

    let put_words_to_bins 
            sent words insts acts causatives mods adjs advs 
            links adposes subords rels transfs 
            nominalizers verbalizers =
        List.iter
            (fun word ->
                try
                    let idx = String.rindex word '/' in
                    let lex = String.sub word 0 idx
                    and tag = String.sub word (idx + 1) (String.length word - idx - 1) in
                    match tag with
                        "i" -> insts := lex :: !insts
                      | "a" -> acts := lex :: !acts
                      | "c" -> causatives := lex :: !causatives
                      | "m" -> mods := lex :: !mods
                      | "j" -> adjs := lex :: !adjs
                      | "d" -> advs := lex :: !advs
                      | "l" -> links := lex :: !links
                      | "p" -> adposes := lex :: !adposes
                      | "o" -> subords := lex :: !subords
                      | "r" -> rels := lex :: !rels
                      | "t" -> transfs := lex :: !transfs
                      | "nz" -> nominalizers := lex :: !nominalizers
                      | "vz" -> verbalizers := lex :: !verbalizers
                      | _ -> words := lex :: !words
                with Not_found ->
                    words := union !words [word]
            )
            sent

    let collect_words fnames =
        let words = ref [] in
        let insts = ref []
        and acts = ref []
        and causatives = ref []
        and mods = ref []
        and adjs = ref []
        and advs = ref []
        and links = ref []
        and adposes = ref []
        and subords = ref []
        and rels = ref []
        and transfs = ref [] 
        and nominalizers = ref []
        and verbalizers = ref [] in
        List.iter
            (fun fname ->
                let fhdl = open_in fname in
                try
                    while true do
                        let line = input_line fhdl in
                        let sent = split line in
                        put_words_to_bins (list_to_set sent) 
                            words insts acts causatives mods adjs advs 
                            links adposes subords rels transfs
                            nominalizers verbalizers
                    done
                with End_of_file -> close_in fhdl)
            fnames;
        (
            !words, !insts, !acts, !causatives, !mods, !adjs, !advs, 
            !links, !adposes, !subords, !rels, !transfs,
            !nominalizers, !verbalizers
        )

    let read_lex ghg fnames =
        let (
            words, insts, acts, causatives, mods, adjs, advs, 
            links, adposes, subords, rels, transfs,
            nominalizers, verbalizers
        ) = collect_words fnames in
        Utils.cartiter
            (fun word cat ->
                Dict.add_entry ghg.dict word cat)
            words [
                inst; act 0 0; causative; adjmod; advmod; adpos; 
                relativizer; subord; link; 
                nominalizer; verbalizer
            ];
        Utils.cartiter
            (fun words cat ->
                List.iter
                    (fun word -> 
                        Dict.add_entry ghg.dict word cat)
                    words)
            [
                insts; acts; causatives; mods; adjs; advs; 
                links; adposes; subords; rels; transfs;
                nominalizers; verbalizers
            ] 
            [
                inst; act 0 0; causative; modatt; adjmod; advmod; 
                link; adpos; subord; relativizer; transf;
                nominalizer; verbalizer
            ]

end

(** Macro Expander and its Representation. *)

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

(** Macro for Syntactic Category. *)
module MacroSyncat :
sig

    (** Attribute-value matrix. *)
    type avm_t = Avm.Avm.t

    (** Syntactic category. *)
    type syncat_t = Cg.Syncat.t

    (** Dependency direction. *)
    type depdir_t = Cg.Syncat.depdir_t

    (** Syntactic category macro. *)
    type t

    (** [create_atom l] creates a macro with a label [l]. *)
    val create_atom : string -> t

    (** [create_syncat c] creates a macro value with a syntactic
        category [c]. *)
    val create_syncat : syncat_t -> t

    (** [create_bwd l r d f] creates a backward slash category with
        the LHS value [l], the RHS value [r], a dependency direction 
        [d], and an attribute-value matrix [f]. *)
    val create_bwd : t -> t -> depdir_t -> avm_t -> t

    (** [create_bwd l r d f] creates a forward slash category with
        the LHS value [l], the RHS value [r], a dependency direction 
        [d], and an attribute-value matrix [f]. *)
    val create_fwd : t -> t -> depdir_t -> avm_t -> t

    val is_atom : t -> bool

    val get_label : t -> string

    (** [to_string m] represents a macro category [m] with a string. *)
    val to_string : t -> string

    (** [to_string_paren m] represents a macro category [m] with a
        string, always parenthesized. *)
    val to_string_paren : t -> string

    (** [equal m1 m2] checks if two macros [m1] and [m2] are equal. *)
    val equal : 'a -> 'a -> bool

end

(** Macro Expander. *)
module MacroExpander :
sig

    (** Macro label. *)
    type macroname_t = string

    (** Syntactic category. *)
    type syncat_t = Cg.Syncat.t

    (** Macro for syntactic category. *)
    type macrosyncat_t = MacroSyncat.t

    (** Macro expander. *)
    type t

    (** [create n] creates a macro expander with a number of entries
        [n]. *)
    val create : int -> t

    (** [to_string me] represents a macro expander with a string. *)
    val to_string : t -> string

    (** [mem me l] checks if there is a macro named [l] in a macro
        expander [me]. *)
    val mem : t -> macroname_t -> bool

    (** [find me l] computes all syntactic categories by expanding a
        macro named [l]. *)
    val find : t -> macroname_t -> syncat_t list

    (** [add_cats me l cs] adds a list of syntactic categories [cs] into
        a macro named [l] in a macro expander [me]. *)
    val add_cats : t -> macroname_t -> syncat_t list -> unit

    (** [expand me mc] expands a macro syntactic category [mc] with
        the values in a macro expander [me]. *)
    val expand : t -> macrosyncat_t -> syncat_t list

    (** [add me l mcs] adds a list of macro syntactic categories [mcs]
        into a macro named [l] in a macro expander [me]. *)
    val add : t -> macroname_t -> macrosyncat_t list -> unit

end

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Cg
open Utils
open Printf

module HashedInt = struct
    type t = int
    let hash = Hashtbl.hash
    let equal = Stdlib.(=)
end

type depdir_t = Syncat.depdir_t

module DepPair = struct

    type word_t = string

    type t = word_t * word_t * depdir_t option

    let create head dep depdir =
        (head, dep, depdir)

    let get_head deppair =
        let (head, _, _) = deppair in
        head

    let get_dep deppair =
        let (_, dep, _) = deppair in
        dep

    let get_depdir deppair =
        let (_, _, depdir) = deppair in
        match depdir with
            Some dir -> dir
          | None -> failwith "get_depdir"

    let to_string deppair =
        let (head, dep, depdir) = deppair in
        match depdir with
            Some dir ->
                sprintf "'%s' %s '%s'" head 
                    (Syncat.depdir_to_string dir) dep
          | None ->
                sprintf "'%s' <-> '%s'" head dep

    let is_dirdep deppair =
        let (_, _, depdir) = deppair in
        match depdir with
            Some _ -> true
          | None -> false

    let hash = Hashtbl.hash

    let equal = Stdlib.(=)
    
    let compare = Stdlib.compare

end

module DepStruct = struct

    type word_t = string
    type syncat_t = Syncat.t

    module DepTbl = Hashtbl.Make(HashedInt)
    module CatTbl = Hashtbl.Make(HashedInt)

    type span_t = int * int

    type deptbl_t = int list DepTbl.t
    
    type headtbl_t = int DepTbl.t

    type deppair_t = DepPair.t

    type cattbl_t = string CatTbl.t

    type t = {
        sentence : word_t list;
        rootpos  : int;
        deptbl   : deptbl_t;
        headtbl  : headtbl_t;
        cattbl   : cattbl_t;
        mutable reltbl : ((int * int) * Syncat.depdir_t) list;
    }

    let create n sentence rootpos =
    {
        sentence = sentence;
        rootpos  = rootpos;
        deptbl   = DepTbl.create n;
        headtbl  = DepTbl.create n;
        cattbl   = CatTbl.create n;
        reltbl   = [];
    }

    let get_sentence depstr =
        depstr.sentence

    let get_rootpos depstr =
        depstr.rootpos

    let mem_head depstr headpos =
        DepTbl.mem depstr.deptbl headpos

    let get depstr headpos =
        DepTbl.find depstr.deptbl headpos

    let find_head depstr deppos =
        if DepTbl.mem depstr.headtbl deppos then
            DepTbl.find depstr.headtbl deppos
        else raise Not_found

    let mem depstr headpos deppos =
        if mem_head depstr headpos then
            List.mem deppos (get depstr headpos)
        else false

    let remove depstr headpos =
        if mem_head depstr headpos then
            DepTbl.remove depstr.deptbl headpos
        else ()

    let entry_to_string headpos deppos sentence =
        let headword = List.nth sentence headpos
        and depword = List.nth sentence deppos in
        sprintf "Head '%s'/%d -> Dep '%s'/%d"
            headword headpos depword deppos

    let deptbl_to_string depstr =
        let lines = ref [] in
        let sentlen = List.length depstr.sentence in
        let deppos_to_string headpos deppos =
            entry_to_string headpos deppos depstr.sentence in
        for headpos = sentlen - 1 downto 0 do
            if DepTbl.mem depstr.deptbl headpos then
                let depposes = DepTbl.find depstr.deptbl headpos in
                lines := (List.map (deppos_to_string headpos) depposes) @ !lines
            else ()
        done;
        if List.length !lines <> 0 then String.concat "\n" !lines
        else "Empty dependency"

    let reltbl_to_string depstr =
        let lines =
            List.map
                (fun ((headpos, deppos), depdir) ->
                    let deps = 
                        match depdir with
                            Syncat.Left -> "Left"
                          | Syncat.Right -> "Right"
                          | Syncat.LeftSerial -> "LeftSerial"
                          | Syncat.RightSerial -> "RightSerial"
                          | Syncat.Coor -> "Coor"
                          | Syncat.Unary -> "Unary"
                    in
                    Printf.sprintf "(%d, %d): %s" headpos deppos deps
                )
                depstr.reltbl
        in
        String.concat "\n" lines

    let to_string depstr =
        sprintf "<Dependency>\nSentence = [%s]\nRoot = '%s'/%d\n%s\n%s"
            (String.concat " " depstr.sentence) 
            (List.nth depstr.sentence depstr.rootpos) depstr.rootpos
            (deptbl_to_string depstr)
            (reltbl_to_string depstr)

    let add_depdir depstr headpos deppos depdir =
        if not (List.mem_assoc (headpos, deppos) depstr.reltbl) then
            depstr.reltbl <- ((headpos, deppos), depdir) :: depstr.reltbl
        else ()

    let add depstr headpos deppos depdir =
        if headpos = deppos then ()
        else if mem_head depstr headpos then begin
            let oldmembers = get depstr headpos in
            if not (List.mem deppos oldmembers) then begin
                DepTbl.replace depstr.deptbl headpos (deppos :: oldmembers);
                DepTbl.replace depstr.headtbl deppos headpos;
            end else ();
            add_depdir depstr headpos deppos depdir
        end else begin
            DepTbl.replace depstr.deptbl headpos [deppos];
            DepTbl.replace depstr.headtbl deppos headpos;
            add_depdir depstr headpos deppos depdir
        end

    let set_syncat depstr pos cat =
        CatTbl.replace depstr.cattbl pos (Syncat.to_string cat)

    let get_syncat depstr pos =
        if CatTbl.mem depstr.cattbl pos then
            CatTbl.find depstr.cattbl pos
        else "_"

    let get_depdir depstr headpos deppos =
        List.assoc (headpos, deppos) depstr.reltbl

    let swap_pos headpos headword deppos depword =
        if headpos < deppos then 
            (headword, depword, headpos, deppos, Syncat.Left)
        else 
            (depword, headword, deppos, headpos, Syncat.Right)

    let rec make_spantbl depstr idx passed : (int * span_t) list =
        if List.mem idx passed then []
        else if DepTbl.mem depstr idx then begin
            let subtbl = Utils.list_to_set
                (List.concat
                    (List.map
                        (fun dtridx -> 
                            make_spantbl depstr dtridx (idx :: passed))
                        (DepTbl.find depstr idx)))
            in
            let (lcnr, rcnr) = (ref idx, ref idx) in
            List.iter
                (fun (dtridx, (lidx, ridx)) ->
                    if lidx < !lcnr then lcnr := lidx;
                    if ridx > !rcnr then rcnr := ridx)
                subtbl;
            Utils.union [(idx, (!lcnr, !rcnr))] subtbl
        end else [(idx, (idx, idx))]

    let ubrackets depstr : span_t list =
        let spantbl = make_spantbl depstr.deptbl depstr.rootpos [(-1)] in
        let spantbl = ref 
            (List.filter 
                (fun (_, (lidx, ridx)) -> lidx != ridx) 
                spantbl)
        in
        (*
        for i = 0 to List.length depstr.sentence - 1 do
            spantbl := (i, (i, i)) :: !spantbl
        done;
        *)
        Utils.list_to_set
            (List.map
                (fun (_, (lidx, ridx)) -> (lidx, ridx))
                !spantbl)

    let depset depstr : deppair_t list =
        let result = ref [] in
        DepTbl.iter
            (fun headpos depposes ->
                let headword = List.nth depstr.sentence headpos in
                List.iter 
                    (fun deppos ->
                        let depword = List.nth depstr.sentence deppos in
                        let (left, right, leftpos, rightpos, depdir) = 
                            swap_pos headpos headword deppos depword in
                        let left = sprintf "%d" leftpos
                        and right = sprintf "%d" rightpos in
                        let dep = DepPair.create left right (Some depdir) in
                        result := !result @ [dep]) 
                    depposes)
            depstr.deptbl;
        !result

    let depset_noorder depstr : deppair_t list =
        let result = ref [] in
        DepTbl.iter
            (fun headpos depposes ->
                let headword = sprintf "%d" headpos in
                List.iter 
                    (fun deppos ->
                        let depword = sprintf "%d" deppos in
                        let dep =
                            if headpos < deppos then
                                DepPair.create headword depword None 
                            else 
                                DepPair.create depword headword None 
                        in
                        result := !result @ [dep]) 
                    depposes)
            depstr.deptbl;
        !result

    let depset_words depstr : deppair_t list =
        let result = ref [] in
        DepTbl.iter
            (fun headpos depposes ->
                let headword = List.nth depstr.sentence headpos in
                List.iter 
                    (fun deppos ->
                        let depword = List.nth depstr.sentence deppos in
                        let (left, right, leftpos, rightpos, depdir) = 
                            swap_pos headpos headword deppos depword in
                        let dep = DepPair.create left right (Some depdir) in
                        result := !result @ [dep]) 
                    depposes)
            depstr.deptbl;
        !result

    let depset_words_noorder depstr : deppair_t list =
        let result = ref [] in
        DepTbl.iter
            (fun headpos depposes ->
                let headword = List.nth depstr.sentence headpos in
                List.iter 
                    (fun deppos ->
                        let depword = List.nth depstr.sentence deppos in
                        let dep =
                            if headpos < deppos then
                                DepPair.create headword depword None 
                            else 
                                DepPair.create depword headword None 
                        in
                        result := !result @ [dep]) 
                    depposes)
            depstr.deptbl;
        !result

end

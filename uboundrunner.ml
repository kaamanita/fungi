open Cg
open Chart
open Chartdb
open Prob
open Dep
open Deriv
open Models
open Uboundalgo
open Tree
open Treedb
open Utils
open Printf

module DecoderRunner = struct

    type depstruct_t = DepStruct.t

    type t = {
        depstrs  : depstruct_t list;
        wrapper  : ChartDbWrapper.t;
        treedb   : TreeDb.t;
        tdbfname : string;
    }

    let create depstrs fnames treedbfname =
        {
            depstrs  = depstrs;
            wrapper  = ChartDbWrapper.from_files fnames;
            treedb   = TreeDb.create treedbfname;
            tdbfname = treedbfname;
        }

    let parse_chart depstr chart =
        (* Printf.printf "(viterbirunner.parse_chart) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let (tree, _) = Viterbi.compute depstr chart in
        let depstr = Tree.to_depstr tree in
        (tree, 1.0, depstr)
    
    let parse_wrapper depstrs wrapper treedb =
        let nocharts = ChartDbWrapper.length wrapper in
        Scale.add (Scale.GuageWithNumber nocharts) "parsing charts " "";
        let tmp_depstrs = ref depstrs in
        for idx = 0 to nocharts - 1 do
            match !tmp_depstrs with
                [] -> failwith "uboundrunner.parse_wrapper: unmatched depstrs and charts"
              | depstr :: rest -> begin
                    Scale.inc ();
                    Scale.display ();
                    tmp_depstrs := rest;
                    let chart = ChartDbWrapper.find wrapper idx in
                    let enum = Chart.is_filledup chart in
                    try 
                        let (tree, prob, depstr) = parse_chart depstr chart in
                        let words = Chart.get_words chart in
                        TreeDb.add treedb (MarshTree.create tree prob depstr words enum)
                    with Not_found ->
                        TreeDb.add treedb None
                end
        done;
        Scale.discard ()

    let run vtb =
        parse_wrapper vtb.depstrs vtb.wrapper vtb.treedb;
        TreeDb.to_file vtb.treedb vtb.tdbfname

    let compute depstrs fnames treedbfname =
        let vtb = create depstrs fnames treedbfname in
        run vtb
        
end

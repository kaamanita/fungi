(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Dep
open Tree
open Conlldep
open Hdb
open Treedb
open Bank
open Utils
open Printf

module HashedDirDep = struct
    type t = DepPair.t
    let equal = Stdlib.(=)
    let hash = Hashtbl.hash
end

module DirDepTbl = Hashtbl.Make(HashedDirDep)

module type DATA_TYPE = sig
    type tree_t
    type count_t
    type t
    type storage_t
    val extract : tree_t -> t list
    val extract_words : tree_t -> t list
    val get_headpos : tree_t -> int
    val tree_to_string : tree_t -> string
    val to_string : t -> string
    val create_storage : unit -> storage_t
    val store_correct : storage_t -> t -> unit
    val store_gcorrect : storage_t -> t -> unit
    val count_correct : storage_t -> t -> count_t
    val store_overgen : storage_t -> t -> unit
    val store_undergen : storage_t -> t -> unit
    val store_total : storage_t -> t -> unit
    val top_history : storage_t -> int -> (count_t * t) list
end

module Metric 
    (GoldData : DATA_TYPE) 
    (TestData : DATA_TYPE with 
        type t = GoldData.t
        and type count_t = GoldData.count_t
        and type storage_t = GoldData.storage_t) 
= struct

    type goldtree_t = GoldData.tree_t

    type testtree_t = TestData.tree_t

    type count_t = TestData.count_t

    type data_t = GoldData.t

    type storage_t = GoldData.storage_t

    type comp_recall_t = data_t list -> data_t list -> data_t list

    type t = {
        mutable gcorrectno : int;
        mutable tcorrectno : int;
        mutable goldno     : int;
        mutable testno     : int;
        corrects           : storage_t;
        gcorrects          : storage_t;
        overgens           : storage_t;
        undergens          : storage_t;
        totals             : storage_t;
        comp_grecfn        : comp_recall_t;
        comp_trecfn        : comp_recall_t;
    }

    let create grecfn trecfn =
    {
        gcorrectno  = 0;
        tcorrectno  = 0;
        goldno      = 0;
        testno      = 0;
        corrects    = GoldData.create_storage ();
        gcorrects   = GoldData.create_storage ();
        overgens    = GoldData.create_storage ();
        undergens   = GoldData.create_storage ();
        totals      = GoldData.create_storage ();
        comp_grecfn = grecfn;
        comp_trecfn = trecfn;
    }

    let create_from_history grecfn trecfn (corhist, gcorhist, ovghist, unghist, tothist) =
    {
        gcorrectno  = 0;
        tcorrectno  = 0;
        goldno      = 0;
        testno      = 0;
        corrects    = corhist;
        gcorrects   = gcorhist;
        overgens    = ovghist;
        undergens   = unghist;
        totals      = tothist;
        comp_grecfn = grecfn;
        comp_trecfn = trecfn;
    }

    let get_history metric =
        (metric.corrects, metric.gcorrects, metric.overgens, metric.undergens, metric.totals)

    let store_history metric golds tests grecalls trecalls =
        let overgens = diff tests trecalls
        and undergens = diff golds grecalls in
        List.iter (GoldData.store_correct metric.corrects) trecalls;
        List.iter (GoldData.store_gcorrect metric.gcorrects) grecalls;
        List.iter (GoldData.store_overgen metric.overgens) overgens;
        List.iter (GoldData.store_undergen metric.undergens) undergens;
        List.iter (GoldData.store_total metric.totals) golds

    let process ?(printout=false) metric goldtree testtree =
        (* printf "extracting golds\n"; *)
        if printout then begin
            fprintf stderr "goldtree:\n%s\n" (GoldData.tree_to_string goldtree);
            fprintf stderr "testtree:\n%s\n" (TestData.tree_to_string testtree)
        end else ();
        let golds = GoldData.extract goldtree in
        (* printf "extracting tests\n"; *)
        let tests = TestData.extract testtree in
        (* printf "intersecting recalls\n"; *)
        let grecalls = metric.comp_grecfn golds tests 
        and trecalls = metric.comp_trecfn tests golds in
        if printout then begin
            fprintf stderr "goldstd = [%s]\n"
                (String.concat ", " (List.map GoldData.to_string golds));
            fprintf stderr "test    = [%s]\n"
                (String.concat ", " (List.map TestData.to_string tests));
            fprintf stderr "grecall = [%s]\n"
                (String.concat ", " (List.map GoldData.to_string grecalls));
            fprintf stderr "trecall = [%s]\n"
                (String.concat ", " (List.map GoldData.to_string trecalls));
            let undgen = diff golds grecalls 
            and ovrgen = diff tests trecalls in
            fprintf stderr "ovrgen  = [%s]\n"
                (String.concat ", " (List.map GoldData.to_string ovrgen));
            fprintf stderr "undgen  = [%s]\n\n"
                (String.concat ", " (List.map GoldData.to_string undgen))
        end else ();
        (* printf "intersection ends.\n"; *)
        metric.goldno <- metric.goldno + List.length golds + 1;
        metric.testno <- metric.testno + List.length tests + 1;
        metric.gcorrectno <- metric.gcorrectno + List.length grecalls;
        metric.tcorrectno <- metric.tcorrectno + List.length trecalls;
        if (GoldData.get_headpos goldtree) = (TestData.get_headpos testtree) then begin
            metric.gcorrectno <- metric.gcorrectno + 1;
            metric.tcorrectno <- metric.tcorrectno + 1
        end else ();
        (* printf "store_history starts.\n"; *)
        let golds' = GoldData.extract_words goldtree in
        let tests' = TestData.extract_words testtree in
        let grecalls' = metric.comp_grecfn golds' tests'
        and trecalls' = metric.comp_trecfn tests' golds' in
        store_history metric golds' tests' grecalls' trecalls' (* ;
        printf "store_history ends.\n" *)

    let get_precision metric =
        100.0 *. (float_of_int metric.tcorrectno) /. (float_of_int metric.testno)

    let get_recall metric =
        100.0 *. (float_of_int metric.gcorrectno) /. (float_of_int metric.goldno)

    let get_f1 metric =
        let precision = get_precision metric
        and recall = get_recall metric in
        2.0 /. (1.0 /. precision +. 1.0 /. recall)

    let top_corrects metric entryno =
        GoldData.top_history metric.corrects entryno

    let top_overgens metric entryno =
        GoldData.top_history metric.overgens entryno

    let top_undergens metric entryno =
        GoldData.top_history metric.undergens entryno

    let top_totals metric entryno =
        let lst = GoldData.top_history metric.totals entryno in
        List.map
            (fun (totcnt, data) ->
                let corcnt = GoldData.count_correct metric.gcorrects data in
                (totcnt, corcnt, data))
            lst

end

module UBracket = struct

    type tree_t = Tree.t

    type count_t = int

    type t = int * int

    type storage_t = t option

    let extract = Tree.to_ubrackets
    
    let extract_words = Tree.to_ubrackets

    let get_headpos = Tree.get_headpos

    let tree_to_string = Tree.pp ~indent:0

    let to_string ubracket = 
        let (begpos, endpos) = ubracket in
        sprintf "(%d, %d)" begpos endpos

    let create_storage _ = None

    let store_correct _ _ = ()
    
    let store_gcorrect _ _ = ()
    
    let count_correct _ _ = 0

    let store_overgen _ _ = ()

    let store_undergen _ _ = ()
    
    let store_total _ _ = ()

    let top_history _ _ = []

end

module UndirDep = struct

    type tree_t = Tree.t

    type count_t = int

    type t = DepPair.t

    type storage_t = t option

    let extract tree = 
        let depstr = Tree.to_depstr tree in
        DepStruct.depset_noorder depstr

    let extract_words tree = 
        let depstr = Tree.to_depstr tree in
        DepStruct.depset_words_noorder depstr

    let get_headpos = Tree.get_headpos

    let tree_to_string = Tree.pp ~indent:0

    let to_string = DepPair.to_string

    let create_storage _ = None

    let store_correct _ _ = ()
    
    let store_gcorrect _ _ = ()

    let count_correct _ _ = 0

    let store_overgen _ _ = ()

    let store_undergen _ _ = ()
    
    let store_total _ _ = ()

    let top_history _ _ = []

end

let is_punc word =
    match word.[0] with
        '!' | '@' | '#' | '%' | '^' | '&' | '*' 
      | '(' | ')' | '_' | '+' | '-' | '=' | '[' 
      | ']' | '{' | '}' | ';' | '\'' | '\\' | ':' 
      | '"' | '|' | '`' | '~' | ',' | '.' | '/' 
      | '<' | '>' | '?' | ' ' | '$' -> true
      | _ -> false

let rec elim_punc depset =
    match depset with
        [] -> []
      | deppair :: rest ->
            let (w1, w2) = (DepPair.get_head deppair, DepPair.get_dep deppair) in
            if is_punc w1 || is_punc w2 then elim_punc rest
            else deppair :: elim_punc rest

module DirDep = struct

    type tree_t = Tree.t

    type count_t = int

    type t = DepPair.t

    type storage_t = count_t DirDepTbl.t

    let extract tree = 
        let depstr = Tree.to_depstr tree in
        let depset = DepStruct.depset depstr in
        elim_punc depset

    let extract_words tree = 
        let depstr = Tree.to_depstr tree in
        let depset = DepStruct.depset_words depstr in
        elim_punc depset

    let get_headpos = Tree.get_headpos

    let tree_to_string = Tree.pp_syncat ~indent:0

    let to_string = DepPair.to_string

    let create_storage _ = 
        DirDepTbl.create 100

    let store_history storage data =
        let oldcnt =
            if DirDepTbl.mem storage data then DirDepTbl.find storage data
            else 0 in
        DirDepTbl.replace storage data (oldcnt + 1)

    let store_correct = store_history
    
    let store_gcorrect = store_history

    let count_correct storage data =
        if DirDepTbl.mem storage data then
            DirDepTbl.find storage data
        else 0

    let store_overgen = store_history

    let store_undergen = store_history
    
    let store_total = store_history

    let sort_history storage : (int * t) list =
        let result = ref [] in
        DirDepTbl.iter 
            (fun dirdep freq ->
                result := (freq, dirdep) :: !result) 
            storage;
        List.sort 
            (fun (freq1, dirdep1) (freq2, dirdep2) ->
                (* Sort from max -> min *)
                if freq1 <> freq2 then Stdlib.compare freq2 freq1
                else Stdlib.compare dirdep2 dirdep1) 
            !result

    let top_history storage entryno =
        let history = sort_history storage in
        let result = ref [] in
        for i = 0 to min (entryno - 1) (List.length history - 1) do
            result := !result @ [List.nth history i]
        done;
        !result

end

module ConllUBracket = struct

    type tree_t = DepStruct.t

    type count_t = int

    type t = int * int

    type storage_t = t option

    let extract depstr = DepStruct.ubrackets depstr

    let extract_words depstr = DepStruct.ubrackets depstr

    let get_headpos = DepStruct.get_rootpos

    let tree_to_string = DepStruct.to_string

    let to_string ubracket =
        let (begpos, endpos) = ubracket in
        sprintf "(%d, %d)" begpos endpos

    let create_storage _ = None

    let store_history _ _ = ()

    let store_correct _ _ = ()
    
    let store_gcorrect _ _ = ()

    let count_correct _ _ = 0

    let store_overgen _ _ = ()

    let store_undergen _ _ = ()
    
    let store_total _ _ = ()

    let top_history _ _ = []

end

module ConllUndirDep = struct

    type tree_t = DepStruct.t

    type count_t = int

    type t = DepPair.t

    type storage_t = t option

    let extract depstr = 
        let depset = DepStruct.depset_noorder depstr in
        let result = elim_punc depset in
        result
    
    let extract_words depstr = 
        let depset = DepStruct.depset_words_noorder depstr in
        let result = elim_punc depset in
        result
    
    let get_headpos = DepStruct.get_rootpos

    let tree_to_string = DepStruct.to_string

    let to_string = DepPair.to_string

    let create_storage _ = None

    let store_history _ _ = ()

    let store_correct _ _ = ()
    
    let store_gcorrect _ _ = ()

    let count_correct _ _ = 0

    let store_overgen _ _ = ()

    let store_undergen _ _ = ()
    
    let store_total _ _ = ()

    let top_history _ _ = []

end

module ConllDirDep = struct

    type tree_t = DepStruct.t

    type count_t = int

    type t = DepPair.t

    type storage_t = count_t DirDepTbl.t

    let extract depstr = 
        let depset = DepStruct.depset depstr in
        let result = elim_punc depset in
        result
    
    let extract_words depstr = 
        let depset = DepStruct.depset_words depstr in
        let result = elim_punc depset in
        result
    
    let get_headpos = DepStruct.get_rootpos

    let tree_to_string = DepStruct.to_string

    let to_string = DepPair.to_string

    let create_storage _ = 
        DirDepTbl.create 100

    let store_history storage data =
        let oldcnt =
            if DirDepTbl.mem storage data then DirDepTbl.find storage data
            else 0 in
        DirDepTbl.replace storage data (oldcnt + 1)

    let store_correct = store_history
    
    let store_gcorrect = store_history

    let count_correct storage data =
        if DirDepTbl.mem storage data then
            DirDepTbl.find storage data
        else 0

    let store_overgen = store_history

    let store_undergen = store_history
    
    let store_total = store_history

    let sort_history storage : (int * t) list =
        let result = ref [] in
        DirDepTbl.iter 
            (fun dirdep freq ->
                result := (freq, dirdep) :: !result) 
            storage;
        List.sort 
            (fun (freq1, dirdep1) (freq2, dirdep2) ->
                (* Sort from max -> min *)
                if freq1 <> freq2 then Stdlib.compare freq2 freq1
                else Stdlib.compare dirdep2 dirdep1) 
            !result

    let top_history storage entryno =
        let history = sort_history storage in
        let result = ref [] in
        for i = 0 to min (entryno - 1) (List.length history - 1) do
            result := !result @ [List.nth history i]
        done;
        !result

end

module HeadBracket = struct

    type tree_t = Tree.t

    type count_t = int

    type span_t = int * int

    type word_t = string

    type t = span_t * word_t

    type storage_t = t option

    let extract = Tree.to_brackets
    
    let extract_words = Tree.to_brackets
    
    let get_headpos = Tree.get_headpos

    let tree_to_string = Tree.pp ~indent:0

    let to_string headbracket =
        let ((begpos, endpos), head) = headbracket in
        sprintf "(%d, %d) : '%s'" begpos endpos head

    let create_storage _ = None

    let store_correct _ _ = ()
    
    let store_gcorrect _ _ = ()

    let count_correct _ _ = 0

    let store_overgen _ _ = ()

    let store_undergen _ _ = ()
    
    let store_total _ _ = ()

    let top_history _ _ = []

end

module Eval = struct

    module UBracketMetric = Metric(UBracket)(UBracket)

    module UndirDepMetric = Metric(UndirDep)(UndirDep)

    module DirDepMetric = Metric(DirDep)(DirDep)
    
    module HeadBracketMetric = Metric(HeadBracket)(HeadBracket)

    type count_t = int

    type tree_t = Tree.t

    type depstr_t = DepStruct.t

    type ubracketmetric_t = UBracketMetric.t

    type undirdepmetric_t = UndirDepMetric.t

    type dirdepmetric_t = DirDepMetric.t
    
    type headbracketmetric_t = HeadBracketMetric.t

    type storage_t = count_t DirDepTbl.t

    type dirdepstorage_t = DirDepMetric.storage_t

    type t = {
        golds   : tree_t list;
        tests   : tree_t list;
        ubrkmtr : ubracketmetric_t;
        udepmtr : undirdepmetric_t;
        ddepmtr : dirdepmetric_t;
        hbrkmtr : headbracketmetric_t;
    }

    let create golds tests =
    {
        golds   = golds;
        tests   = tests;
        ubrkmtr = UBracketMetric.create intersect intersect;
        udepmtr = UndirDepMetric.create intersect intersect;
        ddepmtr = DirDepMetric.create intersect intersect;
        hbrkmtr = HeadBracketMetric.create intersect intersect;
    }

    let get_history eval =
        DirDepMetric.get_history eval.ddepmtr

    let create_from_history golds tests history =
    {
        golds   = golds;
        tests   = tests;
        ubrkmtr = UBracketMetric.create intersect intersect;
        udepmtr = UndirDepMetric.create intersect intersect;
        ddepmtr = DirDepMetric.create_from_history intersect intersect history;
        hbrkmtr = HeadBracketMetric.create intersect intersect;
    }

    let run_brk eval =
        List.iter2 
            (fun gold test ->
                UBracketMetric.process eval.ubrkmtr gold test)
            eval.golds eval.tests

    let run_dep eval =
        List.iter2 
            (fun gold test ->
                UndirDepMetric.process eval.udepmtr gold test;
                DirDepMetric.process ~printout:false eval.ddepmtr gold test;
                HeadBracketMetric.process eval.hbrkmtr gold test) 
            eval.golds eval.tests

    let get_ubrkscores eval =
        let precision = UBracketMetric.get_precision eval.ubrkmtr
        and recall = UBracketMetric.get_recall eval.ubrkmtr
        and f1 = UBracketMetric.get_f1 eval.ubrkmtr in
        (precision, recall, f1)

    let get_udepscores eval =
        let precision = UndirDepMetric.get_precision eval.udepmtr
        and recall = UndirDepMetric.get_recall eval.udepmtr
        and f1 = UndirDepMetric.get_f1 eval.udepmtr in
        (precision, recall, f1)

    let get_ddepscores eval =
        let precision = DirDepMetric.get_precision eval.ddepmtr
        and recall = DirDepMetric.get_recall eval.ddepmtr
        and f1 = DirDepMetric.get_f1 eval.ddepmtr in
        (precision, recall, f1)

    let get_hbrkscores eval =
        let precision = HeadBracketMetric.get_precision eval.hbrkmtr
        and recall = HeadBracketMetric.get_recall eval.hbrkmtr
        and f1 = HeadBracketMetric.get_f1 eval.hbrkmtr in
        (precision, recall, f1)

    let top_dirdep_corrects eval entryno =
        DirDepMetric.top_corrects eval.ddepmtr entryno

    let top_dirdep_overgens eval entryno =
        DirDepMetric.top_overgens eval.ddepmtr entryno

    let top_dirdep_undergens eval entryno =
        DirDepMetric.top_undergens eval.ddepmtr entryno

    let top_dirdep_totals eval entryno =
        DirDepMetric.top_totals eval.ddepmtr entryno

end

module ConllEval = struct

    module UBracketMetric = Metric(ConllUBracket)(UBracket)

    module UndirDepMetric = Metric(ConllUndirDep)(UndirDep)

    module DirDepMetric = Metric(ConllDirDep)(DirDep)
    
    type tree_t = Tree.t

    type count_t = int

    type depstr_t = DepStruct.t
    
    type ubracketmetric_t = UBracketMetric.t

    type undirdepmetric_t = UndirDepMetric.t

    type dirdepmetric_t = DirDepMetric.t
    
    type storage_t = int DirDepTbl.t

    type dirdepstorage_t = DirDepMetric.storage_t

    type t = {
        golds   : depstr_t list;
        tests   : tree_t list;
        ubrkmtr : ubracketmetric_t;
        udepmtr : undirdepmetric_t;
        ddepmtr : dirdepmetric_t;
    }

    let rec check_proj tests golds =
        let rec is_pass_proj test golds =
            match golds with
                [] -> true
              | gold :: rest -> begin
                    let (l1, r1) = test
                    and (l2, r2) = gold in
                    (*
                    fprintf stderr ">>> checking t (%d, %d) and g (%d, %d) ... " l1 r1 l2 r2;
                    *)
                    let cont =
                        if l1 <= l2 && r1 >= r2 then true
                        else if l2 <= l1 && r2 >= r1 then true
                        else if l1 <= r2 && r1 >= l2 then false
                        else if l2 <= r1 && r2 >= l1 then false
                        else true
                    in
                    (*
                    fprintf stderr "%s\n" (if cont then "true" else "false");
                    *)
                    if cont then is_pass_proj test rest
                    else false
                end
        in
        match tests with
             [] -> []
           | test :: rest ->
                if is_pass_proj test golds then
                    test :: check_proj rest golds
                else
                    check_proj rest golds

    let create golds tests =
    {
        golds   = golds;
        tests   = tests;
        ubrkmtr = UBracketMetric.create check_proj check_proj;
        udepmtr = UndirDepMetric.create intersect intersect;
        ddepmtr = DirDepMetric.create intersect intersect;
    }

    let get_history eval =
        DirDepMetric.get_history eval.ddepmtr

    let create_from_history golds tests history =
    {
        golds   = golds;
        tests   = tests;
        ubrkmtr = UBracketMetric.create check_proj check_proj;
        udepmtr = UndirDepMetric.create intersect intersect;
        ddepmtr = DirDepMetric.create_from_history intersect intersect history;
    }

    let run eval =
        let id = ref 0 in
        List.iter2 
            (fun gold test ->
                incr id;
                (* printf "sent %d\n" !id; *)
                UBracketMetric.process ~printout:false eval.ubrkmtr gold test;
                UndirDepMetric.process ~printout:false eval.udepmtr gold test;
                DirDepMetric.process ~printout:false eval.ddepmtr gold test)
            eval.golds eval.tests

    let get_ubrkscores eval =
        let precision = UBracketMetric.get_precision eval.ubrkmtr
        and recall = UBracketMetric.get_recall eval.ubrkmtr
        and f1 = UBracketMetric.get_f1 eval.ubrkmtr in
        (precision, recall, f1)

    let get_udepscores eval =
        let precision = UndirDepMetric.get_precision eval.udepmtr
        and recall = UndirDepMetric.get_recall eval.udepmtr
        and f1 = UndirDepMetric.get_f1 eval.udepmtr in
        (precision, recall, f1)

    let get_ddepscores eval =
        let precision = DirDepMetric.get_precision eval.ddepmtr
        and recall = DirDepMetric.get_recall eval.ddepmtr
        and f1 = DirDepMetric.get_f1 eval.ddepmtr in
        (precision, recall, f1)

    let top_dirdep_corrects eval entryno =
        DirDepMetric.top_corrects eval.ddepmtr entryno

    let top_dirdep_overgens eval entryno =
        DirDepMetric.top_overgens eval.ddepmtr entryno

    let top_dirdep_undergens eval entryno =
        DirDepMetric.top_undergens eval.ddepmtr entryno

    let top_dirdep_totals eval entryno =
        DirDepMetric.top_totals eval.ddepmtr entryno

end

module Evaluator = struct

    type tree_t = Tree.t

    type eval_t = Eval.t

    type storage_t = int DirDepTbl.t

    type dirdepstorage_t = Eval.dirdepstorage_t

    type t = {
        title     : string;
        topno     : int;
        eval_brks : eval_t;
        eval_deps : eval_t;
    }

    let create title topno gbrkfnames gdepfnames treedbs =
        let goldbrks = TreeBank.from_texts gbrkfnames
        and golddeps = DepBank.from_texts gdepfnames
        and treedb = (* TreeDbWrapper.opendbs treedbs [Mdb.ReadOnly] in *)
            TreeDbWrapper.from_files treedbs in
        let tests = ref [] in
        TreeDbWrapper.iter
            (fun _ dataval -> 
                match dataval with
                    Some (tree, _, _, _, _) -> tests := !tests @ [tree]
                  | None -> tests := !tests @ [Tree.create_term "" (-1, -1)])
            treedb;
        {
            title     = title;
            topno     = topno;
            eval_brks = Eval.create goldbrks !tests;
            eval_deps = Eval.create golddeps !tests;
        }

    let get_history eval =
        Eval.get_history eval.eval_deps

    let create_from_history title topno gbrkfnames gdepfnames treedbs history =
        let goldbrks = TreeBank.from_texts gbrkfnames
        and golddeps = DepBank.from_texts gdepfnames
        and treedb = (* TreeDbWrapper.opendbs treedbs [Mdb.ReadOnly] in *)
            TreeDbWrapper.from_files treedbs in
        let tests = ref [] in
        TreeDbWrapper.iter
            (fun _ dataval -> 
                match dataval with
                    Some (tree, _, _, _, _) -> tests := !tests @ [tree]
                  | None -> tests := !tests @ [Tree.create_term "" (-1, -1)])
            treedb;
        {
            title     = title;
            topno     = topno;
            eval_brks = Eval.create goldbrks !tests;
            eval_deps = Eval.create_from_history golddeps !tests history;
        }
    
    let run eval =
        Eval.run_brk eval.eval_brks;
        Eval.run_dep eval.eval_deps

    let compute title topno gbrkfnames gdepfnames treedbs =
        let eval = create title topno gbrkfnames gdepfnames treedbs in
        run eval

    let get_ubrkscores eval =
        Eval.get_ubrkscores eval.eval_brks

    let get_udepscores eval =
        Eval.get_udepscores eval.eval_deps

    let get_ddepscores eval =
        Eval.get_ddepscores eval.eval_deps

    let get_hbrkscores eval =
        Eval.get_hbrkscores eval.eval_deps

    let scores_to_string eval =
        let (ubrkprc, ubrkrec, ubrkf1) = get_ubrkscores eval
        and (udepprc, udeprec, udepf1) = get_udepscores eval
        and (ddepprc, ddeprec, ddepf1) = get_ddepscores eval
        and (hbrkprc, hbrkrec, hbrkf1) = get_hbrkscores eval in
        (*
        let line1 = sprintf
            "%-20s\t%20s\t%20s\t%20s\t%20s"
            "Title" "UBRK" "UDEP" "DDEP" "LBRK"
        and line2 = sprintf 
            "%-20s\t%6.2f/%6.2f/%6.2f\t%6.2f/%6.2f/%6.2f\t%6.2f/%6.2f/%6.2f\t%6.2f/%6.2f/%6.2f"
            eval.title
            ubrkprc ubrkrec ubrkf1
            udepprc udeprec udepf1
            ddepprc ddeprec ddepf1
            hbrkprc hbrkrec hbrkf1 in
        *)
        sprintf 
            "%-20s\t%6.2f\t%6.2f\t%6.2f\t%6.2f"
            eval.title
            (* ubrkprc ubrkrec *) ubrkf1
            (* udepprc udeprec *) udepf1
            (* ddepprc ddeprec *) ddepf1
            (* hbrkprc hbrkrec *) hbrkf1

    let tophist_to_string tophist =
        String.concat "\n" 
            (List.map 
                (fun (freq, dep) ->
                    sprintf "%-30s\t%6d" (DirDep.to_string dep) freq) 
                tophist)

    let toptotal_to_string tophist =
        let cnt = ref 0 in
        String.concat "\n" 
            (List.map 
                (fun (totfreq, corfreq, dep) ->
                    incr cnt;
                    let a = abs (totfreq - corfreq) in
                    sprintf "%-30s\t%6d\t%6d" (DirDep.to_string dep) (totfreq - a) totfreq
                ) 
                tophist)

    let history_to_string eval =
        let topcor = Eval.top_dirdep_corrects eval.eval_deps eval.topno
        and topovg = Eval.top_dirdep_overgens eval.eval_deps eval.topno
        and topung = Eval.top_dirdep_undergens eval.eval_deps eval.topno 
        and toptot = Eval.top_dirdep_totals eval.eval_deps (10 * eval.topno) in
        sprintf "<TITLE>\n%s\n\n<TOP CORRECT>\n%s\n\n<TOP OVERGENERATION>\n%s\n\n<TOP UNDERGENERATION>\n%s\n\n<TOP FREQ ATTACHMENTS>\n%s"
            eval.title
            (tophist_to_string topcor)
            (tophist_to_string topovg)
            (tophist_to_string topung)
            (toptotal_to_string toptot)

    let top_corrects eval =
        Eval.top_dirdep_corrects eval.eval_deps eval.topno

    let top_overgens eval =
        Eval.top_dirdep_overgens eval.eval_deps eval.topno

    let top_undergens eval =
        Eval.top_dirdep_undergens eval.eval_deps eval.topno

    let top_totals eval =
        Eval.top_dirdep_totals eval.eval_deps eval.topno

end

module ConllEvaluator = struct

    type tree_t = Tree.t

    type eval_t = ConllEval.t

    type storage_t = int DirDepTbl.t

    type dirdepstorage_t = ConllEval.dirdepstorage_t

    type t = {
        title     : string;
        topno     : int;
        eval_deps : eval_t;
    }

    let create title topno gdepfnames treedbs =
        let golddeps = ConllDep.from_files gdepfnames
        and treedb = (* TreeDbWrapper.opendbs treedbs [Mdb.ReadOnly] in *)
            TreeDbWrapper.from_files treedbs in
        let tests = ref [] in
        TreeDbWrapper.iter
            (fun _ dataval -> 
                match dataval with
                    Some (tree, _, _, _, _) -> tests := !tests @ [tree]
                  | None -> tests := !tests @ [Tree.create_term "" (-1, -1)])
            treedb;
        {
            title     = title;
            topno     = topno;
            eval_deps = ConllEval.create golddeps !tests;
        }

    let get_history eval =
        ConllEval.get_history eval.eval_deps

    let create_from_history title topno gdepfnames treedbs history =
        let golddeps = ConllDep.from_files gdepfnames
        and treedb = (* TreeDbWrapper.opendbs treedbs [Mdb.ReadOnly] in *)
            TreeDbWrapper.from_files treedbs in
        let tests = ref [] in
        TreeDbWrapper.iter
            (fun _ dataval -> 
                match dataval with
                    Some (tree, _, _, _, _) -> tests := !tests @ [tree]
                  | None -> tests := !tests @ [Tree.create_term "" (-1, -1)])
            treedb;
        {
            title     = title;
            topno     = topno;
            eval_deps = ConllEval.create_from_history golddeps !tests history;
        }
    
    let run eval =
        (* Printf.printf "eval run starts.\n"; *)
        ConllEval.run eval.eval_deps (* ;
        Printf.printf "eval run ends.\n" *)

    let compute title topno gdepfnames treedbs =
        let eval = create title topno gdepfnames treedbs in
        run eval

    let get_ubrkscores eval =
        ConllEval.get_ubrkscores eval.eval_deps

    let get_udepscores eval =
        ConllEval.get_udepscores eval.eval_deps

    let get_ddepscores eval =
        ConllEval.get_ddepscores eval.eval_deps

    let scores_to_string eval =
        let (ubrkprc, ubrkrec, ubrkf1) = get_ubrkscores eval
        and (udepprc, udeprec, udepf1) = get_udepscores eval
        and (ddepprc, ddeprec, ddepf1) = get_ddepscores eval in
        (*
        let line1 = sprintf
            "%-20s\t%20s\t%20s\t%20s"
            "Title" "UBRK" "UDEP" "DDEP"
        and line2 = sprintf 
            "%-20s\t%6.2f/%6.2f/%6.2f\t%6.2f/%6.2f/%6.2f\t%6.2f/%6.2f/%6.2f"
            eval.title
            ubrkprc ubrkrec ubrkf1
            udepprc udeprec udepf1 
            ddepprc ddeprec ddepf1 in
        line1 ^ "\n" ^ line2
        *)
        sprintf 
            "%-20s\t%6.2f\t%6.2f\t%6.2f"
            eval.title
            (* ubrkprc ubrkrec *) ubrkf1
            (* udepprc udeprec *) udepf1 
            (* ddepprc ddeprec *) ddepf1

    let tophist_to_string tophist =
        String.concat "\n" 
            (List.map 
                (fun (freq, dep) ->
                    sprintf "%-30s\t%6d" (DirDep.to_string dep) freq) 
                tophist)

    let toptotal_to_string tophist =
        let cnt = ref 0 in
        String.concat "\n" 
            (List.map 
                (fun (totfreq, corfreq, dep) ->
                    incr cnt;
                    sprintf "%-30s\t%6d\t%6d" (DirDep.to_string dep) corfreq (totfreq - corfreq)) 
                tophist)

    let history_to_string eval =
        let topcor = ConllEval.top_dirdep_corrects eval.eval_deps eval.topno
        and topovg = ConllEval.top_dirdep_overgens eval.eval_deps eval.topno
        and topung = ConllEval.top_dirdep_undergens eval.eval_deps eval.topno 
        and toptot = ConllEval.top_dirdep_totals eval.eval_deps (10 * eval.topno) in
        sprintf "<TITLE>\n%s\n\n<TOP CORRECT>\n%s\n\n<TOP OVERGENERATION>\n%s\n\n<TOP UNDERGENERATION>\n%s\n\n<TOP FREQ ATTACHMENTS>\n%s"
            eval.title
            (tophist_to_string topcor)
            (tophist_to_string topovg)
            (tophist_to_string topung)
            (toptotal_to_string toptot)

    let top_corrects eval =
        ConllEval.top_dirdep_corrects eval.eval_deps eval.topno

    let top_overgens eval =
        ConllEval.top_dirdep_overgens eval.eval_deps eval.topno

    let top_undergens eval =
        ConllEval.top_dirdep_undergens eval.eval_deps eval.topno

    let top_totals eval =
        ConllEval.top_dirdep_totals eval.eval_deps eval.topno

end

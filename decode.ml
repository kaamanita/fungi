open Cg
open Models
open Decoderrunner
open Printf

let main =
    let output = ref None 
    and decoding = ref ViterbiDecoding
    and probtbl = ref None
    and probflags = ref []
    and start = ref (Syncat.create_atom "ROOT")
    and epsilon = ref 1e-6
    and catconst = ref 1.0
    and lbrconst = ref 1.0
    and rbrconst = ref 1.0
    and fnames = ref [] 
    and only_parsable = ref false in
    let args = [
        ("--quiet", Arg.Unit Scale.go_quiet, "  Quiet mode (default = false)");
        ("--probtbl",
            Arg.String (fun p -> probtbl := Some p),
            "[ruleprob.probtbl]   Specify the rule probabilities (required)");
        ("--decoding",
            Arg.Symbol (
                ["viterbi"; "maxexp"; "mbr"],
                set_decoding decoding),
            "  Select the decoding method (Standard Viterbi [default], Maximum Expectation Viterbi, or Minimum Bayes Risk)");
        ("--epsilon",
            Arg.Set_float epsilon,
            "[float]   Set the value of epsilon (a very small value) (default = 1e-6)");
        ("--probftr", 
            Arg.Symbol (
                ["roleemis"; "hockenmaier"; "lexemis"; "heademis"], 
                set_probflags probflags), 
            "  Enable the specified probability feature");
        ("--model",
            Arg.Symbol (
                ["0"; "1"; "2"; "3"; "4"; "5"],
                fun x -> set_probflags probflags ("model" ^ x)),
            "  Enable the specified model (a set of probability features)");
        ("--catconst",
            Arg.Set_float catconst,
            "[float]   Set the value of category preference constant (default = 1.0)");
        ("--lbrconst",
            Arg.Set_float lbrconst,
            "[float]   Set the value of left-branching constant (default = 1.0)");
        ("--rbrconst",
            Arg.Set_float rbrconst,
            "[float]   Set the value of right-branching constant (default = 1.0)");
        ("--only_parsable",
            Arg.Unit (fun _ -> only_parsable := true), "  Decode only parsable charts (default = false)");
        ("--output",
            Arg.String (fun o -> output := Some o),
            "[output.treedb]   Specify the output tree database (required)")
    ]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg =
        "Run the Viterbi parser.\n\n" 
        ^ (sprintf "Usage: %s [options] chart1 chart2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    match !probtbl, !output with
        Some probtblfname, Some treedbfname ->
        begin
            Scale.print (sprintf "[1mDecoding the best parses from the charts [%s] with the model %s.[0m\n"
                (String.concat ", " !fnames) probtblfname);
            Scale.print ">> Progress: ";
            let vtb = DecoderRunner.create !decoding !start !probflags !epsilon probtblfname !catconst !lbrconst !rbrconst !fnames treedbfname !only_parsable in
            DecoderRunner.run vtb;
            Scale.print "Done\n"
        end
      | _ -> Arg.usage args usagemsg

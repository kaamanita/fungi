(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

module Dmv :
sig
    type macroexp_t = Macro.MacroExpander.t
    type dict_t = Dict.Dict.t
    type t
    val create : macroexp_t -> dict_t -> t
    val run : t -> string list -> unit
end

module Evg :
sig
    type macroexp_t = Macro.MacroExpander.t
    type dict_t = Dict.Dict.t
    type t
    val create : macroexp_t -> dict_t -> t
    val run : t -> string list -> unit
end

module Ghg : sig
    type macroexp_t = Macro.MacroExpander.t
    type dict_t = Dict.Dict.t
    type branchpref_t =
        LeftThenRight
      | RightThenLeft
      | LeftOnly
      | RightOnly
      | NoFixedOrder
    type t
    val create : macroexp_t -> dict_t -> t
    val set_noargs : t -> int -> unit
    val set_varg : t -> branchpref_t -> unit
    val set_causearg : t -> branchpref_t -> unit
    val set_adjarg : t -> branchpref_t -> unit
    val set_advarg : t -> branchpref_t -> unit
    val set_linkarg : t -> branchpref_t -> unit
    val set_transfarg : t -> branchpref_t -> unit
    val set_seriesarg : t -> branchpref_t -> unit
    val run : t -> unit
    val read_lex : t -> string list -> unit
end

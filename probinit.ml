open Cg
open Chart
open Prob
open Deriv
open Printf

module ScoreInst = struct

    type syncat_t = Syncat.t
    type word_t = string

    type t = {
        syncat : syncat_t;
        head   : word_t;
    }

    let create syncat head = {
        syncat = syncat;
        head   = head;
    }

    let get_syncat inst =
        inst.syncat

    let get_head inst =
        inst.head

    let equal inst1 inst2 =
        Syncat.equal inst1.syncat inst2.syncat
        && inst1.head = inst2.head

    let hash inst =
        Syncat.hash inst.syncat + Hashtbl.hash inst.head

    let to_string inst =
        sprintf "%s{%s}" (Syncat.to_string inst.syncat) inst.head

end

module ScoreTbl = ProbTbl(ScoreInst)

module ProbInit = struct

    type chart_t = Chart.t

    type genmodel_t = DerivGenModel.t

    type scoretbl_t = ScoreTbl.t

    type fn_get_chart_t = int -> chart_t

    type t = {
        ruleprob         : genmodel_t;
        rulebias         : genmodel_t;
        get_chart        : fn_get_chart_t;
        nocharts         : int;
        catcnttbl        : scoretbl_t;
        mutable derivcnt : int;
        mutable maxslash : float;
    }

    let create ruleprob rulebias get_chart nocharts =
        Random.self_init ();
        {
            ruleprob  = ruleprob;
            rulebias  = rulebias;
            get_chart = get_chart;
            nocharts  = nocharts;
            catcnttbl = ScoreTbl.create 1000 0.0;
            derivcnt  = 0;
            maxslash  = 0.0;
        }

    let to_string probinit =
        sprintf "<Rule Prob>\n%s\n\n<Category Count>\n%s"
            (DerivGenModel.to_string probinit.ruleprob)
            (ScoreTbl.to_string probinit.catcnttbl)

    let get_maxslash probinit =
        probinit.maxslash

    let learn_deriv_lex probinit syncat span headpos word =
        Scale.inc ();
        Scale.display ();
        let deriv = conv_word_to_deriv syncat word in
        let syncat = Deriv.get_syncat (Deriv.get_prems deriv) in
        let inst = ScoreInst.create syncat word in
        DerivGenModel.acc_prob probinit.ruleprob deriv 1.0;
        ScoreTbl.acc_prob probinit.catcnttbl inst 1.0;
        let slashno = Syncat.slashno syncat in
        if slashno > probinit.maxslash then
            probinit.maxslash <- slashno
        else ()

    let learn_deriv_dtrs 
            probinit syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
        let inst = ScoreInst.create syncat word in
        DerivGenModel.acc_prob probinit.ruleprob deriv 1.0;
        ScoreTbl.acc_prob probinit.catcnttbl inst 1.0;
        let slashno = Syncat.slashno syncat in
        if slashno > probinit.maxslash then
            probinit.maxslash <- slashno
        else ()

    let learn_chart probinit chart =
        Scale.add (Scale.WindMill 1500) "" "";
        probinit.derivcnt <- probinit.derivcnt + (Chart.get_derivcnt chart);
        Chart.iter_derivs
            chart Chart.TopDown
            (learn_deriv_lex probinit) (learn_deriv_dtrs probinit)
            (0, List.length (Chart.get_sentence chart) - 1) ;
        Scale.discard ()

    let learn_probinit probinit =
        let nocharts = probinit.nocharts in
        Scale.add (Scale.Number nocharts) "chart " " ";
        for idx = 0 to nocharts - 1 do
            Scale.inc ();
            Scale.display ();
            let chart = probinit.get_chart idx in
            if not (Chart.is_filledup chart) then
                learn_chart probinit chart
            else ()
        done;
        Scale.discard ()

    let marginalize_deriv probinit deriv oldprob =
        Scale.inc ();
        Scale.display ();
        let prems = Deriv.get_prems deriv in
        let syncat = Deriv.get_syncat prems
        and word = Deriv.get_head prems in
        let inst = ScoreInst.create syncat word in
        let denom = DerivGenModel.get_prob probinit.ruleprob deriv
        and nom = ScoreTbl.get_prob probinit.catcnttbl inst in
        let prob = denom /. nom in
        let random_mult = ref 0 in
        while !random_mult = 0 do
            random_mult := Random.int 101
        done;
        DerivGenModel.set_prob probinit.ruleprob deriv 
            (prob *. 0.01 *. (float_of_int !random_mult));
        (* DerivGenModel.set_prob probinit.ruleprob deriv prob; *)
        DerivGenModel.set_prob probinit.rulebias deriv prob

    let marginalize_probinit probinit =
        Scale.add (Scale.WindMill 1500) "randomizing each parameter " "";
        Random.self_init ();
        DerivGenModel.iter probinit.ruleprob (marginalize_deriv probinit);
        Scale.discard ()

    let learn ruleprob rulebias get_chart nocharts =
        Scale.print "Initializing the probability table ... ";
        let probinit = 
            create ruleprob rulebias get_chart nocharts in
        let starttime = Unix.time () in
        learn_probinit probinit;
        marginalize_probinit probinit;
        let endtime = Unix.time () in
        let runtime = (endtime -. starttime) /. 60.0 in
        Scale.print (sprintf 
            "done (%d charts, %d derivs, %d trees, runtime = %.2f mins)\n"
            nocharts
            (DerivGenModel.length ruleprob)
            probinit.derivcnt runtime);
        flush stderr;
        probinit.maxslash

end

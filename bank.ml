open Cg
open Tree
open Printf

type tree_t = Tree.t

module type CMD_TYPE = sig
    type t
    val parse : (Lexing.lexbuf -> Ruleparser.token) -> Lexing.lexbuf -> t
    val interpret : ?begpos:int -> t -> tree_t * int
end

module Bank (Cmd : CMD_TYPE) = struct

    type cmd_t = Cmd.t

    type t = tree_t list

    let parse = Ruleparser.tree

    let interpret ?(begpos = 0) cmd =
        Cmd.interpret ~begpos:begpos cmd

    let from_text fname =
        let lexbuf = Lexing.from_channel (open_in fname) in
        let result = ref []
        and flag = ref true in
        while !flag do
            try
                let cmd = Cmd.parse Rulelexer.token lexbuf in
                let (tree, _) = Cmd.interpret cmd in
                result := !result @ [tree]
            with
                Rulelexer.Eof -> flag := false
              | e -> raise e
        done;
        !result

    let rec from_texts fnames =
        match fnames with
            [] -> []
          | fname :: rest ->
                (from_text fname) @ (from_texts rest)

end

module TreeData = struct

    type t = Ruletypes.tree_t

    let parse = Ruleparser.tree

    let rec interpret ?(begpos = 0) cmd =
        match cmd with
            Ruletypes.LexNode head ->
                let span = (begpos, begpos) in
                (Tree.create_term head span, begpos)
          | Ruletypes.TreeNode (head, dtrcmds) ->
                interpret_dtrcmds begpos head dtrcmds

    and interpret_dtrcmds begpos head dtrcmds =
        let dtrbegpos = ref begpos
        and dtrs = ref [] in
        List.iter 
            (fun dtrcmd ->
                let (tree, endpos) = 
                    interpret ~begpos:!dtrbegpos dtrcmd in
                dtrs := !dtrs @ [tree];
                dtrbegpos := endpos + 1)
            dtrcmds;
        let endpos = !dtrbegpos - 1 in
        let syncat = Syncat.create_atom "X"
        and span = (begpos, endpos) 
        and headpos = begpos
        and depdir = Syncat.Left in
        let tree = Tree.create_nonterm 
            syncat head !dtrs span headpos depdir in
        (tree, endpos)

end

module DepData = struct

    type t = Ruletypes.deptree_t

    let parse = Ruleparser.deptree

    let rec interpret ?(begpos = 0) cmd =
        match cmd with
            Ruletypes.DepLexNode head ->
                let span = (begpos, begpos) in
                (Tree.create_term head span, begpos)
          | Ruletypes.DepTreeNode (head, dtrcmd1, dtrcmd2, depdir) ->
                interpret_dtrcmds begpos head [dtrcmd1; dtrcmd2] depdir
          | Ruletypes.DepCoorNode (head, dtrcmd1, dtrcmd2, dtrcmd3) ->
                interpret_dtrcmds begpos head [dtrcmd1; dtrcmd2; dtrcmd3] Syncat.Coor
          | Ruletypes.DepUnaryNode (head, dtrcmd) ->
                interpret_dtrcmds begpos head [dtrcmd] Syncat.Unary

    and interpret_dtrcmds begpos head dtrcmds depdir =
        let dtrbegpos = ref begpos
        and dtrs = ref [] in
        List.iter 
            (fun dtrcmd ->
                let (tree, endpos) = 
                    interpret ~begpos:!dtrbegpos dtrcmd in
                dtrs := !dtrs @ [tree];
                dtrbegpos := endpos + 1)
            dtrcmds;
        let endpos = !dtrbegpos - 1 in
        let syncat = Syncat.create_atom "X"
        and span = (begpos, endpos) 
        and headpos = interpret_headpos depdir !dtrs in
        let tree = Tree.create_nonterm 
            syncat head !dtrs span headpos depdir in
        (tree, endpos)

    and interpret_headpos depdir dtrs =
        match depdir with
            Syncat.Left | Syncat.RightSerial -> Tree.get_headpos (List.nth dtrs 0)
          | Syncat.Right | Syncat.LeftSerial -> Tree.get_headpos (List.nth dtrs 1)
          | Syncat.Unary -> Tree.get_headpos (List.nth dtrs 0)
          | Syncat.Coor -> Tree.get_headpos (List.nth dtrs 0)

end

module TreeBank = Bank(TreeData)

module DepBank = Bank(DepData)

open Dep
open Dict
open Dictloader
open Conlldep
open Printf

module AnnScheme = struct

    type t = {
        mutable adj : string list;
        mutable n : string list;
        mutable nmod : string list;
        mutable v : string list;
        mutable vmod : string list;
        mutable gerund : string list;
        mutable gmod : string list;
        mutable smod : string list;
        mutable s : string list;
        mutable adv : string list;
        mutable neg : string list;
        mutable modal : string list;
        mutable adpos : string list;
        mutable relpro : string list;
        mutable part : string list;
        mutable subconj : string list;
        mutable poss : string list;
        mutable inf : string list;
        mutable npnom : string list;
        mutable vpnom : string list;
        mutable num : string list;
        mutable cl : string list;

        mutable head_adj : int;
        mutable head_nmod : int;
        mutable head_verb : int;
        mutable head_vmod : int;
        mutable head_gmod : int;
        mutable head_smod : int;
        mutable head_adv : int;
        mutable head_neg : int;
        mutable head_modal : int;
        mutable head_adpos : int;
        mutable head_relpro : int;
        mutable head_part : int;
        mutable head_subconj_smod : int;
        mutable head_subconj_phr : int;
        mutable head_poss_nmod : int;
        mutable head_poss_phr : int;
        mutable head_inf : int;
        mutable head_npnom : int;
        mutable head_vpnom : int;
        mutable head_cl : int;
    }

    let create () = {
        adj = [];
        n = [];
        nmod = [];
        v = [];
        vmod = [];
        gerund = [];
        gmod = [];
        smod = [];
        s = [];
        adv = [];
        neg = [];
        modal = [];
        adpos = [];
        relpro = [];
        part = [];
        subconj = [];
        poss = [];
        inf = [];
        npnom = [];
        vpnom = [];
        num = [];
        cl = [];

        head_adj = 0;
        head_nmod = 0;
        head_verb = 0;
        head_vmod = 0;
        head_gmod = 0;
        head_smod = 0;
        head_adv = 0;
        head_neg = 0;
        head_modal = 0;
        head_adpos = 0;
        head_relpro = 0;
        head_part = 0;
        head_subconj_smod = 0;
        head_subconj_phr = 0;
        head_poss_nmod = 0;
        head_poss_phr = 0;
        head_inf = 0;
        head_npnom = 0;
        head_vpnom = 0;
        head_cl = 0;
    }

    let add_adj sch tag =
        sch.adj <- tag :: sch.adj

    let add_n sch tag =
        sch.n <- tag :: sch.n

    let add_nmod sch tag =
        sch.nmod <- tag :: sch.nmod

    let add_v sch tag =
        sch.v <- tag :: sch.v

    let add_vmod sch tag =
        sch.vmod <- tag :: sch.vmod

    let add_gerund sch tag =
        sch.gerund <- tag :: sch.gerund

    let add_gmod sch tag =
        sch.gmod <- tag :: sch.gmod

    let add_smod sch tag =
        sch.smod <- tag :: sch.smod

    let add_s sch tag =
        sch.s <- tag :: sch.s

    let add_adv sch tag =
        sch.adv <- tag :: sch.adv

    let add_neg sch tag =
        sch.neg <- tag :: sch.neg

    let add_modal sch tag =
        sch.modal <- tag :: sch.modal

    let add_adpos sch tag =
        sch.adpos <- tag :: sch.adpos

    let add_relpro sch tag =
        sch.relpro <- tag :: sch.relpro

    let add_part sch tag =
        sch.part <- tag :: sch.part

    let add_subconj sch tag =
        sch.subconj <- tag :: sch.subconj

    let add_poss sch tag =
        sch.poss <- tag :: sch.poss

    let add_inf sch tag =
        sch.inf <- tag :: sch.inf

    let add_npnom sch tag =
        sch.npnom <- tag :: sch.npnom

    let add_vpnom sch tag =
        sch.vpnom <- tag :: sch.vpnom

    let add_num sch tag =
        sch.num <- tag :: sch.num

    let add_cl sch tag =
        sch.cl <- tag :: sch.cl

    let add_from_dict sch dict =
        Dict.iter_catdict dict
            (fun tag cats ->
                (* printf "adding %s := %s\n" tag (String.concat ", " cats); *)
                List.iter
                    (fun cat ->
                        match cat with
                            "n" | "np" -> add_n sch tag
                          | "adj" -> add_adj sch tag
                          | "nmod" -> add_nmod sch tag
                          | "v" | "vi" | "vt" | "vd" | "vicomp" | "vtcomp" | "vcomp" | "vp" -> add_v sch tag
                          | "vmod" -> add_vmod sch tag
                          | "gerund" -> add_gerund sch tag
                          | "gmod" -> add_gmod sch tag
                          | "smod" -> add_smod sch tag
                          | "s" -> add_s sch tag
                          | "adv" -> add_adv sch tag
                          | "neg" -> add_neg sch tag
                          | "modal" -> add_modal sch tag
                          | "adposition" | "prep" | "post" -> add_adpos sch tag
                          | "relpro" -> add_relpro sch tag
                          | "part" -> add_part sch tag
                          | "subconj" -> add_subconj sch tag
                          | "poss" -> add_poss sch tag
                          | "inf" -> add_inf sch tag
                          | "npnom" -> add_npnom sch tag
                          | "vpnom" -> add_vpnom sch tag
                          | "num" -> add_num sch tag
                          | "cl" | "adjcl" | "advcl" -> add_cl sch tag
                          | _ -> ()
                    )
                    cats
            )

    let learn_headdep sch head dep = begin

        (* N + _Adj_ *)
        if List.mem head sch.n && List.mem dep sch.adj then
            sch.head_adj <- sch.head_adj - 1
        else ();
        if List.mem head sch.adj && List.mem dep sch.n then
            sch.head_adj <- sch.head_adj + 1
        else ();

        (* N + _Nmod_ *)
        if List.mem head sch.n && List.mem dep sch.nmod then
            sch.head_nmod <- sch.head_nmod - 1
        else ();
        if List.mem head sch.nmod && List.mem dep sch.n then
            sch.head_nmod <- sch.head_nmod + 1
        else ();

        (* _V_ + N *)
        if List.mem head sch.v && List.mem dep sch.n then
            sch.head_verb <- sch.head_verb - 1
        else ();
        if List.mem head sch.n && List.mem dep sch.v then
            sch.head_verb <- sch.head_verb + 1
        else ();

        (* V + _Vmod_ *)
        if List.mem head sch.v && List.mem dep sch.vmod then
            sch.head_nmod <- sch.head_nmod - 1
        else ();
        if List.mem head sch.vmod && List.mem dep sch.v then
            sch.head_nmod <- sch.head_nmod + 1
        else ();

        (* Gerund + _Gmod_ *)
        if List.mem head sch.gerund && List.mem dep sch.gmod then
            sch.head_gmod <- sch.head_gmod - 1
        else ();
        if List.mem head sch.gmod && List.mem dep sch.gerund then
            sch.head_gmod <- sch.head_gmod + 1
        else ();

        (* S + _Smod_ *)
        if List.mem head sch.s && List.mem dep sch.smod then
            sch.head_smod <- sch.head_smod - 1
        else ();
        if List.mem head sch.smod && List.mem dep sch.s then
            sch.head_smod <- sch.head_smod + 1
        else ();

        (* V + _Adv_ *)
        if List.mem head sch.v && List.mem dep sch.adv then
            sch.head_adv <- sch.head_adv - 1
        else ();
        if List.mem head sch.adv && List.mem dep sch.v then
            sch.head_adv <- sch.head_adv + 1
        else ();

        (* V/Adj/Adv/Nmod/Vmod + _Neg_ *)
        if (List.mem head sch.v || List.mem head sch.adj || List.mem head sch.adv || List.mem head sch.nmod || List.mem head sch.vmod) && List.mem dep sch.neg then
            sch.head_neg <- sch.head_neg - 1
        else ();
        if List.mem head sch.neg && (List.mem head sch.v || List.mem head sch.adj || List.mem head sch.adv || List.mem head sch.nmod || List.mem head sch.vmod) then
            sch.head_neg <- sch.head_neg + 1
        else ();

        (* _Modal_ + V *)
        if List.mem head sch.v && List.mem dep sch.modal then
            sch.head_modal <- sch.head_modal - 1
        else ();
        if List.mem head sch.modal && List.mem dep sch.v then
            sch.head_modal <- sch.head_modal + 1
        else ();

        (* _Adpos_ + N *)
        if List.mem head sch.adpos && List.mem dep sch.n then
            sch.head_adpos <- sch.head_adpos - 1
        else ();
        if List.mem head sch.n && List.mem dep sch.adpos then
            sch.head_adpos <- sch.head_adpos + 1
        else ();

        (* _Relpro_ + V *)
        if List.mem head sch.v && List.mem dep sch.relpro then
            sch.head_relpro <- sch.head_relpro - 1
        else ();
        if List.mem head sch.relpro && List.mem dep sch.v then
            sch.head_relpro <- sch.head_relpro + 1
        else ();

        (* _Part_ + V *)
        if List.mem head sch.v && List.mem dep sch.part then
            sch.head_part <- sch.head_part - 1
        else ();
        if List.mem head sch.part && List.mem dep sch.v then
            sch.head_part <- sch.head_part + 1
        else ();

        (* _SubConj_ + V *)
        if List.mem head sch.v && List.mem dep sch.subconj then begin
            sch.head_subconj_phr <- sch.head_subconj_phr - 1;
            sch.head_subconj_smod <- sch.head_subconj_smod + 1
        end else ();
        if List.mem head sch.subconj && List.mem dep sch.v then begin
            sch.head_subconj_phr <- sch.head_subconj_phr + 1;
            sch.head_subconj_smod <- sch.head_subconj_smod - 1
        end else ();

        (* _Poss_ + N *)
        if List.mem head sch.n && List.mem dep sch.poss then begin
            sch.head_poss_phr <- sch.head_poss_phr - 1;
            sch.head_poss_nmod <- sch.head_poss_nmod + 1
        end else ();
        if List.mem head sch.poss && List.mem dep sch.n then begin
            sch.head_poss_phr <- sch.head_poss_phr + 1;
            sch.head_poss_nmod <- sch.head_poss_nmod - 1
        end else ();

        (* _Inf_ + V *)
        if List.mem head sch.v && List.mem dep sch.inf then
            sch.head_inf <- sch.head_inf - 1
        else ();
        if List.mem head sch.inf && List.mem dep sch.v then
            sch.head_inf <- sch.head_inf + 1
        else ();

        (* _Npnom_ + N *)
        if List.mem head sch.n && List.mem dep sch.npnom then
            sch.head_npnom <- sch.head_npnom - 1
        else ();
        if List.mem head sch.npnom && List.mem dep sch.n then
            sch.head_npnom <- sch.head_npnom + 1
        else ();

        (* _Vpnom_ + V *)
        if List.mem head sch.v && List.mem dep sch.vpnom then
            sch.head_vpnom <- sch.head_vpnom - 1
        else ();
        if List.mem head sch.vpnom && List.mem dep sch.v then
            sch.head_vpnom <- sch.head_vpnom + 1
        else ();

        (* Num + _Cl_ *)
        if List.mem head sch.num && List.mem dep sch.cl then
            sch.head_cl <- sch.head_cl - 1
        else ();
        if List.mem head sch.cl && List.mem dep sch.num then
            sch.head_cl <- sch.head_cl + 1
        else ()

    end

    let learn sch depstr =
        let deppairs = DepStruct.depset_words depstr in
        List.iter
            (fun deppair ->
                let head = DepPair.get_head deppair
                and dep = DepPair.get_dep deppair in
                learn_headdep sch head dep)
            deppairs

    let show_table sch =
        printf "adj = %s\n" (String.concat ", " sch.adj);
        printf "n = %s\n" (String.concat ", " sch.n);
        printf "nmod = %s\n" (String.concat ", " sch.nmod);
        printf "v = %s\n" (String.concat ", " sch.v);
        printf "vmod = %s\n" (String.concat ", " sch.vmod);
        printf "gerund = %s\n" (String.concat ", " sch.gerund);
        printf "gmod = %s\n" (String.concat ", " sch.gmod);
        printf "s = %s\n" (String.concat ", " sch.s);
        printf "smod = %s\n" (String.concat ", " sch.smod);
        printf "adv = %s\n" (String.concat ", " sch.adv);
        printf "neg = %s\n" (String.concat ", " sch.neg);
        printf "modal = %s\n" (String.concat ", " sch.modal);
        printf "adpos = %s\n" (String.concat ", " sch.adpos);
        printf "relpro = %s\n" (String.concat ", " sch.relpro);
        printf "part = %s\n" (String.concat ", " sch.part);
        printf "subconj = %s\n" (String.concat ", " sch.subconj);
        printf "poss = %s\n" (String.concat ", " sch.poss);
        printf "inf = %s\n" (String.concat ", " sch.inf);
        printf "npnom = %s\n" (String.concat ", " sch.npnom);
        printf "vpnom = %s\n" (String.concat ", " sch.vpnom);
        printf "num = %s\n" (String.concat ", " sch.num);
        printf "cl = %s\n\n" (String.concat ", " sch.cl)

    let repr sch =
        let repr_flag (flag, name) =
            if flag > 0 then "+" ^ name
            else if flag < 0 then "-" ^ name
            else "?" ^ name
        in
        String.concat ", "
            (List.map repr_flag [
                (sch.head_adj, "adj");
                (sch.head_nmod, "nmod");
                (sch.head_verb, "verb");
                (sch.head_vmod, "vmod");
                (sch.head_gmod, "gmod");
                (sch.head_smod, "smod");
                (sch.head_adv, "adv");
                (sch.head_neg, "neg");
                (sch.head_modal, "modal");
                (sch.head_adpos, "adpos");
                (sch.head_relpro, "relpro");
                (sch.head_part, "part");
                (sch.head_subconj_smod, "subconj_smod");
                (sch.head_subconj_phr, "subconj_phr");
                (sch.head_poss_nmod, "poss_nmod");
                (sch.head_poss_phr, "poss_phr");
                (sch.head_inf, "inf");
                (sch.head_npnom, "npnom");
                (sch.head_vpnom, "vpnom");
                (sch.head_cl, "cl")
            ])

end

let main =
    let dictfname = ref "" in
    let sch = AnnScheme.create () in
    let fnames = ref [] in
    let args = [
        ("--adj",
            Arg.String (AnnScheme.add_adj sch),
            "   Define potential adjective tags");
        ("--n",
            Arg.String (AnnScheme.add_n sch),
            "   Define potential noun tags");
        ("--nmod",
            Arg.String (AnnScheme.add_nmod sch),
            "   Define potential noun modifier tags");
        ("--v",
            Arg.String (AnnScheme.add_v sch),
            "   Define potential verb tags");
        ("--vmod",
            Arg.String (AnnScheme.add_v sch),
            "   Define potential verb modifier tags");
        ("--gerund",
            Arg.String (AnnScheme.add_gerund sch),
            "   Define potential gerund tags");
        ("--gmod",
            Arg.String (AnnScheme.add_gmod sch),
            "   Define potential gerund modifier tags");
        ("--s",
            Arg.String (AnnScheme.add_s sch),
            "   Define potential sentence tags");
        ("--smod",
            Arg.String (AnnScheme.add_smod sch),
            "   Define potential sentence modifier tags");
        ("--adv",
            Arg.String (AnnScheme.add_adv sch),
            "   Define potential adverb tags");
        ("--neg",
            Arg.String (AnnScheme.add_neg sch),
            "   Define potential negator tags");
        ("--modal",
            Arg.String (AnnScheme.add_modal sch),
            "   Define potential modal tags");
        ("--adpos",
            Arg.String (AnnScheme.add_adpos sch),
            "   Define potential adposition tags");
        ("--relpro",
            Arg.String (AnnScheme.add_relpro sch),
            "   Define potential adposition tags");
        ("--part",
            Arg.String (AnnScheme.add_part sch),
            "   Define potential particle tags");
        ("--subconj",
            Arg.String (AnnScheme.add_subconj sch),
            "   Define potential subordinate conjunction tags");
        ("--poss",
            Arg.String (AnnScheme.add_poss sch),
            "   Define potential possessive marker tags");
        ("--inf",
            Arg.String (AnnScheme.add_inf sch),
            "   Define potential infinitive marker tags");
        ("--npnom",
            Arg.String (AnnScheme.add_npnom sch),
            "   Define potential NP nominalizer tags");
        ("--vpnom",
            Arg.String (AnnScheme.add_vpnom sch),
            "   Define potential VP nominalizer tags");
        ("--num",
            Arg.String (AnnScheme.add_num sch),
            "   Define potential numeral tags");
        ("--cl",
            Arg.String (AnnScheme.add_cl sch),
            "   Define potential classifier tags");
        ("--dict",
            Arg.String (fun f -> dictfname := f),
            "   Read the category map from the dictionary");
    ]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg =
        "Approximate the annotation scheme of CoNLL2006-formatted dependency files.\n\n"
        ^ "annscheme [options] depfile1 depfile2 ..." in
    Arg.parse args fn_anon usagemsg;
    if String.length !dictfname > 0 then
        let dict = DictLoader.load !dictfname false in
        AnnScheme.add_from_dict sch dict
    else ();
    let depstrs = ConllDep.from_files !fnames in
    AnnScheme.show_table sch;
    List.iter
        (fun depstr -> AnnScheme.learn sch depstr)
        depstrs;
    printf "\nRESULT:\n\n";
    printf "heads = %s\n" (AnnScheme.repr sch)

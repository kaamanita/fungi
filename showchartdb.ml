open Cg
open Chart
open Chartdb
open Deriv
open Printf

let totalcnt = ref 0

let succcnt = ref 0

let list_chart_word syncat span headpos word =
    let (begpos, endpos) = span in
    let deriv = conv_word_to_deriv syncat word in
    printf "(%d, %d) : %s\n" begpos endpos (Deriv.to_string deriv)

let list_chart_dtrs syncat span headpos word depdir dtrargs =
    let (begpos, endpos) = span in
    let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
    printf "(%d, %d) : %s\n" begpos endpos (Deriv.to_string deriv)

let list_chart chart =
    let n = List.length (Chart.get_sentence chart) in
    Chart.iter_derivs chart Chart.BottomUp 
        list_chart_word list_chart_dtrs (0, n - 1)

let showchartdb showchart showderivs showunparsable showderivcnt fname =
    if showchart || showunparsable || showderivcnt then
        printf "\nChart database: %s\n\n" fname
    else ();
    let chartdb = (* ChartDb.opendb fname [Mdb.ReadOnly] *) ChartDb.from_file fname in
    ChartDb.iter
        (fun id chart ->
            let enum = Chart.is_filledup chart in
            if showderivcnt then
                printf "<Chart %d>\nSentence = %s\nDeriv no = %d\nEnum = %s\n\n" 
                    id 
                    (String.concat " "(Chart.get_sentence chart))
                    (Chart.get_derivcnt chart)
                    (string_of_bool enum)
            else if showunparsable && (not (Chart.is_success chart) || Chart.is_filledup chart) then
                printf "<Chart %d>\nSentence = %s\n\n" id (String.concat " "(Chart.get_sentence chart))
            else if showchart then
                printf "<Chart %d>\n%s\nEnum = %s\n\n" id (Chart.to_string chart) (string_of_bool enum)
            else ();
            if showderivs && (showderivcnt || showunparsable || showchart) then begin
                printf "Derivations (Bottom-up):\n";
                list_chart chart;
                printf "\n"
            end else ();
            incr totalcnt;
            if Chart.is_success chart && not (Chart.is_filledup chart) then incr succcnt
            else ())
        chartdb (*;
    ChartDb.close chartdb *)

let main =
    let fnames = ref [] 
    and showchart = ref false 
    and showderivs = ref false
    and showunparsable = ref false 
    and showderivcnt = ref false in
    let args = [
        ("--content",
            Arg.Set showchart,
            "  Also display chart contents");
        ("--derivs",
            Arg.Set showderivs,
            "  Also display all derivations");
        ("--unparsable",
            Arg.Set showunparsable,
            "  Show only unparsable charts");
        ("--derivcnt",
            Arg.Set showderivcnt,
            "  Show only derivation numbers") ]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg = 
        "\nList all charts in the specified chart databases.\n\n"
        ^ (sprintf "Usage: %s chartdb1 chartdb2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    List.iter (showchartdb !showchart !showderivs !showunparsable !showderivcnt) !fnames;
    let perc = 100.0 *. (float_of_int !succcnt /. float_of_int !totalcnt) in
    printf "Summary: success %d / total %d (%6.2f%%)\n" !succcnt !totalcnt perc

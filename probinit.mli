module ProbInit :
sig
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type fn_get_chart_t = int -> chart_t
    val learn : genmodel_t -> genmodel_t -> fn_get_chart_t -> int -> float
end

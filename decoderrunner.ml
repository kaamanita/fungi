open Cg
open Chart
open Chartdb
open Prob
open Deriv
open Models
open Decoderalgo
open Tree
open Treedb
open Utils
open Printf

type decoding_t = ViterbiDecoding | MEVDecoding | MBRDecoding

module DecoderRunner = struct

    type genmodel_t = DerivGenModel.t

    type t = {
        decoding : decoding_t;
        start    : Syncat.t;
        ruleprob : genmodel_t;
        wrapper  : ChartDbWrapper.t;
        catconst : float;
        lbrconst : float;
        rbrconst : float;
        treedb   : TreeDb.t;
        tdbfname : string;
        only_parsable : bool;
    }

    let create decoding start probflags epsilon probtblfname catconst lbrconst rbrconst fnames treedbfname only_parsable =
        let probftrs = conv_probflags probflags 1000 epsilon in
        {
            decoding = decoding;
            start    = start;
            ruleprob = DerivGenModel.from_file probtblfname probftrs;
            wrapper  = ChartDbWrapper.from_files fnames;
            catconst = catconst;
            lbrconst = lbrconst;
            rbrconst = rbrconst;
            treedb   = TreeDb.create treedbfname;
            tdbfname = treedbfname;
            only_parsable = only_parsable;
        }

    let parse_chart decoding ruleprob chart start catconst lbrconst rbrconst =
        (* Printf.printf "(viterbirunner.parse_chart) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let (tree, prob) = match decoding with
            ViterbiDecoding -> Viterbi.compute ruleprob chart start catconst lbrconst rbrconst
          | MEVDecoding -> MaxExpViterbi.compute ruleprob chart start catconst lbrconst rbrconst
          | MBRDecoding -> MinBayesRisk.compute ruleprob chart start catconst lbrconst rbrconst in
        let depstr = Tree.to_depstr tree in
        let enum = Chart.is_filledup chart in
        (tree, prob, depstr, enum)
    
    let parse_wrapper decoding ruleprob start wrapper treedb catconst lbrconst rbrconst only_parsable =
        let nocharts = ChartDbWrapper.length wrapper in
        Scale.add (Scale.GuageWithNumber nocharts) "parsing charts " " ";
        for idx = 0 to nocharts - 1 do
            Scale.inc ();
            Scale.display ();
            let chart = ChartDbWrapper.find wrapper idx in
            try
                let (tree, prob, depstr, enum) = parse_chart decoding ruleprob chart start catconst lbrconst rbrconst in
                if (only_parsable && not (Chart.is_filledup chart)) || (not only_parsable) then
                    let words = Chart.get_words chart in
                    TreeDb.add treedb (MarshTree.create tree prob depstr words enum)
                else ()
            with Not_found ->
                TreeDb.add treedb None
        done;
        Scale.discard ()

    let run vtb =
        parse_wrapper vtb.decoding vtb.ruleprob vtb.start vtb.wrapper vtb.treedb vtb.catconst vtb.lbrconst vtb.rbrconst vtb.only_parsable;
        TreeDb.to_file vtb.treedb vtb.tdbfname

    let compute decoding start probflags epsilon probtblfname catconst lbrconst rbrconst fnames treedbfname only_parsable =
        let vtb = create decoding start probflags epsilon probtblfname catconst lbrconst rbrconst fnames treedbfname only_parsable in
        run vtb
        
end

let set_probflags probflags str =
    let probflag = match str with
        "roleemis" -> [RoleEmis]
      | "hockenmaier" -> [JuliaEmis]
      | "lexemis" -> [LexEmis]
      | "heademis" -> [HeadEmis] 
      | "model0" -> []
      | "model1" -> [RoleEmis]
      | "model2" -> [JuliaEmis]
      | "model3" -> [RoleEmis; LexEmis]
      | "model4" -> [JuliaEmis; HeadEmis]
      | "model5" -> [RoleEmis; LexEmis; JuliaEmis; HeadEmis]
      | _ -> failwith "set_probflags" in
    probflags := union probflag !probflags

let set_decoding decoding str =
    decoding := match str with
        "viterbi" -> ViterbiDecoding
      | "maxexp" -> MEVDecoding
      | "mbr" -> MBRDecoding
      | _ -> failwith "set_decoding"

let set_start start str =
    let lexbuf = Lexing.from_string str in
    let syncat = Ruleparser.syncat Rulelexer.token lexbuf in
    start := syncat

module DecoderRunner :
sig
    type depstruct_t = Dep.DepStruct.t
    type t
    val create : depstruct_t list -> string list -> string -> t
    val parse_chart : depstruct_t -> Chart.Chart.t -> Tree.Tree.t * float * Tree.Tree.depstruct_t
    val parse_wrapper : depstruct_t list -> Chartdb.ChartDbWrapper.t -> Treedb.TreeDb.t -> unit
    val run : t -> unit
    val compute : depstruct_t list -> string list -> string -> unit
end

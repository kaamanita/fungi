open Cg
open Chart
open Prob
open Deriv
open Utils
open Printf

module ScoreInst = struct

    type span_t = int * int
    type syncat_t = Syncat.t
    type word_t = string

    type t = {
        span   : span_t;
        syncat : syncat_t;
        head   : word_t;
    }

    let create span syncat head = {
        span   = span;
        syncat = syncat;
        head   = head;
    }

    let get_span inst =
        inst.span

    let get_syncat inst =
        inst.syncat

    let get_head inst =
        inst.head

    let equal inst1 inst2 =
        inst1.span = inst2.span
        && Syncat.equal inst1.syncat inst2.syncat
        && inst1.head = inst2.head

    let hash inst =
        Hashtbl.hash inst.span
        + Syncat.hash inst.syncat
        + Hashtbl.hash inst.head

    let to_string inst =
        let (begpos, endpos) = inst.span in
        sprintf "(%d, %d, %s{%s})" 
            begpos endpos 
            (Syncat.to_string inst.syncat) inst.head

end

module ScoreTbl = ProbTbl(ScoreInst)

type scoretbl_t = ScoreTbl.t

module InsideScore = struct

    type syncat_t = Syncat.t

    type word_t = string

    type genmodel_t = DerivGenModel.t

    type chart_t = Chart.t

    type t = {
        scoretbl : scoretbl_t;
        genmodel : genmodel_t;
        chart    : chart_t;
        endpos   : int;
    }

    let create n epsilon genmodel chart = 
        let scoretbl = ScoreTbl.create n epsilon in
        {
            scoretbl = scoretbl;
            genmodel = genmodel;
            chart    = chart;
            endpos   = List.length (Chart.get_sentence chart) - 1;
        }

    let to_string insc =
        sprintf "<Inside Table>\n%s\n\n<Rule Prob>\n%s"
            (ScoreTbl.to_string insc.scoretbl)
            (DerivGenModel.to_string insc.genmodel)

    let get_scoretbl insc =
        insc.scoretbl

    let calc_lex_score insc syncat span headpos word =
        Scale.inc ();
        Scale.display ();
        let inst = ScoreInst.create span syncat word in
        let deriv = conv_word_to_deriv syncat word in
        let prob = DerivGenModel.get_smooth_prob insc.genmodel deriv in
        ScoreTbl.acc_prob insc.scoretbl inst prob

    let calc_dtrs_score insc syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let inst = ScoreInst.create span syncat word in
        let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
        let ruleprob = 
            DerivGenModel.get_smooth_prob insc.genmodel deriv in
        let prob = List.fold_left
            (fun v (syncat', span', _, word') -> 
                let inst' = ScoreInst.create span' syncat' word' in
                v *. ScoreTbl.get_smooth_prob insc.scoretbl inst')
            ruleprob dtrargs in
        ScoreTbl.acc_prob insc.scoretbl inst prob

    let compute insc =
        Scale.add 
            (* (Scale.Number (Chart.get_derivcnt insc.chart)) *)
            (Scale.WindMill 1500)
            "Inside  " "";
        Chart.iter_derivs insc.chart 
            Chart.BottomUp
            (calc_lex_score insc) (calc_dtrs_score insc) 
            (0, insc.endpos);
        Scale.discard ()

end

module OutsideScore = struct

    type syncat_t = Syncat.t

    type word_t = string
    
    type genmodel_t = DerivGenModel.t

    type chart_t = Chart.t

    type t = {
        scoretbl : scoretbl_t;
        genmodel : genmodel_t;
        chart    : chart_t;
        insctbl  : scoretbl_t;
        start    : syncat_t;
        endpos   : int;
    }

    let create n epsilon genmodel chart insctbl start = 
        let scoretbl = ScoreTbl.create n epsilon in
        {
            scoretbl = scoretbl;
            genmodel = genmodel;
            chart    = chart;
            insctbl  = insctbl;
            start    = start;
            endpos   = List.length (Chart.get_sentence chart) - 1;
        }

    let to_string outsc =
        sprintf "<Outside Table>\n%s\n\n<Inside Table>\n%s\n\n<Rule Prob>\n%s"
            (ScoreTbl.to_string outsc.scoretbl)
            (ScoreTbl.to_string outsc.insctbl)
            (DerivGenModel.to_string outsc.genmodel)

    let get_scoretbl outsc =
        outsc.scoretbl

    let init_score outsc inst =
        let syncat = ScoreInst.get_syncat inst in
        if Syncat.equal_ign_attrs syncat outsc.start then
            ScoreTbl.set_prob outsc.scoretbl inst 1.0
        else
            ScoreTbl.set_prob outsc.scoretbl inst 0.0

    let calc_lex_score outsc syncat span headpos word =
        let inst = ScoreInst.create span syncat word in
        let deriv = conv_word_to_deriv syncat word in
        let osc = ScoreTbl.get_smooth_prob outsc.scoretbl inst
        and ruleprob = 
            DerivGenModel.get_smooth_prob outsc.genmodel deriv in
        ScoreTbl.acc_prob outsc.scoretbl inst (osc *. ruleprob)

    let calc_subtree_score outsc initscore dtrargs =
        Scale.inc ();
        Scale.display ();
        for dtrpos = 0 to List.length dtrargs - 1 do
            let (syncat', span', _, word') = List.nth dtrargs dtrpos in
            let inst' = ScoreInst.create span' syncat' word' in
            let prob = List.fold_left
                (fun v (syncat'', span'', _, word'') ->
                    let inst'' = 
                        ScoreInst.create span'' syncat'' word'' in
                    v *. ScoreTbl.get_smooth_prob outsc.insctbl inst'')
                initscore (except_pos dtrargs dtrpos) in
            ScoreTbl.acc_prob outsc.scoretbl inst' prob
        done

    let calc_dtrs_score outsc syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let inst = ScoreInst.create span syncat word in
        if span = (0, outsc.endpos) then
            init_score outsc inst
        else ();
        let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
        let osc = ScoreTbl.get_smooth_prob outsc.scoretbl inst
        and ruleprob = 
            DerivGenModel.get_smooth_prob outsc.genmodel deriv in
        calc_subtree_score outsc (osc *. ruleprob) dtrargs

    let compute outsc =
        Scale.add 
            (* (Scale.Number (Chart.get_derivcnt outsc.chart)) *)
            (Scale.WindMill 1500)
            "Outside " "";
        Chart.iter_derivs outsc.chart
            Chart.TopDown
            (calc_lex_score outsc) (calc_dtrs_score outsc)
            (0, outsc.endpos);
        Scale.discard ()

end

let compute n epsilon genmodel chart start =
    (* printf ">> After computing inside and outside scores\n\n";
    printf "<Rule Prob>\n%s\n\n" (DerivGenModel.to_string genmodel); *)
    let insc = InsideScore.create n epsilon genmodel chart in
    InsideScore.compute insc;
    let insctbl  = InsideScore.get_scoretbl insc in
    let outsc = 
        OutsideScore.create n epsilon genmodel chart insctbl start in
    OutsideScore.compute outsc;
    let outsctbl = OutsideScore.get_scoretbl outsc in
    (* printf "<Inside Score>\n%s\n\n<Outside Score>\n%s\n\n" 
        (ScoreTbl.to_string insctbl) (ScoreTbl.to_string outsctbl); *)
    (insctbl, outsctbl)

let compute_insc n epsilon genmodel chart start =
    (* printf ">> After computing inside and outside scores\n\n";
    printf "<Rule Prob>\n%s\n\n" (DerivGenModel.to_string genmodel); *)
    let insc = InsideScore.create n epsilon genmodel chart in
    InsideScore.compute insc;
    InsideScore.get_scoretbl insc

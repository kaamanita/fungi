(** Simulated Annealing Algorithm *)

module type PARAM_TYPE =
sig
    type t
    val to_string : t -> string
    val scale_prob : t -> float -> Deriv.Deriv.t -> float
    val localscore : t -> int -> int -> float
    val inc : t -> t
    val is_stop_iter : t -> bool
end

module Annealer :
functor (Param : PARAM_TYPE) ->
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type deriv_t = Deriv.Deriv.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> deriv_t -> float
    type fn_localscore_t = int -> int -> float
    type fn_compute_inout_t =
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t -> 
        fn_get_chart_t -> int -> unit
    type param_t = Param.t
    type t
    val create :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_annealing : t -> unit
    val compute :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> unit
end

module EmParam :
sig
    type t
    val create : int -> t
    val to_string : t -> string
    val scale_prob : t -> float -> Deriv.Deriv.t -> float
    val localscore : t -> int -> int -> float
    val inc : t -> t
    val is_stop_iter : t -> bool
end

module DetParam :
sig
    type t
    val create : float -> float -> t
    val to_string : t -> string
    val scale_prob : t -> float -> Deriv.Deriv.t -> float
    val localscore : t -> int -> int -> float
    val inc : t -> t
    val is_stop_iter : t -> bool
end

module SkewedDetParam :
sig
    type genmodel_t = Deriv.DerivGenModel.t
    type t
    val create : float -> float -> genmodel_t -> t
    val to_string : t -> string
    val scale_prob : t -> float -> Deriv.Deriv.t -> float
    val localscore : t -> int -> int -> float
    val inc : t -> t
    val is_stop_iter : t -> bool
end

module StructParam :
sig
    type t
    val create : float -> float -> float -> t
    val to_string : t -> string
    val scale_prob : t -> float -> Deriv.Deriv.t -> float
    val localscore : t -> int -> int -> float
    val inc : t -> t
    val is_stop_iter : t -> bool
end

module EmAnnealer :
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type deriv_t = Deriv.Deriv.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> deriv_t -> float
    type fn_localscore_t = int -> int -> float
    type fn_compute_inout_t =
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit
    type param_t = EmParam.t
    type t
    val create :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_annealing : t -> unit
    val compute :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> unit
end

module DetAnnealer :
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type deriv_t = Deriv.Deriv.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> deriv_t -> float
    type fn_localscore_t = int -> int -> float
    type fn_compute_inout_t =
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit
    type param_t = DetParam.t
    type t
    val create :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_annealing : t -> unit
    val compute :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> unit
end

module SkewedDetAnnealer :
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type deriv_t = Deriv.Deriv.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> deriv_t -> float
    type fn_localscore_t = int -> int -> float
    type fn_compute_inout_t =
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit
    type param_t = SkewedDetParam.t
    type t
    val create :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t -> 
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_annealing : t -> unit
    val compute :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> unit
end

module StructAnnealer :
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type deriv_t = Deriv.Deriv.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> deriv_t -> float
    type fn_localscore_t = int -> int -> float
    type fn_compute_inout_t =
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit
    type param_t = StructParam.t
    type t
    val create :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t -> 
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_annealing : t -> unit
    val compute :
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> fn_compute_inout_t -> param_t -> unit
end

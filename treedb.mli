(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

module MarshTree :
sig
    type tree_t = Tree.Tree.t
    type depstruct_t = Dep.DepStruct.t
    type t = (tree_t * float * depstruct_t * string list * bool) option
    type key_t = int
    type value_t = t
    val create : tree_t -> float -> depstruct_t -> string list -> bool -> t
    val equal : 'a -> 'a -> bool
    val hash : 'a -> int
    val get_tree : t -> tree_t
    val get_prob : t -> float
    val get_depstr : t -> depstruct_t
    val get_words : t -> string list
    val get_enum : t -> bool
    val to_string : t -> string
end

module TreeDb :
sig
    type key_t = int
    type value_t = MarshTree.t
    type t
    val create : string -> t
    val add : t -> value_t -> unit
    val copy : t -> t
    val find : t -> key_t -> value_t
    val find_all : t -> key_t -> value_t list
    val mem : t -> key_t -> bool
    val remove : t -> key_t -> unit
    val replace : t -> key_t -> value_t -> unit
    val iter : (key_t -> value_t -> unit) -> t -> unit
    val fold : (key_t -> value_t -> 'a -> 'a) -> t -> 'a -> 'a
    val length : t -> int
    val from_file : string -> t
    val to_file : t -> string -> unit
end

module TreeDbWrapper :
sig
    type key_t = int
    type value_t = MarshTree.t
    type seqhdb_t = TreeDb.t
    type t
    val create : seqhdb_t list -> t
    val from_files : string list -> t
    val get_nodbs : t -> int
    val get_noitems : t -> int
    val nth : t -> int -> seqhdb_t
    val calc_relidx : t -> int -> int * int
    val mem : t -> int -> bool
    val find : t -> int -> value_t
    val find_all : t -> int -> value_t list
    val length : t -> int
    val replace : t -> int -> value_t -> unit
    val iter : (key_t -> value_t -> unit) -> t -> unit
end

open Printf

let is_multicore = ref true

let read_lines fname =
    let revlines = ref [] in
    let chan = open_in fname in
    begin
        try
            while true do
                revlines := input_line chan :: !revlines
            done
        with End_of_file ->
            close_in chan
    end;
    List.rev !revlines

let run_line line =
    ignore (Sys.command line)

let run_batch fname =
    let lines = read_lines fname in
    Scale.print (sprintf ">> Running %s: " fname);
    Scale.add (Scale.GuageWithNumber (List.length lines)) "" "";
    let mapfold = 
        if !is_multicore then
            Functory.Cores.map_local_fold
        else
            Functory.Network.Same.map_local_fold
    in
    mapfold
        ~f:(fun line -> run_line line)
        ~fold:(fun _ _ -> Scale.inc (); Scale.display ())
        () lines;
    Scale.discard ();
    Scale.print "done.\n"

let main =
    let nocores = ref 1
    and fnames = ref [] in
    
    let args = [
        ("--quiet", 
            Arg.Unit Scale.go_quiet, 
            "  Quiet mode (default = false)");
        ("--maxtasks",
            Arg.Int (
                fun n -> 
                    is_multicore := true;
                    nocores := n
            ),
            "  Set the maximum number of parallel tasks to run at the same time (default = 1; it yields best performance if set to N - 1, where N is the number of processing cores)");
        ("--worker",
            Arg.String (
                fun s ->
                    is_multicore := false;
                    let tokens = Utils.split_delim s ":" in
                    match tokens with
                        [hn; nc] -> 
                            Functory.Network.declare_workers ~n:(int_of_string nc) hn
                      | _ -> begin
                            fprintf stderr ">> The worker node %s is in an incorrect format.\n" s;
                            exit 1
                        end
            ),
            "  Set a worker node [format: hostname:cores]")
    ] 
    and fn_anon = (fun f -> fnames := !fnames @ [f]) 
    and usagemsg = 
        "Run a task scheduler. All jobs must be mutually independent as they will not be sequentially executed.\n\n"
        ^ (sprintf "Usage: %s [options] batchfile1 batchfile2 ..." Sys.argv.(0))
    in
    Arg.parse args fn_anon usagemsg;

    if !nocores < 1 then
        Arg.usage args usagemsg
    else ();

    Functory.Cores.set_number_of_cores !nocores;
    
    List.iter run_batch !fnames

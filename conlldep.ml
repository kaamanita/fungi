(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Cg
open Printf
open Utils
open Dep
open Tree

module ConllDep = struct

    type entry_t = {
        id            : int;
        form          : string;
        lemma         : string;
        cpostag       : string;
        postag        : string;
        feats         : string list;
        mutable head  : int;
        deprel        : string;
        mutable phead : int option;
        pdeprel       : string option;
    }

    let repr_entry entry =
        sprintf "Head (%d) -> Dep (%d, %s)" entry.head entry.id entry.form

    let compare_entry entry1 entry2 =
        Stdlib.compare entry1.id entry2.id

    type entries_t = entry_t list * string list * int list * int

    let tagmap = ref []

    let add_tagmap tag attr newtag =
        if List.mem_assoc (tag, attr) !tagmap then
            tagmap := ((tag, attr), newtag) :: (List.remove_assoc (tag, attr) !tagmap)
        else
            tagmap := ((tag, attr), newtag) :: !tagmap

    let entry_to_postag (entry : entry_t) : string =
        if entry.cpostag = entry.postag || entry.postag = "_" then
            sprintf "%s" entry.cpostag
        else
            let cposlen = String.length entry.cpostag in
            let poslen = String.length entry.postag in
            if cposlen < poslen then
                let prefix = String.sub entry.postag 0 cposlen in
                if entry.cpostag = prefix then
                    entry.postag
                else
                    sprintf "%s-%s" entry.cpostag entry.postag
            else
                sprintf "%s-%s" entry.cpostag entry.postag

    let conv_tag_with_map (entry : entry_t) : string * string list =
        let pos = entry_to_postag entry in
        let rec conv_map pos attrs =
            match attrs with
                [] -> 
                    if List.mem_assoc (pos, "*") !tagmap then
                        List.assoc (pos, "*") !tagmap
                    else pos
              | attr :: rest ->
                    if List.mem_assoc (pos, attr) !tagmap then
                        List.assoc (pos, attr) !tagmap
                    else conv_map pos rest
        in
        let newpos = conv_map pos entry.feats in
        (newpos, entry.feats)

    let entry_to_tag (entry : entry_t) : string =
        let (newpos, feats) = conv_tag_with_map entry in
        let attrs = match feats with
            ["_"] -> ""
          | _ -> sprintf "@%s" (String.concat "." feats)
        in
        sprintf "%s%s" newpos attrs

    let entry_to_word (entry : entry_t) : string =
        let (newpos, _) = conv_tag_with_map entry in
        Printf.sprintf "%s/%s" entry.form newpos

    let entries_to_tags (entries : entries_t) : string =
        let (entrylist, _, _, _) = entries in
        String.concat " "
            (List.map entry_to_tag entrylist)

    let entries_to_sents (entries : entries_t) : string =
        let (entrylist, _, _, _) = entries in
        String.concat " "
            (List.map entry_to_word entrylist)

    let entry_to_conll entry =
        let (newpos, feats) = conv_tag_with_map entry in
        sprintf "%d\t%s\t%s\t%s\t%s\t%s\t%d\t%s\t%s\t%s"
            (entry.id + 1) entry.form entry.lemma entry.cpostag newpos
            (String.concat "|" feats) (entry.head + 1) entry.deprel
            (match entry.phead with
                None -> "_"
              | Some phead -> string_of_int (phead + 1))
            (match entry.pdeprel with
                None -> "_"
              | Some pdeprel -> pdeprel)

    let entries_to_conll (entries : entries_t) : string =
        let (entrylist, _, _, _) = entries in
        String.concat "\n"
            (List.map entry_to_conll entrylist)

    type content_t = entries_t list

    type depstr_t = DepStruct.t

    exception End_of_entries

    exception Comment_line

    let prohtags = ref [
        "punct"; "punc"; "pnct"; "pu";
        "Punct"; "Punc"; "Pnct"; "Pu";
        "PUNCT"; "PUNC"; "PNCT"; "PU";
        "."; ","; ":"; "``"; "\'\'"; "\""; "$"; "("; ")"; "#"; "*";
        ":pound:"; "-LRB-"; "-RRB-"; "-NONE-"
    ]

    let is_proh entry =
        List.mem entry.cpostag !prohtags
            (* || List.mem entry.form ["_"] *)

    let mwutags = ref [
        "MWU"
    ]

    let add_prohtag (tag : string) : unit =
        if not (List.mem tag !prohtags) then
            prohtags := tag :: !prohtags
        else ()

    let add_mwutag (tag : string) : unit =
        if not (List.mem tag !mwutags) then
            mwutags := tag :: !mwutags
        else ()
    
    let create_entry 
            id form lemma cpostag postag feats 
            head deprel phead pdeprel =
    {
        id      = id;
        form    = form;
        lemma   = lemma;
        cpostag = cpostag;
        postag  = postag;
        feats   = feats;
        head    = head;
        deprel  = deprel;
        phead   = phead;
        pdeprel = pdeprel;
    }

    let conv_id_option id =
        match id with
            "_" -> None
          | _ -> Some (int_of_string id - 1)

    let conv_deprel_option deprel =
        match deprel with
            "_" -> None
          | _ -> Some deprel

    let clean_postag tag =
        try
            let idx = String.index tag '[' in
            String.sub tag 0 idx
        with Not_found -> tag

    let read_entry line =
        let tokens = split line in
        if List.length tokens = 0 || String.length line = 0 then
            raise End_of_entries
        else if List.nth tokens 0 = "#" then
            raise Comment_line
        else if List.length tokens = 10 then
            try
                let id = int_of_string (List.nth tokens 0) - 1
                and form = List.nth tokens 1
                and lemma = List.nth tokens 2
                and cpostag = List.nth tokens 3
                and postag = clean_postag (List.nth tokens 4)
                and feats = split_delim "|" (List.nth tokens 5)
                and head = int_of_string (List.nth tokens 6) - 1
                and deprel = List.nth tokens 7
                and phead = conv_id_option (List.nth tokens 8)
                and pdeprel = conv_deprel_option (List.nth tokens 9) in
                let postag =
                    if List.mem cpostag !mwutags then "_"
                    else postag
                and feats =
                    if List.mem cpostag !mwutags then ["_"]
                    else feats
                in
                create_entry
                    id form lemma cpostag postag feats 
                    head deprel phead pdeprel
            with (Failure _) ->
                failwith (sprintf "Unrecognizable entry 1: %s" line)
        else
            failwith (sprintf "Unrecognizable entry 2: %s" line)

    let sort_words entries =
        entries := List.sort compare_entry !entries

    let rec find_parents' (entries : entry_t list) (tbl : int list list) : int list list =
        let wordidx = ref (-1) in
        let tbl' =
            (List.map
                (fun heads ->
                    wordidx := !wordidx + 1;
                    match heads with
                        [] -> 
                            let entry' = List.nth entries !wordidx in
                            [entry'.head]
                      | head :: _ ->
                            if head < 0 then heads
                            else
                                let entry' = List.nth entries head in
                                if not (List.mem entry'.head heads) then
                                    entry'.head :: heads
                                else (-1) :: heads
                )
                tbl
            )
        in
        if tbl = tbl' then tbl'
        else find_parents' entries tbl'

    let find_parents (entries : entry_t list) : int list list =
        let tbl =
            (List.map 
                (fun entry -> [entry.head]) 
                entries)
        in
        let tbl' = find_parents' entries tbl in
        List.map
            (fun entry -> List.rev entry)
            tbl'

    let map_oldidx_newidx (entries : entry_t list) (delpos : int list) : int list =
        let oldidx = ref (-1)
        and newidx = ref (-1) in
        List.map
            (fun _ ->
                oldidx := !oldidx + 1;
                if not (List.mem !oldidx delpos) then
                    newidx := !newidx + 1
                else ();
                !newidx
            )
            entries

    let resolve_parent (parents : int list list) (delpos : int list) (headpos : int) : int =
        let len = List.length parents in
        if headpos < 0 then
            headpos
        else if headpos > len then
            failwith "resolve_parent"
        else
            let entry = List.nth parents headpos in
            let rec find_head entry' =
                match entry' with
                    [] -> (-1)
                  | h :: rest ->
                        if not (List.mem h delpos) then h
                        else find_head rest
            in
            find_head entry

    let map_idx (idxmap : int list) (idx : int) : int =
        let len = List.length idxmap in
        if idx < 0 then idx
        else if idx > len then failwith "map_idx"
        else List.nth idxmap idx

    let rec del_prohentries 
        (entries : entry_t list) 
        (delpos : int list) (idxmap : int list) 
        (parents : int list list)
    : entry_t list =
        match entries with
            [] -> []
          | entry :: entries' ->
                if List.mem entry.id delpos then begin
                    (* Printf.printf ">>> Index = %d is deleted.\n" entry.id; *)
                    del_prohentries entries' delpos idxmap parents
                end else begin
                    let parentidx = resolve_parent parents delpos entry.id in
                    let head' = map_idx idxmap parentidx
                    and phead' = 
                        match entry.phead with
                            None -> None
                          | Some p -> Some (map_idx idxmap (resolve_parent parents delpos p))
                    in
                    (*
                    Printf.printf ">>> Index = %d that has the head = %d the parentidx = %d now has the new head = %d.\n" entry.id entry.head parentidx head';
                    *)
                    let entry' = {
                        id = List.nth idxmap entry.id;
                        form = entry.form;
                        lemma = entry.lemma;
                        cpostag = entry.cpostag;
                        postag = entry.postag;
                        feats = entry.feats;
                        head = head';
                        deprel = entry.deprel;
                        phead = phead';
                        pdeprel = entry.pdeprel;
                    } in
                    entry' :: (del_prohentries entries' delpos idxmap parents)
                end

    let elim_punc (entries : entry_t list) (delpos : int list) : entry_t list =
        let parents = find_parents entries in
        (*
        let idx = ref (-1) in
        Printf.printf "Delpos = [%s]\n"
            (String.concat ", " (List.map string_of_int delpos));
        List.iter 
            (fun entry ->
                incr idx;
                Printf.printf "Idx = %d: [%s]\n" !idx
                    (String.concat ", " 
                        (List.map string_of_int entry))
            ) 
            parents;
        *)
        let idxmap = map_oldidx_newidx entries delpos in
        del_prohentries entries delpos idxmap parents

    let elim_punc_sent (sent : string list) (delpos : int list) : string list =
        let rec elim' sent delpos idx =
            match sent with
                [] -> []
              | w :: sent' ->
                    if List.mem idx delpos then
                        elim' sent' delpos (idx + 1)
                    else
                        w :: elim' sent' delpos (idx + 1)
            in
            elim' sent delpos 0

    let make_one_root (entries : entry_t list) : entry_t list =
        
        let rec find_root entries' rootidx deprel =
            match entries' with
                [] -> rootidx
              | entry :: rest ->
                    if entry.head = (-1) && deprel != "ROOT" then
                        find_root rest entry.id entry.deprel
                    else
                        find_root rest rootidx deprel
        in

        let rec change_root entries' rootidx =
            match entries' with
                [] -> []
              | entry :: rest ->
                    if entry.head = (-1) && entry.id != rootidx then 
                        { entry with head = rootidx } 
                            :: (change_root rest rootidx)
                    else
                        entry :: (change_root rest rootidx)
        in

        let rootidx = find_root entries (-1) "-" in
        change_root entries rootidx

    let rec elim_longsents_inner is_elim sentlen content : content_t =
        match content with
            [] -> []
          | depstr :: rest ->
            begin
                Scale.inc ();
                Scale.display ();
                let (entries, sent, delpos, rootpos) = depstr in
                let e' = ref entries
                and s' = ref sent in
                if is_elim then begin
                    e' := elim_punc !e' delpos;
                    e' := make_one_root !e';
                    s' := elim_punc_sent !s' delpos
                end else ();
                let delposno = 
                    if is_elim then List.length delpos 
                    else 0 in
                let len = List.length entries - delposno in
                if sentlen <= 0 || len <= sentlen then
                    let depstr' = (!e', !s', delpos, rootpos) in
                    depstr' :: elim_longsents_inner is_elim sentlen rest
                else
                    elim_longsents_inner is_elim sentlen rest
            end

    let elim_longsents is_elim sentlen content =
        Scale.add (Scale.WindMill 3000) "Eliminating long sentences ... " "";
        let result = 
            match sentlen with
                None ->
                    elim_longsents_inner is_elim 0 content
              | Some sentlen' -> 
                    elim_longsents_inner is_elim sentlen' content
        in
        Scale.discard ();
        result

    let compile fname : content_t =
        Scale.add (Scale.WindMill 3000) "Loading content ... " "";
        let ifhdl = open_in fname in
        let content = ref [] in
        let entries = ref []
        and sent = ref []
        and delpos = ref []
        and rootpos = ref (-1) in begin
            try
                while true do
                    try begin
                        let line = input_line ifhdl in
                        let entry = read_entry line in
                        begin
                            Scale.inc ();
                            Scale.display ();
                            entries := !entries @ [entry];
                            if is_proh entry then
                                delpos := !delpos @ [entry.id]
                            else ();
                            if entry.head = -1 then
                                rootpos := entry.id
                            else ()
                        end
                    end with 
                        End_of_entries -> begin
                            sort_words entries;
                            List.iter
                                (fun entry ->
                                    sent := !sent @ [entry.postag])
                                !entries;
                            if List.length !entries > 0 then begin
                                content := (!entries, !sent, !delpos, !rootpos) :: !content;
                                entries := [];
                                sent := [];
                                delpos := [];
                                rootpos := 0
                            end else ()
                        end
                      | Comment_line -> ()
                done
            with End_of_file -> begin
                if List.length !entries > 0 then
                    content := (!entries, !sent, !delpos, !rootpos) :: !content
                else ();
                close_in ifhdl
            end
        end;
        Scale.discard ();
        let result = List.rev !content in
        result

    let compile_many fnames =
        let result = ref [] in
        List.iter
            (fun fname ->
                let entries = compile fname in
                result := !result @ entries)
            fnames;
        !result

    let repl_id id idmap =
        List.assoc id idmap

    let repl_id_option id idmap =
        match id with
            None -> None
          | Some id' -> Some (repl_id id' idmap)

    let conv_entries_to_depstr entries sent delpos rootpos : depstr_t =
        let depstr = DepStruct.create (List.length entries) sent rootpos in
        List.iter
            (fun entry ->
                if entry.head >= 0 then
                    let depdir =
                        if entry.head < entry.id then Syncat.Left
                        else Syncat.Right
                    in
                    DepStruct.add depstr entry.head entry.id depdir
                else ())
            entries;
        depstr

    let rec conv_to_depstrs content =
        match content with
            [] -> []
          | (entries, _, delpos, rootpos) :: rest ->
                let sent = List.map entry_to_word entries in
                conv_entries_to_depstr entries sent delpos rootpos
                :: conv_to_depstrs rest

    let read_postags_deprels content =
        Scale.add (Scale.WindMill 3000) "Compiling the description ... " "";
        let postags = ref []
        and tags = ref []
        and attrs = ref []
        and deprels = ref [] in
        List.iter
            (fun (entries, _, _, _) ->
                List.iter
                    (fun entry ->
                        Scale.inc ();
                        Scale.display ();
                        let postag = entry_to_postag entry
                        and tag = entry_to_tag entry in
                        if not (List.mem postag !postags) then
                            postags := postag :: !postags
                        else ();
                        if not (List.mem entry.deprel !deprels) then
                            deprels := entry.deprel :: !deprels
                        else ();
                        List.iter
                           (fun attr ->
                               if attr <> "_" && not (List.mem attr !attrs) then
                                   attrs := attr :: !attrs
                               else ())
                           entry.feats;
                        if not (List.mem tag !tags) then
                            tags := tag :: !tags
                        else ())
                    entries)
            content;
        Scale.discard ();
        Scale.print "Compiling the description ... done\n";
        (!postags, !tags, !attrs, !deprels)

    let from_file fname =
        let content = compile fname in
        (* let sentid = ref 0 in *)
        let result = List.map
            (fun (entries, sent, delpos, rootpos) ->
                (* incr sentid;
                printf "Sent ID = %d\n" !sentid;
                printf "Sentence = %s\n" (String.concat " " sent); *)
                let depstr = conv_entries_to_depstr entries sent delpos rootpos in
                (* printf "%s" (DepStruct.to_string depstr); *)
                depstr)
            content in
        result

    let from_files fnames =
        let result = ref [] in
        List.iter
            (fun fname ->
                let depstrs = from_file fname in
                result := !result @ depstrs)
            fnames;
        !result

end

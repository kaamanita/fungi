type inoutmode_t = StandardInOut | VBInOut

type ann_t = NoAnn | DetAnn | SkewedDetAnn | StructAnn

type annmode_t =
    NoAnnealing
  | DetAnnealing of float * float
  | SkewedDetAnnealing of float * float
  | StructAnnealing of float * float * float

module AnnealRunner :
  sig
    type syncat_t = Cg.Syncat.t
    val score_prior :
      Deriv.Deriv.t -> Dict.Dict.t -> Cg.Syncat.t -> float -> float -> float -> float -> float
    val init_ruleprior :
      int ->
      float ->
      Dict.Dict.t ->
      Cg.Syncat.t ->
      float ->
      float -> float -> Deriv.DerivGenModel.t -> float -> Deriv.DerivGenModel.t
    val init_probs :
      Chartdb.ChartDbWrapper.t ->
      int ->
      float ->
      Models.probflag_t list ->
      Dict.Dict.t ->
      Cg.Syncat.t ->
      float ->
      float ->
      float ->
      Deriv.DerivGenModel.t * Deriv.DerivGenModel.t * Deriv.DerivGenModel.t
    val run_anneal :
      inoutmode_t ->
      bool ->
      annmode_t ->
      Anneal.SkewedDetParam.genmodel_t ->
      int ->
      float ->
      int ->
      float ->
      Anneal.EmAnnealer.genmodel_t ->
      Anneal.EmAnnealer.genmodel_t ->
      Anneal.EmAnnealer.syncat_t -> 
      Anneal.EmAnnealer.fn_get_chart_t -> int -> unit
    val train :
      (int ->
       float ->
       int ->
       float ->
       Anneal.EmAnnealer.genmodel_t ->
       Anneal.EmAnnealer.genmodel_t ->
       Anneal.EmAnnealer.syncat_t ->
       Anneal.EmAnnealer.fn_get_chart_t -> int -> unit) ->
       int ->
       float ->
       int ->
       float -> Anneal.EmAnnealer.genmodel_t -> Anneal.EmAnnealer.genmodel_t -> Anneal.EmAnnealer.syncat_t -> Chartdb.ChartDbWrapper.t -> unit
    val save_prob : Deriv.DerivGenModel.t -> string -> unit
    val compute :
      Models.probflag_t list ->
      inoutmode_t ->
      bool ->
      annmode_t ->
      float ->
      int ->
      float ->
      Anneal.EmAnnealer.syncat_t ->
      Dict.Dict.t -> float -> float -> float -> int -> string list -> string -> unit
  end

val set_probflags : Models.probflag_t list ref -> string -> unit
val set_inoutmode : inoutmode_t ref -> string -> unit
val set_annmode : ann_t ref -> string -> unit
val set_start : Cg.Syncat.t ref -> string -> unit
val conv_annparam : ann_t -> float -> float -> float -> annmode_t

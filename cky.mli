(** CKY Parsing Algorithm *)

(** CKY Parsing Algorithm Module *)
module CKY :
sig
    type syncat_t = Cg.Syncat.t

    (** Span *)
    type span_t = Chart.Chart.span_t
    
    (** Chart *)
    type chart_t = Chart.Chart.t
    
    (** Lexical item *)
    type word_t = Dict.Dict.word_t
    
    (** Dictionary *)
    type dict_t = Dict.Dict.t
    
    (** CKY parser *)
    type t
    
    (** Create a parser with a sentence and a dictionary. *)
    val create : word_t list -> word_t list -> dict_t -> syncat_t -> t

    val change_transf : t -> Cg.CG.transf_t -> unit

    val to_string : t -> string
    
    (** Initialize the chart with the sentence. *)
    val init_chart : t -> unit
    
    (** Apply functional application to two consecutive edges. *)
    val combine_edges : t -> span_t -> unit
    
    (** Apply the coordination rule to three consecutive edges. *)
    val conjoin_edges : t -> span_t -> unit
    
    (** Build the chart from the initialized one with functional 
        application and the coordination rule. *)
    val build_chart : t -> unit
    
    (** Parse a sentence with a dictionary. *)
    val parse : ?transf:Cg.CG.transf_t -> word_t list -> word_t list -> dict_t -> syncat_t -> chart_t
end

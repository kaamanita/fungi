open Cg
open Parserrunner
open Printf

let main =
    let dict = ref None
    and start = ref (Syncat.create_atom "ROOT")
    and output = ref None 
    and fnames = ref [] in
    let args = [
        ("--quiet", Arg.Unit Scale.go_quiet, "  Quiet mode (default = false)");
        ("--seedlvl",
            Arg.Int Utils.set_seedlvl,
            "  Set the seed level (default = 100)");
        ("--dict", 
            Arg.String (fun d -> dict := Some d), 
            "[somedict.txt]   Specify the dictionary for the parser (required)");
        ("--output", 
            Arg.String (fun o -> output := Some o), 
            "[output.chartdb]   Specify the output chart database (required)")
    ]
    and fn_anon = (fun f -> fnames := !fnames @ [f]) 
    and usagemsg = 
        "\nParse the text files line by line with the CKY Algorithm and the specified dictionary.\n\n" 
        ^ (sprintf "Usage: %s [options] text1 text2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    match !dict, !output with
        Some dictfname, Some cdbfname ->
        begin
            Scale.print (sprintf "[1mParsing the input [%s] with the grammar %s.[0m\n"
                (String.concat ", " !fnames) dictfname);
            let prs = ParserRunner.create dictfname cdbfname !fnames !start in
            ParserRunner.run prs
        end
      | _, _ -> Arg.usage args usagemsg

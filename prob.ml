open Utils
open Printf

module type INSTANCE_TYPE = sig
    type t
    val equal : t -> t -> bool
    val hash : t -> int
    val to_string : t -> string
end

module type COND_INSTANCE_TYPE = sig
    include INSTANCE_TYPE
    type prems_t
    type consq_t
    val create : prems_t -> consq_t -> t
    val get_prems : t -> prems_t
    val get_consq : t -> consq_t
    val equal_prems : prems_t -> prems_t -> bool
    val hash_prems : prems_t -> int
    val prems_to_string : prems_t -> string
end

module type BACKOFF_INSTANCE_TYPE = sig
    include COND_INSTANCE_TYPE
    type data_t
    val backoff : data_t -> prems_t * consq_t
end

module ProbTbl (Inst : INSTANCE_TYPE) = struct

    type inst_t = Inst.t

    module InstTbl = Hashtbl.Make(Inst)

    type t = {
        insttbl : float InstTbl.t;      (** Instance table *)
        epsilon : float;                (** Default value *)
    }

    let create n epsilon = {
        insttbl = InstTbl.create n;
        epsilon = epsilon;
    }

    let get_epsilon probtbl =
        probtbl.epsilon

    let mem probtbl inst =
        InstTbl.mem probtbl.insttbl inst

    let length probtbl =
        let cnt = ref 0 in
        InstTbl.iter 
            (fun _ _ -> incr cnt)
            probtbl.insttbl;
        !cnt

    let get_prob probtbl inst =
        if mem probtbl inst then
            InstTbl.find probtbl.insttbl inst
        else probtbl.epsilon

    let get_smooth_prob probtbl inst =
        let prob = get_prob probtbl inst in
        (prob +. probtbl.epsilon) /. (1.0 +. probtbl.epsilon)

    let set_prob probtbl inst prob =
        InstTbl.replace probtbl.insttbl inst prob

    let acc_prob probtbl inst prob =
        let oldprob = 
            if mem probtbl inst then get_prob probtbl inst
            else 0.0 in
        let newprob = oldprob +. prob in
        set_prob probtbl inst newprob

    let iter probtbl fn =
        InstTbl.iter fn probtbl.insttbl

    let to_string probtbl =
        let result = ref [] in
        InstTbl.iter
            (fun inst prob ->
                let s = sprintf "Prob(%s) = %.2e" 
                    (Inst.to_string inst) prob in
                result := !result @ [s])
            probtbl.insttbl;
        sprintf "%s\nEpsilon = %.2e" 
            (String.concat "\n" !result) probtbl.epsilon

end

module CondProbTbl (Inst : COND_INSTANCE_TYPE) = struct

    type prems_t = Inst.prems_t
    type consq_t = Inst.consq_t
    type inst_t = Inst.t

    module HashedPrems = struct
        type t = prems_t
        let equal = Inst.equal_prems
        let hash = Inst.hash_prems
    end

    module HashedInst = struct
        type t = Inst.t
        let equal = Inst.equal
        let hash = Inst.hash
        let to_string = Inst.to_string
    end

    module PremsTbl = Hashtbl.Make(HashedPrems)

    module ProbTbl = ProbTbl(HashedInst)

    type premstbl_t = float PremsTbl.t

    type probtbl_t = ProbTbl.t

    type t = {
        probtbl  : probtbl_t;       (** Instance table *)
        premstbl : premstbl_t;      (** Premise table *)
        epsilon  : float;           (** Default value *)
    }

    let create n epsilon = {
        probtbl  = ProbTbl.create n epsilon;
        premstbl = PremsTbl.create n;
        epsilon  = epsilon;
    }

    let get_epsilon cprobtbl =
        cprobtbl.epsilon

    let mem cprobtbl inst =
        ProbTbl.mem cprobtbl.probtbl inst

    let length cprobtbl =
        ProbTbl.length cprobtbl.probtbl

    let mem_prems cprobtbl prems =
        PremsTbl.mem cprobtbl.premstbl prems

    let get_prems_prob cprobtbl prems =
        if mem_prems cprobtbl prems then
            PremsTbl.find cprobtbl.premstbl prems
        else cprobtbl.epsilon

    let get_prob cprobtbl inst =
        let prems = Inst.get_prems inst in
        if mem_prems cprobtbl prems then
            if ProbTbl.mem cprobtbl.probtbl inst then
                let prems_prob = get_prems_prob cprobtbl prems
                and inst_prob = ProbTbl.get_prob cprobtbl.probtbl inst in
                if prems_prob <> 0.0 && inst_prob <> 0.0 then
                    inst_prob /. prems_prob
                else cprobtbl.epsilon
            else cprobtbl.epsilon
        else cprobtbl.epsilon

    let get_smooth_prob cprobtbl inst =
        let prob = get_prob cprobtbl inst in
        (prob +. cprobtbl.epsilon) /. (1.0 +. cprobtbl.epsilon)

    let set_prob cprobtbl inst prob =
        ProbTbl.set_prob cprobtbl.probtbl inst prob

    let set_prems_prob cprobtbl prems prob =
        PremsTbl.replace cprobtbl.premstbl prems prob

    let acc_prob cprobtbl inst prob =
        let prems = Inst.get_prems inst in
        ProbTbl.acc_prob cprobtbl.probtbl inst prob;
        let prems_prob =
            if mem_prems cprobtbl prems then
                get_prems_prob cprobtbl prems
            else 0.0 in
        let new_prems_prob = prems_prob +. prob in
        set_prems_prob cprobtbl prems new_prems_prob

    let iter cprobtbl fn =
        ProbTbl.iter cprobtbl.probtbl fn

    let iter_prems cprobtbl fn =
        PremsTbl.iter fn cprobtbl.premstbl

    let to_string_premstbl premstbl =
        let result = ref [] in
        PremsTbl.iter
            (fun prems prob ->
                let s = sprintf "Premise_prob(%s) = %.2e" 
                    (Inst.prems_to_string prems) prob in
                result := !result @ [s])
            premstbl;
        String.concat "\n" !result

    let to_string cprobtbl =
        sprintf "%s\n%s\nEpsilon = %.2e" 
            (ProbTbl.to_string cprobtbl.probtbl) 
            (to_string_premstbl cprobtbl.premstbl)
            cprobtbl.epsilon

end

module GenModel (Inst : COND_INSTANCE_TYPE) = struct

    type inst_t = Inst.t

    module ProbTbl = ProbTbl(Inst)

    module CondProbTbl = CondProbTbl(Inst)

    type probtbl_t = ProbTbl.t

    type cprobtbl_t = CondProbTbl.t

    type fn_acc_prob_t = inst_t -> float -> unit
    
    type fn_get_prob_t = inst_t -> float

    type fn_set_prob_t = inst_t -> float -> unit

    type probftr_t = {
        get_prob        : fn_get_prob_t;
            (** How to calculate the probability of an instance *)
        get_smooth_prob : fn_get_prob_t;
            (** How to calculate the smoothened probability of 
                an instance *)
        set_prob        : fn_set_prob_t;
            (** How to set the probability of an instance *)
        acc_prob        : fn_acc_prob_t;
            (** How to accumulate the probability of an instance *)
    }

    let create_probftr 
            get_prob get_smooth_prob set_prob acc_prob = {
        get_prob        = get_prob;
        get_smooth_prob = get_smooth_prob;
        set_prob        = set_prob;
        acc_prob        = acc_prob;
    }

    type t = {
        probtbl  : probtbl_t;
        probftrs : probftr_t list;
        n        : int;
        epsilon  : float;
    }

    let create n epsilon probftrs = {
        probtbl  = ProbTbl.create n epsilon;
        probftrs = probftrs;
        n        = n;
        epsilon  = epsilon;
    }

    let get_epsilon genmodel = genmodel.epsilon

    let length genmodel =
        ProbTbl.length genmodel.probtbl

    let mem genmodel inst =
        ProbTbl.mem genmodel.probtbl inst

    let get_ftrprob genmodel inst =
        List.fold_left
            (fun prob probftr ->
                let prob' = probftr.get_prob inst in
                prob *. prob')
            1.0 genmodel.probftrs

    let get_smooth_ftrprob genmodel inst =
        List.fold_left
            (fun prob probftr ->
                let prob' = probftr.get_smooth_prob inst in
                prob *. prob')
            1.0 genmodel.probftrs

    let get_prob_noftr genmodel inst =
        ProbTbl.get_prob genmodel.probtbl inst

    let get_prob genmodel inst =
        let fundprob = get_prob_noftr genmodel inst in
        fundprob *. get_ftrprob genmodel inst

    let get_smooth_prob_noftr genmodel inst =
        ProbTbl.get_smooth_prob genmodel.probtbl inst

    let get_smooth_prob genmodel inst =
        let fundprob = get_smooth_prob_noftr genmodel inst in
        fundprob *. get_smooth_ftrprob genmodel inst

    let set_prob genmodel inst prob =
        ProbTbl.set_prob genmodel.probtbl inst prob;
        List.iter
            (fun probftr ->
                probftr.set_prob inst prob)
            genmodel.probftrs

    let acc_prob genmodel inst prob =
        ProbTbl.acc_prob genmodel.probtbl inst prob;
        List.iter
            (fun probftr ->
                probftr.acc_prob inst prob)
            genmodel.probftrs

    let iter genmodel fn =
        ProbTbl.iter genmodel.probtbl fn

    let to_string genmodel =
        let result = ref [] in
        ProbTbl.iter genmodel.probtbl
            (fun inst prob ->
                let line = sprintf "Prob(%s) = %.2e (smoothed: %.2e)"
                    (Inst.to_string inst)
                    (get_prob genmodel inst)
                    (get_smooth_prob genmodel inst) in
                result := !result @ [line]);
        String.concat "\n" !result

    let print genmodel =
        ProbTbl.iter genmodel.probtbl
            (fun inst prob ->
                let p = get_prob genmodel inst in
                if p > 0.0 then
                    printf "Prob(%s) = %.2e (smoothed: %.2e)\n"
                        (Inst.to_string inst)
                        (get_prob genmodel inst)
                        (get_smooth_prob genmodel inst)
                else ())

    let from_file fname probftrs =
        let probtbl = unmarshal_from_file fname in
        let epsilon = ProbTbl.get_epsilon probtbl in
        let result = create 100 epsilon probftrs in
        ProbTbl.iter probtbl
            (fun inst prob ->
                acc_prob result inst prob);
        result

    let to_file (genmodel : t) fname =
        let probtbl = ProbTbl.create genmodel.n genmodel.epsilon in
        iter genmodel
            (fun inst prob ->
                ProbTbl.set_prob probtbl inst prob);
        marshal_to_file fname probtbl

    let dkl genmodel1 genmodel2 =
        let result = ref 0.0 
        and divp = ref 0.0 
        and divq = ref 0.0 in
        iter
            genmodel1
            (fun inst p -> divp := !divp +. p);
        iter
            genmodel2
            (fun inst q -> divq := !divq +. q);
        if !divp = 0.0 || !divq = 0.0 then infinity
        else begin
            iter
                genmodel1
                (fun inst p ->
                    let q = get_prob genmodel2 inst in
                    let p' = p /. !divp
                    and q' = q /. !divq in
                    if (p' > 0.0) && (q' > 0.0) then begin
                        let r = q' *. (log q' -. log p') in
                        result := !result +. r
                    end else ());
            !result
        end

end

module GenModelWithBackoff 
    (Inst : COND_INSTANCE_TYPE) 
    (BackoffInst : BACKOFF_INSTANCE_TYPE with type data_t = Inst.t and type prems_t = Inst.t) 
= struct

    type inst_t = Inst.t
    
    module ProbTbl = ProbTbl(Inst)

    type probtbl_t = ProbTbl.t

    module CondProbTbl = CondProbTbl(BackoffInst)
    
    type cprobtbl_t = CondProbTbl.t

    module PureGenModel = GenModel(Inst)

    type genmodel_t = PureGenModel.t

    module BackoffGenModel = GenModel(BackoffInst)

    type bo_genmodel_t = BackoffGenModel.t

    type fn_acc_prob_t = inst_t -> float -> unit
    
    type fn_get_prob_t = inst_t -> float

    type fn_set_prob_t = inst_t -> float -> unit

    type probftr_t = PureGenModel.probftr_t
    
    let create_probftr = PureGenModel.create_probftr

    type t = {
        genmodel    : genmodel_t;
        backoff     : bo_genmodel_t;
        cprobtbl_pg : cprobtbl_t;
        cprobtbl_bo : cprobtbl_t;
    }

    let create n epsilon probftrs = {
        genmodel    = PureGenModel.create n epsilon probftrs;
        backoff     = BackoffGenModel.create n epsilon [];
        cprobtbl_pg = CondProbTbl.create n epsilon;
        cprobtbl_bo = CondProbTbl.create n epsilon;
    }

    let get_epsilon model =
        PureGenModel.get_epsilon model.genmodel

    let length model =
        PureGenModel.length model.genmodel

    let get_prob_noftr model inst =
        let (inst', taken) = BackoffInst.backoff inst in
        let pg = BackoffInst.create inst taken in
        let bo = BackoffInst.create inst' taken in
        let pgcondprob = CondProbTbl.get_prob model.cprobtbl_pg pg in
        let takenprob = CondProbTbl.get_prob model.cprobtbl_bo bo in
        let pgprob = PureGenModel.get_prob model.genmodel inst in
        let boprob = BackoffGenModel.get_prob model.backoff bo in
        pgcondprob *. pgprob +. takenprob *. boprob

    let get_prob model inst =
        let ftrprob = PureGenModel.get_ftrprob model.genmodel inst in
        let fundprob = get_prob_noftr model inst in
        ftrprob *. fundprob

    let get_smooth_prob_noftr model inst =
        let (inst', taken) = BackoffInst.backoff inst in
        let pg = BackoffInst.create inst taken in
        let bo = BackoffInst.create inst' taken in
        let pgcondprob = CondProbTbl.get_prob model.cprobtbl_pg pg in
        let takenprob = CondProbTbl.get_smooth_prob model.cprobtbl_bo bo in
        let pgprob = PureGenModel.get_smooth_prob model.genmodel inst in
        let boprob = BackoffGenModel.get_smooth_prob model.backoff bo in
        pgcondprob *. pgprob +. takenprob *. boprob

    let get_smooth_prob model inst =
        let ftrprob = PureGenModel.get_smooth_ftrprob model.genmodel inst in
        let fundprob = get_smooth_prob_noftr model inst in
        ftrprob *. fundprob

    let set_prob model inst prob =
        let (inst', taken) = BackoffInst.backoff inst in
        let pg = BackoffInst.create inst taken in
        let bo = BackoffInst.create inst' taken in
        PureGenModel.set_prob model.genmodel inst prob;
        BackoffGenModel.set_prob model.backoff bo prob;
        CondProbTbl.set_prob model.cprobtbl_pg pg prob;
        CondProbTbl.set_prob model.cprobtbl_bo bo prob

    let acc_prob model inst prob =
        let (inst', taken) = BackoffInst.backoff inst in
        let pg = BackoffInst.create inst taken in
        let bo = BackoffInst.create inst' taken in
        PureGenModel.acc_prob model.genmodel inst prob;
        BackoffGenModel.acc_prob model.backoff bo prob;
        CondProbTbl.set_prob model.cprobtbl_pg pg prob;
        CondProbTbl.acc_prob model.cprobtbl_bo bo prob

    let iter model fn =
        PureGenModel.iter model.genmodel fn

    let to_string model =
        let result = ref [] in
        PureGenModel.iter model.genmodel
            (fun inst prob ->
                let line = sprintf "Prob(%s) = %.2e (smoothed: %.2e)"
                    (Inst.to_string inst)
                    (get_prob model inst)
                    (get_smooth_prob model inst) in
                result := !result @ [line]);
        String.concat "\n" !result

    let print model =
        PureGenModel.iter model.genmodel
            (fun inst prob ->
                printf "Prob(%s) = %.2e (smoothed: %.2e)\n"
                    (Inst.to_string inst)
                    (get_prob model inst)
                    (get_smooth_prob model inst))

    let from_file fname probftrs =
        let probtbl = unmarshal_from_file fname in
        let epsilon = ProbTbl.get_epsilon probtbl in
        let result = create 100 epsilon probftrs in
        ProbTbl.iter probtbl
            (fun inst prob ->
                acc_prob result inst prob);
        result

    let to_file (model : t) fname =
        let n = 100
        and epsilon = PureGenModel.get_epsilon model.genmodel in
        let probtbl = ProbTbl.create n epsilon in
        iter model
            (fun inst prob ->
                ProbTbl.set_prob probtbl inst prob);
        marshal_to_file fname probtbl

end

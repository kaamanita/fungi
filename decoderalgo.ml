open Cg
open Chart
open Prob
open Deriv
open Tree
open Inout
open Utils

module Inst = struct

    type span_t = int * int
    
    type syncat_t = Syncat.t
    
    type word_t = string
    
    type t = span_t * syncat_t * word_t

    let create span syncat head = (span, syncat, head)

    let get_span inst =
        let (span, _, _) = inst in
        span

    let get_syncat inst =
        let (_, syncat, _) = inst in
        syncat

    let get_head inst =
        let (_, _, head) = inst in
        head
    
    let hash inst = 
        let (span, cat, head) = inst in
        Hashtbl.hash span + Syncat.hash cat + Hashtbl.hash head
    
    let equal inst1 inst2 =
        let (span1, cat1, head1) = inst1
        and (span2, cat2, head2) = inst2 in
        span1 = span2 && Syncat.equal cat1 cat2 && head1 = head2

    let to_string inst =
        let ((begpos, endpos), syncat, word) = inst in
        Printf.sprintf "(%d, %d, %s, %s)" begpos endpos (Syncat.to_string syncat) word

end

module InstTbl = Hashtbl.Make(Inst)

module Viterbi = struct

    type syncat_t = Syncat.t

    type word_t = string

    type genmodel_t = DerivGenModel.t

    type chart_t = Chart.t

    type tree_t = Tree.t

    type span_t = int * int

    type inst_t = Inst.t

    type insttbl_t = (tree_t * float) InstTbl.t

    type t = {
        insttbl         : insttbl_t;
        ruleprob        : genmodel_t;
        chart           : chart_t;
        start           : syncat_t;
        endpos          : int;
        catconst        : float;
        lbrconst        : float;
        rbrconst        : float;
        mutable maxroot : (inst_t * float) option;
    }

    let create ruleprob chart start catconst lbrconst rbrconst =
        (* Printf.printf "(viterbi.create           ) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let sentlen = List.length (Chart.get_sentence chart) in
        {   
            insttbl  = InstTbl.create (sentlen * sentlen);
            ruleprob = ruleprob;
            chart    = chart;
            start    = start;
            endpos   = sentlen - 1;
            catconst = catconst;
            lbrconst = lbrconst;
            rbrconst = rbrconst;
            maxroot  = None;
        }

    let mem_inst viterbi inst =
        InstTbl.mem viterbi.insttbl inst

    let get_inst viterbi inst =
        if mem_inst viterbi inst then
            InstTbl.find viterbi.insttbl inst
        else raise Not_found

    let get_tree viterbi inst =
        let (tree, _) = get_inst viterbi inst in
        tree

    let get_prob viterbi inst =
        let (_, prob) = get_inst viterbi inst in
        prob

    let set_inst viterbi inst tree prob =
        if not (mem_inst viterbi inst) 
                || get_prob viterbi inst < prob then
            InstTbl.replace viterbi.insttbl inst (tree, prob)
        else ()

    let set_maxroot viterbi inst prob =
        if Inst.get_span inst = (0, viterbi.endpos) 
                && Syncat.equal_ign_attrs 
                    (Inst.get_syncat inst) viterbi.start then
            match viterbi.maxroot with
                None ->
                    viterbi.maxroot <- Some (inst, prob)
              | Some (inst', prob') ->
                begin
                    if prob > prob' then
                        viterbi.maxroot <- Some (inst, prob)
                    else ()
                end
        else () 

    let build_lex_tree viterbi syncat span headpos word =
        Scale.inc ();
        Scale.display ();
        let inst = Inst.create span syncat word in
        let deriv = conv_word_to_deriv syncat word in
        let lexnode = Tree.create_term word span in
        let catscore = Stdlib.log (Syncat.score viterbi.catconst syncat) in
        let tree = Tree.create_nonterm 
            syncat word [lexnode] span headpos Syncat.Unary in
        let prob = 
            catscore +. Stdlib.log (DerivGenModel.get_smooth_prob viterbi.ruleprob deriv) in
        set_inst viterbi inst tree prob;
        set_maxroot viterbi inst prob

    let build_dtrs_tree viterbi syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let inst = Inst.create span syncat word in
        let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
        let dtrs = ref [] in
        let catscore = Stdlib.log (Syncat.score viterbi.catconst syncat) in
        (* Add preference of left/right branching *)
        let prob = 
            ref (catscore +. Stdlib.log (DerivGenModel.get_smooth_prob viterbi.ruleprob deriv)) in
        List.iter
            (fun (syncat', span', headpos', word') ->
                let inst' = Inst.create span' syncat' word' in
                let (tree', prob') = get_inst viterbi inst' in
                dtrs := !dtrs @ [tree'];
                prob := !prob +. prob')
            dtrargs;
        let tree = Tree.create_nonterm
            syncat word !dtrs span headpos depdir in
        let brascore = Tree.score_branching tree viterbi.lbrconst viterbi.rbrconst in
        set_inst viterbi inst tree (!prob +. brascore);
        set_maxroot viterbi inst !prob

    let compute_viterbi viterbi =
        Scale.add (Scale.WindMill 5000) " " "";
        Chart.iter_derivs viterbi.chart
            Chart.BottomUp
            (build_lex_tree viterbi) (build_dtrs_tree viterbi)
            (0, viterbi.endpos);
        Scale.discard ()

    let decapsulate_tree start tree =
        if Syncat.equal_ign_attrs (Tree.get_syncat tree) start then begin
            if Tree.is_nonterm tree then begin
                let dtrs = Tree.get_dtrs tree in
                let depdir = Tree.get_depdir tree in
                match depdir with
                    Syncat.Left | Syncat.RightSerial -> List.nth dtrs 0
                  | Syncat.Right | Syncat.LeftSerial -> List.nth dtrs 1
                  | Syncat.Coor -> List.nth dtrs 0
                  | Syncat.Unary -> List.nth dtrs 0
            end else tree
        end else tree

    let find_max viterbi =
        let (tree, prob) = match viterbi.maxroot with
            None -> raise Not_found
          | Some (inst, prob) -> begin
                (* Printf.printf "Inst = %s\n" (Inst.to_string inst); *)
                get_inst viterbi inst
            end in
        (* Printf.printf "tree      = %s\n" (Tree.to_string tree); *)
        let decaptree = decapsulate_tree viterbi.start tree in
        (* Printf.printf "decaptree = %s\n" (Tree.to_string decaptree); *)
        (decaptree, prob)

    let compute ruleprob chart start catconst lbrconst rbrconst =
        (* Printf.printf "(viterbi.compute          ) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let viterbi = create ruleprob chart start catconst lbrconst rbrconst in
        compute_viterbi viterbi;
        find_max viterbi

end

module MaxExpViterbi = struct

    type syncat_t = Syncat.t

    type word_t = string

    type genmodel_t = DerivGenModel.t

    type chart_t = Chart.t

    type tree_t = Tree.t

    type span_t = int * int

    type inst_t = Inst.t

    type insttbl_t = (tree_t * float) InstTbl.t

    type t = {
        insttbl         : insttbl_t;
        ruleprob        : genmodel_t;
        insctbl         : Inout.ScoreTbl.t;
        outsctbl        : Inout.ScoreTbl.t;
        chart           : chart_t;
        start           : syncat_t;
        endpos          : int;
        catconst        : float;
        lbrconst        : float;
        rbrconst        : float;
        mutable maxroot : (inst_t * float) option;
    }

    let create ruleprob chart start catconst lbrconst rbrconst =
        (* Printf.printf "(viterbi.create           ) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let sentlen = List.length (Chart.get_sentence chart) in
        let (insctbl, outsctbl) = 
            Inout.compute 100 1e-25 ruleprob chart start in
        {   
            insttbl  = InstTbl.create (sentlen * sentlen);
            ruleprob = ruleprob;
            insctbl  = insctbl;
            outsctbl = outsctbl;
            chart    = chart;
            start    = start;
            endpos   = sentlen - 1;
            catconst = catconst;
            lbrconst = lbrconst;
            rbrconst = rbrconst;
            maxroot  = None;
        }

    let mem_inst mev inst =
        InstTbl.mem mev.insttbl inst

    let get_inst mev inst =
        if mem_inst mev inst then
            InstTbl.find mev.insttbl inst
        else raise Not_found

    let get_tree mev inst =
        let (tree, _) = get_inst mev inst in
        tree

    let get_prob mev inst =
        let (_, prob) = get_inst mev inst in
        prob

    let set_inst mev inst tree prob =
        if not (mem_inst mev inst) 
                || get_prob mev inst < prob then
            InstTbl.replace mev.insttbl inst (tree, prob)
        else ()

    let set_maxroot mev inst prob =
        if Inst.get_span inst = (0, mev.endpos) 
                && Syncat.equal_ign_attrs 
                    (Inst.get_syncat inst) mev.start then
            match mev.maxroot with
                None ->
                    mev.maxroot <- Some (inst, prob)
              | Some (inst', prob') ->
                begin
                    if prob > prob' then
                        mev.maxroot <- Some (inst, prob)
                    else ()
                end
        else () 

    let build_lex_tree mev syncat span headpos word =
        Scale.inc ();
        Scale.display ();
        let inst = Inst.create span syncat word 
        and ioinst = Inout.ScoreInst.create span syncat word in
        let deriv = conv_word_to_deriv syncat word in
        let lexnode = Tree.create_term word span in
        let catscore = Stdlib.log (Syncat.score mev.catconst syncat) in
        let tree = Tree.create_nonterm 
            syncat word [lexnode] span headpos Syncat.Unary in
        let ioscore = Stdlib.log (Inout.ScoreTbl.get_smooth_prob mev.insctbl ioinst) 
            +. Stdlib.log (Inout.ScoreTbl.get_smooth_prob mev.outsctbl ioinst) in
        let prob = 
            catscore +. Stdlib.log (DerivGenModel.get_smooth_prob mev.ruleprob deriv) +. ioscore in
        set_inst mev inst tree prob;
        set_maxroot mev inst prob

    let build_dtrs_tree mev syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let inst = Inst.create span syncat word
        and ioinst = Inout.ScoreInst.create span syncat word in
        let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
        let dtrs = ref [] in
        let catscore = Stdlib.log (Syncat.score mev.catconst syncat) in
        let ioscore = Stdlib.log (Inout.ScoreTbl.get_smooth_prob mev.insctbl ioinst) 
            +. Stdlib.log (Inout.ScoreTbl.get_smooth_prob mev.outsctbl ioinst) in
        let prob = 
            ref (catscore +. Stdlib.log (DerivGenModel.get_smooth_prob mev.ruleprob deriv) +. ioscore) in
        List.iter
            (fun (syncat', span', headpos', word') ->
                let inst' = Inst.create span' syncat' word' in
                let (tree', prob') = get_inst mev inst' in
                dtrs := !dtrs @ [tree'];
                prob := !prob +. prob')
            dtrargs;
        let tree = Tree.create_nonterm
            syncat word !dtrs span headpos depdir in
        let brascore = Tree.score_branching tree mev.lbrconst mev.rbrconst in
        set_inst mev inst tree (!prob +. brascore);
        set_maxroot mev inst !prob

    let compute_viterbi mev =
        Scale.add (Scale.WindMill 5000) "Viterbi " "";
        Chart.iter_derivs mev.chart
            Chart.BottomUp
            (build_lex_tree mev) (build_dtrs_tree mev)
            (0, mev.endpos);
        Scale.discard ()

    let decapsulate_tree start tree =
        if Syncat.equal_ign_attrs (Tree.get_syncat tree) start then begin
            if Tree.is_nonterm tree then begin
                let dtrs = Tree.get_dtrs tree in
                let depdir = Tree.get_depdir tree in
                match depdir with
                    Syncat.Left | Syncat.RightSerial -> List.nth dtrs 0
                  | Syncat.Right | Syncat.LeftSerial -> List.nth dtrs 1
                  | Syncat.Coor -> List.nth dtrs 0
                  | Syncat.Unary -> List.nth dtrs 0
            end else tree
        end else tree

    let find_max mev =
        let (tree, prob) = match mev.maxroot with
            None -> raise Not_found
          | Some (inst, prob) -> begin
                (* Printf.printf "Inst = %s\n" (Inst.to_string inst); *)
                get_inst mev inst
            end in
        (* Printf.printf "tree      = %s\n" (Tree.to_string tree); *)
        let decaptree = decapsulate_tree mev.start tree in
        (* Printf.printf "decaptree = %s\n" (Tree.to_string decaptree); *)
        (decaptree, prob)

    let compute ruleprob chart start catconst lbrconst rbrconst =
        (* Printf.printf "(mev.compute          ) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let mev = create ruleprob chart start catconst lbrconst rbrconst in
        compute_viterbi mev;
        find_max mev

end

module HeadInst = struct

    type syncat_t = Syncat.t

    type t = int * int * syncat_t * string

    let create begpos endpos headcat headword =
        (begpos, endpos, headcat, headword)

    let hash = Hashtbl.hash

    let equal = Stdlib.(=)

end

module HeadCountTbl = Hashtbl.Make (HeadInst)

module SpanInst = struct

    type t = int * int

    let create begpos endpos = (begpos, endpos)

    let hash = Hashtbl.hash

    let equal = Stdlib.(=)

end

module SpanCountTbl = Hashtbl.Make (SpanInst)

module MinBayesRisk = struct

    type syncat_t = Syncat.t

    type word_t = string

    type genmodel_t = DerivGenModel.t

    type chart_t = Chart.t

    type tree_t = Tree.t

    type span_t = int * int

    type inst_t = Inst.t

    type insttbl_t = (tree_t * float) InstTbl.t

    type headinst_t = HeadInst.t

    type headcounttbl_t = float HeadCountTbl.t

    type spaninst_t = SpanInst.t

    type spancounttbl_t = float SpanCountTbl.t

    type t = {
        insttbl         : insttbl_t;
        headcounttbl    : headcounttbl_t;
        spancounttbl    : spancounttbl_t;
        ruleprob        : genmodel_t;
        chart           : chart_t;
        start           : syncat_t;
        endpos          : int;
        catconst        : float;
        lbrconst        : float;
        rbrconst        : float;
        mutable minroot : (inst_t * float) option;
    }

    let create ruleprob chart start catconst lbrconst rbrconst =
        (* Printf.printf "(mbr.create           ) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let sentlen = List.length (Chart.get_sentence chart) in
        {   
            insttbl      = InstTbl.create (sentlen * sentlen);
            headcounttbl = HeadCountTbl.create (sentlen * sentlen * 100);
            spancounttbl = SpanCountTbl.create (sentlen * sentlen * 10);
            ruleprob     = ruleprob;
            chart        = chart;
            start        = start;
            endpos       = sentlen - 1;
            catconst     = catconst;
            lbrconst     = lbrconst;
            rbrconst     = rbrconst;
            minroot      = None;
        }

    let mem_inst mbr inst =
        InstTbl.mem mbr.insttbl inst

    let get_inst mbr inst =
        if mem_inst mbr inst then
            InstTbl.find mbr.insttbl inst
        else raise Not_found

    let get_tree mbr inst =
        let (tree, _) = get_inst mbr inst in
        tree

    let get_prob mbr inst =
        let (_, prob) = get_inst mbr inst in
        prob

    let set_inst mbr inst tree prob =
        if not (mem_inst mbr inst) 
                || get_prob mbr inst < prob then
            InstTbl.replace mbr.insttbl inst (tree, prob)
        else ()

    let set_minroot mbr inst prob =
        if Inst.get_span inst = (0, mbr.endpos) 
                && Syncat.equal_ign_attrs 
                    (Inst.get_syncat inst) mbr.start then
            match mbr.minroot with
                None ->
                    mbr.minroot <- Some (inst, prob)
              | Some (inst', prob') ->
                begin
                    if prob < prob' then
                        mbr.minroot <- Some (inst, prob)
                    else ()
                end
        else () 

    let inc_headscore mbr begpos endpos syncat word value =
        let headinst = HeadInst.create begpos endpos syncat word in
        if not (HeadCountTbl.mem mbr.headcounttbl headinst) then
            HeadCountTbl.replace mbr.headcounttbl headinst value
        else
            let score = HeadCountTbl.find mbr.headcounttbl headinst in
            HeadCountTbl.replace mbr.headcounttbl headinst (value +. score)
    
    let inc_spanscore mbr begpos endpos value =
        let spaninst = SpanInst.create begpos endpos in
        if not (SpanCountTbl.mem mbr.spancounttbl spaninst) then
            SpanCountTbl.replace mbr.spancounttbl spaninst value
        else
            let score = SpanCountTbl.find mbr.spancounttbl spaninst in
            SpanCountTbl.replace mbr.spancounttbl spaninst (value +. score)

    let build_risktbl_lex mbr syncat span headpos word =
        Scale.inc ();
        Scale.display ();
        let (begpos, endpos) = span in
        inc_headscore mbr begpos endpos syncat word 1.0;
        inc_spanscore mbr begpos endpos 1.0

    let build_risktbl_dtrs mbr syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let (begpos, endpos) = span in
        List.iter
            (fun (syncat', span', headpos', word') ->
                let (begpos', endpos') = span' in
                let headinst' = HeadInst.create begpos' endpos' syncat' word' in
                let spaninst' = SpanInst.create begpos' endpos' in
                inc_headscore mbr begpos endpos syncat word
                    (HeadCountTbl.find mbr.headcounttbl headinst');
                inc_spanscore mbr begpos endpos
                    (SpanCountTbl.find mbr.spancounttbl spaninst'))
            dtrargs;
        inc_headscore mbr begpos endpos syncat word 1.0;
        inc_spanscore mbr begpos endpos 1.0

    let compute_risktbl mbr =
        Scale.add (Scale.WindMill 5000) " MBR " "";
        Chart.iter_derivs mbr.chart
            Chart.BottomUp
            (build_risktbl_lex mbr) (build_risktbl_dtrs mbr)
            (0, mbr.endpos);
        Scale.discard ()

    let riskscore mbr begpos endpos syncat word =
        let headinst = HeadInst.create begpos endpos syncat word
        and spaninst = SpanInst.create begpos endpos in
        let spanscore = SpanCountTbl.find mbr.spancounttbl spaninst
        and headscore = HeadCountTbl.find mbr.headcounttbl headinst in
        spanscore -. headscore

    let build_lex_tree mbr syncat span headpos word =
        Scale.inc ();
        Scale.display ();
        let inst = Inst.create span syncat word in
        let deriv = conv_word_to_deriv syncat word in
        let lexnode = Tree.create_term word span in
        let catscore = Stdlib.log (Syncat.score mbr.catconst syncat) in
        let tree = Tree.create_nonterm 
            syncat word [lexnode] span headpos Syncat.Unary in
        let prob = 
            catscore *. (DerivGenModel.get_smooth_prob mbr.ruleprob deriv) in
        let (begpos, endpos) = span in
        let risk = riskscore mbr begpos endpos syncat word in
        set_inst mbr inst tree (prob *. risk);
        set_minroot mbr inst prob

    let build_dtrs_tree mbr syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let inst = Inst.create span syncat word in
        let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
        let dtrs = ref [] in
        let catscore = Syncat.score mbr.catconst syncat in
        let prob = 
            ref (catscore *. DerivGenModel.get_smooth_prob mbr.ruleprob deriv) in
        let (begpos, endpos) = span in
        let risk = riskscore mbr begpos endpos syncat word in
        prob := !prob *. risk;
        List.iter
            (fun (syncat', span', headpos', word') ->
                let inst' = Inst.create span' syncat' word' in
                let (tree', prob') = get_inst mbr inst' in
                dtrs := !dtrs @ [tree'];
                prob := !prob +. prob')
            dtrargs;
        let tree = Tree.create_nonterm
            syncat word !dtrs span headpos depdir in
        let brascore = Tree.score_branching tree mbr.lbrconst mbr.rbrconst in
        set_inst mbr inst tree (!prob +. brascore);
        set_minroot mbr inst !prob

    let compute_viterbi mbr =
        Scale.add (Scale.WindMill 5000) " DP  " "";
        Chart.iter_derivs mbr.chart
            Chart.BottomUp
            (build_lex_tree mbr) (build_dtrs_tree mbr)
            (0, mbr.endpos);
        Scale.discard ()

    let decapsulate_tree start tree =
        if Syncat.equal_ign_attrs (Tree.get_syncat tree) start then begin
            if Tree.is_nonterm tree then begin
                let dtrs = Tree.get_dtrs tree in
                let depdir = Tree.get_depdir tree in
                match depdir with
                    Syncat.Left | Syncat.RightSerial -> List.nth dtrs 0
                  | Syncat.Right | Syncat.LeftSerial -> List.nth dtrs 1
                  | Syncat.Coor -> List.nth dtrs 0
                  | Syncat.Unary -> List.nth dtrs 0
            end else tree
        end else tree

    let find_min mbr =
        let (tree, prob) = match mbr.minroot with
            None -> raise Not_found
          | Some (inst, prob) -> begin
                (* Printf.printf "Inst = %s\n" (Inst.to_string inst); *)
                get_inst mbr inst
            end in
        (* Printf.printf "tree      = %s\n" (Tree.to_string tree); *)
        let decaptree = decapsulate_tree mbr.start tree in
        (* Printf.printf "decaptree = %s\n" (Tree.to_string decaptree); *)
        (decaptree, prob)

    let compute ruleprob chart start catconst lbrconst rbrconst =
        (* Printf.printf "(mbr.compute          ) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let mbr = create ruleprob chart start catconst lbrconst rbrconst in
        compute_risktbl mbr;
        compute_viterbi mbr;
        find_min mbr

end

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Printf
open Utils

module type ITEM_TYPE = sig
    type key_t
    type value_t
    val equal : key_t -> key_t -> bool
    val hash : key_t -> int
end

module HDB (Item : ITEM_TYPE) = struct

    type key_t = Item.key_t

    type value_t = Item.value_t

    module HashedItem = struct
        type t = Item.key_t
        let equal = Item.equal
        let hash = Item.hash
    end

    module ItemTbl = Hashtbl.Make(HashedItem)

    type t = value_t ItemTbl.t

    let create = ItemTbl.create

    let add = ItemTbl.add

    let copy = ItemTbl.copy

    let find = ItemTbl.find

    let find_all = ItemTbl.find_all

    let mem = ItemTbl.mem

    let remove = ItemTbl.remove

    let replace = ItemTbl.replace

    let iter = ItemTbl.iter

    let fold = ItemTbl.fold

    let length = ItemTbl.length

    let from_file fname : t =
        unmarshal_from_file fname

    let to_file (hdb : t) fname =
        marshal_to_file fname hdb

end

module SeqHDB (Item : ITEM_TYPE with type key_t = int) = struct

    type key_t = Item.key_t

    type value_t = Item.value_t

    type t = {
        dbname          : string;
        mutable counter : int;
    }

    let index_fname dbname =
        let l = String.length dbname in
        if dbname.[l - 1] <> '/' then
            Printf.sprintf "%s/header" dbname
        else
            Printf.sprintf "%sheader" dbname

    let item_fname dbname index =
        let l = String.length dbname in
        if dbname.[l - 1] <> '/' then
            Printf.sprintf "%s/%016d" dbname index
        else
            Printf.sprintf "%s%016d" dbname index

    let create dbname = 
        if not (Sys.file_exists dbname) then
            Unix.mkdir dbname 0o744
        else failwith "SeqHBD.create: file already exists.";
        {
            dbname  = dbname;
            counter = 0;
        }

    let add hdb value =
        let fname = item_fname hdb.dbname hdb.counter in
        marshal_to_file fname value;
        hdb.counter <- hdb.counter + 1

    let copy hdb = {
        dbname  = hdb.dbname;
        counter = hdb.counter;
    }

    let mem hdb index =
        if index < hdb.counter then
            let fname = item_fname hdb.dbname hdb.counter in
            Sys.file_exists fname
        else false

    let find hdb index =
        if not (mem hdb index) then
            let fname = item_fname hdb.dbname index in
            unmarshal_from_file fname
        else raise Not_found

    let find_all hdb index =
        [find hdb index]

    let remove hdb index =
        let fname = item_fname hdb.dbname hdb.counter in
        if Sys.file_exists fname then
            Sys.remove fname
        else ()

    let replace hdb index value =
        let fname = item_fname hdb.dbname index in
        marshal_to_file fname value

    let iter fn hdb =
        for index = 0 to hdb.counter - 1 do
            try
                fn index (find hdb index)
            with Not_found -> ()
        done

    let fold fn hdb initval =
        let temp = ref initval in
        for index = 0 to hdb.counter - 1 do
            try
                temp := fn index (find hdb index) !temp
            with Not_found -> ()
        done;
        !temp

    let length hdb =
        hdb.counter

    let from_file dbfname : t =
        unmarshal_from_file (index_fname dbfname)

    let to_file (seqhdb : t) dbfname =
        marshal_to_file (index_fname dbfname) seqhdb

end

module SeqHDBWrapper (Item : ITEM_TYPE with type key_t = int) = struct

    type key_t = Item.key_t

    type value_t = Item.value_t

    module SeqHDB = SeqHDB(Item)

    type seqhdb_t = SeqHDB.t

    type t = {
        seqhdbs  : seqhdb_t array;
        nodbs    : int;
        noitems  : int;
        rangetbl : int array;
    }

    let init wrapper =
        let sum = ref 0 in
        for index = 0 to wrapper.nodbs - 1 do
            wrapper.rangetbl.(index) <- !sum;
            sum := !sum + SeqHDB.length wrapper.seqhdbs.(index)
        done

    let create seqhdbs =
        let seqhdb_arr = Array.of_list seqhdbs in
        let nodbs = List.length seqhdbs in
        let noitems = List.fold_left
            (fun v seqhdb -> v + SeqHDB.length seqhdb)
            0 seqhdbs in
        let wrapper = {
            seqhdbs  = seqhdb_arr;
            nodbs    = nodbs;
            noitems  = noitems;
            rangetbl = Array.make nodbs 0;
        } in
        init wrapper;
        wrapper

    let from_files fnames : t =
        let seqhdbs = List.map SeqHDB.from_file fnames in
        create seqhdbs

    let get_nodbs wrapper =
        wrapper.nodbs

    let get_noitems wrapper =
        wrapper.noitems

    let nth wrapper index =
        wrapper.seqhdbs.(index)

    let calc_relidx wrapper index =
        let i = ref 0 in
        while !i < wrapper.nodbs - 1 && index > wrapper.rangetbl.(!i) do
            incr i
        done;
        (!i, index - wrapper.rangetbl.(!i))

    let mem wrapper index =
        let (dbid, relidx) = calc_relidx wrapper index in
        SeqHDB.mem wrapper.seqhdbs.(dbid) relidx

    let find wrapper index =
        let (dbid, relidx) = calc_relidx wrapper index in
        SeqHDB.find wrapper.seqhdbs.(dbid) relidx

    let find_all wrapper index =
        let (dbid, relidx) = calc_relidx wrapper index in
        SeqHDB.find_all wrapper.seqhdbs.(dbid) relidx

    let length wrapper =
        wrapper.noitems

    let replace wrapper index value =
        let (dbid, relidx) = calc_relidx wrapper index in
        SeqHDB.replace wrapper.seqhdbs.(dbid) relidx value

    let iter fn wrapper =
        for dbid = 0 to wrapper.nodbs - 1 do
            SeqHDB.iter fn wrapper.seqhdbs.(dbid)
        done

end

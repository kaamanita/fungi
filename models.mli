(** Probability Models for Syntactic Derivation.

    There are four different probabilities available in this module.
    {ol
        {- PCFG: P(tree) = product of P(rule) for all rules constructing the tree.}
        {- Role-emission probability: P(role | category, word), where role can be 'head,' 'dependent,' or 'connective.'}
        {- Lexicon-emission probability: P(word | category) for all preterminal nodes.}
        {- Head-emission probability: P(word | category) for all nodes.}
    }
*)

type probflag_t = RoleEmis | JuliaEmis | LexEmis | HeadEmis

(** Create PCFG. *)
val create_pcfg : int -> float -> Deriv.DerivGenModel.t

(** Create Model 1 (PCFG). *)
val create_model1 : int -> float -> Deriv.DerivGenModel.t

(** Create Model 2 (PCFG + role-emission probability). *)
val create_model2 : int -> float -> Deriv.DerivGenModel.t

(** Create Model 3 (PCFG + lexicon-emission probability). *)
val create_model3 : int -> float -> Deriv.DerivGenModel.t

(** Create Model 4 (PCFG + role-emission probability + 
    lexicon-emission probability). *)
val create_model4 : int -> float -> Deriv.DerivGenModel.t

(** Create Model 5 (PCFG + head-emission probability). *)
val create_model5 : int -> float -> Deriv.DerivGenModel.t

(** Create Model 6 (PCFG + role-emission probability + 
    head-emission probability). *)
val create_model6 : int -> float -> Deriv.DerivGenModel.t

val conv_probflags : probflag_t list -> int -> float -> Deriv.DerivGenModel.probftr_t list

val create_model : int -> float -> probflag_t list -> Deriv.DerivGenModel.t

open Cg
open Chart
open Prob
open Printf

module Deriv = struct

    type syncat_t = CG.syncat_t

    type depdir_t = CG.depdir_t

    type word_t = string

    type prems_t = {
        syncat : syncat_t;
        head   : word_t;
    }

    let create_prems syncat head = {
        syncat = syncat;
        head   = head;
    }

    let prems_to_string prems =
        sprintf "%s{%s}" (Syncat.to_string prems.syncat) prems.head

    let get_syncat prems =
        prems.syncat

    let get_head prems =
        prems.head

    type dtrs_t = {
        dtrs   : (syncat_t * word_t) list;
        depdir : depdir_t;
    }

    type consq_t =
        Lex of word_t
      | Dtrs of dtrs_t

    let create_consq_dtrs dtrs depdir = 
        Dtrs {
            dtrs   = dtrs;
            depdir = depdir;
        }

    let create_consq_lex word =
        Lex word

    let consq_to_string consq =
        match consq with
            Lex head -> sprintf "'%s'" head
          | Dtrs dtrs -> 
                sprintf "%s : %s"
                    (String.concat " " 
                        (List.map 
                            (fun (c, w) -> 
                                sprintf "%s{%s}" 
                                    (Syncat.to_string c) w) 
                            dtrs.dtrs))
                    (Syncat.depdir_to_string dtrs.depdir)

    let get_dtrs consq =
        match consq with
            Lex _ -> failwith "get_dtrs"
          | Dtrs content -> content.dtrs

    let get_depdir consq =
        match consq with
            Lex _ -> failwith "get_depdir"
          | Dtrs content -> content.depdir

    let get_word consq =
        match consq with
            Lex word -> word
          | Dtrs _ -> failwith "get_word"

    let hash_consq consq =
        match consq with
            Lex word ->
                Hashtbl.hash word
          | Dtrs content ->
                Hashtbl.hash content.depdir
                + List.fold_left
                    (fun v (dtr, word) -> 
                        v + Syncat.hash dtr + Hashtbl.hash word)
                    0 content.dtrs

    let is_consq_dtrs consq =
        match consq with
            Lex _ -> false
          | Dtrs _ -> true

    let is_consq_lex consq =
        match consq with
            Lex _ -> true
          | Dtrs _ -> false

    type t = {
        prems  : prems_t;
        consq  : consq_t;
    }

    let create prems consq = {
        prems  = prems;
        consq  = consq;
    }

    let create_deriv_dtrs syncat head dtrcats depdir = {
        prems = create_prems syncat head;
        consq = create_consq_dtrs dtrcats depdir;
    }

    let create_deriv_lex syncat word = {
        prems = create_prems syncat word;
        consq = create_consq_lex word;
    }

    let to_string deriv =
        sprintf "%s -> %s" 
            (prems_to_string deriv.prems) 
            (consq_to_string deriv.consq)

    let get_prems deriv =
        deriv.prems

    let get_consq deriv =
        deriv.consq

    let equal_prems prems1 prems2 = 
        Syncat.equal prems1.syncat prems2.syncat
        && prems1.head = prems2.head

    let hash_prems prems = 
        Syncat.hash prems.syncat + Hashtbl.hash prems.head

    let equal deriv1 deriv2 =
        deriv1.prems = deriv2.prems && deriv1.consq = deriv2.consq

    let hash deriv =
        hash_prems deriv.prems + hash_consq deriv.consq

    let backoff_dtrcats dtrs depdir =
        match depdir, dtrs with
            Syncat.Left, [(_, w1); (_, w2)] ->
                [(Syncat.bo_cat Syncat.NoDep, w1); (Syncat.bo_cat Syncat.NoDep, w2)]
          | Syncat.Right, [(_, w1); (_, w2)] ->
                [(Syncat.bo_cat Syncat.NoDep, w1); (Syncat.bo_cat Syncat.NoDep, w2)]
          | Syncat.Coor, [(_, w1); (_, w2); (_, w3)] ->
                [(Syncat.bo_cat Syncat.NoDep, w1); (Syncat.bo_cat Syncat.NoDep, w2); (Syncat.bo_cat Syncat.NoDep, w3)]
          | Syncat.Unary, [(_, w1)] ->
                [(Syncat.bo_cat Syncat.NoDep, w1)]
          | _, _ -> failwith "backoff_arg_dtrcats"

    let backoff deriv =
        let head = get_head (get_prems deriv) in
        let deriv' = match deriv.consq with
            Lex _ ->
                create_deriv_lex (Syncat.bo_cat Syncat.NoDep) head
          | Dtrs content -> 
                let dtrcats = 
                    backoff_dtrcats content.dtrs content.depdir in
                create_deriv_dtrs 
                    (Syncat.bo_cat Syncat.NoDep) head dtrcats content.depdir
        in
        (deriv', head)

end

module DerivBackoff = struct

    type deriv_t = Deriv.t

    type word_t = string

    type data_t = deriv_t

    type t = {
        deriv : deriv_t;
        word  : word_t;
    }

    let equal dbo1 dbo2 =
        Deriv.equal dbo1.deriv dbo2.deriv
        && dbo1.word = dbo2.word

    let hash dbo =
        Deriv.hash dbo.deriv + Hashtbl.hash dbo.word

    let to_string dbo =
        sprintf "%s : %s" (Deriv.to_string dbo.deriv) dbo.word

    type prems_t = deriv_t

    type consq_t = word_t

    let create deriv word = {
        deriv = deriv;
        word  = word;
    }

    let get_prems dbo = dbo.deriv

    let get_consq dbo = dbo.word

    let equal_prems = Deriv.equal

    let hash_prems = Deriv.hash

    let prems_to_string = Deriv.to_string

    let backoff = Deriv.backoff

end

type deriv_t = Deriv.t

module DerivProbTbl = ProbTbl(Deriv)

module DerivGenModel = GenModel(Deriv)
(* module DerivGenModel = GenModelWithBackoff(Deriv)(DerivBackoff) *)

let conv_word_to_deriv syncat word =
    let prems = Deriv.create_prems syncat word
    and consq = Deriv.create_consq_lex word in
    Deriv.create prems consq

let conv_dtrargs_to_deriv syncat word depdir dtrargs =
    let dtrs = List.map
        (fun (syncat', _, _, word') -> (syncat', word'))
        dtrargs in
    let prems = Deriv.create_prems syncat word
    and consq = Deriv.create_consq_dtrs dtrs depdir in
    Deriv.create prems consq

module type FTR_TYPE = sig
    type prems_t
    type t
    val equal_prems : prems_t -> prems_t -> bool
    val hash_prems : prems_t -> int
    val equal : t -> t -> bool
    val hash : t -> int
    val extract_mtrftrs : deriv_t -> (prems_t * t) list
    val extract_dtrftrs : deriv_t -> (prems_t * t) list
    val to_string : t -> string
    val prems_to_string : prems_t -> string
end

module ProbInst (Ftr : FTR_TYPE) = struct

    type prems_t = Ftr.prems_t

    type consq_t = Ftr.t

    type t = {
        prems : prems_t;
        consq : consq_t;
    }

    let create prems consq = {
        prems = prems;
        consq = consq;
    }

    let get_prems probinst =
        probinst.prems

    let get_consq probinst =
        probinst.consq

    let equal_prems = Ftr.equal_prems

    let hash_prems = Ftr.hash_prems

    let equal probinst1 probinst2 =
        equal_prems probinst1.prems probinst2.prems
        && Ftr.equal probinst1.consq probinst2.consq

    let hash probinst =
        hash_prems probinst.prems + Ftr.hash probinst.consq

    let extract_mtrftrs deriv =
        List.map 
            (fun (prems, ftr) -> create prems ftr)
            (Ftr.extract_mtrftrs deriv)

    let extract_dtrftrs deriv =
        List.map
            (fun (prems, ftr) -> create prems ftr)
            (Ftr.extract_dtrftrs deriv)

    let extract_ftrs deriv =
        (extract_mtrftrs deriv) @ (extract_dtrftrs deriv)

    let to_string probinst = 
        sprintf "%s : %s" 
            (Ftr.prems_to_string probinst.prems) 
            (Ftr.to_string probinst.consq)

    let prems_to_string = Ftr.prems_to_string

end

module DerivFtr (Ftr : FTR_TYPE) = struct
  
    module Inst = ProbInst(Ftr)

    type inst_t = Inst.t

    module CondProbTbl = CondProbTbl(Inst)

    type t = CondProbTbl.t

    type probftr_t = DerivGenModel.probftr_t

    let create_cprobtbl n epsilon =
        CondProbTbl.create n epsilon

    let get_prob probmodel deriv =
        List.fold_left
            (fun prob symbol ->
                prob *. CondProbTbl.get_prob probmodel symbol)
            1.0 (Inst.extract_ftrs deriv)

    let get_smooth_prob probmodel deriv =
        List.fold_left
            (fun prob symbol ->
                prob *. CondProbTbl.get_smooth_prob probmodel symbol)
            1.0 (Inst.extract_ftrs deriv)

    let set_prob probmodel deriv prob =
        List.iter
            (fun symbol ->
                CondProbTbl.set_prob probmodel symbol prob)
            (Inst.extract_ftrs deriv)

    let acc_prob probmodel deriv prob =
        List.iter
            (fun symbol ->
                CondProbTbl.acc_prob probmodel symbol prob)
            (Inst.extract_ftrs deriv)

    let create_probftr probmodel =
        let fn_get_prob = get_prob probmodel
        and fn_get_smooth_prob = get_smooth_prob probmodel
        and fn_set_prob = set_prob probmodel
        and fn_acc_prob = acc_prob probmodel in
        DerivGenModel.create_probftr 
            fn_get_prob fn_get_smooth_prob fn_set_prob fn_acc_prob

    let create n epsilon : probftr_t =
        let probmodel = create_cprobtbl n epsilon in
        create_probftr probmodel

end

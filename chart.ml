open Cg
open Dict
open Printf
open Utils

module HashedInt = struct
    type t = int
    let hash = Hashtbl.hash
    let equal = Stdlib.(=)
end

module ItemTbl = Hashtbl.Make(HashedInt)

module HashedSpan = struct
    type t = int * int
    let hash = Hashtbl.hash
    let equal = Stdlib.(=)
end

module SpanTbl = Hashtbl.Make(HashedSpan)

module Item = struct

    type id_t = int

    type syncat_t = CG.syncat_t

    type depdir_t = CG.depdir_t

    type span_t = int * int

    type word_t = string

    type proddtr_t =
        Lex of word_t       (** Lexical item *)
      | Ids of id_t list    (** Item IDs *)

    let create_proddtr_lex word = Lex word

    let create_proddtr_ids ids = Ids ids

    let to_string_proddtr proddtr =
        match proddtr with
            Lex word -> 
                "'" ^ word ^ "'"
          | Ids ids -> 
                string_of_list string_of_int ids

    let is_proddtr_lex proddtr =
        match proddtr with
            Lex _ -> true
          | Ids _ -> false

    let is_proddtr_ids proddtr =
        match proddtr with
            Lex _ -> false
          | Ids _ -> true

    type prod_t = {
        proddtr : proddtr_t;    (** Daughter *)
        headpos : int;          (** Position of syntactic head *)
        depdir  : depdir_t;     (** Dependency direction TOWARDS head *)
    }

    let create_prod_ids dtrids headpos depdir = {
        proddtr = Ids dtrids;
        headpos = headpos;
        depdir  = depdir;
    }

    let create_prod_lex word pos = {
        proddtr = Lex word;
        headpos = pos;
        depdir  = Syncat.Unary;
    }

    let to_string_prod prod =
        sprintf "(%s, %d, %s)" 
            (to_string_proddtr prod.proddtr) prod.headpos
            (Syncat.depdir_to_string prod.depdir)

    let get_proddtr prod = prod.proddtr

    let get_headpos prod = prod.headpos

    let get_depdir prod = prod.depdir

    let is_terminal prod =
        is_proddtr_lex prod.proddtr

    let get_word prod =
        match prod.proddtr with
            Lex word -> word
          | Ids _ -> failwith "get_word"

    let get_ids prod =
        match prod.proddtr with
            Lex word -> failwith "get_ids"
          | Ids ids -> ids

    type enumlevel_t = Dict.enumlevel_t

    type t = {
        id            : id_t;           (** Item ID *)
        syncat        : syncat_t;       (** Syntactic category *)
        span          : span_t;         (** Position span *)
        headpos       : int;            (** Position of syntactic head *)
        mutable prods : prod_t list;    (** Productions *)
    }

    let create id syncat span headpos prods = {
        id      = id;
        syncat  = syncat;
        span    = span;
        headpos = headpos;
        prods   = prods;
    }

    let to_string item =
        let (begpos, endpos) = item.span in
        sprintf "Item %d (%d, %d, %s, %d): %s"
            item.id begpos endpos (Syncat.to_string item.syncat) item.headpos
            (string_of_list to_string_prod item.prods ~bracket:false)

    let get_id item = item.id

    let get_syncat item = item.syncat

    let get_span item = item.span

    let get_headpos_of_item item = item.headpos

    let get_prods item = item.prods

    let is_term item =
        item.prods = []

    let add_prod item prod =
        if not (List.mem prod item.prods) then
            item.prods <- prod :: item.prods
        else ()

    let set_prods item prods =
        item.prods <- prods

    let equal item1 item2 =
        item1.id = item2.id
            && Syncat.equal item1.syncat item2.syncat
            && item1.span = item2.span
            && item1.headpos = item2.headpos

    let hash item =
        Hashtbl.hash item.id + Syncat.hash item.syncat
            + Hashtbl.hash item.span + Hashtbl.hash item.headpos

end

module Chart = struct

    type item_t = Item.t

    type id_t = Item.id_t

    type span_t = int * int

    type prod_t = Item.prod_t

    type syncat_t = Item.syncat_t

    type word_t = string

    type depdir_t = Item.depdir_t

    type enumlevel_t = Dict.enumlevel_t
   
    type t = {
        itemtbl           : item_t ItemTbl.t;       (** Item table *)
        spantbl           : id_t list SpanTbl.t;    (** Span table *)
        mutable idcnt     : id_t;                   (** Item counter *)
        sentence          : word_t list;            (** Sentence *)
        words             : word_t list;            (** Sequence of words *)
        mutable enumlevel : enumlevel_t;            (** Enumeration level *)
        mutable derivcnt  : int;                    (** Derivation counter *)
        mutable is_enum   : bool;
    }

    type dtrargs_t = (syncat_t * span_t * int * word_t) list

    type fn_word_t = syncat_t -> span_t -> int -> word_t -> unit

    type fn_dtrs_t = 
        syncat_t -> span_t -> int -> word_t -> depdir_t -> 
        (syncat_t * span_t * int * word_t) list -> unit

    type srchstr_t = BottomUp | TopDown

    let create sentence words = 
        let n = List.length sentence in
        {
            itemtbl   = ItemTbl.create (n * n);
            spantbl   = SpanTbl.create (n * (n - 1) / 2);
            idcnt     = 0;
            sentence  = sentence;
            words     = words;
            enumlevel = Dict.NoEnum;
            derivcnt  = 0;
            is_enum   = false;
        }

    let length chart =
        ItemTbl.length chart.itemtbl

    let to_string_itemtbl itemtbl idcnt =
        let result = ref [] in
        for id = 1 to idcnt do
            if ItemTbl.mem itemtbl id then
                let item = ItemTbl.find itemtbl id in
                result := !result @ [(Item.to_string item)]
            else ()
        done;
        !result

    let to_string_spanentry spantbl begpos endpos =
        let ids = List.sort compare 
            (SpanTbl.find spantbl (begpos, endpos)) in
        sprintf "Span (%d, %d): %s" 
            begpos endpos
            (string_of_list string_of_int ids ~bracket:false)

    let to_string_spantbl spantbl sentlen =
        let result = ref [] in
        for edgelen = 0 to sentlen - 1 do
            for begpos = 0 to sentlen - edgelen - 1 do
                let endpos = begpos + edgelen in
                let span = (begpos, endpos) in
                if SpanTbl.mem spantbl span then
                    result := !result @ [
                        to_string_spanentry spantbl begpos endpos
                    ]
                else ()
            done
        done;
        !result

    let to_string chart =
        let sentlen = List.length chart.sentence in
        sprintf "%s\n%s\nItem count = %d\nSentence = [%s]\nEnumeration = %s\nDerivation count = %d"
            (String.concat "\n" 
                (to_string_itemtbl chart.itemtbl chart.idcnt))
            (String.concat "\n" (to_string_spantbl chart.spantbl sentlen))
            chart.idcnt
            (String.concat " " chart.sentence)
            (Dict.enumlevel_to_string chart.enumlevel)
            chart.derivcnt

    let set_enumlevel chart enumlevel =
        chart.enumlevel <- enumlevel

    let get_enumlevel chart =
        chart.enumlevel

    let get_derivcnt chart =
        chart.derivcnt

    let get_sentence chart = 
        chart.sentence

    let get_words chart =
        chart.words

    let get_nonterms chart =
        let result = ref [] in
        ItemTbl.iter
            (fun _ item ->
                let syncat = Item.get_syncat item in
                if not (List.mem syncat !result) then
                    result := syncat :: !result
                else ())
            chart.itemtbl;
        !result

    let get_item chart id =
        ItemTbl.find chart.itemtbl id

    let get_all_ids chart =
        ItemTbl.fold (fun id _ result -> id :: result) chart.itemtbl []

    let mem chart span =
        SpanTbl.mem chart.spantbl span 

    let add_new_item_to_chart chart item =
        let span = Item.get_span item in
        let oldset =
            if not (mem chart span) then []
            else SpanTbl.find chart.spantbl span in
        let newset = (Item.get_id item) :: oldset in
        SpanTbl.replace chart.spantbl span newset

    let create_new_item chart syncat span headpos =
        chart.idcnt <- chart.idcnt + 1;
        let id = chart.idcnt in
        let prods = [] in
        let item = Item.create id syncat span headpos prods in
        ItemTbl.replace chart.itemtbl id item;
        add_new_item_to_chart chart item;
        item

    let find_syncat_in_span chart span syncat headpos =
        if not (mem chart span) then
            raise Not_found
        else
            (* Note that List.find can also raise Not_found. *)
            let id = List.find 
                (fun id -> 
                    let item = get_item chart id in
                    Item.get_syncat item = syncat
                      && Item.get_headpos_of_item item = headpos)
                (SpanTbl.find chart.spantbl span) in
            ItemTbl.find chart.itemtbl id

    let add_deriv chart span syncat headpos prod =
        let item =
            try
                find_syncat_in_span chart span syncat headpos
            with Not_found ->
                create_new_item chart syncat span headpos in
        Item.add_prod item prod
   
    let add_deriv_ret_item chart span syncat headpos prod =
        let item =
            try
                find_syncat_in_span chart span syncat headpos
            with Not_found ->
                create_new_item chart syncat span headpos in
        Item.add_prod item prod;
        item
   
    let get_ids_from_span chart span =
        if not (mem chart span) then []
        else SpanTbl.find chart.spantbl span

    let get_items chart span =
        List.map 
            (get_item chart) 
            (get_ids_from_span chart span)

    let rec find_dtrids_from_prods prods result =
        match prods with
            [] -> elim_dups result
          | prod :: rest ->
            begin
                let ids' = match Item.get_proddtr prod with
                    Item.Lex _ -> []
                  | Item.Ids idlist -> idlist in
                find_dtrids_from_prods rest (result @ ids')
            end

    let find_dtrids_from_id chart id result =
        let item = get_item chart id in
        let id = Item.get_id item
        and prods = Item.get_prods item in
        if not (List.mem id result) then
            find_dtrids_from_prods prods result
        else result

    let rec find_rchids_by_id chart rchids id =
        if not (List.mem id !rchids) then begin
            rchids := id :: !rchids;
            let item = get_item chart id in
            List.iter
                (fun prod ->
                    let proddtr = Item.get_proddtr prod in
                    match proddtr with
                        Item.Lex _ -> ()
                      | Item.Ids ids ->
                            List.iter (find_rchids_by_id chart rchids) ids)
                (Item.get_prods item)
        end else ()

    let find_rchids chart span syncat =
        if SpanTbl.mem chart.spantbl span then begin
            let rchids = ref [] in
            List.iter
                (fun id ->
                    let item = get_item chart id in
                    let syncat' = Item.get_syncat item in
                    if Syncat.equal_ign_attrs syncat' syncat then
                        find_rchids_by_id chart rchids id
                    else ())
                (SpanTbl.find chart.spantbl span);
            !rchids
        end else []

    let elim_excess chart syncat =
        let span = (0, List.length chart.sentence - 1) in
        let rchids = find_rchids chart span syncat in
        let unrchids = diff (get_all_ids chart) rchids in
        let emptyspans = ref [] in
        SpanTbl.iter
            (fun span ids ->
                let newids = diff ids unrchids in
                SpanTbl.replace chart.spantbl span newids;
                if List.length newids = 0 then
                    emptyspans := span :: !emptyspans
                else ())
            chart.spantbl;
        List.iter
            (fun span ->
                SpanTbl.remove chart.spantbl span)
            !emptyspans;
        List.iter
            (fun id ->
                ItemTbl.remove chart.itemtbl id)
            unrchids

    let rec iter_dtrargs ?(backoff=false) chart fn_dtrs 
            syncat span headpos word depdir ids dtrargs =
        match ids with
            [] ->
            begin
                if not backoff then
                    fn_dtrs syncat span headpos word depdir dtrargs
                else
                    let deptag = Syncat.get_deptag syncat in
                    fn_dtrs (Syncat.bo_cat deptag) span headpos word depdir dtrargs
            end
          | id :: rest ->
            begin
                let item' = get_item chart id in
                let syncat' = Item.get_syncat item'
                and span' = Item.get_span item' in
                List.iter
                    (fun prod' ->
                        let headpos' = Item.get_headpos prod' in
                        let word' = List.nth 
                            (get_sentence chart) headpos' in
                        let dtr' = 
                            if not backoff || syncat' = Syncat.conn_cat then
                                (syncat', span', headpos', word') 
                            else
                                let deptag' = Syncat.get_deptag syncat' in
                                (Syncat.bo_cat deptag', span', headpos', word') in
                        iter_dtrargs ~backoff:backoff chart fn_dtrs 
                            syncat span headpos word depdir rest 
                            (dtrargs @ [dtr']))
                    (Item.get_prods item')
            end

    let iter_deriv_by_dtrs_onelevel ?(backoff=false) chart 
            fn_word fn_dtrs syncat span headpos word depdir ids =
        iter_dtrargs ~backoff:backoff chart 
            fn_dtrs syncat span headpos word depdir ids []

    let iter_derivs_by_prod_onelevel ?(backoff=false) chart fn_word fn_dtrs item prod =
        let syncat = Item.get_syncat item
        and span = Item.get_span item 
        and headpos = Item.get_headpos prod 
        and depdir = Item.get_depdir prod in
        match (Item.get_proddtr prod) with
            Item.Lex word -> 
                fn_word syncat span headpos word
          | Item.Ids ids ->
                let word = List.nth (get_sentence chart) headpos in
                iter_deriv_by_dtrs_onelevel ~backoff:backoff chart 
                    fn_word fn_dtrs syncat span headpos word depdir ids

    let iter_derivs_by_id_onelevel ?(backoff=false) chart fn_word fn_dtrs id =
        let item = get_item chart id in
        List.iter
            (iter_derivs_by_prod_onelevel ~backoff:backoff chart fn_word fn_dtrs item)
            (Item.get_prods item)

    let iter_derivs_onelevel ?(backoff=false) chart fn_word fn_dtrs span =
        List.iter
            (iter_derivs_by_id_onelevel ~backoff:backoff chart fn_word fn_dtrs)
            (get_ids_from_span chart span)

    let iter_derivs_topdown ?(backoff=false) chart fn_word fn_dtrs span =
        let (begpos, endpos) = span in
        for edgelen = (endpos - begpos) downto 0 do
            for b = begpos to endpos - edgelen do
                let e = b + edgelen in
                let span' = (b, e) in
                if mem chart span' then
                    let ids' = get_ids_from_span chart span' in
                    List.iter (iter_derivs_by_id_onelevel ~backoff:backoff chart fn_word fn_dtrs) ids'
                else ()
            done
        done

    let iter_derivs_bottomup ?(backoff=false) chart fn_word fn_dtrs span =
        let (begpos, endpos) = span in
        for edgelen = 0 to (endpos - begpos) do
            for b = begpos to endpos - edgelen do
                let e = b + edgelen in
                let span' = (b, e) in
                if mem chart span' then
                    let ids' = get_ids_from_span chart span' in
                    List.iter 
                        (iter_derivs_by_id_onelevel 
                            ~backoff:backoff chart fn_word fn_dtrs) 
                        ids'
                else ()
            done
        done

    let iter_derivs chart srchstr fn_word fn_dtrs span =
        match srchstr with
            BottomUp -> 
            begin
                iter_derivs_bottomup ~backoff:false chart fn_word fn_dtrs span (*;
                iter_derivs_bottomup ~backoff:true chart fn_word fn_dtrs span *)
            end
          | TopDown ->
            begin
                iter_derivs_topdown ~backoff:false chart fn_word fn_dtrs span (*;
                iter_derivs_topdown ~backoff:true chart fn_word fn_dtrs span *)
            end

    let is_success chart =
        mem chart (0, List.length chart.sentence - 1)

    let is_filledup chart =
        chart.is_enum

    let set_filledup chart =
        chart.is_enum <- true

    let count_derivs chart =
        let n = List.length chart.sentence in
        let (begpos, endpos) = (0, n - 1) in
        let cnt = ref 0 in
        for edgelen = 0 to (endpos - begpos) do
            for b = begpos to endpos - edgelen do
                let e = b + edgelen in
                let span' = (b, e) in
                if mem chart span' then
                    let ids' = get_ids_from_span chart span' in
                    List.iter 
                        (iter_derivs_by_id_onelevel 
                            ~backoff:false chart 
                            (fun _ _ _ _ -> incr cnt)
                            (fun _ _ _ _ _ _ -> incr cnt))
                        ids'
                else ()
            done
        done;
        chart.derivcnt <- !cnt

end

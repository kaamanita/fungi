open Cg
open Chart
open Prob
open Deriv
open Models
open Inout
open Em
open Utils
open Printf

module type PARAM_TYPE = sig
    type t
    val to_string : t -> string
    val scale_prob : t -> float -> Deriv.t -> float
    val localscore : t -> int -> int -> float
    val inc : t -> t
    val is_stop_iter : t -> bool
end

module Annealer(Param : PARAM_TYPE) = struct

    type syncat_t = Syncat.t

    type word_t = string

    type chart_t = Chart.t

    type genmodel_t = DerivGenModel.t

    type deriv_t = Deriv.t

    type fn_get_chart_t = int -> chart_t

    type fn_scale_prob_t = float -> deriv_t -> float

    type fn_localscore_t = int -> int -> float
    
    type fn_compute_inout_t =
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit

    type param_t = Param.t

    type t = {
        noentries     : int;
        diffthr       : float;
        rndthr        : int;
        epsilon       : float;
        ruleprob      : genmodel_t;
        ruleprior     : genmodel_t;
        start         : syncat_t;
        get_chart     : fn_get_chart_t;
        nocharts      : int;
        compute_inout : fn_compute_inout_t;
        mutable param : param_t;
        mutable rnd   : int;
    }

    let create 
        n diffthr rndthr 
        epsilon ruleprob ruleprior start get_chart nocharts 
        compute_inout initparam =
    {
        noentries     = n;
        diffthr       = diffthr;
        rndthr        = rndthr;
        epsilon       = epsilon;
        ruleprob      = ruleprob;
        ruleprior     = ruleprior;
        start         = start;
        get_chart     = get_chart;
        nocharts      = nocharts;
        compute_inout = compute_inout;
        param         = initparam;
        rnd           = 0;
    }

    let get_round ann =
        ann.rnd

    let reset_round ann =
        ann.rnd <- 0

    let compute_annealing ann =
        Scale.add Scale.NumberNoMax ">> Annealing " " ";
        while not (Param.is_stop_iter ann.param) do
            Scale.inc ();
            Scale.display ();
            Scale.add Scale.StaticInfix (Param.to_string ann.param) " > ";
            Scale.display ();
            ann.compute_inout
                ~scale_prob:(Param.scale_prob ann.param)
                ~localscore:(Param.localscore ann.param)
                ann.noentries ann.diffthr ann.rndthr ann.epsilon
                ann.ruleprob ann.ruleprior 
                ann.start ann.get_chart ann.nocharts;
            ann.param <- Param.inc ann.param;
            Scale.discard ();
        done;
        Scale.discard ()

    let compute
            n diffthr rndthr 
            epsilon ruleprob ruleprior start get_chart nocharts 
            compute_inout initparam =
        let ann = create
            n diffthr rndthr 
            epsilon ruleprob ruleprior start get_chart nocharts 
            compute_inout initparam in
        compute_annealing ann

end

module EmParam = struct

    type t = {
        mutable cnt : int;
        iterno      : int;
        mutable s   : bool;
    }

    let create iterno = 
    {
        cnt    = 0;
        iterno = iterno;
        s      = false;
    }

    let to_string param =
        sprintf "[%d iters]" param.iterno

    let scale_prob param prob deriv = prob

    let localscore param i j = 1.0

    let inc param =
    {
        cnt    = param.cnt + 1;
        iterno = param.iterno;
        s      = param.cnt = 0;
    }

    let is_stop_iter param = param.s

end

module DetParam = struct

    type t = {
        temp      : float;
        epoch     : float;
        mutable s : bool;
    }

    let create inittemp epoch =
        assert (inittemp > 0.0 && epoch > 1.0);
        {
            temp      = inittemp;
            epoch     = epoch;
            s         = false;
        }

    let to_string param =
        sprintf "[temperature = %.2e]"
            param.temp

    let scale_prob param prob deriv =
        prob ** param.temp

    let localscore param i j = 1.0

    let inc param =
        let newtemp = param.temp *. param.epoch in
        {
            temp  = if newtemp < 1.0 then newtemp else 1.0;
            epoch = param.epoch;
            s     = param.temp >= 1.0;
        }

    let is_stop_iter param = param.s

end

module SkewedDetParam = struct

    type genmodel_t = DerivGenModel.t

    type t = {
        temp      : float;
        epoch     : float;
        rulebias  : genmodel_t;
        mutable s : bool;
    }

    let create inittemp epoch rulebias =
        assert (inittemp > 0.0 && epoch > 1.0);
        {
            temp     = inittemp;
            epoch    = epoch;
            rulebias = rulebias;
            s        = false;
        }

    let to_string param =
        sprintf "[temperature = %.2e]"
            param.temp

    let scale_prob param prob deriv =
        let bias = DerivGenModel.get_smooth_prob param.rulebias deriv in
        (prob ** param.temp) *. (bias ** (1.0 -. param.temp))

    let localscore param i j = 1.0

    let inc param =
        let newtemp = param.temp *. param.epoch in
        {
            temp     = if newtemp < 1.0 then newtemp else 1.0;
            epoch    = param.epoch;
            rulebias = param.rulebias;
            s        = param.temp >= 1.0;
        }

    let is_stop_iter param = param.s

end

module StructParam = struct

    type t = {
        delta      : float;
        epoch      : float;
        finaldelta : float;
    }

    let create initdelta epoch finaldelta =
        assert (epoch <> 0.0);
        {
            delta = initdelta;
            epoch = epoch;
            finaldelta = finaldelta;
        }

    let to_string param =
        sprintf "[delta = %.2e]" param.delta

    let scale_prob param prob deriv = prob

    let localscore param i j =
        exp (param.delta *. float_of_int (abs (i - j)))

    let inc param =
    {
        delta      = param.delta +. param.epoch;
        epoch      = param.epoch;
        finaldelta = param.finaldelta;
    }

    let is_stop_iter param =
        if param.epoch > 0.0 then
            param.delta > param.finaldelta
        else
            param.delta < param.finaldelta

end

module EmAnnealer = Annealer(EmParam)

module DetAnnealer = Annealer(DetParam)

module SkewedDetAnnealer = Annealer(SkewedDetParam)

module StructAnnealer = Annealer(StructParam)

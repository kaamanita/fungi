(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Cg
open Avm
open Printf
open Utils

module HashedString = struct
    type t = string
    let equal = Stdlib.(=)
    let hash = Hashtbl.hash
end

module StrDict = Hashtbl.Make(HashedString)

module Dict = struct

    type word_t = StrDict.key

    type syncat_t = CG.syncat_t

    type avm_t = Avm.t

    type enumlevel_t = NoEnum | EnumOnUncertain | EnumOnAll

    let enumlevel_to_string enumlevel =
        match enumlevel with
            NoEnum -> "NoEnum"
          | EnumOnUncertain -> "EnumOnUncertain"
          | EnumOnAll -> "EnumOnAll"

    type t = {
        lexdict           : syncat_t list StrDict.t;  (** Content *)
        attrdict          : avm_t list StrDict.t;      (** Attribute dictionary *)
        mutable unclexs   : word_t list;          (** Uncertainty list *)
        mutable allcats   : syncat_t list;        (** All categories *)
        mutable enumlevel : enumlevel_t;          (** Enumeration level *)
        mutable accepts   : syncat_t list;        (** Accepted categories *)
        mutable unarydrv  : CG.transf_t;          (** Unary transformation rules *)
        mutable leftser   : syncat_t list;        (** Left-serializable list *)
        mutable rightser  : syncat_t list;        (** Right-serializable list *)
        mutable cats_aux  : word_t list;
        mutable cats_v    : word_t list;
        mutable cats_n    : word_t list;
        mutable cats_adj  : word_t list;
        mutable cats_adv  : word_t list;
        mutable cats_prep : word_t list;
        mutable cats_num  : word_t list;
        mutable cats_conj : word_t list;
        catdict           : string list StrDict.t;
    }

    let create n enumlevel = 
    {
        lexdict   = StrDict.create n;
        attrdict  = StrDict.create n;
        unclexs   = [];
        allcats   = [];
        enumlevel = enumlevel;
        accepts   = [];
        unarydrv  = [];
        leftser   = [];
        rightser  = [];
        cats_aux  = [];
        cats_v    = [];
        cats_n    = [];
        cats_adj  = [];
        cats_adv  = [];
        cats_prep = [];
        cats_num  = [];
        cats_conj = [];
        catdict   = StrDict.create n;
    }

    let add_aux dict cat =
        dict.cats_aux <- cat :: dict.cats_aux

    let is_aux dict cat =
        List.mem cat dict.cats_aux

    let add_v dict cat =
        dict.cats_v <- cat :: dict.cats_v

    let is_v dict cat =
        List.mem cat dict.cats_v

    let add_n dict cat =
        dict.cats_n <- cat :: dict.cats_n

    let is_n dict cat =
        List.mem cat dict.cats_n

    let add_adj dict cat =
        dict.cats_adj <- cat :: dict.cats_adj

    let is_adj dict cat =
        List.mem cat dict.cats_adj

    let add_adv dict cat =
        dict.cats_adv <- cat :: dict.cats_adv

    let is_adv dict cat =
        List.mem cat dict.cats_adv

    let add_prep dict cat =
        dict.cats_prep <- cat :: dict.cats_prep

    let is_prep dict cat =
        List.mem cat dict.cats_prep

    let add_num dict cat =
        dict.cats_num <- cat :: dict.cats_num

    let is_num dict cat =
        List.mem cat dict.cats_num

    let add_conj dict cat =
        dict.cats_conj <- cat :: dict.cats_conj

    let is_conj dict cat =
        List.mem cat dict.cats_conj

    let rec to_string dict =
        sprintf "Lexicon:\n%s\nAttributes:\n%s\nUncertainty: %s\nEnumeration: %s\nTransformation rules:\n%s\nAccepted Categories: %s"
            (to_string_lexdict dict.lexdict)
            (to_string_attrdict dict.attrdict)
            (to_string_unclexs dict.unclexs)
            (enumlevel_to_string dict.enumlevel)
            (to_string_unarydrv dict.unarydrv)
            (to_string_accepts dict.accepts)

    and to_string_lexdict lexdict =
        let lexs = List.sort Stdlib.compare 
            (StrDict.fold (fun lex _ v -> lex :: v) lexdict []) in
        String.concat "\n"
            (List.fold_right
                (fun lex v ->
                    let cats = StrDict.find lexdict lex in
                    let s = sprintf "    '%s' :- %s" lex
                        (String.concat ", " 
                            (List.map Syncat.to_string cats)) in
                    s :: v)
                lexs [])

    and to_string_attrdict attrdict =
        let attrs = List.sort Stdlib.compare
            (StrDict.fold (fun a _ v -> a :: v) attrdict []) in
        String.concat "\n"
            (List.fold_right
                (fun attr v ->
                    let avms = StrDict.find attrdict attr in
                    let s = sprintf "    '%s' :- %s" attr (String.concat ", " (List.map Avm.to_string avms)) in
                    s :: v)
                attrs [])

    and to_string_unclexs unclexs =
        String.concat ", " 
            (List.map (fun s -> sprintf "'%s'" s) unclexs)

    and to_string_allcats allcats =
        String.concat ", "
            (List.map (Syncat.to_string) allcats)
        ^ (sprintf " (%d categories)" (List.length allcats))

    and to_string_unarydrv unarydrv =
        String.concat "\n"
            (List.map
                (fun (syncat, newsyncats) ->
                    Printf.sprintf "    %s => {%s}"
                        (Syncat.to_string syncat)
                        (String.concat ", " (List.map Syncat.to_string newsyncats)))
                unarydrv)

    and to_string_accepts accepts =
        String.concat ", " 
            (List.map (Syncat.to_string) accepts)

    let get_enumlevel dict =
        dict.enumlevel

    let set_enumlevel dict enumlevel =
        dict.enumlevel <- enumlevel

    let mem dict word =
        StrDict.mem dict.lexdict word

    let find dict word =
        StrDict.find dict.lexdict word

    let is_uncertain dict word =
        List.mem word dict.unclexs

    let add_cat dict cat =
        if not (List.mem cat dict.allcats) then
            dict.allcats <- cat :: dict.allcats
        else ()

    let add_entry dict word cat =
        let oldset =
            if mem dict word then find dict word
            else [] in
        if not (List.mem cat oldset) then begin
            let newset = cat :: oldset in
            StrDict.replace dict.lexdict word newset;
            add_cat dict cat
        end else ()

    let add_attr dict attr avm =
        let avms =
            if StrDict.mem dict.attrdict attr then
                StrDict.find dict.attrdict attr
            else []
        in
        if not (List.mem avm avms) then
            StrDict.replace dict.attrdict attr (avm :: avms)
        else ()

    let add_accept dict cat =
        if not (List.mem cat dict.accepts) then begin
            dict.accepts <- cat :: dict.accepts;
            let acccat = 
                Syncat.create_bwd Syncat.root_cat cat Syncat.Left in
            add_entry dict ending_mark acccat
        end else ()

    let is_leftser dict cat =
        List.mem cat dict.leftser

    let is_rightser dict cat =
        List.mem cat dict.rightser

    let add_leftser dict cat =
        if not (is_leftser dict cat) then
            dict.leftser <- cat :: dict.leftser
        else ()

    let add_rightser dict cat =
        if not (is_rightser dict cat) then
            dict.rightser <- cat :: dict.rightser
        else ()

    let add_uncertain dict word =
        if not (List.mem word dict.unclexs) then
            dict.unclexs <- word :: dict.unclexs
        else ()

    let add_unarydrv dict syncat newcat =
        if not (List.mem_assoc syncat dict.unarydrv) then
            dict.unarydrv <- (syncat, [newcat]) :: dict.unarydrv
        else begin
            let entry = List.assoc syncat dict.unarydrv in
            if not (List.mem newcat entry) && not (Syncat.equal syncat newcat) then
                dict.unarydrv <- (syncat, newcat :: entry) :: (List.remove_assoc syncat dict.unarydrv)
            else ()
        end

    let get_cats_noenum dict word =
        if mem dict word then
            StrDict.find dict.lexdict word
        else []

    let get_cats dict word =
        match dict.enumlevel with
            NoEnum | EnumOnUncertain -> 
                get_cats_noenum dict word
          | EnumOnAll -> 
                if word = ending_mark then get_cats_noenum dict word
                else dict.allcats

    let get_avms dict attr =
        if StrDict.mem dict.attrdict attr then
            StrDict.find dict.attrdict attr
        else
            try
                let idx = String.index attr '=' in
                let a = String.sub attr 0 idx
                and v = String.sub attr (idx + 1) (String.length attr - idx - 1) in
                [Avm.create [(a, v)]]
            with Not_found -> [Avm.create []]

    let get_allcats dict =
        match dict.enumlevel with
            NoEnum -> []
          | EnumOnUncertain | EnumOnAll -> dict.allcats

    let get_accepts dict =
        dict.accepts

    let get_unarydrv dict =
        dict.unarydrv

    let is_allowed_univdep dict headcat depcat =
        (headcat = "ROOT" 
            && (is_aux dict depcat || is_v dict depcat))
        || (is_v dict headcat 
            && (is_n dict depcat || is_adv dict depcat || is_v dict depcat))
        || (is_n dict headcat
            && (is_adj dict depcat || is_n dict depcat || is_num dict depcat))
        || (is_prep dict headcat && is_n dict depcat)
        || (is_adj dict headcat && is_adv dict depcat)

    let add_catdict dict word tag =
        if not (StrDict.mem dict.catdict word) then
            StrDict.replace dict.catdict word []
        else ();
        let values = StrDict.find dict.catdict word in
        if not (List.mem tag values) then
            StrDict.replace dict.catdict word (tag :: values)
        else ()

    let iter_catdict dict fn =
        StrDict.iter fn dict.catdict

    let get_catdict dict word =
        if not (StrDict.mem dict.catdict word) then []
        else StrDict.find dict.catdict word

end

(** Inside-Outside Score Calculation *)

(** Score Instance *)
module ScoreInst :
sig

    (** Position span *)
    type span_t = int * int

    (** Syntactic category *)
    type syncat_t = Cg.Syncat.t
    
    (** Lexical item *)
    type word_t = string
    
    (** Score instance *)
    type t
    
    (** Create a score instance with a span, a syntactic 
        category, and a head word. *)
    val create : span_t -> syncat_t -> word_t -> t
    
    (** Get the span from the score instance. *)
    val get_span : t -> span_t
    
    (** Get the syntactic category from the score instance. *)
    val get_syncat : t -> syncat_t
    
    (** Get the head word from the score instance. *)
    val get_head : t -> word_t
    
    (** Check if two score instances equal. *)
    val equal : t -> t -> bool
    
    (** Calculate the hash value of the score instance. *)
    val hash : t -> int

    val to_string : t -> string

end

(** Score Table *)
module ScoreTbl :
sig
    
    (** Score instance *)
    type inst_t = ScoreInst.t
    
    (** Score table *)
    type t
    
    (** Create a probability table with the size and the default value. *)
    val create : int -> float -> t
    
    (** Get the default value. *)
    val get_epsilon : t -> float
    
    (** Check if an instance is a member of the table. *)
    val mem : t -> inst_t -> bool
    
    (** Get the probability of the instance. *)
    val get_prob : t -> inst_t -> float
    
    (** Get the smoothened probability of the instance. *)
    val get_smooth_prob : t -> inst_t -> float
    
    (** Set the probability of the instance. *)
    val set_prob : t -> inst_t -> float -> unit
    
    (** Accumulate the probability of the instance. *)
    val acc_prob : t -> inst_t -> float -> unit

    val to_string : t -> string

end

(** Compute the inside and outside score tables from the size, 
    the default value, the generative model of derivations, 
    the chart, and the start syntactic category. Please note that
    all scores are calculated based on smoothened probabilities. *)
val compute :
    int -> float -> Deriv.DerivGenModel.t -> Chart.Chart.t -> 
    Cg.Syncat.t -> ScoreTbl.t * ScoreTbl.t

val compute_insc :
    int -> float -> Deriv.DerivGenModel.t -> Chart.Chart.t -> 
    Cg.Syncat.t -> ScoreTbl.t

open Cg
open Models
open Conlldep
open Uboundrunner
open Printf

let main =
    let output = ref None 
    and depfname = ref None
    and fnames = ref [] in
    let args = [
        ("--quiet", Arg.Unit Scale.go_quiet, "  Quiet mode (default = false)");
        ("--golddeps",
            Arg.String (fun p -> depfname := Some p),
            "[deps.txt]   Specify the gold standard dependency in the CoNLL2006 format (required)");
        ("--output",
            Arg.String (fun o -> output := Some o),
            "[output.treedb]   Specify the output tree database (required)")
    ]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg =
        "Run the upper bound parser.\n\n" 
        ^ (sprintf "Usage: %s [options] text1 text2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    match !depfname, !output with
        Some goldfname, Some treedbfname ->
        begin
            Scale.print (sprintf "[1mBuilding the upper bound parses from the charts [%s] with the gold standard dependency [%s].[0m\n"
                (String.concat ", " !fnames) goldfname);
            let deps = ConllDep.from_file goldfname in
            Scale.print ">> Progress: ";
            let vtb = DecoderRunner.create deps !fnames treedbfname in
            DecoderRunner.run vtb;
            Scale.print "Done\n"
        end
      | _ -> Arg.usage args usagemsg

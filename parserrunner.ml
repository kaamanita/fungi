open Cg
open Dict
open Dictloader
open Chart
open Cky
open Chartdb
open Utils
open Printf

module ParserRunner = struct

    type syncat_t = Syncat.t

    type t = {
        dict     : Dict.t;
        chartdb  : ChartDb.t;
        cdbfname : string;
        fnames   : string list;
        start    : syncat_t;
    }

    let create dictfname cdbfname fnames start =
    {
        dict     = DictLoader.load dictfname false;
        chartdb  = ChartDb.create cdbfname;
        cdbfname = cdbfname;
        fnames   = fnames;
        start    = start;
    }

    let tryparse_elt dict sent words enumlevel start =
        Dict.set_enumlevel dict enumlevel;
        let chart = CKY.parse ~transf:(Dict.get_unarydrv dict) sent words dict start in
        (chart, Chart.is_success chart)
    
    let rec tryparse dict sent words enumlevels start result =
        match enumlevels with
            [] -> result
          | enumlevel :: rest ->
                let (chart, success) = tryparse_elt dict sent words enumlevel start in
                if success then 
                    tryparse dict sent words [] start (Some chart, enumlevel, success)
                else 
                    tryparse dict sent words rest start (Some chart, enumlevel, success)
    
    let list_enumlevels enumlevel =
        match enumlevel with
            Dict.NoEnum -> 
                [Dict.NoEnum]
          | Dict.EnumOnUncertain -> 
                [Dict.NoEnum; Dict.EnumOnUncertain]
          | Dict.EnumOnAll -> 
                [Dict.NoEnum; Dict.EnumOnUncertain; Dict.EnumOnAll]
    
    let repr_enumlevel chart enumlevel =
        if Chart.is_filledup chart then
            "[1mFILLED[0m"
        else
            Dict.enumlevel_to_string enumlevel

    let parse dict chartdb sent words start =
        let enumlevel = Dict.get_enumlevel dict in
        let enumlevels = list_enumlevels enumlevel in
        let (ochart, enumlevel', success) = 
            tryparse dict sent words enumlevels start (None, Dict.NoEnum, false) in
        Dict.set_enumlevel dict enumlevel;
        match ochart with
            Some chart -> 
            begin
                ChartDb.add chartdb chart;
                (success, enumlevel', chart)
            end
          | None -> failwith "parse"
    
    let parse_file dict chartdb start fname =
        let fhdl = open_in fname in
        let cnt = ref 0 
        and cnt_filled = ref 0 in
        Scale.print (sprintf "Parsing file: %s\n" fname);
        try
            while true do
                flush stdout;
                incr cnt;
                let line = input_line fhdl in
                let sent = split line in
                Scale.print (sprintf "Line %d: [%s] ... " !cnt (String.concat " " sent));
                let starttime = Unix.time () in
                let tags = only_tags sent 
                and words = only_words sent in
                let (success, enumlevel, chart) = parse dict chartdb tags words start in
                let endtime = Unix.time () in
                let runtime = endtime -. starttime in
                if success then begin
                    Scale.print (sprintf "successful at %s (%d derivations, runtime = %.2f secs).\n" 
                        (repr_enumlevel chart enumlevel)
                        (Chart.get_derivcnt chart)
                        runtime);
                    if (Chart.is_filledup chart) then
                        incr cnt_filled
                    else ()
                end else begin
                    Scale.print (sprintf "[1munsuccessful at %s (runtime = %.2f secs).[0m\n" 
                        (Dict.enumlevel_to_string enumlevel) runtime)
                end;
                flush stderr
            done
        with End_of_file -> begin
            Scale.print (sprintf "No. filled-up charts = %d\n" !cnt_filled);
            close_in fhdl
        end

    let run psr =
        List.iter (parse_file psr.dict psr.chartdb psr.start) psr.fnames;
        ChartDb.to_file psr.chartdb psr.cdbfname

    let compute dictfname cdbfname fnames start =
        let psr = create dictfname cdbfname fnames start in
        run psr

end

open Cg
open Dict
open Dictloader
open Models
open Annealrunner
open Printf

let main =
    let probflags = ref []
    and inoutmode = ref StandardInOut
    and babystep = ref false
    and leapfrogthr = ref 15
    and annmode = ref NoAnn
    and diffthr = ref 1e-6
    and rndthr = ref 5
    and epsilon = ref 1e-6
    and start = ref (Syncat.create_atom "ROOT")
    and dictfname = ref ""
    and defprior = ref 10.0
    and knownderivprior = ref 1.0
    and startderivprior = ref 10000.0
    and initparam = ref 0.0
    and epoch = ref 0.0
    and catconst = ref 0.0
    and finalparam = ref 0.0
    and fnames = ref []
    and ofname = ref "" in
    let args = [
        ("--quiet", Arg.Unit Scale.go_quiet, "  Quiet mode (default = false)");
        ("--seedlvl",
            Arg.Int Utils.set_seedlvl,
            "  Set the seed level (default = 100)");
        ("--probftr", 
            Arg.Symbol (
                ["roleemis"; "hockenmaier"; "lexemis"; "heademis"], 
                set_probflags probflags), 
            "  Enable the specified probability feature");
        ("--model",
            Arg.Symbol (
                ["0"; "1"; "2"; "3"; "4"; "5"],
                fun x -> set_probflags probflags ("model" ^ x)),
            "  Enable the specified model (a set of probability features)");
        ("--priormult",
            Arg.Set_float catconst,
            "[float]   Set the value of the prior multiplier (default = 0.0)");
        ("--inout",
            Arg.Symbol (["standard"; "vb"], set_inoutmode inoutmode),
            "  Select the Inside/Outside Algorithm (default = standard)");
        ("--babystep",
            Arg.Set babystep,
            "  Use the Babystep estimation (Spitkovsky et al, 2010) (default = false)");
        ("--leapfrogthr",
            Arg.Set_int leapfrogthr,
            "[int]   Set the leapfrog threshold (default = 15)");
        ("--annealing", 
            Arg.Symbol (["none"; "da"; "sda"; "sa"], set_annmode annmode), 
            "  Select the simulated annealing algorithm (default = none)");
        ("--diffthr",
            Arg.Set_float diffthr,
            "[float]   Set the termination threshold of expectation difference measured by Kullback-Leibler distance (default = 1e-6)");
        ("--rndthr",
            Arg.Set_int rndthr,
            "[int]   Set the termination threshold of iteration number (default = 5)");
        ("--epsilon",
            Arg.Set_float epsilon,
            "[float]   Set the value of epsilon (a very small value) (default = 1e-6)");
        ("--defprior",
            Arg.Set_float defprior,
            "[float]   Set the default rule prior (default = 10.0)");
        ("--knownderivprior",
            Arg.Set_float knownderivprior,
            "[float]   Set the prior for known lexicon items (default = 1.0)");
        ("--startderivprior",
            Arg.Set_float startderivprior,
            "[float]   Set the prior for sentence formation derivations (default = 10000.0)");
        ("--initparam",
            Arg.Set_float initparam,
            "[float]   Set the simulated annealing's initial parameter value (required by da, sda, and sa)");
        ("--epoch",
            Arg.Set_float epoch,
            "[float]   Set the simulated annealing's epoch value (required by da, sda, and sa)");
        ("--finalparam",
            Arg.Set_float finalparam,
            "[float]   Set the simulated annealing's final parameter value (required by sa)");
        ("--dict",
            Arg.Set_string dictfname,
            "[somefile.txt]   Set the dictionary file name (required)");
        ("--output",
            Arg.Set_string ofname,
            "[output.probdb]   Set the output file name for the result probability table (required)")
    ]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg =
        "\nTrain the model with the specified chart databases.\n\n"
        ^ (sprintf "Usage: %s [options] chartdb1 chartdb2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    Utils.set_catconst !catconst;
    match !dictfname, !ofname with
        "", "" -> Arg.usage args usagemsg
      | _, _ -> ();
    match !annmode, !initparam, !epoch, !finalparam with
        DetAnn, _, 0.0, _
      | DetAnn, 0.0, _, _
      | SkewedDetAnn, _, 0.0, _
      | SkewedDetAnn, 0.0, _, _
      | StructAnn, _, 0.0, _ -> Arg.usage args usagemsg
      | _, _, _, _ -> ();
    let annparam = conv_annparam !annmode !initparam !epoch !finalparam in
    let dict = DictLoader.load !dictfname false in
    Scale.print (sprintf "[1mTraining the model on [%s].[0m\n>> " (String.concat ", " !fnames));
    leapfrogthr := !leapfrogthr + 1;
    let starttime = Unix.time () in
    AnnealRunner.compute 
        !probflags !inoutmode !babystep annparam !diffthr !rndthr !epsilon 
        !start dict !defprior !knownderivprior !startderivprior !leapfrogthr
        !fnames !ofname;
    let endtime = Unix.time () in
    Scale.print ">> Done\n";
    let duration = endtime -. starttime in
    Scale.print (sprintf ">> Total runtime: %.2f mins\n" (duration /. 60.0))


open Cg
open Chart
open Prob
open Deriv
open Printf

type probflag_t = RoleEmis | JuliaEmis | LexEmis | HeadEmis

module RoleConsq = struct

    type syncat_t = Syncat.t

    type word_t = string

    type prems_t = syncat_t * word_t

    type t = 
        Head    (** Head *)
      | Dep     (** Dependent *)
      | Conn    (** Connective *)
      | Lex     (** Lexicon *)

    let to_string role =
        match role with
            Head -> "head"
          | Dep -> "dep"
          | Conn -> "conn"
          | Lex -> "lex"
    
    let prems_to_string prems =
        let (syncat, word) = prems in
        sprintf "%s{%s}" (Syncat.to_string syncat) word

    let equal_prems prems1 prems2 = 
        let (syncat1, word1) = prems1
        and (syncat2, word2) = prems2 in
        Syncat.equal syncat1 syncat2 && word1 = word2

    let hash_prems prems =
        let (syncat, word) = prems in
        Syncat.hash syncat + Hashtbl.hash word

    let equal = Stdlib.(=)

    let hash = Hashtbl.hash

    let map_symbols_to_ftrs dtrs (ftrs : t list) =
        List.map2
            (fun dtr ftr ->
                (dtr, ftr))
            dtrs ftrs

    let extract_mtrftrs deriv = []

    let extract_dtrftrs_from_dtrs consq =
        let dtrcats = List.map 
            (fun (syncat, word) -> (syncat, word)) 
            (Deriv.get_dtrs consq)
        and depdir = Deriv.get_depdir consq in
        match depdir with
            Syncat.Left | Syncat.RightSerial ->
                map_symbols_to_ftrs dtrcats [Head; Dep]
          | Syncat.Unary ->
                map_symbols_to_ftrs dtrcats [Head]
          | Syncat.Coor ->
                map_symbols_to_ftrs dtrcats [Head; Conn; Dep]
          | Syncat.Right | Syncat.LeftSerial ->
                map_symbols_to_ftrs dtrcats [Dep; Head]

    let extract_dtrftrs deriv =
        let consq = Deriv.get_consq deriv in
        if Deriv.is_consq_dtrs consq then
            extract_dtrftrs_from_dtrs consq
        else []

end

module JuliaConsq = struct

    type syncat_t = Syncat.t

    type word_t = string

    type prems_t = syncat_t * word_t

    type t = 
        Head            (** Head *)
      | Dep of word_t   (** Dependent *)
      | Conn            (** Connective *)
      | Lex             (** Lexicon *)

    let to_string role =
        match role with
            Head -> "head"
          | Dep head -> sprintf "dep %s" head
          | Conn -> "conn"
          | Lex -> "lex"
    
    let prems_to_string prems =
        let (syncat, word) = prems in
        sprintf "%s{%s}" (Syncat.to_string syncat) word

    let equal_prems prems1 prems2 = 
        let (syncat1, word1) = prems1
        and (syncat2, word2) = prems2 in
        Syncat.equal syncat1 syncat2 && word1 = word2

    let hash_prems prems =
        let (syncat, word) = prems in
        Syncat.hash syncat + Hashtbl.hash word

    let equal = Stdlib.(=)

    let hash = Hashtbl.hash

    let map_symbols_to_ftrs dtrs (ftrs : t list) =
        List.map2
            (fun dtr ftr ->
                (dtr, ftr))
            dtrs ftrs

    let extract_mtrftrs deriv = []

    let extract_dtrftrs_from_dtrs head consq =
        let dtrcats = List.map 
            (fun (syncat, word) -> (syncat, word)) 
            (Deriv.get_dtrs consq)
        and depdir = Deriv.get_depdir consq in
        match depdir with
            Syncat.Left | Syncat.RightSerial ->
                map_symbols_to_ftrs dtrcats [Head; Dep head]
          | Syncat.Unary ->
                map_symbols_to_ftrs dtrcats [Head]
          | Syncat.Coor ->
                map_symbols_to_ftrs dtrcats [Head; Conn; Dep head]
          | Syncat.Right | Syncat.LeftSerial ->
                map_symbols_to_ftrs dtrcats [Dep head; Head]

    let extract_dtrftrs deriv =
        let prems = Deriv.get_prems deriv
        and consq = Deriv.get_consq deriv in
        let head = Deriv.get_head prems in
        if Deriv.is_consq_dtrs consq then
            extract_dtrftrs_from_dtrs head consq
        else []

end

module LexConsq = struct

    type prems_t = Syncat.t

    type t = string

    let equal_prems = Syncat.equal

    let hash_prems = Syncat.hash

    let equal = Stdlib.(=)

    let hash = Hashtbl.hash

    let to_string lex = lex
    
    let prems_to_string = Syncat.to_string

    let extract_mtrftrs deriv =
        let prems = Deriv.get_prems deriv
        and consq = Deriv.get_consq deriv in
        if Deriv.is_consq_lex consq then
            let word = Deriv.get_word consq in
            [(Deriv.get_syncat prems, word)]
        else []

    let extract_dtrftrs deriv = []

end

module HeadConsq = struct

    type prems_t = Syncat.t

    type word_t = string

    type t = string

    let to_string head = head
    
    let prems_to_string = Syncat.to_string

    let equal_prems = Syncat.equal

    let hash_prems = Syncat.hash

    let equal = Stdlib.(=)

    let hash = Hashtbl.hash

    let extract_mtrftrs deriv =
        let prems = Deriv.get_prems deriv in
        let head = Deriv.get_head prems in
        [(Deriv.get_syncat prems, head)]

    let extract_dtrftrs deriv = []

end

module RoleFtr = DerivFtr(RoleConsq)
module JuliaFtr = DerivFtr(JuliaConsq)
module LexFtr = DerivFtr(LexConsq)
module HeadFtr = DerivFtr(HeadConsq)

let create_pcfg n epsilon =
    DerivGenModel.create n epsilon []

let create_model1 n epsilon =
    DerivGenModel.create n epsilon []

let create_model2 n epsilon =
    DerivGenModel.create n epsilon [RoleFtr.create n epsilon]

let create_model3 n epsilon =
    DerivGenModel.create n epsilon [LexFtr.create n epsilon]

let create_model4 n epsilon =
    DerivGenModel.create n epsilon 
        [RoleFtr.create n epsilon; LexFtr.create n epsilon]

let create_model5 n epsilon =
    DerivGenModel.create n epsilon [HeadFtr.create n epsilon]

let create_model6 n epsilon =
    DerivGenModel.create n epsilon 
        [RoleFtr.create n epsilon; HeadFtr.create n epsilon]

let conv_probflag probflag n epsilon =
    match probflag with
      RoleEmis -> RoleFtr.create n epsilon
    | JuliaEmis -> JuliaFtr.create n epsilon
    | LexEmis -> LexFtr.create n epsilon
    | HeadEmis -> HeadFtr.create n epsilon

let conv_probflags probflags n epsilon =
    List.map 
        (fun probflag -> conv_probflag probflag n epsilon)
        probflags

let create_model n epsilon probflags =
    let probftrs = conv_probflags probflags n epsilon in
    DerivGenModel.create n epsilon probftrs

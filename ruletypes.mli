(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

type word_t = string

type macroname_t = string

type syncat_t = Cg.Syncat.t

type depdir_t = Cg.Syncat.depdir_t

type macrosyncat_t = Macro.MacroSyncat.t

type dictentry_t =
    LexEntry of word_t * macrosyncat_t list
  | MacroEntry of macroname_t * macrosyncat_t list
  | TransfEntry of macrosyncat_t * macrosyncat_t list
  | UncEntry of word_t list
  | EnumEntry of Dict.Dict.enumlevel_t
  | AddAttr of string * Avm.Avm.t list
  | AddCats of macrosyncat_t list
  | AddLeftPuncs of word_t list
  | AddRightPuncs of word_t list
  | AddLeftSer of macrosyncat_t list
  | AddRightSer of macrosyncat_t list
  | Accept of macrosyncat_t list
  | Typology of (string * string list) list
  | DMV of string list
  | EVG of string list
  | GHG of (string * string list) list
  | EnableSplitHead
  | DisableSplitHead

type tree_t =
    TreeNode of word_t * tree_t list
  | LexNode of word_t

type deptree_t =
    DepTreeNode of word_t * deptree_t * deptree_t * depdir_t
  | DepCoorNode of word_t * deptree_t * deptree_t * deptree_t
  | DepUnaryNode of word_t * deptree_t
  | DepLexNode of word_t

(** Chart and Item *)

(** Chart Item Module *)
module Item :
sig
    (** Item ID *)
    type id_t = int
    
    (** Syntactic category *)
    type syncat_t = Cg.CG.syncat_t
    
    (** Dependency direction *)
    type depdir_t = Cg.CG.depdir_t
    
    (** Position span *)
    type span_t = int * int
    
    (** Lexical item *)
    type word_t = string
    
    (** Daughter of production *)
    type proddtr_t
    
    (** Create a lexical-item daughter. *)
    val create_proddtr_lex : word_t -> proddtr_t
    
    (** Create an item-list daughter. *)
    val create_proddtr_ids : id_t list -> proddtr_t

    val to_string_proddtr : proddtr_t -> string
    
    (** Check if it is a lexical-item daughter. *)
    val is_proddtr_lex : proddtr_t -> bool
    
    (** Check if it is an item-list daughter. *)
    val is_proddtr_ids : proddtr_t -> bool
    
    (** Item production *)
    type prod_t
    
    (** Create an item production with the list of daughter item IDs 
        and the position of the syntactic head. *)
    val create_prod_ids : id_t list -> int -> depdir_t -> prod_t
    
    (** Create an item production with the lexical item and 
        its position. *)
    val create_prod_lex : word_t -> int -> prod_t

    val to_string_prod : prod_t -> string
    
    (** Get the daughter IDs from the production. *)
    val get_proddtr : prod_t -> proddtr_t
    
    (** Get the position of the syntactic head from the production. *)
    val get_headpos : prod_t -> int
    
    (** Get the dependency direction from the production. *)
    val get_depdir : prod_t -> depdir_t
    
    (** Check if the production is a terminal node. *)
    val is_terminal : prod_t -> bool
    
    (** Get a word from the production if it is a terminal node. *)
    val get_word : prod_t -> word_t
    
    (** Get the daughter item list from the production if 
        it is not a terminal node. *)
    val get_ids : prod_t -> id_t list

    (** Chart item *)
    type t
    
    (** Create a chart item with ID, syntactic category, span, and 
        productions. *)
    val create : id_t -> syncat_t -> span_t -> int -> prod_t list -> t

    val to_string : t -> string
    
    (** Get the item ID. *)
    val get_id : t -> id_t
    
    (** Get the syntactic category of the item. *)
    val get_syncat : t -> syncat_t
    
    (** Get the position span of the item. *)
    val get_span : t -> span_t

    val get_headpos_of_item : t -> int
    
    (** Get the production list of the item. *)
    val get_prods : t -> prod_t list
    
    (** Check if the item is a terminal node. *)
    val is_term : t -> bool
    
    (** Add a new production to the item. *)
    val add_prod : t -> prod_t -> unit

    val set_prods : t -> prod_t list -> unit
    
    (** Check if two items equal. Two items are said to equal if they 
        have the same IDs, syntactic categories, and spans. *)
    val equal : t -> t -> bool
    
    (** Calculate the hash value of the item. *)
    val hash : t -> int
end

(** Packed Chart Module *)
module Chart :
sig
    (** Item *)
    type item_t = Item.t
    
    (** Item ID *)
    type id_t = Item.id_t
    
    (** Span *)
    type span_t = int * int
    
    (** Production *)
    type prod_t = Item.prod_t
    
    (** Syntactic category *)
    type syncat_t = Item.syncat_t

    (** Lexical item *)
    type word_t = string

    (** Dependency direction *)
    type depdir_t = Item.depdir_t
    
    (** Chart *)
    type t

    type dtrargs_t = (syncat_t * span_t * int * word_t) list
    
    (** Function for dealing with lexicon items: 
        syncat -> span -> headpos -> word *)
    type fn_word_t = syncat_t -> span_t -> int -> word_t -> unit

    (** Function for dealing with all other derivations:
        syncat -> span -> headpos -> head -> depdir -> 
        (syncat, span, headpos, head) list *)
    type fn_dtrs_t = 
        syncat_t -> span_t -> int -> word_t -> depdir_t ->
        dtrargs_t -> unit
    
    (** Search strategy. *)
    type srchstr_t = 
        BottomUp    (** Bottom-up left-first search *)
      | TopDown     (** Top-down left-first search *)

    type enumlevel_t = Dict.Dict.enumlevel_t

    (** Create a new chart with the sentence. *)
    val create : word_t list -> word_t list -> t

    val length : t -> int

    val to_string : t -> string

    val set_enumlevel : t -> enumlevel_t -> unit

    val get_enumlevel : t -> enumlevel_t

    val get_derivcnt : t -> int

    (** Get the sentence from the chart. *)
    val get_sentence : t -> word_t list
    
    val get_words : t -> word_t list

    val get_nonterms : t -> syncat_t list
    
    (** Get an item by its ID. *)
    val get_item : t -> id_t -> item_t
    
    (** Get all item IDs. *)
    val get_all_ids : t -> id_t list
    
    (** Check if a span is in the chart. *)
    val mem : t -> span_t -> bool
    
    (** Find an item which has the specified syntactic category in 
        the specified span. *)
    val find_syncat_in_span : t -> span_t -> syncat_t -> int -> item_t
    
    (** Add a derivation (syntactic category generating a production 
        on the specific span) to the chart. *)
    val add_deriv : t -> span_t -> syncat_t -> int -> prod_t -> unit
    
    val add_deriv_ret_item : t -> span_t -> syncat_t -> int -> prod_t -> item_t
    
    (** Get all item IDs from the span. *)
    val get_ids_from_span : t -> span_t -> id_t list
    
    (** Get all items from the span. *)
    val get_items : t -> span_t -> item_t list
    
    (* Eliminate all unreachable items from the chart. *)
    (* val elim_unreachable_items : t -> span_t -> unit *)
    val elim_excess : t -> syncat_t -> unit

    (** Iterate all derivations in the chart with the specified 
        search strategy (bottom-up or top-down) beginning 
        from the specified span with two functions: one dealing 
        with preterminal derivations and the other one dealing 
        with all other derivations. *)
    val iter_derivs : 
        t -> srchstr_t -> fn_word_t -> fn_dtrs_t -> span_t -> unit

    (** Iterate all derivations in the chart only in the 
        specified span with two functions: one dealing 
        with preterminal derivations and the other one dealing 
        with all other derivations. *)
    val iter_derivs_onelevel : 
        ?backoff:bool -> t -> fn_word_t -> fn_dtrs_t -> span_t -> unit

    (** Check if at least one successful parse is obtained. *)
    val is_success : t -> bool

    val is_filledup : t -> bool

    val set_filledup : t -> unit

    val count_derivs : t -> unit
end

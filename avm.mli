(** Attribute Value Matrix. *)

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

(** Attribute Value Matrix. *)
module Avm :
sig

    (** Attribute value matrix. *)
    type t

    (** [create avl] creates an attribute value matrix from a list of
        (attribute * value) pairs [avl]. *)
    val create : (string * string) list -> t

    (** [create_empty ()] creates an empty attribute value matrix. *)
    val create_empty : unit -> t

    (** [is_empty avm] checks if an attribute value matrix [avm] is
        empty. *)
    val is_empty : t -> bool

    (** [subsumes avm1 avm2] checks if an attribute value matrix [avm1]
        subsumes another [avm2]. [avm1] is said to subsume [avm2] if
        all its attribute-value pairs are also a member of [avm2]. A pair
        of values are said to match if they have the same value or at
        least one of them is "$" (value elimination). *)
    val subsumes : t -> t -> bool

    (** [subsumes_attrs avm1 avm2] checks if an attribute value matrix
        [avm1] subsumes another [avm2] by its attributes. [avm1] is said
        to subsume [avm2] if all its attributes are also a member of 
        [avm2]. *)
    val subsumes_attrs : t -> t -> bool

    (** [combine avm1 avm2] combines two attribute value matrices [avm1]
        [avm2]. There are four rules of combination as follows.
            (1) [[a : v] :: t1] + [avm2] = [[a : v]] :: ([t1] + [avm2])]
                if an attribute [a] is not a member [avm2].
            (2) [[a : v] :: t1] + [[a : v] :: t2] = 
                [[a : v]] :: ([t1] + [t2])
            (3) Deletion rule 1 (delete any value of [a]):
                [[a : "$"] :: t1] + [[a : v] :: t2]
                = [[a : v] :: t1] + [[a : "$"] :: t2]
                = [t1] + [t2]
            (4) Deletion rule 2 (delete [a] if it has the value [v]):
                [[a : "$"v] :: t1] + [[a : v] :: t2]
                = [[a : v] :: t1] + [[a : "$"v] :: t2]
                = [t1] + [t2]
        where + is short for 'combine'. [combine] is a symmetric 
        function. *)
    val combine : t -> t -> t

    (** [replace avm1 avm2] replaces an attribute-value matrix [avm1]
        with another [avm2]. 
            (1) [[a : v] :: t1] -> [avm2] = [[a : v]] :: ([t1] + [avm2])]
                if an attribute [a] is not a member [avm2].
            (2) [[a : v] :: t1] -> [[a : v] :: t2] = 
                [[a : v]] :: ([t1] + [t2])
            (3) Deletion rule 1 (delete any value of [a]):
                [[a : v] :: t1] -> [[a : "$"] :: t2]
                = [t1] + [t2]
            (4) Deletion rule 2 (delete [a] if it has the value [v]):
                [[a : v] :: t1] + [[a : "$"v] :: t2]
                = [t1] + [t2]
        where -> represents 'is replaced with'. [replace] is an asymmetric
        function. *)
    val replace : t -> t -> t

    (** [to_string avm] represents an attribute value matrix [avm] with
        a string representation. *)
    val to_string : t -> string

    (** [to_string_cat avm] represents an attribute value matrix [avm]
        with a string representation. This is used in displaying a
        syntactic category only. *)
    val to_string_cat : t -> string

    (** [equal avm1 avm2] checks if two attribute value matrices [avm1]
        [avm2] are equal. *)
    val equal : t -> t -> bool

    (** [hash avm] computes the hash value of an attribute value matrix
        [avm]. *)
    val hash : t -> int

end

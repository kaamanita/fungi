(** Syntactic Tree *)

(** Tree Module *)
module Tree :
sig

    (** Syntactic category *)
    type syncat_t = Cg.CG.syncat_t
    
    (** Dependency direction *)
    type depdir_t = Cg.CG.depdir_t
   
    (** Span *)
    type span_t = Chart.Chart.span_t
    
    (** Lexical item *)
    type word_t = Dict.Dict.word_t

    type depstruct_t = Dep.DepStruct.t
    
    (** Tree *)
    type t
    
    (** Create a terminal node from a word and its span. *)
    val create_term : word_t -> span_t -> t
    
    (** Create a nonterminal node from a syntactic category, a word,
        a list of daughters, its span, its head's position, and its
        dependency direction. *)
    val create_nonterm :
        syncat_t -> word_t -> t list -> span_t -> int -> depdir_t -> t
    
    (** Check if a node is a terminal one. *)
    val is_term : t -> bool
    
    (** Check if a node is a nonterminal one. *)
    val is_nonterm : t -> bool
    
    (** Get the head word of the tree. *)
    val get_word : t -> word_t
    
    (** Get the span of the tree. *)
    val get_span : t -> span_t
    
    (** Get the syntactic category of the nonterminal node. *)
    val get_syncat : t -> syncat_t
    
    (** Get the daughter list of the nonterminal node. *)
    val get_dtrs : t -> t list
    
    (** Get the position of the head of the nonterminal node. *)
    val get_headpos : t -> int
    
    (** Get the dependency direction of the nonterminal node. *)
    val get_depdir : t -> depdir_t
    
    (** Represent a tree with a string. *)
    val to_string : t -> string

    val score_branching : t -> float -> float -> float
    
    (** Pretty-print the tree into a string. *)
    val pp : ?indent:int -> t -> string
    
    val pp_syncat : ?indent:int -> t -> string

    val to_depstr : t -> depstruct_t

    val to_ubrackets : t -> span_t list

    val to_brackets : t -> (span_t * word_t) list

    val to_catwordlist : t -> (syncat_t * word_t) list

    val to_chart : t -> string list -> Chart.Chart.t

    val to_conll : t -> string list -> string

    val pp_depstruct : ?highlight:bool -> ?showcat:bool -> ?comment:bool -> t -> string list -> string

end

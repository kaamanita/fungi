(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Dep
open Conlldep
open Printf

let load_content elimpunc sentlen fnames =
    let content = ConllDep.compile_many fnames in
    let content = ConllDep.elim_longsents elimpunc sentlen content in
    (* printf "    Size = %d\n" (List.length content); *)
    flush stdout;
    content

let divide_content content nosections =
    Scale.add (Scale.WindMill 1000) "Dividing content ... " "";
    let size = List.length content in
    let size' =
        if size mod nosections = 0 then size
        else size + nosections - (size mod nosections) in
    let divsize = size' / nosections in
    let divsents = Array.make nosections [] in
    let diventries = Array.make nosections [] in
    let id = ref 0 in
    List.iter
        (fun entries ->
            Scale.inc ();
            Scale.display ();
            let (_, sent, _, _) = entries in
            let divid = !id / divsize in
            divsents.(divid) <- sent :: divsents.(divid);
            diventries.(divid) <- entries :: diventries.(divid);
            incr id)
        content;
    Scale.discard ();
    for divid = 0 to (nosections - 1) do
        divsents.(divid) <- List.rev divsents.(divid);
        diventries.(divid) <- List.rev diventries.(divid)
    done;
    (divsents, diventries)

let write_to_files oprefix divsents diventries nosections =
    for i = 0 to (nosections - 1) do
        printf "Preparing section %d ... " (i + 1);
        flush stdout;
        let ofname_sents = 
            if nosections > 1 then sprintf "%s_sents_%d.txt" oprefix i
            else sprintf "%s_sents.txt" oprefix
        (* and ofname_tags = 
            if nosections > 1 then sprintf "%s_tags_%d.txt" oprefix i
            else sprintf "%s_tags.txt" oprefix *)
        and ofname_deps = 
            if nosections > 1 then sprintf "%s_conll_%d.txt" oprefix i 
            else sprintf "%s_conll.txt" oprefix
        in
        let ofhdl_sents = open_out ofname_sents
        (* and ofhdl_tags = open_out ofname_tags *)
        and ofhdl_deps = open_out ofname_deps in
        (*
        List.iter
            (fun sent ->
                if List.length sent > 0 then
                    fprintf ofhdl_sents "%s\n" (String.concat " " sent)
                else ()
            )
            divsents.(i);
        *)
        let sentid = ref 0 in
        List.iter
            (fun entries ->
                let (entrylist, _, _, _) = entries in
                if List.length entrylist > 0 then begin
                    incr sentid;
                    fprintf ofhdl_sents "%s\n" (ConllDep.entries_to_sents entries);
                    (* fprintf ofhdl_tags "%s\n" (ConllDep.entries_to_tags entries); *)
                    fprintf ofhdl_deps "# Sentence %d\n" !sentid;
                    fprintf ofhdl_deps "%s\n\n" (ConllDep.entries_to_conll entries)
                end else ()
            )
            diventries.(i);
        close_out ofhdl_sents;
        (* close_out ofhdl_tags; *)
        close_out ofhdl_deps;
        printf "done\n";
        printf "    Size = %d entries\n" (List.length divsents.(i));
        printf "    Sentence file: %s\n    Dependency file: %s\n"
            ofname_sents (* ofname_tags *) ofname_deps;
        flush stdout
    done

let write_desc oprefix content =
    let (postags, tags, attrs, deprels) = ConllDep.read_postags_deprels content in
    let ofname_postags = sprintf "%s_postags.txt" oprefix
    and ofname_tags = sprintf "%s_tags.txt" oprefix
    and ofname_attrs = sprintf "%s_attrs.txt" oprefix
    and ofname_deprels = sprintf "%s_deprels.txt" oprefix in
    let ofhdl_postags = open_out ofname_postags
    and ofhdl_tags = open_out ofname_tags
    and ofhdl_attrs = open_out ofname_attrs
    and ofhdl_deprels = open_out ofname_deprels in
    List.iter
        (fun postag ->
            fprintf ofhdl_postags "%s\n" postag)
        (List.sort Stdlib.compare postags);
    List.iter
        (fun tag ->
            fprintf ofhdl_tags "%s\n" tag)
        (List.sort Stdlib.compare tags);
    List.iter
        (fun attr ->
            fprintf ofhdl_attrs "%s\n" attr)
        (List.sort Stdlib.compare attrs);
    List.iter
        (fun deprel ->
            fprintf ofhdl_deprels "%s\n" deprel)
        (List.sort Stdlib.compare deprels);
    close_out ofhdl_postags;
    close_out ofhdl_tags;
    close_out ofhdl_attrs;
    close_out ofhdl_deprels

let prepare_files elimpunc desc oprefix sentlen fnames nosections =
    let content = load_content elimpunc sentlen fnames in
    let (divsents, diventries) = divide_content content nosections in
    if not desc then
        write_to_files oprefix divsents diventries nosections
    else
        write_desc oprefix content

let add_tagmap text =
    let tokens = Utils.split_delim "|" text in
    match tokens with
        [tagstr; attr; newtag] ->
            let tags = Utils.split_delim "," tagstr in
            List.iter
                (fun tag ->
                    ConllDep.add_tagmap tag attr newtag
                )
                tags
      | _ -> ()

let main =
    let fnames = ref [] in
    let nosections = ref 0 in
    let elimpunc = ref false in
    let desc = ref false in
    let oprefix = ref "output" in
    let sentlen = ref None in
    let args = [
        ("--prohtag",
            Arg.String (fun tag -> ConllDep.add_prohtag tag),
            "   Declare an additional prohibited tag of punctuation marks");
        ("--mwutag",
            Arg.String (fun tag -> ConllDep.add_mwutag tag),
            "   Declare an additional tag of multiple word units");
        ("--tagmap",
            Arg.String (fun tag -> add_tagmap tag),
            "\"Tag|Attr|NewTag\"   Convert the specified tag and attribute into a new tag (Attr can be specified as a single wildcard \'*\')");
        ("--nosections",
            Arg.Int (fun l -> nosections := l),
            sprintf 
                "[int]   Set the number of parts (must be greater than 0; default = %d)" 
                !nosections);
        ("--elimpunc",
            Arg.Set elimpunc,
            "   Eliminate all punctuation marks (default = false)");
        ("--desc",
            Arg.Set desc,
            "   Prepare only the description files (default = false)");
        ("--oprefix", 
            Arg.Set_string oprefix,
            "[path_prefix]   Set the path prefix for the output (default = output)");
        ("--sentlen",
            Arg.Int (fun l -> if l > 0 then sentlen := Some l else sentlen := None),
            "[sentlen]   Set the length of longest sentences allowed in the corpus (default = none)")
    ]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg =
        "Prepare the input files from CoNLL2006-formatted dependency files for an experiment.\n\n" 
        ^ "prepareconll [options] depfile1 depfile2 ..." in
    Arg.parse args fn_anon usagemsg;
    if !nosections < 0 then
        Arg.usage args usagemsg
    else ();
    prepare_files !elimpunc !desc !oprefix !sentlen !fnames !nosections

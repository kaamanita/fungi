(** Dependency Structure. *)

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

(** Dependency Pair. *)
module DepPair :
sig

    (** Word. *)
    type word_t = string

    (** Dependency pair. *)
    type t

    (** [create hw dw d] creates a dependency pair of a head word [hw]
        and a dependent word [dw] with a dependency direction [d]. The
        dependency direction is an option; if [d] equals [None], it
        means that there is no direction. *)
    val create : word_t -> word_t -> Cg.Syncat.depdir_t option -> t

    (** [get_head dp] finds the head word of a dependency pair [dp]. *)
    val get_head : t -> word_t

    (** [get_dep dp] finds the dependent word of a dependency pair [dp]. *)
    val get_dep : t -> word_t

    (** [get_depdir dp] finds the dependency direction of a dependency
        pair [dp]. The returned dependency direction is not an option;
        if this dependency pair has no direction, a failure exception 
        will be thrown. *)
    val get_depdir : t -> Cg.Syncat.depdir_t

    (** [to_string dp] represents a dependency pair [dp] with a string. *)
    val to_string : t -> string

    (** [is_dirdep dp] checks if a dependency pair [dp] has a dependency 
        direction. *)
    val is_dirdep : t -> bool

    (** [hash dp] computes the hash value of a dependency pair [dp]. *)
    val hash : t -> int

    (** [equal dp1 dp2] checks if two dependency pairs [dp1] and [dp2]
        equal. *)
    val equal : t -> t -> bool

    (** [compare dp1 dp2] compares two dependency pairs [dp1] and [dp2]
        for sorting. *)
    val compare : t -> t -> int
end

(** Dependency Structure. *)
module DepStruct :
sig
    
    (** Word. *)
    type word_t = string

    type syncat_t = Cg.Syncat.t

    type span_t = int * int

    (** Dependency pair. *)
    type deppair_t = DepPair.t

    (** Dependency structure. *)
    type t

    (** [create n sent r] creates a dependency structure with a sentence
        [sent] of length [n], having the root index at [r]. *)
    val create : int -> word_t list -> int -> t

    (** [get_sentence ds] gets the sentence of a dependency structure
        [ds]. *)
    val get_sentence : t -> word_t list

    (** [get_rootpos ds] gets the root index of a dependency structure
        [ds]. *)
    val get_rootpos : t -> int

    (** [mem_head ds i] checks if the [i]-th word is a head anywhere in
        a dependency structure [ds]. *)
    val mem_head : t -> int -> bool

    (** [get ds i] finds the dependent indices of a head index [i] in
        a dependency structure [ds]. *)
    val get : t -> int -> int list

    val find_head : t -> int -> int

    (** [mem ds h d] checks if the [d]-th word is a dependent of the
        [h]-th word in a dependency structure [ds]. *)
    val mem : t -> int -> int -> bool

    (** [remove ds h] removes the dependency entry of the [h]-th word. *)
    val remove : t -> int -> unit

    (** [entry_to_string h d sent] represents an entry of the dependency
        from the head word [h] to the dependent word [d] in the sentence
        [sent]. *)
    val entry_to_string : int -> int -> string list -> string

    (** [deptbl_to_string dt] represents a dependency table [dt] with
        a string. *)
    val deptbl_to_string : t -> string

    (** [to_string ds] represents a dependency structure [ds] with a
        string. *)
    val to_string : t -> string

    (** [add ds h d r] adds a dependency from a head word [h] to a
        dependent word [d] with relation type [r] to a dependency structure [ds]. *)
    val add : t -> int -> int -> Cg.Syncat.depdir_t -> unit

    (** [set_syncat ds h c] sets the syntactic category of a head word [h]. *)
    val set_syncat : t -> int -> syncat_t -> unit

    val get_syncat : t -> int -> string

    val get_depdir : t -> int -> int -> Cg.Syncat.depdir_t

    val ubrackets : t -> span_t list

    (** [depset ds] converts a dependency structure [ds] into a set of
        dependency pairs, preserving the dependency directions and word
        indices. *)
    val depset : t -> deppair_t list

    (** [depset_noorder ds] converts a dependency structure [ds] into 
        a set of dependency pairs, not preserving the dependency 
        directions. *)
    val depset_noorder : t -> deppair_t list

    (** [depset_words ds] converts a dependency structure [ds] into a 
        set of dependency pairs, preserving the dependency directions but
        not preserving the word indices. *)
    val depset_words : t -> deppair_t list

    (** [depset_words_noorder ds] converts a dependency structure [ds] 
        into a set of dependency pairs, not preserving the dependency 
        directions nor preserving the word indices. *)
    val depset_words_noorder : t -> deppair_t list
end

(** Inside-Outside Algorithm *)

module type EXPCALC_TYPE = sig
    val calc_prob : float -> float -> float
end

module InOutAlgo :
functor (ExpCalc : EXPCALC_TYPE) ->
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type fn_get_chart_t = int -> chart_t
    type deriv_t = Deriv.Deriv.t
    type fn_scale_prob_t = float -> deriv_t -> float
    type fn_localscore_t = int -> int -> float
    type t
    val create :
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> t
    val get_round : t -> int
    val reset_round : t -> unit
    (* val compute_onernd : t -> float *)
    val compute_inout : t -> unit
    val compute :
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> unit
end

module BabystepInOutAlgo :
functor (ExpCalc : EXPCALC_TYPE) ->
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type fn_get_chart_t = int -> chart_t
    type deriv_t = Deriv.Deriv.t
    type fn_scale_prob_t = float -> deriv_t -> float
    type fn_localscore_t = int -> int -> float
    type t
    val create :
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_babystep : t -> unit
    val compute :
        ?scale_prob:fn_scale_prob_t -> ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> syncat_t ->
        fn_get_chart_t -> int -> unit
end

(** Standard Inside-Outside Algorithm *)
module StandardInOutAlgo :
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> Deriv.Deriv.t -> float
    type fn_localscore_t = int -> int -> float
    type t
    val create :
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_inout : t -> unit
    val compute :
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit
end

(** Variational-Bayesian Inside-Outside Algorithm *)
module VBInOutAlgo :
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> Deriv.Deriv.t -> float
    type fn_localscore_t = int -> int -> float
    type t
    val create :
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_inout : t -> unit
    val compute :
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit
end

val leapfrogthr : int ref

(** Standard Babystep Inside-Outside Algorithm *)
module StandardBabystepInOutAlgo :
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> Deriv.Deriv.t -> float
    type fn_localscore_t = int -> int -> float
    type t
    val create :
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_babystep : t -> unit
    val compute :
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit
end

(** Variational-Bayesian Babystep Inside-Outside Algorithm *)
module VBBabystepInOutAlgo :
sig
    type syncat_t = Cg.Syncat.t
    type word_t = string
    type chart_t = Chart.Chart.t
    type genmodel_t = Deriv.DerivGenModel.t
    type fn_get_chart_t = int -> chart_t
    type fn_scale_prob_t = float -> Deriv.Deriv.t -> float
    type fn_localscore_t = int -> int -> float
    type t
    val create :
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> t
    val get_round : t -> int
    val reset_round : t -> unit
    val compute_babystep : t -> unit
    val compute :
        ?scale_prob:fn_scale_prob_t ->
        ?localscore:fn_localscore_t ->
        int -> float -> int -> float ->
        genmodel_t -> genmodel_t -> 
        syncat_t -> fn_get_chart_t -> int -> unit
end


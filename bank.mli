type tree_t = Tree.Tree.t

module type CMD_TYPE =
sig
    type t
    val parse : (Lexing.lexbuf -> Ruleparser.token) -> Lexing.lexbuf -> t
    val interpret : ?begpos:int -> t -> tree_t * int
end

module Bank :
functor (Cmd : CMD_TYPE) ->
sig
    type cmd_t
    type t = tree_t list
    val from_text : string -> t
    val from_texts : string list -> t
end

module TreeBank :
sig
    type cmd_t
    type t = tree_t list
    val from_text : string -> t
    val from_texts : string list -> t
end

module DepBank :
sig
    type cmd_t
    type t = tree_t list
    val from_text : string -> t
    val from_texts : string list -> t
end

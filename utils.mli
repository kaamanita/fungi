(** Utility Functions *)

(** Find the cartesian product of two lists. *)
val cartprod : 'a list -> 'b list -> ('a * 'b) list

(** Map the function to each element of the cartesian product 
    of the two lists. *)
val cartmap : ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list

(** Map the function to each element of the cartesian product of 
    the two lists.  This function ignores all exceptions occurred
    during mapping. *)
val cartmap_noerr : ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list

(** Iterate the function on each element of the cartesian product 
    of the two lists. *)
val cartiter : ('a -> 'b -> 'c) -> 'a list -> 'b list -> unit

(** Iterate the function on each element of the cartesian product 
    of the two lists.  This function ignores all exceptions occurred
    during iteration. *)
val cartiter_noerr : ('a -> 'b -> 'c) -> 'a list -> 'b list -> unit

(** Eliminate all duplicates in the list. *)
val elim_dups : 'a list -> 'a list

(** Find the intersection of two lists. *)
val intersect : 'a list -> 'a list -> 'a list

(** Find the difference of two lists. *)
val diff : 'a list -> 'a list -> 'a list

(** Find the union of two lists. *)
val union : 'a list -> 'a list -> 'a list

val list_to_set : 'a list -> 'a list

(** Return every element of the list but one at the 
    specified position. *)
val except_pos : 'a list -> int -> 'a list

(** Marshal the value to a string. *)
val marshal : 'a -> string

(** Unmarshal the marshaled string. *)
val unmarshal : ?begpos:int -> string -> 'a

(** Marshal the value and save it to the file. This function 
    is NOT type-safe, so make sure it is type-casted before using. *)
val marshal_to_file : string -> 'a -> unit

(** Load a marshaled string from the file and unmarshal it. 
    This function is NOT type-safe, so make sure it is type-casted
    before using. *)
val unmarshal_from_file : string -> 'a

(** Calculate the digamma function. *)
val digamma : float -> float

val gammaln : float -> float

val gamma : float -> float

(** Split a string into tokens by spaces. *)
val split : string -> string list

(** Split a string into tokens by the specified delimiter (in regular expression). *)
val split_delim : string -> string -> string list

val only_tags : string list -> string list

val only_words : string list -> string list

(** Represent a list with a string. Bracketing is default. *)
val string_of_list : ?bracket:bool -> ('a -> string) -> 'a list -> string

val ending_mark : string

val set_infl : bool -> unit

val is_infl : unit -> bool

val unicode_length : string -> int

val set_catconst : float -> unit

val get_catconst : unit -> float

val set_verbose_sampler : unit -> unit

val is_verbose_sampler : unit -> bool

val set_seedlvl : int -> unit

val get_seedlvl : unit -> int

val set_save_prob : (unit -> unit) -> unit

val run_save_prob : unit -> unit

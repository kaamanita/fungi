let rec cartprod list1 list2 =
    match list1 with
        [] -> []
      | hd1 :: tl1 ->
            (cartprod_elt hd1 list2) @ (cartprod tl1 list2)

and cartprod_elt hd1 list2 =
    match list2 with
        [] -> []
      | hd2 :: tl2 -> 
            (hd1, hd2) :: (cartprod_elt hd1 tl2)

let rec cartmap fn list1 list2 =
    match list1 with
        [] -> []
      | hd1 :: tl1 ->
            (cartmap_elt fn hd1 list2) @ (cartmap fn tl1 list2)

and cartmap_elt fn hd1 list2 =
    match list2 with
        [] -> []
      | hd2 :: tl2 -> 
            (fn hd1 hd2) :: (cartmap_elt fn hd1 tl2)

let rec cartmap_noerr fn list1 list2 =
    match list1 with
        [] -> []
      | hd1 :: tl1 ->
            (cartmap_noerr_elt fn hd1 list2) @ (cartmap_noerr fn tl1 list2)

and cartmap_noerr_elt fn hd1 list2 =
    match list2 with
        [] -> []
      | hd2 :: tl2 -> 
            try
                (fn hd1 hd2) :: (cartmap_noerr_elt fn hd1 tl2)
            with _ ->
                cartmap_noerr_elt fn hd1 tl2

let rec cartiter fn list1 list2 =
    match list1 with
        [] -> ()
      | hd1 :: tl1 ->
        begin
            cartiter_elt fn hd1 list2;
            cartiter fn tl1 list2
        end

and cartiter_elt fn hd1 list2 =
    match list2 with
        [] -> ()
      | hd2 :: tl2 -> 
        begin
            fn hd1 hd2;
            cartiter_elt fn hd1 tl2
        end

let rec cartiter_noerr fn list1 list2 =
    match list1 with
        [] -> ()
      | hd1 :: tl1 ->
        begin
            cartiter_noerr_elt fn hd1 list2;
            cartiter_noerr fn tl1 list2
        end

and cartiter_noerr_elt fn hd1 list2 =
    match list2 with
        [] -> ()
      | hd2 :: tl2 -> 
            try begin
                fn hd1 hd2;
                cartiter_noerr_elt fn hd1 tl2
            end with _ ->
                cartiter_noerr_elt fn hd1 tl2

let rec elim_dups mylist =
    elim_dups_elt mylist []

and elim_dups_elt mylist result =
    match mylist with
        [] -> result
      | elt :: rest ->
            if List.mem elt result then
                elim_dups_elt rest result
            else
                elim_dups_elt rest (elt :: result)

let rec intersect list1 list2 =
    match list1 with
        [] -> []
      | elt :: rest ->
            if List.mem elt list2 (* && not (List.mem elt rest) *) then 
                elt :: (intersect rest list2)
            else intersect rest list2

let rec diff list1 list2 =
    match list1 with
        [] -> []
      | elt :: rest ->
            if List.mem elt list2 (* || List.mem elt rest *) then 
                diff rest list2
            else elt :: (diff rest list2)

let list_to_set list1 =
    intersect list1 list1

let union list1 list2 =
    let list1' = list_to_set list1 in
    list1' @ (diff list2 list1')

let marshal value = 
    Marshal.to_string value [Marshal.Closures]

let unmarshal ?(begpos=0) mstr = 
    Marshal.from_string mstr begpos

let marshal_to_file fname value =
    let ofhdl = open_out_bin fname in
    Marshal.to_channel ofhdl value [];
    close_out ofhdl

let unmarshal_from_file fname =
    let ifhdl = open_in_bin fname in
    let result = Marshal.from_channel ifhdl in
    close_in ifhdl;
    result

let digamma x =
    assert (x > 0.0);
    let result = ref 0.0 in
    let y = ref x in
    while !y < 7.0 do
        result := !result -. 1.0 /. !y;
        y := !y +. 1.0
    done;
    y := !y -. 0.5;
    let xx = 1.0 /. !y in
    let xx2 = xx *. xx in
    let xx4 = xx2 *. xx2 in
    !result 
        +. log x
        +. xx2 /. 24.0 
        -. (7.0 /. 960.0) *. xx4
        +. (31.0 /. 8064.0) *. xx2 *. xx4
        -. (127.0 /. 30720.0) *. xx4 *. xx4

let pi = 4.0 *. atan 1.0

let gammaln x =
    let z = 1.0 /. x in
    let z2 = z *. z in
    let z3 = z *. z2 in
    let z5 = z3 *. z2 in
    let z7 = z5 *. z2 in
    let z9 = z7 *. z2 in
    let result = ref (x *. log x) in
    result := !result -. x +. 0.5 *. log (2.0 *. pi *. z);
    result := !result +. z /. 12.0;
    result := !result -. z3 /. 360.0;
    result := !result +. z5 /. 1260.0;
    result := !result -. z7 /. 1680.0;
    result := !result +. z9 /. 1188.0;
    !result

let gamma x = exp (gammaln x)

let rec except_pos list1 pos =
    except_pos_elt list1 pos 0

and except_pos_elt list1 pos idx =
    match list1 with
        [] -> []
      | hd :: rest ->
            if pos <> idx then 
                hd :: except_pos_elt rest pos (idx + 1)
            else
                except_pos_elt rest pos (idx + 1)

let split line =
    let tokens = Str.split (Str.regexp "[\ \t]+") line in
    List.fold_right 
        (fun token v -> 
            if String.length token > 0 then token :: v
            else v) 
        tokens []

let split_delim delim line =
    let tokens = Str.split (Str.regexp delim) line in
    List.fold_right 
        (fun token v -> 
            if String.length token > 0 then token :: v
            else v) 
        tokens []

let rec only_tags strtoks =
    match strtoks with
        [] -> []
      | hd :: tl ->
            if String.contains hd '/' then
                let idx = String.rindex hd '/' in
                let hd' = String.sub hd (idx + 1) (String.length hd - idx - 1) in
                hd' :: only_tags tl
            else hd :: only_tags tl

let rec only_words strtoks =
    match strtoks with
        [] -> []
      | hd :: tl ->
            if String.contains hd '/' then
                let idx = String.rindex hd '/' in
                let hd' = String.sub hd 0 idx in
                hd' :: only_words tl
            else hd :: only_words tl

let string_of_list ?(bracket = true) fn lst =
    if bracket then
        Printf.sprintf "[%s]" (String.concat ", " (List.map fn lst))
    else
        String.concat ", " (List.map fn lst)

let ending_mark = "<eos>"

let infl = ref false

let set_infl v =
    infl := v

let is_infl () = !infl

let nolenvowels = 
    List.map
        (fun s ->
            String.sub s 3 3
        )
        [
            "อิ"; "อี"; "อึ"; "อื"; "อุ"; "อู"; 
            "อ่"; "อ้"; "อ๊"; "อ๋"; "อ็"; "อฺ"; 
            "อํ"; "อ์"; "อั"; "อ๎"
        ]

let unicode_chrlen (s : string) : int =
    if String.length s = 0 then 0
    else
        let leading = Char.code s.[0] in
        if (leading land 0b10000000) = 0b00000000 then 1
        else if (leading land 0b11100000) = 0b11000000 then 2
        else if (leading land 0b11110000) = 0b11100000 then 3
        else if (leading land 0b11111000) = 0b11110000 then 4
        else if (leading land 0b11111100) = 0b11111000 then 5
        else if (leading land 0b11111110) = 0b11111100 then 6
        else 1

let rec unicode_length (s : string) : int =
    let len = String.length s in
    if len = 0 then 0
    else
        let cl = unicode_chrlen s in
        if len < cl then len
        else
            let car = String.sub s 0 cl
            and cdr = String.sub s cl (len - cl) in
            if List.mem car nolenvowels then unicode_length cdr
            else 1 + unicode_length cdr

let catconst = ref 0.0

let set_catconst value =
    catconst := value

let get_catconst () = !catconst

let verbose_sampler = ref false

let set_verbose_sampler () =
    verbose_sampler := true

let is_verbose_sampler () = !verbose_sampler

let seedlvl = ref 100

let set_seedlvl value =
    seedlvl := value

let get_seedlvl () =
    !seedlvl

let save_prob_fn : (unit -> unit) ref = ref (fun _ -> ())

let set_save_prob fn =
    save_prob_fn := fn

let run_save_prob () =
    !save_prob_fn ()

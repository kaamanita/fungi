%token <string> ID 
%token <string> QUOTE
%token <string> CATMACRO
%token LDIR RDIR DBCONJ CONJ UNARY
%token FWDSLASH BWDSLASH
%token LPAREN RPAREN LBRC RBRC COLON
%token COMMA
%token DEFN EQUAL CMD_UNCERTAIN CMD_NONE CMD_ARBITRARY
%token BLOCK_START BLOCK_END
%token CMD_ATTR CMD_ADDCATS CMD_UNCERTAIN CMD_NO_ENUM CMD_LEFTPUNC CMD_RIGHTPUNC CMD_LEFTSER CMD_RIGHTSER CMD_ENUM_ONLY_UNC CMD_ENUM_ALL 
%token CMD_ACCEPT CMD_TYPOLOGY CMD_DMV CMD_EVG CMD_GHG CMD_ENABLE_SPLIT_HEAD CMD_DISABLE_SPLIT_HEAD 
%token CMD_TRANSFORM
%token EOS

%left FWDSLASH BWDSLASH
%left COMMA

%start syncat dict_entry tree deptree

%type <Cg.Syncat.t> syncat
%type <Ruletypes.dictentry_t> dict_entry
%type <Ruletypes.tree_t> tree
%type <Ruletypes.deptree_t> deptree

%%

syncat:
    ID                          { Cg.Syncat.create_atom $1 }
  | CONJ                        { Cg.Syncat.conn_cat }
  | syncat FWDSLASH dir syncat  { Cg.Syncat.create_fwd $1 $4 $3 }
  | syncat BWDSLASH dir syncat  { Cg.Syncat.create_bwd $1 $4 $3 }
  | LPAREN syncat RPAREN        { $2 }
;

dir:
    LDIR    { Cg.Syncat.Left }
  | RDIR    { Cg.Syncat.Right }
;

macrosyncat:
    CATMACRO    { Macro.MacroSyncat.create_atom $1 }
  | ID ftrs
    { let syncat = Cg.Syncat.create_atom $1 ~ftrs:(Avm.Avm.create $2) in
      Macro.MacroSyncat.create_syncat syncat }
  | ID
    { let syncat = Cg.Syncat.create_atom $1 in
      Macro.MacroSyncat.create_syncat syncat }
  | CONJ
    { let syncat = Cg.Syncat.conn_cat in
      Macro.MacroSyncat.create_syncat syncat }
  | macrosyncat FWDSLASH dir ftrs macrosyncat
    { Macro.MacroSyncat.create_fwd $1 $5 $3 (Avm.Avm.create $4) }
  | macrosyncat BWDSLASH dir ftrs macrosyncat
    { Macro.MacroSyncat.create_bwd $1 $5 $3 (Avm.Avm.create $4) }
  | macrosyncat FWDSLASH dir macrosyncat
    { Macro.MacroSyncat.create_fwd $1 $4 $3 (Avm.Avm.create_empty ()) }
  | macrosyncat BWDSLASH dir macrosyncat
    { Macro.MacroSyncat.create_bwd $1 $4 $3 (Avm.Avm.create_empty ()) }
  | LPAREN macrosyncat RPAREN       { $2 }
;

macrosyncats:
    macrosyncat                         { [$1] }
  | macrosyncats COMMA macrosyncat      { $1 @ [$3] }
;

words:
    QUOTE                   { [$1] }
  | words COMMA QUOTE       { $1 @ [$3] }
;

ftrs:
    LBRC ftrslist RBRC      { $2 }
  | LBRC RBRC               { [] }
;

ftrslist:
    ID COLON ID                 { [ ($1, $3) ] }
  | ftrslist COMMA ID COLON ID  { $1 @ [ ($3, $5) ] }
;

ftrsseq:
    ftrs                    { [$1] }
  | ftrsseq COMMA ftrs      { $1 @ [$3] }
;

dict_entry:
    dict_lexentry               { $1 }
  | dict_macroentry             { $1 }
  | dict_uncentry               { $1 }
  | dict_transf                 { $1 }
  | dict_enumlevel              { $1 }
  | dict_addattr                { $1 }
  | dict_addcat                 { $1 }
  | dict_addleftpuncs           { $1 }
  | dict_addrightpuncs          { $1 }
  | dict_addleftser             { $1 }
  | dict_addrightser            { $1 }
  | dict_accept                 { $1 }
  | dict_typology               { $1 }
  | dict_dmv                    { $1 }
  | dict_evg                    { $1 }
  | dict_ghg                    { $1 }
  | dict_enable_split_head      { $1 }
  | dict_disable_split_head     { $1 }
;

dict_lexentry:
    QUOTE DEFN macrosyncats EOS    
    { Ruletypes.LexEntry ($1, $3) }
;

dict_macroentry:
    CATMACRO DEFN macrosyncats EOS
    { Ruletypes.MacroEntry ($1, $3) }
;

dict_uncentry:
    CMD_UNCERTAIN words EOS    { Ruletypes.UncEntry $2 }
;

dict_transf:
    CMD_TRANSFORM macrosyncat DEFN macrosyncats EOS
    { Ruletypes.TransfEntry ($2, $4) }
;

enumlevel:
    CMD_NO_ENUM         { Dict.Dict.NoEnum }
  | CMD_ENUM_ONLY_UNC   { Dict.Dict.EnumOnUncertain }
  | CMD_ENUM_ALL        { Dict.Dict.EnumOnAll }
;

dict_enumlevel:
    enumlevel EOS       { Ruletypes.EnumEntry $1 }
;

dict_addattr:
    CMD_ATTR QUOTE DEFN ftrsseq EOS   { Ruletypes.AddAttr ($2, List.map Avm.Avm.create $4) }
;

dict_addcat:
    CMD_ADDCATS macrosyncats EOS    { Ruletypes.AddCats $2 }
;

dict_addleftpuncs:
    CMD_LEFTPUNC words EOS          { Ruletypes.AddLeftPuncs $2 }
;

dict_addrightpuncs:
    CMD_RIGHTPUNC words EOS         { Ruletypes.AddRightPuncs $2 }
;

dict_addleftser:
    CMD_LEFTSER macrosyncats EOS     { Ruletypes.AddLeftSer $2 }
;

dict_addrightser:
    CMD_RIGHTSER macrosyncats EOS    { Ruletypes.AddRightSer $2 }
;

tree:
    LPAREN ID trees RPAREN                  { Ruletypes.TreeNode ($2, $3) }
  | LPAREN ID LDIR tree tree RPAREN         { Ruletypes.TreeNode ($2, [$4; $5]) }
  | LPAREN ID RDIR tree tree RPAREN         { Ruletypes.TreeNode ($2, [$4; $5]) }
  | LPAREN ID CONJ tree tree tree RPAREN    { Ruletypes.TreeNode ($2, [$4; $5; $6]) }
  | LPAREN ID UNARY tree RPAREN             { Ruletypes.TreeNode ($2, [$4]) }
  | ID                                      { Ruletypes.LexNode $1 }
;

trees:
    trees tree      { $1 @ [$2] }
  | tree            { [$1] }
;

deptree:
    LPAREN ID dir deptree deptree RPAREN
    { Ruletypes.DepTreeNode ($2, $4, $5, $3) }
  | LPAREN ID CONJ deptree deptree deptree RPAREN
    { Ruletypes.DepCoorNode ($2, $4, $5, $6) }
  | LPAREN ID UNARY deptree RPAREN
    { Ruletypes.DepUnaryNode ($2, $4) }
  | ID
    { Ruletypes.DepLexNode $1 }
;

dir:
    LDIR    { Cg.Syncat.Left }
  | RDIR    { Cg.Syncat.Right }
;

dict_accept:
    CMD_ACCEPT macrosyncats EOS
    { Ruletypes.Accept $2 }
;

dict_typology:
    CMD_TYPOLOGY BLOCK_START typo_entries BLOCK_END EOS
    { Ruletypes.Typology $3 }
  | CMD_TYPOLOGY CMD_ARBITRARY EOS
    { Ruletypes.Typology [
        ("copula", ["true"]);
        ("gerund_as_np", ["true"]);
        ("gerund_as_nmod", ["true"]);
        ("gerund_as_vmod", ["true"]);
        ("shift", ["true"])] }
;

typo_entries:
    typo_entry                  { [$1] }
  | typo_entries typo_entry     { $1 @ [$2] }
;

typo_entry:
    ID EQUAL values EOS     { ($1, $3) }
;

values:
    value                   { [$1] }
  | CMD_NONE                { ["none"] }
  | values COMMA value      { $1 @ [$3] }
;

value:
    ID      { $1 }
  | QUOTE   { $1 }
;

dict_dmv:
    CMD_DMV words EOS           { Ruletypes.DMV $2 }
  | CMD_DMV EOS                 { Ruletypes.DMV [] }
;

dict_evg:
    CMD_EVG words EOS           { Ruletypes.EVG $2 }
  | CMD_EVG EOS                 { Ruletypes.EVG [] }
;

dict_ghg:
    CMD_GHG BLOCK_START typo_entries BLOCK_END EOS 
    { Ruletypes.GHG $3 }
  | CMD_GHG EOS
    { Ruletypes.GHG [] }
;

dict_enable_split_head:
    CMD_ENABLE_SPLIT_HEAD EOS
    { Ruletypes.EnableSplitHead }
;

dict_disable_split_head:
    CMD_DISABLE_SPLIT_HEAD EOS
    { Ruletypes.DisableSplitHead }
;

open Deriv
open Models
open Printf

let set_probflags probflags str =
    let probflag = match str with
        "roleemis" -> [RoleEmis]
      | "hockenmaier" -> [JuliaEmis]
      | "lexemis" -> [LexEmis]
      | "heademis" -> [HeadEmis] 
      | "model0" -> []
      | "model1" -> [RoleEmis]
      | "model2" -> [JuliaEmis]
      | "model3" -> [RoleEmis; LexEmis]
      | "model4" -> [JuliaEmis; HeadEmis]
      | "model5" -> [RoleEmis; LexEmis; JuliaEmis; HeadEmis]
      | _ -> failwith "set_probflags" in
    probflags := Utils.union probflag !probflags

let showprobtbl probflags epsilon fname =
    let probftrs = conv_probflags probflags 1000 epsilon in
    printf "\nProbability Table: %s\n\n" fname;
    let genmodel = DerivGenModel.from_file fname probftrs in
    DerivGenModel.print genmodel

let main =
    let fnames = ref []
    and probflags = ref []
    and epsilon = ref 1e-6 in
    let args = [
        ("--probftr", 
            Arg.Symbol (
                ["roleemis"; "hockenmaier"; "lexemis"; "heademis"], 
                set_probflags probflags), 
            "  Enable the specified probability feature");
        ("--model",
            Arg.Symbol (
                ["0"; "1"; "2"; "3"; "4"; "5"],
                fun x -> set_probflags probflags ("model" ^ x)),
            "  Enable the specified model (a set of probability features)");
        ("--epsilon",
            Arg.Set_float epsilon,
            "[float]   Set the value of epsilon (a very small value) (default = 1e-6)")]
    and fn_anon = (fun f -> fnames := !fnames @ [f])
    and usagemsg = 
        "\nDisplay the specified generative models.\n\n"
        ^ (sprintf "Usage: %s genmodel1 genmodel2 ..." Sys.argv.(0)) in
    Arg.parse args fn_anon usagemsg;
    List.iter (showprobtbl !probflags !epsilon) !fnames

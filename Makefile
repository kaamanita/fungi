export OCAMLMAKEFILE = OCamlMakefile

# export ANNOTATE = yes
export LIBS = str unix functory
export INCDIRS = +functory
export COMMON_OCAMLDOCFLAGS = \
    -keep-code -colorize-code -sort -m per
export COMMON_SOURCES = \
    scale.mli utils.mli avm.mli cg.mli hdb.mli dict.mli dep.mli chart.mli tree.mli chartdb.mli \
    scale.ml utils.ml avm.ml cg.ml hdb.ml dict.ml dep.ml chart.ml tree.ml chartdb.ml 
export DOC_FILES = $(filter %.mli %.ml, $(COMMON_SOURCES))

define PROJ_showdict
    SOURCES = $(COMMON_SOURCES) \
        macro.mli catgen.mli depgrms.mli ruletypes.mli dictloader.mli \
        macro.ml catgen.ml depgrms.ml ruleparser.mly rulelexer.mll dictloader.ml \
        showdict.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Dictionary Displayer"
    RESULT = fungi_showdict
endef
export PROJ_showdict

define PROJ_parse
    SOURCES = $(COMMON_SOURCES) \
        cky.mli macro.mli catgen.mli depgrms.mli ruletypes.mli dictloader.mli parserrunner.mli \
        cky.ml macro.ml catgen.ml depgrms.ml ruleparser.mly rulelexer.mll dictloader.ml parserrunner.ml \
        parse.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "CKY Parser"
    RESULT = fungi_parse
endef
export PROJ_parse

define PROJ_showchartdb
    SOURCES = $(COMMON_SOURCES) \
        prob.mli deriv.mli \
        prob.ml deriv.ml \
        showchartdb.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Chart Database Lister"
    RESULT = fungi_showchartdb
endef
export PROJ_showchartdb

define PROJ_train
    SOURCES = $(COMMON_SOURCES) \
        macro.mli catgen.mli depgrms.mli ruletypes.mli dictloader.mli prob.mli deriv.mli probinit.mli models.mli inout.mli em.mli anneal.mli annealrunner.mli \
        macro.ml catgen.ml depgrms.ml ruleparser.mly rulelexer.mll dictloader.ml prob.ml deriv.ml probinit.ml models.ml inout.ml em.ml anneal.ml annealrunner.ml \
        train.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Expectation Maximizer"
    RESULT = fungi_train
endef
export PROJ_train

define PROJ_showprobtbl
    SOURCES = $(COMMON_SOURCES) \
        prob.mli deriv.mli models.mli \
        prob.ml deriv.ml models.ml \
        showprobtbl.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Generative Model Lister"
    RESULT = fungi_showprobtbl
endef
export PROJ_showprobtbl

define PROJ_decode
    SOURCES = $(COMMON_SOURCES) \
        macro.mli catgen.mli depgrms.mli ruletypes.mli dictloader.mli prob.mli deriv.mli models.mli inout.mli decoderalgo.mli treedb.mli decoderrunner.mli \
        macro.ml catgen.ml depgrms.ml ruleparser.mly rulelexer.mll dictloader.ml prob.ml deriv.ml models.ml inout.ml decoderalgo.ml treedb.ml decoderrunner.ml \
        decode.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Parse Decoder"
    RESULT = fungi_decode
endef
export PROJ_decode

define PROJ_ubound
    SOURCES = $(COMMON_SOURCES) \
        macro.mli catgen.mli depgrms.mli ruletypes.mli dictloader.mli prob.mli deriv.mli models.mli uboundalgo.mli treedb.mli uboundrunner.mli conlldep.mli \
        macro.ml catgen.ml depgrms.ml ruleparser.mly rulelexer.mll dictloader.ml prob.ml deriv.ml models.ml uboundalgo.ml treedb.ml uboundrunner.ml conlldep.ml \
        ubound.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Upper Bound Parser"
    RESULT = fungi_ubound
endef
export PROJ_ubound

define PROJ_showtreedb
    SOURCES = $(COMMON_SOURCES) \
        treedb.mli \
        treedb.ml \
        showtreedb.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Tree Database Lister"
    RESULT = fungi_showtreedb
endef
export PROJ_showtreedb

define PROJ_showconll
    SOURCES = $(COMMON_SOURCES) \
        conlldep.mli \
        conlldep.ml \
        showconll.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "CoNLL2006-formatted Dependency Database Lister"
    RESULT = fungi_showconll
endef
export PROJ_showconll

define PROJ_prepareconll
    SOURCES = $(COMMON_SOURCES) \
        conlldep.mli \
        conlldep.ml \
        prepareconll.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Prepare CoNLL2006-formatted Dependency databases."
    RESULT = fungi_prepareconll
endef
export PROJ_prepareconll

define PROJ_annscheme
    SOURCES = $(COMMON_SOURCES) \
        macro.mli catgen.mli depgrms.mli ruletypes.mli dictloader.mli conlldep.mli \
        macro.ml catgen.ml depgrms.ml ruleparser.mly rulelexer.mll dictloader.ml conlldep.ml \
        annscheme.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Annotation Scheme Approximator"
    RESULT = fungi_annscheme
endef
export PROJ_annscheme

define PROJ_evaluate
    SOURCES = $(COMMON_SOURCES) \
        treedb.mli macro.mli ruletypes.mli bank.mli conlldep.mli eval.mli \
        treedb.ml macro.ml ruleparser.mly rulelexer.mll bank.ml conlldep.ml eval.ml \
        evaluate.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Evaluator"
    RESULT = fungi_evaluate
endef
export PROJ_evaluate

define PROJ_scheduler
    LIBS = $(LIBS) functory
    SOURCES = \
        scale.mli utils.mli \
        scale.ml utils.ml \
        scheduler.ml
    OCAMLDOCFLAGS = $(COMMON_OCAMLDOCFLAGS) \
        -t "Job Scheduler"
    RESULT = fungi_scheduler
endef
export PROJ_scheduler

ifndef SUBPROJS
    export SUBPROJS = showdict parse showchartdb train showprobtbl decode ubound showtreedb showconll prepareconll annscheme evaluate scheduler
endif

all: nc

%:
	@make -f $(OCAMLMAKEFILE) subprojs SUBTARGET=$@

git-commit: clean clean-test
	git commit -a

# include $(OCAMLMAKEFILE)

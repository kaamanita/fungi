@evg;
@enable_split_head;

########## POS Tags (STTS) ##########

# ADJA      Attributive adjective
'ADJA' :- [word];

# ADJD      Adverb or predicative adjective (e.g. 'fast' in 'I run fast' and 'I am fast')
'ADJD' :- [word];

# ADV       Adverb
'ADV' :- [word];

# APPR      Preposition
'APPR' :- [word];

# APPRART   Preposition merged with an article
'APPRART' :- [word];

# APPO      Postposition
'APPO' :- [word];

# APZR      The right part of a circumposition
'APZR' :- [word];

# ART       Article
'ART' :- [word];

# CARD      Cardinal number
'CARD' :- [word];

# FM        Foreign word
'FM' :- [word];

# ITJ       Interjection
'ITJ' :- [word];

# ORD       Ordinal number
'ORD' :- [word];

# KOUI      Subordinating conjunction with infinitive
'KOUI' :- [word];

# KOUS      Subordinating conjunction
'KOUS' :- [word];

# KON       Coordinating conjunction
'KON' :- [word];

# KOKOM     Comparative conjunction (i.e. 'than')
'KOKOM' :- [word];

# NN        Common noun
'NN' :- [word];

# NE        Proper noun
'NE' :- [word];

# PDS       Substitutional demonstrative pronoun
'PDS' :- [word];

# PDAT      Demonstrative pronominal adjective
'PDAT' :- [word];

# PIS       Substitutional indefinite pronoun
'PIS' :- [word];

# PIAT      Indefinite pronominal adjective
'PIAT' :- [word];

# PIDAT     Indefinite pronominal adjective with an article
'PIDAT' :- [word];

# PPER      Irreflexive personal pronoun
'PPER' :- [word];

# PPOSS     Substitutional possessive pronoun
'PPOSS' :- [word];

# PPOSAT    Possessive pronominal adjective
'PPOSAT' :- [word];

# PRELS     Substitutional relative pronoun
'PRELS' :- [word];

# PRELAT    Relative pronominal adjective
'PRELAT' :- [word];

# PRF       Reflexive personal pronoun
'PRF' :- [word];

# PWS       Substitutional interrogative pronoun
'PWS' :- [word];

# PWAT      Interrogative pronominal adjective
'PWAT' :- [word];

# PWAV      Interrogative pronominal adverb
'PWAV' :- [word];

# PAV       Pronominal adverb
'PAV' :- [word];
'PROAV' :- [word];

# PTKZU     Infinitive marker 'zu'
'PTKZU' :- [word];

# PTKNEG    Negative marker 'nicht'
'PTKNEG' :- [word];

# PTKVZ     Particle of a phrasal verb
'PTKVZ' :- [word];

# PTKANT    Reply particle (i.e. 'yes', 'no')
'PTKANT' :- [word];

# PTKA      Particle with adjective or adverb (i.e. zu [schnell])
'PTKA' :- [word];

# SGML      SGML markup

# SPELL     Spelling
'SPELL' :- [word];

# TRUNC     First element of composition (e.g. 'pre-' and postwar casualties)
'TRUNC' :- [word];

# VVFIN     Verb, finite
'VVFIN' :- [word];

# VVIMP     Verb, imperative
'VVIMP' :- [word];

# VVINF     Verb, infinitive
'VVINF' :- [word];

# VVIZU     Verb, infinitive with 'zu'
'VVIZU' :- [word];

# VVPP      Past participle
'VVPP' :- [word];

# VAFIN     Auxiliary verb, finite
'VAFIN' :- [word];

# VAIMP     Auxiliary verb, imperative
'VAIMP' :- [word];

# VAINF     Auxiliary verb, infinitive
'VAINF' :- [word];

# VAPP      Auxiliary verb, past participle
'VAPP' :- [word];

# VMFIN     Modal verb, finite
'VMFIN' :- [word];

# VMINF     Modal verb, infinite
'VMINF' :- [word];

# VMPP      Modal verb, past participle
'VMINF' :- [word];

# XY        Not a word
'XY' :- [word];

# $,        Comma
# $.        Period
# $(        Parenthesis


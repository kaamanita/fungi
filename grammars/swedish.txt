@typology begin

    depscheme = head-outward;
    wordorder = fixed;
    inflection = no;

    sentence = SVIO, SIOV, VSIO;
    vicomp = SVC, VSC;
    vtcomp = SVOC, OVSC;
    adj = adj+noun;
    adv = vp+adv, adv+vp;
    advadj = adv+adj;
    modal = modal+vp;
    adposition = prep+np;

    nmod = np+nmod;
    vmod = vp+vmod, vmod+vp;
    poss = owner+poss+ownee;
    relpro = relpro+comp;
    relcls = np+relcls;
    subcls = main+conj+subcls, conj+subcls+main;
    
    npgerund = np+gerund;
    vpgerund = vp+gerund;
    infvp = inf+vp;
    npnom = np+nom;
    vpnom = nom+vp;
    smod = smod+sent;
    part = part+vp, vp+part;
    
    copula = true;
    gerund_as_np = false;
    gerund_as_nmod = false;
    gerund_as_vmod = true;
    shift = true;
    
    neg = neg+v, neg+adj, neg+adv;
    drop = @none;

    heads = +modal, +part, -subconj_smod, +subconj_phr, +inf, +npnom;
end;

@accept [s], [np], [vp], [nmod], [vmod];
@uncertain 'PU', 'XX', 'EH', 'RJ', 'UJ', 'NJ', 'QQ', 'UU';

########## POS Tags ##########

# ++    Coordinating conjunction
'++' :- [conj_truedep], [smod], [adv];

# AB    Adverb
'AB' :- [adv], [pp] /> [s], [adj];

# AJ    Adjective
'AJ' :- [adj], [adv];

# AN    Adjective noun
'AN' :- [np];

# AV    The verb "vara" (be)
'AV' :- [copula], [modal], [v];

# BV    The verb "bli(va)" (become)
'BV' :- [copula], [modal], [v];

# EN    Indefinite article or numeral "en", "ett" (one)
'EN' :- [adj], [np];

# FV    The verb "få" (get)
'FV' :- [copula], [modal], [v];

# GV    The verb "göra" (do, make)
'GV' :- [copula], [modal], [v];

# HV    The verb "ha(va)" (have)
'HV' :- [copula], [modal], [v];

# I?    Question mark
@rightpunc 'I?';

# IC    Quotation mark
@leftpunc 'IC';
@rightpunc 'IC';

# ID    Part of idiom (multi-word unit)
'ID' :- [part];

# IG    Other punctuation mark
@leftpunc 'IG';
@rightpunc 'IG';

# IK    Comma
@rightpunc 'IK';

# IM    Infinitive marker
'IM' :- [inf], [pp] /> [vp], [prep], [pp] /< [adv];

# IP    Period
@rightpunc 'IP';

# IQ    Colon
'IQ' :- [conj_truedep];
@rightpunc 'IQ';

# IR    Parenthesis
@leftpunc 'IR';
@rightpunc 'IR';

# IS    Semicolon
@rightpunc 'IS';

# IT    Dash
'IT' :- [conj_truedep];
@leftpunc 'IT';
@rightpunc 'IT';

# IU    Exclamation mark
@rightpunc 'IU';

# KV    The verb locution "komma att" (periphrastic future)
'KV' :- [v], [modal];

# MN    Adversative
'MN' :- [part];

# MV    The verb "måste" (must)
'MV' :- [modal], [v];

# NN    Other noun
'NN' :- [np], [adj];

# PN    Proper name
'PN' :- [np], [adj];

# PO    Pronoun
'PO' :- [np], [adj];

# PR    Preposition
'PR' :- [prep], [prep] \< [prep], [npnom], [adv];

# PU    Pause
@leftpunc 'PU';
@rightpunc 'PU';

# QV    The verb "kunna" (can)
'QV' :- [modal], [vtcomp], [vi];

# RO    Numeral other than "en", "ett" (one)
'RO' :- [np], [adj];

# SP    Present participle
'SP' :- [gerund];

# SV    The verb "skola" (will, shall)
'SV' :- [modal], [vtcomp], [vi];

# TP    Totality pronoun
'TP' :- [gerund];

# UK    Subordinating conjunction
'UK' :- [subconj], [adv], [smod], [prep];

# VN    Verbal noun
'VN' :- [np], [adj], [gerund];

# VV    Other verb
'VV' :- [v];

# WV    The verb "vilja" (want)
'WV' :- [v];

# XX    Unclassified
'XX' :- [np], [adj];

# YY    Interjection
'YY' :- [part], [smod], [adv];

# EH    Filled pause
'EH' :- [part];

# RJ    Juncture - straight
# UJ    Juncture - rise
# NJ    Juncture - fall

# QQ    Dummy for final omission
'QQ' :- [part];

# UU    Exclamative or optative
'UU' :- [part];

# TT    Vocative
'TT' :- [part];

@dmv;
@enable_split_head;

########## POS tags ##########

# A     Adjective
'A' :- [word];
'A1' :- [word];
'A2' :- [word];
'A3' :- [word];
'A4' :- [word];
'A5' :- [word];
'A6' :- [word];
'A7' :- [word];
'AX' :- [word];

'A-2' :- [word];

# AA    General adjective
'A-A' :- [word];

# AC    Nominal adjective
'A-C' :- [word];

# AG    Adjective derived from present participle
'A-G' :- [word];

# AM    Adjective derived from past participle
'A-M' :- [word];

'A-O' :- [word];

# AU    Possessive adjective
'A-U' :- [word];

# C     Numeral
'C' :- [word];

# C*    Frequency numeral (i.e. 'times')
'C-*' :- [word];

# C}    Roman numeral
'C-}' :- [word];

# C=    Numeral written using digits
'C-=' :- [word];

# C?    How-many, how-much numeral
'C-?' :- [word];

# Ca    Indefinite numeral
'C-a' :- [word];

# Cd    Generic numeral with adjective declension
'C-d' :- [word];

'C-h' :- [word];

# Cj    Numeral, generic >= 4 used with a syntactic noun
'C-j' :- [word];

# Ck    Numeral, generic >= 4 in short form
'C-k' :- [word];

# Cl    Cardinal number
'C-l' :- [word];

# Cn    Cardinal number >= 5
'C-n' :- [word];

# Co    Numeral, multiplicative indefinite (e.g. many times)
'C-o' :- [word];

# Cr    Ordinal numeral
'C-r' :- [word];

# Cu    Numeral, interrogative (i.e. 'how many times')
'C-u' :- [word];

# Cv    Numeral, multiplicative, definite (i.e. 'five times')
'C-v' :- [word];

# Cw    Numeral, indefinite, adjectival declension
'C-w' :- [word];

# Cy    Numeral, fraction
'C-y' :- [word];

# Cz    Numeral, interrogative
'C-z' :- [word];

# D     Adverb
'D' :- [word];

# Db    Adverb
'D-b' :- [word];

# Dg    Negation adverb
'D-g' :- [word];

# I     Interjection
'I' :- [word];

# II    Interjection
'I-I' :- [word];

# J     Conjunction
'J' :- [word];

'J-*' :- [word];

# J,    Conjunction subordinate
'J-,' :- [word];

# J^    Conjunction connecting main clauses
'J-^' :- [word];

# N     Noun
'N' :- [word];
'N1' :- [word];
'N2' :- [word];
'N3' :- [word];
'N4' :- [word];
'N5' :- [word];
'N6' :- [word];
'N7' :- [word];
'NX' :- [word];

# NN    General noun
'N-N' :- [word];

# P     Pronoun
'P' :- [word];

'P-1' :- [word];

# P4    Relative/interrogative pronoun
'P-4' :- [word];

# P5    The pronoun 'he' after preposition
'P-5' :- [word];

# P6    Reflexive pronoun
'P-6' :- [word];

# P7    Reflexive pronoun
'P-7' :- [word];

# P8    Possessive reflexive pronoun
'P-8' :- [word];

# P9    Relative pronoun after a preposition
'P-9' :- [word];

# PD    Demonstrative pronoun
'P-D' :- [word];

# PE    Relative pronoun
'P-E' :- [word];

# PH    Personal pronoun
'P-H' :- [word];

# PJ    Relative pronoun
'P-J' :- [word];

# PK    Relative/interrogative pronoun
'P-K' :- [word];

# PL    Indefinite pronoun
'P-L' :- [word];

# PO    Sole reflexive pronoun (like. oneself)
'P-O' :- [word];

# PP    Personal pronoun
'P-P' :- [word];

# PQ    Relative/interrogative pronoun
'P-Q' :- [word];

# PS    Possessive pronoun
'P-S' :- [word];

# PW    Negative pronoun (e.g. nobody, nothing)
'P-W' :- [word];

# PY    Relative/interrogative pronoun used as an enclitic
'P-Y' :- [word];

# PZ    Indefinite pronoun
'P-Z' :- [word];

# V     Verb
'V' :- [word];

# VB    Verb, present or future form
'V-B' :- [word];

# Vc    Conditional verb (i.e. would)
'V-c' :- [word];

# Ve    Verb, present participle
'V-e' :- [word];

# Vf    Infinitive verb
'V-f' :- [word];

# Vi    Imperative verb
'V-i' :- [word];

# Vm    Verb, past transgressive
'V-m' :- [word];

# Vp    Verb, past participle, active
'V-p' :- [word];

# Vq    Verb, past participle, active, with enclitic -t
'V-q' :- [word];

# Vs    Verb, past participle, passive
'V-s' :- [word];

# Vt    Verb, present or future tense, with enclitic -t
'V-t' :- [word];

# R     Preposition
'R' :- [word];

# RR    General preposition
'R-R' :- [word];

# RV    Preposition with vocalization
'R-V' :- [word];

# RF    Composite preposition (e.g. not only ... but also ...)
'R-F' :- [word];

# T     Particle
'T' :- [word];

# TT    General particle
'T-T' :- [word];

# X     Unknown
'X' :- [word];

'X-x' :- [word];

# X@    Unrecognized word form
'X-@' :- [word];

# Z     Punctuation
'Z' :- [word];

# Z#    Sentence boundary
'Z-#' :- [word];

# Z:    Punctuation
'Z-:' :- [word];

########## Inflectional attributes ##########

@attr 'Cas=1' :- { };
@attr 'Cas=2' :- { };
@attr 'Cas=3' :- { };
@attr 'Cas=4' :- { };
@attr 'Cas=5' :- { };
@attr 'Cas=6' :- { };
@attr 'Cas=7' :- { };
@attr 'Cas=X' :- { };

@attr 'Gen=F' :- { };
@attr 'Gen=H' :- { };
@attr 'Gen=I' :- { };
@attr 'Gen=M' :- { };
@attr 'Gen=N' :- { };
@attr 'Gen=Q' :- { };
@attr 'Gen=T' :- { };
@attr 'Gen=X' :- { };
@attr 'Gen=Y' :- { };
@attr 'Gen=Z' :- { };

@attr 'Gra=1' :- {};
@attr 'Gra=2' :- {};
@attr 'Gra=3' :- {};

@attr 'Neg=A' :- {};
@attr 'Neg=N' :- {};

@attr 'Num=D' :- { };
@attr 'Num=P' :- { };
@attr 'Num=S' :- { };
@attr 'Num=W' :- { };
@attr 'Num=X' :- { };

@attr 'PGe=F' :- { };
@attr 'PGe=M' :- { };
@attr 'PGe=X' :- { };
@attr 'PGe=Z' :- { };

@attr 'Per=1' :- { };
@attr 'Per=2' :- { };
@attr 'Per=3' :- { };
@attr 'Per=X' :- { };

@attr 'Sem=E' :- {};
@attr 'Sem=G' :- {};
@attr 'Sem=K' :- {};
@attr 'Sem=R' :- {};
@attr 'Sem=S' :- {};
@attr 'Sem=Y' :- {};
@attr 'Sem=m' :- {};

@attr 'Ten=F' :- {};
@attr 'Ten=P' :- {};
@attr 'Ten=R' :- {};
@attr 'Ten=X' :- {};

@attr 'Var=1' :- {};
@attr 'Var=2' :- {};
@attr 'Var=3' :- {};
@attr 'Var=4' :- {};
@attr 'Var=5' :- {};
@attr 'Var=6' :- {};
@attr 'Var=7' :- {};
@attr 'Var=8' :- {};
@attr 'Var=9' :- {};

@attr 'Voi=A' :- {};
@attr 'Voi=P' :- {};

@dmv;
@enable_split_head;

########## POS Tags ##########

# Vfin      Verb, finite
'Vfin' :- [word];

# Vimp      Verb, imperative
'Vimp' :- [word];

# Vcnd      Verb, conditional (e.g. 'if')
'Vcnd' :- [word];

# Vte       Verb, participle (e.g. '-ing')
'Vte' :- [word];

# Vbas      Verb, base form
'Vbas' :- [word];

# V         Verb (either nagation '-azu' or exemplative '-tari'; -tari form is used in enumerating actions when asked 'What did you do on Sunday?' -- 'I was _singing_ and _doing_ homework.')
'V' :- [word];

# VAUXfin   Auxiliary verb, finite (e.g. 'iru' to be -- 'Watashi-ha gohan-o tabe iru.' I am eating rice.)
'VAUXfin' :- [word];

# VAUXimp   Auxiliary verb, imperative
'VAUXimp' :- [word];

# VAUXcnd   Auxiliary verb, conditional
'VAUXcnd' :- [word];

# VAUXte    Auxiliary verb, participle
'VAUXte' :- [word];

# VAUXbas   Auxiliary verb, base form
'VAUXbas' :- [word];

# VAUX      Auxiliary verb, in -tari or -azu forms
'VAUX' :- [word];

# VSfin     Supporting verb, finite (e.g. 'suru' to do -- 'Watashi-ha benkyoo shimasu.' I am doing the study.)
'VSfin' :- [word];

# VSimp     Supporting verb, imperative
'VSimp' :- [word];

# VScnd     Supporting verb, conditional
'VScnd' :- [word];

# VSte      Supporting verb, participle
'VSte' :- [word];

# VSbas     Supporting verb, base form
'VSbas' :- [word];

# VS        Supporting verb, in -tari or -azu forms
'VS' :- [word];

# PVfin     Particle verb, finite (e.g. 'da/desu' to be -- 'Watashi-ha gakusei desu.' I am a student.)
'PVfin' :- [word];

# PVimp     Particle verb, imperative
'PVimp' :- [word];

# PVcnd     Particle verb, conditional
'PVcnd' :- [word];

# PVte      Particle verb, participle
'PVte' :- [word];

# PV        Particle verb
'PV' :- [word];

# NN        Common noun
'NN' :- [word];

# NF        Formal noun
'NF' :- [word];

# PRON      Personal pronoun
'N-PRON' :- [word];

# NAME      Proper noun
'NAME' :- [word];

# NAMEper   Proper noun, person
'NAMEper' :- [word];

# NAMEloc   Proper noun, location
'NAMEloc' :- [word];

# NAMEorg   Proper noun, organization
'NAMEorg' :- [word];

# NT        Temporal noun
'NT' :- [word];

# Ndem      Demonstrative noun
'Ndem' :- [word];

# Nwh       Interrogative noun
'Nwh' :- [word];

'Ntmp' :- [word];

# CD        Cardinal numeral
'CD' :- [word];

# CDtime    Cardinal numeral with time unit
'CDtime' :- [word];

# CDdate    Cardinal numeral with date unit
'CDdate' :- [word];

# CDU       Cardinal numeral with other unit
'CDU' :- [word];

# UNIT      Unit/classifier
'UNIT' :- [word];

# PreN      Noun prefix (e.g. 'dai' the great)
'PreN' :- [word];

# Nsf       Noun suffix (e.g. 'tomo' both)
'Nsf' :- [word];

# PNsf      Personal name suffix (e.g. 'sama' like)
'PNsf' :- [word];

# VN        Verbal noun (e.g. 'benkyoo' in 'benkyoo suru' to do a study)
'N-VN' :- [word];

# P         Postposition
'P' :- [word];

# Pacc      Accusative case (i.e. '-o')
'Pacc' :- [word];

# Pnom      Nominative case (i.e. '-ga')
'Pnom' :- [word];

# Pgen      Genitive case (i.e. '-no')
'Pgen' :- [word];

# Pfoc      Focus postposition (e.g. '-wa')
'Pfoc' :- [word];

# PQ        Quotative postposition (e.g. 'that')
'PQ' :- [word];

# Pcnj      Conjunctive postposition
'Pcnj' :- [word];

# PSSa      Subordinate clause postposition, conditional/causal (e.g. if)
'PSSa' :- [word];

# PSSb      Subordinate clause postposition, adversative (i.e. but)
'PSSb' :- [word];

# PSSq      Subordinate clause postposition, interrogative (i.e. whether)
'PSSq' :- [word];

# PSE       Sentence end postposition (e.g. 'ka' QUES)
'PSE' :- [word];

# ADJifin   i-adjective, finite
'ADJifin' :- [word];

# ADJiku    i-adjective, adverbial
'ADJiku' :- [word];

# ADJite    i-adjective, participle
'ADJite' :- [word];

# ADJicnd   i-adjective, conditional
'ADJicnd' :- [word];

# VADJi     i-adjective verb
'VADJi' :- [word];

# ADJ_n     na-adjective
'ADJ_n' :- [word];

# ADJteki   na-adjective, ending in -teki
'ADJteki' :- [word];

# VADJ_n    na-adjective verb
'VADJ_n' :- [word];

# ADJ       Attributive adjective
'ADJ' :- [word];

# ADJdem    Demonstrative adjective
'ADJdem' :- [word];

# ADJwh     Wh-adjective
'ADJwh' :- [word];

# ADJsf     Adjective suffix
'ADJsf' :- [word];

# PADJ      Particle adjective
'PADJ' :- [word];

# ADV       Adverb
'ADV' :- [word];

# ADVdem    Demonstrative adverb
'ADVdem' :- [word];

# ADVdgr    Degree adverb
'ADVdgr' :- [word];

# ADVtmp    Temporal adverb
'ADVtmp' :- [word];

# ADVwh     Wh-adverb
'ADVwh' :- [word];

# PADV      Particle adverb
'PADV' :- [word];

# CNJ       Conjunction
'CNJ' :- [word];

# ITJ       Interjection
'ITJ' :- [word];

########## Inflectional attributes ##########

@attr 'eN' :- {};
@attr 'kute' :- {};
@attr 'ta' :- {};
@attr 'u' :- {};


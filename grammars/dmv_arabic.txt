@dmv;
@enable_split_head;

########## POS tags ##########

# A     adjective
'A' :- [word];

# C     conjunction/subjunction
'C' :- [word];

# D     adverb
'D' :- [word];

# F     function word, other particle
'F' :- [word];

# FI    interrogative particle
'FI' :- [word];

# FN    negation particle
'FN' :- [word];

# G     punctuation
'G' :- [word];

# I     interjection
'I' :- [word];

# N     noun
'N' :- [word];

# Nnom  nominative noun
'Nnom' :- [word];

# Nga   genitive/accusative noun
'Nga' :- [word];

# Ngen  genitive noun
'Ngen' :- [word];

# P     preposition
'P' :- [word];

# Q     number
'Q' :- [word];

# SD    demonstrative pronoun
'SD' :- [word];

# SR    relative pronoun
'SR' :- [word];

'Snom' :- [word];

'Sgen' :- [word];

'Sga' :- [word];

# S     other pronoun
'S' :- [word];

# T     typo
'T' :- [word];

# VP    verb, perfect
'VP' :- [word];

# VI    verb, imperfect
'VI' :- [word];

'V' :- [word];

# X     non-alphabetic
'X' :- [word];

# Y     abbreviation
'Y' :- [word];

# Z     proper noun
'Z' :- [word];

########## Inflectional attributes ##########

@attr 'case=1' :- {};
@attr 'case=2' :- {};
@attr 'case=4' :- {};
@attr 'def=D' :- {};
@attr 'def=I' :- {};
@attr 'def=R' :- {};
@attr 'gen=F' :- {};
@attr 'gen=M' :- {};
@attr 'mood=D' :- {};
@attr 'mood=I' :- {};
@attr 'mood=S' :- {};
@attr 'num=D' :- {};
@attr 'num=P' :- {};
@attr 'num=S' :- {};
@attr 'pers=1' :- {};
@attr 'pers=2' :- {};
@attr 'pers=3' :- {};
@attr 'voice=P' :- {};

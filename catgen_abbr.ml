(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Cg
open Macro
open Dict
open Utils
open Printf
open Avm

module Catgen = struct

    type slashdir_t = Fwd | Bwd

    let make_syncat inner outer slashdir is_inner_head =
        match (slashdir, is_inner_head) with
            (Bwd, false) ->
                Syncat.create_bwd inner outer Syncat.Left
          | (Bwd, true) ->
                Syncat.create_bwd inner outer Syncat.Right
          | (Fwd, false) ->
                Syncat.create_fwd inner outer Syncat.Right
          | (Fwd, true) ->
                Syncat.create_fwd inner outer Syncat.Left

    type depscheme_t = HeadOutwardScheme | DepbankScheme

    type word_order_t =
        SVOI | SVIO | SIVO | ISVO
      | SOVI | SOIV | SIOV | ISOV
      | VSOI | VSIO | VISO | IVSO
      | OVSI | OVIS | OIVS | IOVS
      | OSVI | OSIV | OISV | IOSV
      | VOSI | VOIS | VIOS | IVOS
    
    let all_word_orders = [
        SVOI; SVIO; SIVO; ISVO;
        SOVI; SOIV; SIOV; ISOV;
        VSOI; VSIO; VISO; IVSO;
        OVSI; OVIS; OIVS; IOVS;
        OSVI; OSIV; OISV; IOSV;
        VOSI; VOIS; VIOS; IVOS
    ]
    
    type v_order_t = VerbMedial | VerbFinal | VerbInitial
    
    type so_order_t = SubjObj | ObjSubj
    
    type iobj_order_t = 
        IobjOutLeft | IobjOutRight | IobjInLeft | IobjInRight
    
    let interpret_word_order word_order =
        match word_order with
            SVOI -> (VerbMedial, SubjObj, IobjOutRight)
          | SVIO -> (VerbMedial, SubjObj, IobjInRight)
          | SIVO -> (VerbMedial, SubjObj, IobjInLeft)
          | ISVO -> (VerbMedial, SubjObj, IobjOutLeft)
          | SOVI -> (VerbFinal, SubjObj, IobjOutRight)
          | SOIV -> (VerbFinal, SubjObj, IobjInRight)
          | SIOV -> (VerbFinal, SubjObj, IobjInLeft)
          | ISOV -> (VerbFinal, SubjObj, IobjOutLeft)
          | VSOI -> (VerbInitial, SubjObj, IobjOutRight)
          | VSIO -> (VerbInitial, SubjObj, IobjInRight)
          | VISO -> (VerbInitial, SubjObj, IobjInLeft)
          | IVSO -> (VerbInitial, SubjObj, IobjOutLeft)
          | OVSI -> (VerbMedial, ObjSubj, IobjOutRight)
          | OVIS -> (VerbMedial, ObjSubj, IobjInRight)
          | OIVS -> (VerbMedial, ObjSubj, IobjInLeft)
          | IOVS -> (VerbMedial, ObjSubj, IobjOutLeft)
          | OSVI -> (VerbFinal, ObjSubj, IobjOutRight)
          | OSIV -> (VerbFinal, ObjSubj, IobjInRight)
          | OISV -> (VerbFinal, ObjSubj, IobjInLeft)
          | IOSV -> (VerbFinal, ObjSubj, IobjOutLeft)
          | VOSI -> (VerbInitial, ObjSubj, IobjOutRight)
          | VOIS -> (VerbInitial, ObjSubj, IobjInRight)
          | VIOS -> (VerbInitial, ObjSubj, IobjInLeft)
          | IVOS -> (VerbInitial, ObjSubj, IobjOutLeft)
    
    type vicomp_order_t =
        SVC | SCV | CSV
      | VSC | VCS | CVS
    
    let all_vicomp_orders = [
        SVC; SCV; CSV; VSC; VCS; CVS
    ]
    
    type vtcomp_order_t =
        SVOC | SVCO | SCVO | CSVO
      | SOVC | SOCV | SCOV | CSOV
      | VSOC | VSCO | VCSO | CVSO
      | OVSC | OVCS | OCVS | COVS
      | OSVC | OSCV | OCSV | COSV
      | VOSC | VOCS | VCOS | CVOS
    
    let all_vtcomp_orders = [
        SVOC; SVCO; SCVO; CSVO;
        SOVC; SOCV; SCOV; CSOV;
        VSOC; VSCO; VCSO; CVSO;
        OVSC; OVCS; OCVS; COVS;
        OSVC; OSCV; OCSV; COSV;
        VOSC; VOCS; VCOS; CVOS
    ]
    
    type objcomp_order_t = ObjComp | CompObj
    
    type comp_order_t = 
        CompOutLeft | CompOutRight | CompInLeft | CompInRight

    let interpret_vicomp_order vicomp_order =
        match vicomp_order with
            SVC -> (VerbMedial, CompOutRight)
          | SCV -> (VerbMedial, CompInRight)
          | CSV -> (VerbMedial, CompOutLeft)
          | VSC -> (VerbInitial, CompOutRight)
          | VCS -> (VerbInitial, CompInRight)
          | CVS -> (VerbInitial, CompOutLeft)
    
    let interpret_vtcomp_order vtcomp_order =
        match vtcomp_order with
            SVOC -> (VerbMedial, ObjComp, CompOutRight)
          | SVCO -> (VerbMedial, CompObj, CompInRight)
          | SCVO -> (VerbMedial, CompObj, CompInLeft)
          | CSVO -> (VerbMedial, CompObj, CompOutLeft)
          | SOVC -> (VerbFinal, ObjComp, CompOutRight)
          | SOCV -> (VerbFinal, ObjComp, CompInRight)
          | SCOV -> (VerbFinal, ObjComp, CompInLeft)
          | CSOV -> (VerbFinal, ObjComp, CompOutLeft)
          | VSOC -> (VerbInitial, ObjComp, CompOutRight)
          | VSCO -> (VerbInitial, CompObj, CompInRight)
          | VCSO -> (VerbInitial, CompObj, CompInLeft)
          | CVSO -> (VerbInitial, CompObj, CompOutLeft)
          | OVSC -> (VerbMedial, ObjComp, CompOutRight)
          | OVCS -> (VerbMedial, ObjComp, CompInRight)
          | OCVS -> (VerbMedial, ObjComp, CompInLeft)
          | COVS -> (VerbMedial, CompObj, CompOutLeft)
          | OSVC -> (VerbFinal, ObjComp, CompOutRight)
          | OSCV -> (VerbFinal, ObjComp, CompInRight)
          | OCSV -> (VerbFinal, ObjComp, CompInLeft)
          | COSV -> (VerbFinal, CompObj, CompOutLeft)
          | VOSC -> (VerbInitial, ObjComp, CompOutRight)
          | VOCS -> (VerbInitial, ObjComp, CompInRight)
          | VCOS -> (VerbInitial, CompObj, CompInLeft)
          | CVOS -> (VerbInitial, CompObj, CompOutLeft)
    
    type adj_order_t = NounAdj | AdjNoun
    
    let all_adj_orders = [NounAdj; AdjNoun]
    
    type adv_order_t = VpAdv | AdvVp
    
    let all_adv_orders = [VpAdv; AdvVp]
    
    type advadj_order_t = AdvAdj | AdjAdv
    
    let all_advadj_orders = [AdvAdj; AdjAdv]

    type modal_order_t = ModalVp | VpModal

    let all_modal_orders = [ModalVp; VpModal]
    
    type part_order_t = PartVp | VpPart

    let all_part_orders = [PartVp; VpPart]
    
    type adposition_t = Preposition | Postposition
    
    let all_adpositions = [Preposition; Postposition]
    
    type nmod_order_t = NpNmod | NmodNp
    
    let all_nmod_orders = [NpNmod; NmodNp]
    
    type vmod_order_t = VpVmod | VmodVp
    
    let all_vmod_orders = [VpVmod; VmodVp]
    
    type poss_order_t =
        OwnerPossOwnee | OwneePossOwner 
      | OwnerOwneePoss | OwneeOwnerPoss
      | PossOwnerOwnee | PossOwneeOwner
    
    let all_poss_orders = [
        OwnerPossOwnee; OwneePossOwner; 
        OwnerOwneePoss; OwneeOwnerPoss; 
        PossOwnerOwnee; PossOwneeOwner
    ]
    
    type relpro_order_t = RelproComp | CompRelpro
    
    let all_relpro_orders = [RelproComp; CompRelpro]
    
    type relcls_order_t = NpRelcls | RelclsNp

    let all_relcls_orders = [NpRelcls; RelclsNp]
    
    type subcls_order_t =
        ConjMainSubcls | MainConjSubcls | MainSubclsConj
      | ConjSubclsMain | SubclsConjMain | SubclsMainConj

    let all_subcls_orders = [
        ConjMainSubcls; MainConjSubcls; MainSubclsConj; 
        ConjSubclsMain; SubclsConjMain; SubclsMainConj
    ]
    
    type npgerund_order_t = NpGerund | GerundNp

    let all_npgerund_orders = [NpGerund; GerundNp]
    
    type vpgerund_order_t = VpGerund | GerundVp

    let all_vpgerund_orders = [VpGerund; GerundVp]
    
    type infvp_order_t = InfVp | VpInf
    
    let all_infvp_orders = [InfVp; VpInf]
    
    type npnom_order_t = NomNp | NpNom
    
    let all_npnom_orders = [NomNp; NpNom]
    
    type vpnom_order_t = NomVp | VpNom
    
    let all_vpnom_orders = [NomVp; VpNom]
    
    type smod_order_t = SmodSent | SentSmod
    
    let all_smod_orders = [SmodSent; SentSmod]

    type nump_order_t = NumCl | ClNum

    let all_nump_orders = [NumCl; ClNum]

    type nump_position_t = NumpAsAdj | NumpAsAdv | NumpAsNmod | NumpAsVmod
    
    let all_nump_positions = [NumpAsAdj; NumpAsAdv; NumpAsNmod; NumpAsVmod]
    
    type drop_t = SubjDrop | ObjDrop | IobjDrop

    type neg_order_t =
        NegV | VNeg 
      | NegAdj | AdjNeg 
      | NegAdv | AdvNeg 
      | NegNmod | NmodNeg 
      | NegVmod | VmodNeg

    let all_neg_orders = [NegV; VNeg ; NegAdj; AdjNeg ; NegAdv; AdvNeg ; NegNmod; NmodNeg ; NegVmod; VmodNeg]

    type verb_decl_t = VDecl_by_subj | VDecl_by_obj | VDecl_by_iobj

    let all_verb_decls = [VDecl_by_subj; VDecl_by_obj; VDecl_by_iobj]

    type noun_verb_agr_t = NVAgr_by_gender | NVAgr_by_number | NVAgr_by_person

    let all_noun_verb_agrs = [NVAgr_by_gender; NVAgr_by_number; NVAgr_by_person]

    type adj_noun_agr_t = ANAgr_by_gender | ANAgr_by_number | ANAgr_by_case

    let all_adj_noun_agrs = [ANAgr_by_gender; ANAgr_by_number; ANAgr_by_case]

    type modal_infl_t = Modal_infl | Modal_req_infv

    type macroexp_t = MacroExpander.t

    type dict_t = Dict.t
    
    type t = {
        mutable depscheme             : depscheme_t;
        mutable is_freewordorder      : bool;
        mutable is_infl               : bool;
        mutable verb_decls            : verb_decl_t list option;
        mutable noun_verb_agrs        : noun_verb_agr_t list option;
        mutable adj_noun_agrs         : adj_noun_agr_t list option;
        mutable genders               : string list;
        mutable persons               : string list;
        mutable numbers               : string list;
        mutable cases_arg             : string list;
        mutable cases_apposarg        : string list;
        mutable cases_adj             : string list;
        mutable cases_all             : string list;
        mutable word_orders           : word_order_t list option;
        mutable vicomp_orders         : vicomp_order_t list option;
        mutable vtcomp_orders         : vtcomp_order_t list option;
        mutable adj_orders            : adj_order_t list option;
        mutable adv_orders            : adv_order_t list option;
        mutable advadj_orders         : advadj_order_t list option;
        mutable modal_orders          : modal_order_t list option;
        mutable modal_infls           : modal_infl_t list option;
        mutable part_orders           : part_order_t list option;
        mutable adpositions           : adposition_t list option;
        mutable nmod_orders           : nmod_order_t list option;
        mutable vmod_orders           : vmod_order_t list option;
        mutable poss_orders           : poss_order_t list option;
        mutable relpro_orders         : relpro_order_t list option;
        mutable relpro_infls          : adj_noun_agr_t list option;
        mutable relcls_orders         : relcls_order_t list option;
        mutable subcls_orders         : subcls_order_t list option;
        mutable npgerund_orders       : npgerund_order_t list option;
        mutable npgerund_infls        : adj_noun_agr_t list option;
        mutable vpgerund_orders       : vpgerund_order_t list option;
        mutable infvp_orders          : infvp_order_t list option;
        mutable npnom_orders          : npnom_order_t list option;
        mutable vpnom_orders          : vpnom_order_t list option;
        mutable smod_orders           : smod_order_t list option;
        mutable nump_orders           : nump_order_t list option;
        mutable nump_positions        : nump_position_t list option;
        mutable nump_infls            : adj_noun_agr_t list option;
        mutable neg_orders            : neg_order_t list option;
        mutable allows_copula         : bool;
        mutable allows_gerund_as_np   : bool;
        mutable allows_gerund_as_nmod : bool;
        mutable allows_gerund_as_vmod : bool;
        mutable allows_shift          : bool;
        mutable drops                 : drop_t list option;
        macroexp                      : macroexp_t;
        dict                          : dict_t;
        mutable head_adj              : bool;
        mutable head_nmod             : bool;
        mutable head_verb             : bool;
        mutable head_copula           : bool;
        mutable head_vmod             : bool;
        mutable head_gmod             : bool;
        mutable head_smod             : bool;
        mutable head_adv              : bool;
        mutable head_neg              : bool;
        mutable head_modal            : bool;
        mutable head_adpos            : bool;
        mutable head_relpro           : bool;
        mutable head_part             : bool;
        mutable head_subconj_smod     : bool;
        mutable head_subconj_phr      : bool;
        mutable head_poss_nmod        : bool;
        mutable head_poss_phr         : bool;
        mutable head_inf              : bool;
        mutable head_npnom            : bool;
        mutable head_vpnom            : bool;
        mutable head_cl               : bool;
    }
    
    let create macroexp dict = {
        depscheme = HeadOutwardScheme;
        is_freewordorder = false;
        is_infl = false;
        verb_decls = None;
        noun_verb_agrs = None;
        adj_noun_agrs = None;
        genders = [];
        persons = [];
        numbers = [];
        cases_arg = [];
        cases_apposarg = [];
        cases_adj = [];
        cases_all = [];
        word_orders = None;
        vicomp_orders = None;
        vtcomp_orders = None;
        adj_orders = None;
        adv_orders = None;
        advadj_orders = None;
        modal_orders = None;
        modal_infls = None;
        part_orders = None;
        adpositions = None;
        nmod_orders = None;
        vmod_orders = None;
        poss_orders = None;
        relpro_orders = None;
        relpro_infls = None;
        relcls_orders = None;
        subcls_orders = None;
        npgerund_orders = None;
        npgerund_infls = None;
        vpgerund_orders = None;
        infvp_orders = None;
        npnom_orders = None;
        vpnom_orders = None;
        smod_orders = None;
        nump_orders = None;
        nump_positions = None;
        nump_infls = None;
        neg_orders = Some [];
        allows_copula = false;
        allows_gerund_as_np = false;
        allows_gerund_as_nmod = false;
        allows_gerund_as_vmod = false;
        allows_shift = false;
        drops = None;
        macroexp = macroexp;
        dict = dict;
        head_adj = false;
        head_nmod = false;
        head_verb = true;
        head_copula = true;
        head_vmod = false;
        head_gmod = false;
        head_smod = false;
        head_adv = false;
        head_neg = false;
        head_modal = true;
        head_adpos = true;
        head_relpro = true;
        head_part = false;
        head_subconj_smod = false;
        head_subconj_phr = true;
        head_poss_nmod = false;
        head_poss_phr = false;
        head_inf = true;
        head_npnom = true;
        head_vpnom = true;
        head_cl = false;
    }

    let change_depscheme catgen depscheme = begin
        catgen.depscheme <- depscheme;
        match depscheme with
            HeadOutwardScheme -> begin
                catgen.head_relpro <- true;
                catgen.head_inf <- true
            end
          | DepbankScheme -> begin
                catgen.head_relpro <- false;
                catgen.head_inf <- false
            end
    end

    let set_freewordorder catgen value =
        catgen.is_freewordorder <- value

    let set_infl catgen value =
        catgen.is_infl <- value

    let add_verb_decl catgen verb_decl =
        let old =
            match catgen.verb_decls with
                None -> []
              | Some x -> x in
        catgen.verb_decls <- Some (verb_decl :: old)

    let add_noun_verb_agr catgen noun_verb_agr =
        let old =
            match catgen.noun_verb_agrs with
                None -> []
              | Some x -> x in
        catgen.noun_verb_agrs <- Some (noun_verb_agr :: old)

    let add_adj_noun_agr catgen adj_noun_agr =
        let old =
            match catgen.adj_noun_agrs with
                None -> []
              | Some x -> x in
        catgen.adj_noun_agrs <- Some (adj_noun_agr :: old)

    let add_gender catgen gender =
        if not (List.mem gender catgen.genders) then
            catgen.genders <- catgen.genders @ [gender]
        else ()

    let add_person catgen person =
        if not (List.mem person catgen.persons) then
            catgen.persons <- catgen.persons @ [person]
        else ()

    let add_number catgen number =
        if not (List.mem number catgen.numbers) then
            catgen.numbers <- catgen.numbers @ [number]
        else ()

    let add_case_arg catgen case =
        if not (List.mem case catgen.cases_arg) then
            catgen.cases_arg <- catgen.cases_arg @ [case]
        else ();
        if not (List.mem case catgen.cases_all) then
            catgen.cases_all <- catgen.cases_all @ [case]
        else ()
    
    let add_case_apposarg catgen case =
        if not (List.mem case catgen.cases_apposarg) then
            catgen.cases_apposarg <- catgen.cases_apposarg @ [case]
        else ();
        if not (List.mem case catgen.cases_all) then
            catgen.cases_all <- catgen.cases_all @ [case]
        else ()
    
    let add_case_adj catgen case =
        if not (List.mem case catgen.cases_adj) then
            catgen.cases_adj <- catgen.cases_adj @ [case]
        else ();
        if not (List.mem case catgen.cases_all) then
            catgen.cases_all <- catgen.cases_all @ [case]
        else ()
    
    let add_word_order catgen word_order =
        let old = 
            match catgen.word_orders with
                None -> []
              | Some x -> x in
        catgen.word_orders <- Some (word_order :: old)
    
    let allow_all_word_orders catgen =
        catgen.word_orders <- None
    
    let add_vicomp_order catgen vicomp_order =
        let old = 
            match catgen.vicomp_orders with
                None -> []
              | Some x -> x in
        catgen.vicomp_orders <- Some (vicomp_order :: old)
    
    let allow_all_vicomp_orders catgen =
        catgen.vicomp_orders <- None
    
    let add_vtcomp_order catgen vtcomp_order =
        let old = 
            match catgen.vtcomp_orders with
                None -> []
              | Some x -> x in
        catgen.vtcomp_orders <- Some (vtcomp_order :: old)
    
    let allow_all_vtcomp_orders catgen =
        catgen.vtcomp_orders <- None
    
    let add_adj_order catgen adj_order =
        let old =
            match catgen.adj_orders with
                None -> []
              | Some x -> x in
        catgen.adj_orders <- Some (adj_order :: old)
    
    let allow_all_adj_orders catgen =
        catgen.adj_orders <- None
    
    let add_adv_order catgen adv_order =
        let old =
            match catgen.adv_orders with
                None -> []
              | Some x -> x in
        catgen.adv_orders <- Some (adv_order :: old)
    
    let allow_all_adv_orders catgen =
        catgen.adv_orders <- None
    
    let add_advadj_order catgen advadj_order =
        let old =
            match catgen.advadj_orders with
                None -> []
              | Some x -> x in
        catgen.advadj_orders <- Some (advadj_order :: old)
    
    let allow_all_advadj_orders catgen =
        catgen.advadj_orders <- None
    
    let add_modal_order catgen modal_order =
        let old =
            match catgen.modal_orders with
                None -> []
              | Some x -> x in
        catgen.modal_orders <- Some (modal_order :: old)
    
    let allow_all_modal_orders catgen =
        catgen.modal_orders <- None
    
    let add_modal_infl catgen modal_infl =
        let old =
            match catgen.modal_infls with
                None -> []
              | Some x -> x in
        catgen.modal_infls <- Some (modal_infl :: old)

    let add_part_order catgen part_order =
        let old =
            match catgen.part_orders with
                None -> []
              | Some x -> x in
        catgen.part_orders <- Some (part_order :: old)
    
    let allow_all_part_orders catgen =
        catgen.part_orders <- None
    
    let add_adposition catgen adposition =
        let old =
            match catgen.adpositions with
                None -> []
              | Some x -> x in
        catgen.adpositions <- Some (adposition :: old)
    
    let allow_all_adpositions catgen =
        catgen.adpositions <- None
    
    let add_nmod_order catgen nmod_order =
        let old =
            match catgen.nmod_orders with
                None -> []
              | Some x -> x in
        catgen.nmod_orders <- Some (nmod_order :: old)
    
    let allow_all_nmod_orders catgen =
        catgen.nmod_orders <- None
    
    let add_vmod_order catgen vmod_order =
        let old =
            match catgen.vmod_orders with
                None -> []
              | Some x -> x in
        catgen.vmod_orders <- Some (vmod_order :: old)
    
    let allow_all_vmod_orders catgen =
        catgen.vmod_orders <- None
    
    let add_poss_order catgen poss_order =
        let old =
            match catgen.poss_orders with
                None -> []
              | Some x -> x in
        catgen.poss_orders <- Some (poss_order :: old)
    
    let allow_all_poss_orders catgen =
        catgen.poss_orders <- None
    
    let add_relpro_order catgen relpro_order =
        let old =
            match catgen.relpro_orders with
                None -> []
              | Some x -> x in
        catgen.relpro_orders <- Some (relpro_order :: old)
    
    let allow_all_relpro_orders catgen =
        catgen.relpro_orders <- None

    let add_relpro_infl catgen relpro_infl =
        let old =
            match catgen.relpro_infls with
                None -> []
              | Some x -> x in
        catgen.relpro_infls <- Some (relpro_infl :: old)
    
    let add_relcls_order catgen relcls_order =
        let old =
            match catgen.relcls_orders with
                None -> []
              | Some x -> x in
        catgen.relcls_orders <- Some (relcls_order :: old)
    
    let allow_all_relcls_orders catgen =
        catgen.relcls_orders <- None
    
    let add_subcls_order catgen subcls_order =
        let old =
            match catgen.subcls_orders with
                None -> []
              | Some x -> x in
        catgen.subcls_orders <- Some (subcls_order :: old)
    
    let allow_all_subcls_orders catgen =
        catgen.subcls_orders <- None
    
    let add_npgerund_order catgen npgerund_order =
        let old =
            match catgen.npgerund_orders with
                None -> []
              | Some x -> x in
        catgen.npgerund_orders <- Some (npgerund_order :: old)
    
    let allow_all_npgerund_orders catgen =
        catgen.npgerund_orders <- None

    let add_npgerund_infl catgen npgerund_infl =
        let old =
            match catgen.npgerund_infls with
                None -> []
              | Some x -> x in
        catgen.npgerund_infls <- Some (npgerund_infl :: old)
    
    let add_vpgerund_order catgen vpgerund_order =
        let old =
            match catgen.vpgerund_orders with
                None -> []
              | Some x -> x in
        catgen.vpgerund_orders <- Some (vpgerund_order :: old)
    
    let allow_all_vpgerund_orders catgen =
        catgen.vpgerund_orders <- None
    
    let add_infvp_order catgen infvp_order =
        let old =
            match catgen.infvp_orders with
                None -> []
              | Some x -> x in
        catgen.infvp_orders <- Some (infvp_order :: old)
    
    let allow_all_infvp_orders catgen =
        catgen.infvp_orders <- None
    
    let add_npnom_order catgen npnom_order =
        let old =
            match catgen.npnom_orders with
                None -> []
              | Some x -> x in
        catgen.npnom_orders <- Some (npnom_order :: old)
    
    let allow_all_npnom_orders catgen =
        catgen.npnom_orders <- None
    
    let add_vpnom_order catgen vpnom_order =
        let old =
            match catgen.vpnom_orders with
                None -> []
              | Some x -> x in
        catgen.vpnom_orders <- Some (vpnom_order :: old)
    
    let allow_all_vpnom_orders catgen =
        catgen.vpnom_orders <- None
    
    let add_smod_order catgen smod_order =
        let old =
            match catgen.smod_orders with
                None -> []
              | Some x -> x in
        catgen.smod_orders <- Some (smod_order :: old)
    
    let allow_all_smod_orders catgen =
        catgen.smod_orders <- None
    
    let add_nump_order catgen nump_order =
        let old =
            match catgen.nump_orders with
                None -> []
              | Some x -> x in
        catgen.nump_orders <- Some (nump_order :: old)
    
    let allow_all_nump_orders catgen =
        catgen.nump_orders <- None

    let add_nump_infl catgen nump_infl =
        let old =
            match catgen.nump_infls with
                None -> []
              | Some x -> x in
        catgen.nump_infls <- Some (nump_infl :: old)
    
    let add_nump_position catgen nump_position =
        let old =
            match catgen.nump_positions with
                None -> []
              | Some x -> x in
        catgen.nump_positions <- Some (nump_position :: old)
    
    let allow_all_nump_positions catgen =
        catgen.nump_positions <- None
    
    let add_neg_order catgen neg_order =
        let old =
            match catgen.neg_orders with
                None -> []
              | Some x -> x in
        catgen.neg_orders <- Some (neg_order :: old)
    
    let allow_all_neg_orders catgen =
        catgen.neg_orders <- None
    
    let enable_copula catgen =
        catgen.allows_copula <- true
    
    let disable_copula catgen =
        catgen.allows_copula <- false
    
    let enable_gerund_as_np catgen =
        catgen.allows_gerund_as_np <- true
    
    let disable_gerund_as_np catgen =
        catgen.allows_gerund_as_np <- false
    
    let enable_gerund_as_nmod catgen =
        catgen.allows_gerund_as_nmod <- true
    
    let disable_gerund_as_nmod catgen =
        catgen.allows_gerund_as_nmod <- false
    
    let enable_gerund_as_vmod catgen =
        catgen.allows_gerund_as_vmod <- true
    
    let disable_gerund_as_vmod catgen =
        catgen.allows_gerund_as_vmod <- false
    
    let enable_shift catgen =
        catgen.allows_shift <- true
    
    let disable_shift catgen =
        catgen.allows_shift <- false
    
    let add_drop catgen drop =
        let old =
            match catgen.drops with
                None -> []
              | Some x -> x in
        catgen.drops <- Some (drop :: old)
    
    let disable_drops catgen =
        catgen.drops <- Some []
    
    let allow_all_drops catgen =
        catgen.drops <- None
    
    let allows_subjdrop catgen =
        match catgen.drops with
            None -> true
          | Some x -> List.mem SubjDrop x
    
    let allows_objdrop catgen =
        match catgen.drops with
            None -> true
          | Some x -> List.mem ObjDrop x
    
    let allows_iobjdrop catgen =
        match catgen.drops with
            None -> true
          | Some x -> List.mem IobjDrop x

    let set_head_adj catgen value =
        catgen.head_adj <- value

    let set_head_nmod catgen value =
        catgen.head_nmod <- value

    let set_head_verb catgen value =
        catgen.head_verb <- value

    let set_head_copula catgen value =
        catgen.head_copula <- value

    let set_head_vmod catgen value =
        catgen.head_vmod <- value

    let set_head_gmod catgen value =
        catgen.head_gmod <- value

    let set_head_smod catgen value =
        catgen.head_smod <- value

    let set_head_adv catgen value =
        catgen.head_adv <- value

    let set_head_neg catgen value =
        catgen.head_neg <- value

    let set_head_modal catgen value =
        catgen.head_modal <- value

    let set_head_adpos catgen value =
        catgen.head_adpos <- value

    let set_head_relpro catgen value =
        catgen.head_relpro <- value

    let set_head_part catgen value =
        catgen.head_part <- value

    let set_head_subconj_smod catgen value =
        catgen.head_subconj_smod <- value

    let set_head_subconj_phr catgen value =
        catgen.head_subconj_phr <- value

    let set_head_poss_nmod catgen value =
        catgen.head_poss_nmod <- value

    let set_head_poss_phr catgen value =
        catgen.head_poss_phr <- value

    let set_head_inf catgen value =
        catgen.head_inf <- value

    let set_head_npnom catgen value =
        catgen.head_npnom <- value

    let set_head_vpnom catgen value =
        catgen.head_vpnom <- value

    let set_head_cl catgen value =
        catgen.head_cl <- value

    let s = Syncat.create_atom "s"
    
    let np = Syncat.create_atom "np"

    let adj = Syncat.create_atom "adj"

    let adv = Syncat.create_atom "adv"
    
    let neg = Syncat.create_atom "neg"
    
    let modal = Syncat.create_atom "modal"

    let nmod = Syncat.create_atom "nmod"

    let vp = Syncat.create_atom "vp"

    let vt = Syncat.create_atom "vt"

    let vd = Syncat.create_atom "vd"
    
    let v = Syncat.create_atom "v"

    let copula = Syncat.create_atom "cop"

    let vcomp = Syncat.create_atom "vcomp"
    
    let vicomp = Syncat.create_atom "vicomp"
    
    let vtcomp = Syncat.create_atom "vtcomp"

    let vmod = Syncat.create_atom "vmod"
    
    let gmod = Syncat.create_atom "gmod"
    
    let smod = Syncat.create_atom "smod"
            
    let g = Syncat.create_atom "g"
    
    let gerund = Syncat.create_atom "gerund"

    let pp = Syncat.create_atom "pp"
    
    let conj = Syncat.conn_cat

    let conj_depbank = Syncat.conn_cat_depbank
    
    let conj_truedep = Syncat.conn_cat_truedep
    
    let conj_rev = Syncat.conn_cat_rev

    let num = Syncat.create_atom "num"

    (***** FEATURE GENERATION *****)

    let gen_avm ?(is_infl=false) gender person number case fin =
        let content = ref [] in
        if is_infl then begin
            if gender <> "-NONE-" then
                content := !content @ [("g", gender)]
            else ();
            if person <> "-NONE-" then
                content := !content @ [("p", person)]
            else ();
            if number <> "-NONE-" then
                content := !content @ [("n", number)]
            else ();
            if case <> "-NONE-" then
                content := !content @ [("c", case)]
            else ();
            if fin <> "-NONE-" then
                content := !content @ [("f", fin)]
            else ()
        end else ();
        Avm.create !content

    let enum_np_ftrs ?(is_infl=false) catgen =
        let results = ref [] in
        List.iter
            (fun gender ->
                List.iter
                    (fun person ->
                        List.iter
                            (fun number -> 
                                List.iter
                                    (fun case ->
                                        List.iter
                                            (fun fin ->
                                                let avm = gen_avm gender person number case fin ~is_infl:is_infl in
                                                results := avm :: !results)
                                            ["-NONE-"])
                                    (if List.length catgen.cases_all > 0 then catgen.cases_all
                                     else ["-NONE-"]))
                            (if List.length catgen.numbers > 0 then catgen.numbers
                             else ["-NONE-"]))
                    (if List.length catgen.persons > 0 then catgen.persons
                     else ["-NONE-"]))
            (if List.length catgen.genders > 0 then catgen.genders
             else ["-NONE-"]);
        if List.length !results > 0 then !results
        else [Avm.create_empty ()]

    let enum_s_ftrs ?(is_infl=false) catgen =
        let results = ref [] in
        List.iter
            (fun gender ->
                List.iter
                    (fun person ->
                        List.iter
                            (fun number -> 
                                List.iter
                                    (fun fin ->
                                        let avm = gen_avm gender person number "-NONE-" fin ~is_infl:is_infl in
                                        results := avm :: !results)
                                    ["+"; "-"])
                            (if List.length catgen.numbers > 0 then catgen.numbers
                             else ["-NONE-"]))
                    (if List.length catgen.persons > 0 then catgen.persons
                     else ["-NONE-"]))
            (if List.length catgen.genders > 0 then catgen.genders
             else ["-NONE-"]);
        if List.length !results > 0 then !results
        else [Avm.create_empty ()]

    (***** NOUN MODIFIERS *****)
    
    let gen_adjavms ?(is_infl=false) catgen =
        let results = ref [] in
        if catgen.is_infl then begin
            let is_agr_by_gender = ref false
            and is_agr_by_number = ref false
            and is_agr_by_case = ref false in
            let adj_noun_agrs =
                match catgen.adj_noun_agrs with
                    Some x -> x
                  | None -> all_adj_noun_agrs in
            List.iter
                (fun x ->
                    match x with
                        ANAgr_by_gender -> is_agr_by_gender := true
                      | ANAgr_by_number -> is_agr_by_number := true
                      | ANAgr_by_case -> is_agr_by_case := true)
                adj_noun_agrs;
            let genders = catgen.genders in
            let cases = catgen.cases_all in
            let numbers = catgen.numbers in
            List.iter
                (fun gender ->
                    List.iter
                        (fun number ->
                            List.iter
                                (fun case ->
                                    results := (gen_avm gender "-NONE-" number case "-NONE-" ~is_infl:is_infl) :: !results)
                                cases)
                        numbers)
                genders;
            !results
        end else [Avm.create_empty ()]

    let gen_adjs catgen =
        let adj_orders =
            match catgen.adj_orders with
                Some x -> x
              | None -> all_adj_orders in
        (*
        List.map
            (fun adj_order ->
                match adj_order with
                    NounAdj -> make_syncat np np Bwd catgen.head_adj
                  | AdjNoun -> make_syncat np np Fwd catgen.head_adj)
            adj_orders
        *)
        List.iter
            (fun adj_order ->
                let cat = 
                    match adj_order with
                        NounAdj -> make_syncat np np Bwd catgen.head_adj
                      | AdjNoun -> make_syncat np np Fwd catgen.head_adj
                in
                Dict.add_unarydrv catgen.dict adj cat
            )
            adj_orders;
        [adj]
    
    let gen_nmods catgen =
        let nmod_orders =
            match catgen.nmod_orders with
                Some x -> x
              | None -> all_nmod_orders in
        (*
        List.map
            (fun nmod_order ->
                match nmod_order with
                    NpNmod -> make_syncat np np Bwd catgen.head_nmod
                  | NmodNp -> make_syncat np np Fwd catgen.head_nmod)
            nmod_orders
        *)
        List.iter
            (fun nmod_order ->
                let cat =
                    match nmod_order with
                        NpNmod -> make_syncat np np Bwd catgen.head_nmod
                      | NmodNp -> make_syncat np np Fwd catgen.head_nmod
                in
                Dict.add_unarydrv catgen.dict nmod cat)
            nmod_orders;
        [nmod]

    let gen_nmods_for_gerund catgen =
        let npgerund_orders =
            match catgen.npgerund_orders with
                Some x -> x
              | None -> all_npgerund_orders in
        (*
        List.map
            (fun npgerund_order ->
                match npgerund_order with
                    NpGerund -> make_syncat np np Bwd catgen.head_nmod
                  | GerundNp -> make_syncat np np Fwd catgen.head_nmod)
            npgerund_orders
        *)
        List.iter
            (fun npgerund_order ->
                let cat =
                    match npgerund_order with
                        NpGerund -> make_syncat np np Bwd catgen.head_nmod
                      | GerundNp -> make_syncat np np Fwd catgen.head_nmod
                in
                Dict.add_unarydrv catgen.dict nmod cat)
            npgerund_orders;
        [nmod]
    
    let gen_nmods_for_relpro catgen =
        let relcls_orders =
            match catgen.relcls_orders with
                Some x -> x
              | None -> all_relcls_orders in
        (*
        List.map
            (fun npgerund_order ->
                match npgerund_order with
                    NpRelcls -> make_syncat np np Bwd catgen.head_nmod
                  | RelclsNp -> make_syncat np np Bwd catgen.head_nmod)
            relcls_orders
        *)
        List.iter
            (fun npgerund_order ->
                let cat =
                    match npgerund_order with
                        NpRelcls -> make_syncat np np Bwd catgen.head_nmod
                      | RelclsNp -> make_syncat np np Bwd catgen.head_nmod
                in
                Dict.add_unarydrv catgen.dict nmod cat)
            relcls_orders;
        [nmod]
    
    (***** VERB SYSTEM *****)
    
    let gen_vis catgen v_order consq =
        let vi = match v_order with
            VerbMedial -> 
                (* C \> np *)
                make_syncat consq np Bwd catgen.head_verb
          | VerbFinal ->
                (* C \> np *)
                make_syncat consq np Bwd catgen.head_verb
          | VerbInitial ->
                (* C /< np *)
                make_syncat consq np Fwd catgen.head_verb
        in
        (*
        if allows_subjdrop catgen then [vi; consq]
        else [vi]
        *)
        let cats = 
            if allows_subjdrop catgen then [vi; consq]
            else [vi]
        in
        List.iter 
            (fun cat -> 
                Dict.add_unarydrv catgen.dict vp cat) 
            cats;
        [vp]
    
    let gen_vis_with_prems catgen v_order consq prems =
        let vi = match v_order with
            VerbMedial -> 
                (* C \> np *)
                make_syncat consq prems Bwd catgen.head_verb
          | VerbFinal ->
                (* C \> np *)
                make_syncat consq prems Bwd catgen.head_verb
          | VerbInitial ->
                (* C /< np *)
                make_syncat consq prems Fwd catgen.head_verb
        in
        let cats =
            if allows_subjdrop catgen then [vi; consq]
            else [vi]
        in
        List.iter 
            (fun cat -> Dict.add_unarydrv catgen.dict vp cat) 
            cats;
        [vp]
    
    let gen_vts catgen v_order vis =
        let vts = List.map
            (fun vi -> match v_order with
                VerbMedial -> 
                    (* VI /< np *)
                    make_syncat vi np Fwd catgen.head_verb
              | VerbFinal ->
                    (* VI \> np *)
                    make_syncat vi np Bwd catgen.head_verb
              | VerbInitial ->
                    (* VI /< np *)
                    make_syncat vi np Fwd catgen.head_verb)
            vis in
        (*
        if allows_objdrop catgen then list_to_set (vts @ vis)
        else list_to_set vts
        *)
        let cats =
            if allows_objdrop catgen then list_to_set (vts @ vis)
            else list_to_set vts
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vt cat)
            cats;
        [vt]
    
    let gen_vts_with_comp catgen v_order vis comp =
        let vts = List.map
            (fun vi -> match v_order with
                VerbMedial -> 
                    (* VI /< comp *)
                    make_syncat vi comp Fwd catgen.head_verb
              | VerbFinal ->
                    (* VI \> comp *)
                    make_syncat vi comp Bwd catgen.head_verb
              | VerbInitial ->
                    (* VI /< comp *)
                    make_syncat vi comp Fwd catgen.head_verb)
            vis in
        (*
        if allows_objdrop catgen then list_to_set (vts @ vis)
        else list_to_set vts
        *)
        let cats =
            if allows_objdrop catgen then list_to_set (vts @ vis)
            else list_to_set vts
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vt cat)
            cats;
        [vt]
    
    let gen_copula_with_comp catgen v_order vis comp =
        let vts = List.map
            (fun vi -> match v_order with
                VerbMedial -> 
                    (* VI /< comp *)
                    make_syncat vi comp Fwd catgen.head_copula
              | VerbFinal ->
                    (* VI \> comp *)
                    make_syncat vi comp Bwd catgen.head_copula
              | VerbInitial ->
                    (* VI /< comp *)
                    make_syncat vi comp Fwd catgen.head_copula)
            vis in
        (*
        if allows_objdrop catgen then list_to_set (vts @ vis)
        else list_to_set vts
        *)
        let cats =
            if allows_objdrop catgen then list_to_set (vts @ vis)
            else list_to_set vts
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict copula cat)
            cats;
        [copula]
    
    let gen_vds catgen v_order iobj_order vis =
        let vds =
            match v_order, iobj_order with
                VerbMedial, IobjOutLeft | VerbMedial, IobjInLeft ->
                    (* VI \> np /< np *)
                    List.map 
                        (fun vi ->
                            make_syncat 
                                (make_syncat vi np Bwd catgen.head_verb) 
                                np Fwd catgen.head_verb)
                        vis
              | VerbMedial, IobjInRight | VerbMedial, IobjOutRight ->
                    (* VI /< np /< np *)
                    let vts = gen_vts catgen v_order vis in
                    List.map 
                        (fun vt -> make_syncat vt np Fwd catgen.head_verb) 
                        vts
              | VerbFinal, IobjOutLeft | VerbFinal, IobjInLeft
              | VerbFinal, IobjInRight ->
                    (* VI \> np \> np *)
                    let vts = gen_vts catgen v_order vis in
                    List.map 
                        (fun vt -> make_syncat vt np Bwd catgen.head_verb) 
                        vts
              | VerbFinal, IobjOutRight -> 
                    (* VI \> np /> np *)
                    let vts = gen_vts catgen v_order vis in
                    List.map 
                        (fun vt -> make_syncat vt np Fwd catgen.head_verb) 
                        vts
              | VerbInitial, IobjOutLeft ->
                    (* VI /< np /< np *)
                    let vts = gen_vts catgen v_order vis in
                    List.map 
                        (fun vt -> make_syncat vt np Bwd catgen.head_verb) 
                        vts
              | VerbInitial, IobjInLeft ->
                    (* VI /< np \> np *)
                    let vts = gen_vts catgen v_order vis in
                    List.map 
                        (fun vt -> make_syncat vt np Fwd catgen.head_verb) 
                        vts
              | VerbInitial, IobjInRight | VerbInitial, IobjOutRight ->
                    (* VI /< np /< np *)
                    let vts = gen_vts catgen v_order vis in
                    List.map 
                        (fun vt -> make_syncat vt np Fwd catgen.head_verb) 
                        vts in
        (* 
        if allows_iobjdrop catgen then
            let vts = gen_vts catgen v_order vis in
            list_to_set vds @ vts
        else list_to_set vds
        *)
        let cats =
            if allows_iobjdrop catgen then
                let vts = gen_vts catgen v_order vis in
                list_to_set vds @ vts
            else list_to_set vds
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vd cat)
            cats;
        [vd]
    
    let gen_vcompsents catgen v_order =
        let word_orders = 
            match catgen.word_orders with
                Some x -> x
              | None -> all_word_orders in
        let result = ref [s] in
        result := gen_adjs catgen @ !result;
        List.iter
            (fun word_order ->
                let (v_order, _, iobj_order) = 
                    interpret_word_order word_order in
                let vis = gen_vis catgen v_order s in
                let vts = gen_vts catgen v_order vis in
                let vds = gen_vds catgen v_order iobj_order vis in
                result := union (vis @ vts @ vds) !result)
            word_orders;
        (* !result *)
        List.iter 
            (fun cat ->
                Dict.add_unarydrv catgen.dict vcomp cat) 
            !result;
        [vcomp]

    let gen_vicomps catgen vicomp_order consq =
        let (v_order, comp_order) = interpret_vicomp_order vicomp_order in
        let cats =
            match vicomp_order with
                SVC | VCS | SCV | CVS ->
                begin
                    let vis = gen_vis catgen v_order consq in
                    let vis' comp' = match vicomp_order with
                        SVC | VCS ->
                            List.map
                                (fun vi ->
                                    make_syncat vi comp' Fwd catgen.head_verb)
                                vis
                      | SCV | CVS ->
                            List.map
                                (fun vi ->
                                    make_syncat vi comp' Bwd catgen.head_verb)
                                vis
                      | _ -> failwith "gen_vicomps" in
                    list_to_set 
                        (List.concat (List.map vis' (gen_vcompsents catgen v_order)))
                end
              | CSV | VSC ->
                begin
                    let vis' comp' = match comp_order with
                        CompOutRight ->
                            make_syncat consq comp' Fwd catgen.head_verb
                      | CompOutLeft ->
                            make_syncat consq comp' Bwd catgen.head_verb
                      | _ -> failwith "gen_vicomps" in
                    let vts' comp' = gen_vis catgen v_order (vis' comp') in
                    list_to_set
                        (List.concat (List.map vts' (gen_vcompsents catgen v_order)))
                end
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vicomp cat)
            cats;
        [vicomp]

    let gen_vicomps_rplc_vi catgen vicomp_order consq =
        let (v_order, comp_order) = interpret_vicomp_order vicomp_order in
        let cats =
            match vicomp_order with
                SVC | VCS | SCV | CVS ->
                begin
                    let vis = [consq] in
                    let vis' comp' = match vicomp_order with
                        SVC | VCS ->
                            List.map
                                (fun vi ->
                                    make_syncat vi comp' Fwd catgen.head_verb)
                                vis
                      | SCV | CVS ->
                            List.map
                                (fun vi ->
                                    make_syncat vi comp' Bwd catgen.head_verb)
                                vis
                      | _ -> failwith "gen_vicomps" in
                    list_to_set 
                        (List.concat (List.map vis' (gen_vcompsents catgen v_order)))
                end
              | CSV | VSC ->
                begin
                    let vis' comp' = match comp_order with
                        CompOutRight -> 
                            make_syncat consq comp' Fwd catgen.head_verb
                      | CompOutLeft -> 
                            make_syncat consq comp' Bwd catgen.head_verb
                      | _ -> failwith "gen_vicomps" in
                    let vts' comp' = vis' comp' in
                    list_to_set
                        (List.map vts' (gen_vcompsents catgen v_order))
                end
    in
    List.iter
        (fun cat ->
            Dict.add_unarydrv catgen.dict vicomp cat)
        cats;
    [vicomp]

    let gen_vtcomps catgen vtcomp_order consq =
        let (v_order, _, _) = interpret_vtcomp_order vtcomp_order in
        let cats =
            match vtcomp_order with
                CSVO | CSOV | VSOC | OVSC | OSCV | COSV | VOSC ->
                begin
                    let consq' s' =
                        match vtcomp_order with
                            CSVO | CSOV | OSCV | COSV -> 
                                make_syncat consq s' Bwd catgen.head_verb
                          | VSOC | OVSC | VOSC -> 
                                make_syncat consq s' Fwd catgen.head_verb
                          | _ -> failwith "gen_vtcomps" in
                    let vis = list_to_set (List.concat
                        (List.map
                            (fun s' -> gen_vis catgen v_order (consq' s')) 
                            (gen_vcompsents catgen v_order))) in
                    gen_vts catgen v_order vis
                end
              | SVCO | SOVC | SOCV | VCSO | CVSO | OCVS | OSVC | VCOS | CVOS ->
                begin
                    let vis = gen_vis catgen v_order consq in
                    let vts = gen_vts catgen v_order vis in
                    match vtcomp_order with
                        SVCO | SOVC | VCSO | OSVC | VCOS ->
                            list_to_set (List.map 
                                (fun (vt, s) -> 
                                    make_syncat vt s Fwd catgen.head_verb)
                                (cartprod vts (s :: vis)))
                      | SOCV | CVSO | OCVS | CVOS ->
                            list_to_set (List.map
                                (fun (vt, s) -> 
                                    make_syncat vt s Bwd catgen.head_verb)
                                (cartprod vts (s :: vis)))
                      | _ -> failwith "gen_vtcomps"
                end
              | SVOC | SCVO | SCOV | VSCO | OVCS | COVS | OCSV | VOCS ->
                begin
                    let vis = gen_vis catgen v_order consq in
                    let vis' s' = match vtcomp_order with
                        SVOC | VSCO | OVCS | VOCS ->
                            List.map 
                                (fun vi -> 
                                    make_syncat vi s' Fwd catgen.head_verb) 
                                vis
                      | SCVO | SCOV | COVS | OCSV ->
                            List.map 
                                (fun vi -> 
                                    make_syncat vi s' Bwd catgen.head_verb) 
                                vis
                      | _ -> failwith "gen_vtcomps" in
                    list_to_set (List.concat
                        (List.map
                            (fun s' -> gen_vts catgen v_order (vis' s'))
                            (gen_vcompsents catgen v_order)))
                end
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vtcomp cat)
            cats;
        [vtcomp]

    let gen_vtcomps_rplc_vi catgen vtcomp_order consq =
        let (v_order, _, _) = interpret_vtcomp_order vtcomp_order in
        let cats =
            match vtcomp_order with
                CSVO | CSOV | VSOC | OVSC | OSCV | COSV | VOSC ->
                begin
                    let consq' s' =
                        match vtcomp_order with
                            CSVO | CSOV | OSCV | COSV -> 
                                make_syncat consq s' Bwd catgen.head_verb
                          | VSOC | OVSC | VOSC -> 
                                make_syncat consq s' Fwd catgen.head_verb
                          | _ -> failwith "gen_vtcomps" in
                    let vis = list_to_set
                        (List.map
                            (fun s' -> consq' s') 
                            (gen_vcompsents catgen v_order)) in
                    gen_vts catgen v_order vis
                end
              | SVCO | SOVC | SOCV | VCSO | CVSO | OCVS | OSVC | VCOS | CVOS ->
                begin
                    let vis = [consq] in
                    let vts = gen_vts catgen v_order vis in
                    match vtcomp_order with
                        SVCO | SOVC | VCSO | OSVC | VCOS ->
                            list_to_set (List.map 
                                (fun (vt, s) -> 
                                    make_syncat vt s Fwd catgen.head_verb)
                                (cartprod vts (s :: vis)))
                      | SOCV | CVSO | OCVS | CVOS ->
                            list_to_set (List.map
                                (fun (vt, s) -> 
                                    make_syncat vt s Bwd catgen.head_verb)
                                (cartprod vts (s :: vis)))
                      | _ -> failwith "gen_vtcomps"
                end
              | SVOC | SCVO | SCOV | VSCO | OVCS | COVS | OCSV | VOCS ->
                begin
                    let vis = [consq] in
                    let vis' s' = match vtcomp_order with
                        SVOC | VSCO | OVCS | VOCS ->
                            List.map 
                                (fun vi -> 
                                    make_syncat vi s' Fwd catgen.head_verb) 
                                vis
                      | SCVO | SCOV | COVS | OCSV ->
                            List.map 
                                (fun vi -> 
                                    make_syncat vi s' Bwd catgen.head_verb) 
                                vis
                      | _ -> failwith "gen_vtcomps" in
                    list_to_set (List.concat
                        (List.map
                            (fun s' -> gen_vts catgen v_order (vis' s'))
                            (gen_vcompsents catgen v_order)))
                end
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vtcomp cat)
            cats;
        [vtcomp]

    let gen_verbs_with_consq catgen consq =
        let result = ref [] in
        let word_orders = 
            match catgen.word_orders with
                Some x -> x
              | None -> all_word_orders in
        let vicomp_orders =
            match catgen.vicomp_orders with
                Some x -> x
              | None -> all_vicomp_orders in
        let vtcomp_orders =
            match catgen.vtcomp_orders with
                Some x -> x
              | None -> all_vtcomp_orders in
        List.iter
            (fun word_order ->
                let (v_order, _, iobj_order) = 
                    interpret_word_order word_order in
                let vis = gen_vis catgen v_order consq in
                let vts = gen_vts catgen v_order vis in
                let vds = gen_vds catgen v_order iobj_order vis in
                result := union (union vis vts) vds)
            word_orders;
        List.iter
            (fun vicomp_order ->
                let vicomps = gen_vicomps catgen vicomp_order consq in
                result := union vicomps !result)
            vicomp_orders;
        List.iter
            (fun vtcomp_order ->
                let vtcomps = gen_vtcomps catgen vtcomp_order consq in
                result := union vtcomps !result)
            vtcomp_orders;
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict v cat)
            (list_to_set !result);
        [v]
    
    let gen_verbs_with_consq_vis catgen consq =
        let result = ref [] in
        let word_orders = 
            match catgen.word_orders with
                Some x -> x
              | None -> all_word_orders in
        List.iter
            (fun word_order ->
                let (v_order, _, iobj_order) = 
                    interpret_word_order word_order in
                let vis = gen_vis catgen v_order consq in
                result := vis)
            word_orders;
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vp cat)
            (list_to_set !result);
        [vp]
    
    let gen_verbs_with_consq_vts catgen consq =
        let result = ref [] in
        let word_orders = 
            match catgen.word_orders with
                Some x -> x
              | None -> all_word_orders in
        List.iter
            (fun word_order ->
                let (v_order, _, iobj_order) = 
                    interpret_word_order word_order in
                let vis = gen_vis catgen v_order consq in
                let vts = gen_vts catgen v_order vis in
                result := vts)
            word_orders;
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vt cat)
            (list_to_set !result);
        [vt]
    
    let gen_verbs_with_consq_vds catgen consq =
        let result = ref [] in
        let word_orders = 
            match catgen.word_orders with
                Some x -> x
              | None -> all_word_orders in
        List.iter
            (fun word_order ->
                let (v_order, _, iobj_order) = 
                    interpret_word_order word_order in
                let vis = gen_vis catgen v_order consq in
                let vds = gen_vds catgen v_order iobj_order vis in
                result := vds)
            word_orders;
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vd cat)
            (list_to_set !result);
        [vd]
    
    let gen_verbs_with_consq_vicomps catgen consq =
        let result = ref [] in
        let vicomp_orders =
            match catgen.vicomp_orders with
                Some x -> x
              | None -> all_vicomp_orders in
        List.iter
            (fun vicomp_order ->
                let vicomps = gen_vicomps catgen vicomp_order consq in
                result := union vicomps !result)
            vicomp_orders;
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vicomp cat)
            (list_to_set !result);
        [vicomp]
    
    let gen_verbs_with_consq_vtcomps catgen consq =
        let result = ref [] in
        let vtcomp_orders =
            match catgen.vtcomp_orders with
                Some x -> x
              | None -> all_vtcomp_orders in
        List.iter
            (fun vtcomp_order ->
                let vtcomps = gen_vtcomps catgen vtcomp_order consq in
                result := union vtcomps !result)
            vtcomp_orders;
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vtcomp cat)
            (list_to_set !result);
        [vtcomp]
    
    let gen_verbs catgen =
        if not catgen.is_freewordorder then
            gen_verbs_with_consq catgen s
        else [s]
    
    let gen_verbs_vis catgen =
        if not catgen.is_freewordorder then
            gen_verbs_with_consq_vis catgen s
        else [s]
    
    let gen_verbs_vts catgen =
        if not catgen.is_freewordorder then
            gen_verbs_with_consq_vts catgen s
        else [s]
    
    let gen_verbs_vds catgen =
        if not catgen.is_freewordorder then
            gen_verbs_with_consq_vds catgen s
        else [s]
    
    let gen_verbs_vicomps catgen =
        if not catgen.is_freewordorder then
            gen_verbs_with_consq_vicomps catgen s
        else [s]
    
    let gen_verbs_vtcomps catgen =
        if not catgen.is_freewordorder then
            gen_verbs_with_consq_vtcomps catgen s
        else [s]

    let gen_verbs_vcomps catgen =
        if not catgen.is_freewordorder then
            let cats = 
                union 
                    (gen_verbs_vicomps catgen) 
                    (gen_verbs_vtcomps catgen)
            in
            List.iter
                (fun cat ->
                    Dict.add_unarydrv catgen.dict vcomp cat)
                cats;
            [vcomp]
        else [s]
    
    let gen_vps_with_consq catgen consq =
        if not catgen.is_freewordorder then
            let word_orders = 
                match catgen.word_orders with
                    Some x -> x
                  | None -> all_word_orders in
            list_to_set (List.concat
                (List.map 
                    (fun word_order ->
                        let (v_order, _, _) = 
                            interpret_word_order word_order in
                        gen_vis catgen v_order consq)
                    word_orders))
        else [consq]
    
    let gen_vps catgen =
        if not catgen.is_freewordorder then
            gen_vps_with_consq catgen s
        else [s]
    
    (***** VERB MODIFIERS *****)
    
    let gen_vmods catgen =
        let vmod_orders =
            match catgen.vmod_orders with
                Some x -> x
              | None -> all_vmod_orders in
        let cats = 
            list_to_set (List.map
                (fun (vmod_order, vp) ->
                    match vmod_order with
                        VpVmod -> make_syncat vp vp Bwd catgen.head_vmod
                      | VmodVp -> make_syncat vp vp Fwd catgen.head_vmod)
                (cartprod vmod_orders (gen_vps catgen)))
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vmod cat)
            cats;
        [vmod]
    
    let gen_vmods_for_gerund catgen =
        let vpgerund_orders =
            if catgen.allows_gerund_as_np
                    || catgen.allows_gerund_as_nmod
                    || catgen.allows_gerund_as_vmod then
                match catgen.vpgerund_orders with
                    Some x -> x
                  | None -> all_vpgerund_orders 
            else [] in
        let cats =
            list_to_set (List.map
                (fun (vpgerund_order, vp) ->
                    match vpgerund_order with
                        VpGerund -> make_syncat vp vp Bwd catgen.head_vmod
                      | GerundVp -> make_syncat vp vp Fwd catgen.head_vmod)
                (cartprod vpgerund_orders (gen_vps catgen)))
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict vmod cat)
            cats;
        [vmod]

    let gen_gmods catgen =
        let nmods = 
            if catgen.allows_gerund_as_nmod then gen_nmods catgen
            else []
        and vmods = 
            if catgen.allows_gerund_as_vmod then gen_vmods catgen 
            else [] in
        let consqs = 
            if catgen.allows_gerund_as_np then list_to_set (np :: nmods @ vmods) 
            else list_to_set (nmods @ vmods) in
        let vpgerund_orders =
            match catgen.vpgerund_orders with
                Some x -> x
              | None -> all_vpgerund_orders in
        let cats =
            list_to_set (List.map
                (fun (vpgerund_order, consq) ->
                    match vpgerund_order with
                        VpGerund -> make_syncat consq consq Bwd catgen.head_gmod
                      | GerundVp -> make_syncat consq consq Fwd catgen.head_gmod)
                (cartprod vpgerund_orders consqs))
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict gmod cat)
            cats;
        [gmod]
    
    let gen_smods catgen =
        let smod_orders =
            match catgen.smod_orders with
                Some x -> x
              | None -> all_smod_orders in
        let cats =
            list_to_set 
                (List.map
                    (fun smod_order ->
                        match smod_order with
                            SentSmod -> make_syncat s s Bwd catgen.head_smod
                          | SmodSent -> make_syncat s s Fwd catgen.head_smod)
                    smod_orders)
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict smod cat)
            cats;
        [smod]

    let gen_smods_final catgen =
        let smod_orders =
            match catgen.smod_orders with
                Some x -> x
              | None -> all_smod_orders in
        let cats =
            list_to_set (List.map
                (fun (smod_order, acc) ->
                    match smod_order with
                        SentSmod -> make_syncat s acc Bwd catgen.head_smod
                      | SmodSent -> make_syncat s acc Fwd catgen.head_smod)
                (cartprod smod_orders (Dict.get_accepts catgen.dict)))
        in
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict smod cat)
            cats;
        [smod]
    
    let gen_advs catgen =
        let adv_orders =
            match catgen.adv_orders with
                Some x -> x
              | None -> all_adv_orders in
        let advadj_orders =
            match catgen.advadj_orders with
                Some x -> x
              | None -> all_advadj_orders in
        let result = ref [] in
        List.iter
            (fun (v, adv_order) ->
                let adv = match adv_order with
                    VpAdv -> make_syncat v v Bwd catgen.head_adv
                  | AdvVp -> make_syncat v v Fwd catgen.head_adv
                in
                result := union [adv] !result)
            (cartprod (gen_verbs catgen) adv_orders);
        List.iter
            (fun (adj, advadj_order) ->
                let adv = match advadj_order with
                    AdjAdv -> make_syncat adj adj Bwd catgen.head_adv
                  | AdvAdj -> make_syncat adj adj Fwd catgen.head_adv
                in
                result := union [adv] !result)
            (cartprod (gen_adjs catgen) advadj_orders);
        List.iter
            (fun (nmod, advadj_order) ->
                let adv = match advadj_order with
                    AdjAdv -> make_syncat nmod nmod Bwd catgen.head_adv
                  | AdvAdj -> make_syncat nmod nmod Fwd catgen.head_adv
                in
                result := union [adv] !result)
            (cartprod (gen_nmods catgen) advadj_orders);
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict adv cat)
            (list_to_set !result);
        [adv]

    let gen_negs catgen =
        let neg_orders =
            match catgen.neg_orders with
                Some x -> x
              | None -> all_neg_orders in
        let result = ref [] in
        let vs = gen_verbs catgen
        and adjs = gen_adjs catgen
        and advs = gen_advs catgen
        and nmods = gen_nmods catgen
        and vmods = gen_vmods catgen in
        List.iter
            (fun neg_order ->
                match neg_order with
                    NegV ->
                        List.iter
                            (fun v -> result := (make_syncat v v Fwd catgen.head_neg) :: !result)
                            vs
                  | VNeg ->
                        List.iter
                            (fun v -> result := (make_syncat v v Bwd catgen.head_neg) :: !result)
                            vs
                  | NegAdj ->
                        List.iter
                            (fun adj -> result := (make_syncat adj adj Fwd catgen.head_neg) :: !result)
                            adjs
                  | AdjNeg ->
                        List.iter
                            (fun adj -> result := (make_syncat adj adj Bwd catgen.head_neg) :: !result)
                            adjs
                  | NegAdv ->
                        List.iter
                            (fun adv -> result := (make_syncat adv adv Fwd catgen.head_neg) :: !result)
                            advs
                  | AdvNeg ->
                        List.iter
                            (fun adv -> result := (make_syncat adv adv Bwd catgen.head_neg) :: !result)
                            advs
                  | NegNmod ->
                        List.iter
                            (fun nmod -> result := (make_syncat nmod nmod Fwd catgen.head_neg) :: !result)
                            nmods
                  | NmodNeg ->
                        List.iter
                            (fun nmod -> result := (make_syncat nmod nmod Bwd catgen.head_neg) :: !result)
                            nmods
                  | NegVmod ->
                        List.iter
                            (fun vmod -> result := (make_syncat vmod vmod Fwd catgen.head_neg) :: !result)
                            vmods
                  | VmodNeg -> 
                        List.iter
                            (fun vmod -> result := (make_syncat vmod vmod Bwd catgen.head_neg) :: !result)
                            vmods)
            neg_orders;
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict neg cat)
            (list_to_set !result);
        [neg]
    
    let gen_modals catgen =
        let modal_orders =
            match catgen.modal_orders with
                Some x -> x
              | None -> all_modal_orders in
        let result = ref [] in
        List.iter
            (fun (v, modal_order) ->
                let modal = 
                    match modal_order with
                        VpModal -> make_syncat v v Bwd catgen.head_modal
                      | ModalVp -> make_syncat v v Fwd catgen.head_modal
                in
                result := union [modal] !result)
            (cartprod (gen_verbs catgen) modal_orders);
        (* list_to_set !result *)
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict modal cat)
            (list_to_set !result);
        [modal]
    
    (***** GERUNDS *****)
    
    let gen_gerunds_with_consq catgen consq =
        let result = ref [] in
        let word_orders = 
            match catgen.word_orders with
                Some x -> x
              | None -> all_word_orders in
        let vicomp_orders =
            match catgen.vicomp_orders with
                Some x -> x
              | None -> all_vicomp_orders in
        let vtcomp_orders =
            match catgen.vtcomp_orders with
                Some x -> x
              | None -> all_vtcomp_orders in
        List.iter
            (fun word_order ->
                let (v_order, _, iobj_order) = 
                    interpret_word_order word_order in
                let vis = [gerund] in
                let vts = gen_vts catgen v_order vis in
                let vds = gen_vds catgen v_order iobj_order vis in
                result := union vis (union vts vds))
            word_orders;
        List.iter
            (fun vicomp_order ->
                let vicomps = gen_vicomps_rplc_vi catgen vicomp_order gerund in
                result := union vicomps !result)
            vicomp_orders;
        List.iter
            (fun vtcomp_order ->
                let vtcomps = gen_vtcomps_rplc_vi catgen vtcomp_order gerund in
                result := union vtcomps !result)
            vtcomp_orders;
        Dict.add_unarydrv catgen.dict gerund consq;
        (list_to_set !result)
    
    let gen_gerund_nouns catgen =
        if catgen.allows_gerund_as_np then
            gen_gerunds_with_consq catgen np
        else []
    
    let gen_gerund_nmods catgen =
        let result = ref [] in
        if catgen.allows_gerund_as_nmod then
            List.iter
                (fun nmod ->
                    result := union 
                        (gen_gerunds_with_consq catgen nmod) !result)
                (gen_nmods_for_gerund catgen)
        else ();
        list_to_set !result
    
    let gen_gerund_vmods catgen =
        let result = ref [] in
        if catgen.allows_gerund_as_vmod then
            List.iter
                (fun vmod ->
                    result := union 
                        (gen_gerunds_with_consq catgen vmod) !result)
                (gen_vmods_for_gerund catgen)
        else ();
        list_to_set !result
    
    let gen_gerunds catgen =
        let result = ref [] in
        result := (gen_gerund_nouns catgen) @ !result;
        result := (gen_gerund_nmods catgen) @ !result;
        result := (gen_gerund_vmods catgen) @ !result;
        list_to_set !result
    
    (***** APPOSITIONS *****)

    let gen_pps catgen =
        let consqs = ref [] in
        consqs := union (gen_nmods catgen) !consqs;
        consqs := union (gen_vmods catgen) !consqs;
        consqs := union (gen_gmods catgen) !consqs;
        consqs := union (gen_smods catgen) !consqs;
        List.iter
            (fun cat ->
                Dict.add_unarydrv catgen.dict pp cat)
            !consqs;
        [pp]
    
    let gen_adpositions catgen =
        let adpositions = 
            match catgen.adpositions with
                Some x -> x
              | None -> all_adpositions in
        let consqs = ref [pp] in
        let result = ref [] in
        (* 
        consqs := union (gen_nmods catgen) !consqs;
        consqs := union (gen_vmods catgen) !consqs;
        consqs := union (gen_gmods catgen) !consqs;
        consqs := union (gen_smods catgen) !consqs;
        *)
        List.iter
            (fun case ->
                (* let avm = gen_avm "$" "-NONE-" "$" (Printf.sprintf "$%s" case) "-NONE-" ~is_infl:catgen.is_infl in *)
                List.iter
                    (fun (consq, adposition) ->
                        let prep =
                            match adposition with
                                Preposition -> 
                                    make_syncat consq np Fwd catgen.head_adpos
                              | Postposition -> 
                                    make_syncat consq np Bwd catgen.head_adpos
                        in
                        result := union [prep] !result)
                    (cartprod !consqs adpositions))
            catgen.cases_apposarg;
        list_to_set !result
    
    let gen_prepositions catgen =
        let consqs = ref [pp] in
        let result = ref [] in
        (*
        consqs := union (gen_nmods catgen) !consqs;
        consqs := union (gen_vmods catgen) !consqs;
        consqs := union (gen_gmods catgen) !consqs;
        consqs := union (gen_smods catgen) !consqs;
        *)
        List.iter
            (fun case ->
                (* let avm = gen_avm "$" "-NONE-" "$" (Printf.sprintf "$%s" case) "-NONE-" ~is_infl:catgen.is_infl in *)
                List.iter
                    (fun (consq, adposition) ->
                        let prep =
                            match adposition with
                                Preposition -> 
                                    make_syncat consq np Fwd catgen.head_adpos
                              | Postposition -> 
                                    make_syncat consq np Bwd catgen.head_adpos
                        in
                        result := union [prep] !result)
                    (cartprod !consqs [Preposition]))
            catgen.cases_apposarg;
        list_to_set !result

    let gen_postpositions catgen =
        let consqs = ref [pp] in
        let result = ref [] in
        (*
        consqs := union (gen_nmods catgen) !consqs;
        consqs := union (gen_vmods catgen) !consqs;
        consqs := union (gen_gmods catgen) !consqs;
        consqs := union (gen_smods catgen) !consqs;
        *)
        List.iter
            (fun case ->
                (* let avm = gen_avm "$" "-NONE-" "$" (Printf.sprintf "$%s" case) "-NONE-" ~is_infl:catgen.is_infl in *)
                List.iter
                    (fun (consq, adposition) ->
                        let prep =
                            match adposition with
                                Preposition -> 
                                    make_syncat consq np Fwd catgen.head_adpos
                              | Postposition -> 
                                    make_syncat consq np Bwd catgen.head_adpos
                        in
                        result := union [prep] !result)
                    (cartprod !consqs [Postposition]))
            catgen.cases_apposarg;
        list_to_set !result

    (***** RELATIVE PRONOUNS *****)
    
    let gen_relpros catgen =
        let relpro_orders =
            match catgen.relpro_orders with
                Some x -> x
              | None -> all_relpro_orders in
        let comps = ref [s] in
        comps := union (gen_vps catgen) !comps;
        let result = ref [] in
        (* let avm = gen_avm "-NONE-" "$" "$" "-NONE-" "+" ~is_infl:catgen.is_infl in *)
        List.iter
            (fun ((relpro_order, nmod), comp) ->
                let relpro =
                        match relpro_order with
                          RelproComp -> 
                              make_syncat nmod comp Fwd catgen.head_relpro
                        | CompRelpro -> 
                              make_syncat nmod comp Bwd catgen.head_relpro
                in
                result := union [relpro] !result) 
            (cartprod 
                (cartprod relpro_orders (gen_nmods_for_relpro catgen)) 
                !comps);
        list_to_set !result
    
    (***** COPULAS *****)
    
    let gen_copulas catgen =
        if not catgen.allows_copula then []
        else
            let vps = gen_vps catgen in
            let comps = union (gen_adjs catgen) (gen_nmods catgen) in
            let word_orders =
                match catgen.word_orders with
                    Some x -> x
                  | None -> all_word_orders in
            let result = ref [] in
            List.iter
                (fun (word_order, comp) ->
                    let (v_order, _, _) = 
                        interpret_word_order word_order in
                    result := union 
                        (gen_copula_with_comp catgen v_order vps comp) 
                        !result)
                (cartprod word_orders comps);
            List.iter
                (fun cat ->
                    Dict.add_unarydrv catgen.dict copula cat)
                (list_to_set !result);
            [copula]
    
    (***** PARTICLES *****)
    
    let gen_parts catgen =
        let part_orders =
            match catgen.part_orders with
                Some x -> x
              | None -> all_part_orders in
        let result = ref [] in
        List.iter
            (fun (v, part_order) ->
                let part = match part_order with
                    VpPart -> make_syncat v v Bwd catgen.head_part
                  | PartVp -> make_syncat v v Fwd catgen.head_part
                in
                result := union [part] !result)
            (cartprod (gen_verbs catgen) part_orders);
        list_to_set !result
    
    (***** SUBORDINATE CONJUNCTIONS *****)
    
    let gen_subconjs catgen =
        let subcls_orders = 
            match catgen.subcls_orders with
                Some x -> x
              | None -> all_subcls_orders in
        let comps = list_to_set (s :: gen_adjs catgen @ gen_gerund_nmods catgen) in
        let result = ref [] in
        List.iter
            (fun (subcls_order, comp) ->
                (* let avm = 
                    if Syncat.equal_ign_attrs comp s then 
                        gen_avm "-NONE-" "$" "$" "-NONE-" "$" ~is_infl:catgen.is_infl 
                    else 
                        gen_avm "$" "-NONE-" "$" "$" "-NONE-" ~is_infl:catgen.is_infl 
                    in *)
                let subconj =
                        match subcls_order with
                            MainConjSubcls -> 
                                make_syncat (make_syncat s s Bwd catgen.head_subconj_phr) comp Fwd catgen.head_subconj_smod
                          | SubclsConjMain -> 
                                make_syncat (make_syncat s s Fwd catgen.head_subconj_phr) comp Bwd catgen.head_subconj_smod
                          | MainSubclsConj -> 
                                make_syncat (make_syncat s s Bwd catgen.head_subconj_phr) comp Bwd catgen.head_subconj_smod
                          | SubclsMainConj -> 
                                make_syncat (make_syncat s comp Bwd catgen.head_subconj_smod) s Bwd catgen.head_subconj_phr
                          | ConjMainSubcls -> 
                                make_syncat (make_syncat s comp Fwd catgen.head_subconj_smod) s Fwd catgen.head_subconj_phr
                          | ConjSubclsMain -> 
                                make_syncat (make_syncat s s Fwd catgen.head_subconj_phr) comp Fwd catgen.head_subconj_smod
                in
                result := union [subconj] !result)
            (cartprod subcls_orders comps);
        list_to_set !result
    
    (***** POSSESSIVE MARKERS *****)
    
    let gen_posses catgen =
        let poss_orders =
            match catgen.poss_orders with
                Some x -> x
              | None -> all_poss_orders in
        let result = ref [] in
        List.iter
            (fun poss_order ->
                (* let avm = gen_avm "$" "-NONE-" "$" "$" "-NONE-" ~is_infl:catgen.is_infl in *)
                let poss = 
                    match poss_order with
                        OwnerPossOwnee ->
                            make_syncat (make_syncat np np Fwd catgen.head_poss_nmod) np Bwd catgen.head_poss_phr
                      | OwneePossOwner -> 
                            make_syncat (make_syncat np np Bwd catgen.head_poss_nmod) np Fwd catgen.head_poss_phr
                      | OwnerOwneePoss ->
                            make_syncat (make_syncat np np Bwd catgen.head_poss_nmod) np Bwd catgen.head_poss_phr
                      | OwneeOwnerPoss ->
                            make_syncat (make_syncat np np Bwd catgen.head_poss_nmod) np Bwd catgen.head_poss_phr
                      | PossOwnerOwnee ->
                            make_syncat (make_syncat np np Fwd catgen.head_poss_nmod) np Fwd catgen.head_poss_phr
                      | PossOwneeOwner ->
                            make_syncat (make_syncat np np Fwd catgen.head_poss_nmod) np Fwd catgen.head_poss_phr
                in
                result := union [poss] !result)
            poss_orders;
        list_to_set !result
    
    (***** INFINITIVE MARKERS *****)
    
    let gen_infs catgen =
        let infvp_orders =
            match catgen.infvp_orders with
                Some x -> x
              | None -> all_infvp_orders in
        let premss = ref [] in
        let result = ref [] in
        premss := union (gen_verbs catgen) !premss;
        (* let avm = gen_avm "-NONE-" "$" "$" "-NONE-" "$-" ~is_infl:catgen.is_infl in *)
        List.iter
            (fun (infvp_order, prems) ->
                let inf =
                        match infvp_order with
                          InfVp -> make_syncat np prems Fwd catgen.head_inf
                        | VpInf -> make_syncat np prems Bwd catgen.head_inf
                in
                result := union [inf] !result)
            (cartprod infvp_orders !premss);
        list_to_set !result
    
    (***** NOMINALIZER *****)
    
    let gen_npnoms catgen =
        let npnom_orders =
            match catgen.npnom_orders with
                Some x -> x
              | None -> all_npnom_orders in
        let premss = ref [] in
        let result = ref [] in
        premss := union (gen_vps catgen) !premss;
        (* let avm = gen_avm "-NONE-" "$" "$" "-NONE-" "$-" ~is_infl:catgen.is_infl in *)
        List.iter
            (fun (npnom_order, prems) ->
                let inf = 
                    match npnom_order with
                        NomNp -> make_syncat np prems Fwd catgen.head_npnom
                      | NpNom -> make_syncat np prems Bwd catgen.head_npnom
                in
                result := union [inf] !result)
            (cartprod npnom_orders !premss);
        list_to_set !result
    
    let gen_vpnoms catgen =
        let vpnom_orders =
            match catgen.vpnom_orders with
                Some x -> x
              | None -> all_vpnom_orders in
        let premss = ref [] in
        let result = ref [] in
        premss := union (gen_vps catgen) !premss;
        (* let avm = gen_avm "-NONE-" "$" "$" "-NONE-" "$-" ~is_infl:catgen.is_infl in *)
        List.iter
            (fun (vpnom_order, prems) ->
                let inf =
                    match vpnom_order with
                        NomVp -> make_syncat np prems Fwd catgen.head_vpnom
                      | VpNom -> make_syncat np prems Bwd catgen.head_vpnom
                in
                result := union [inf] !result)
            (cartprod vpnom_orders !premss);
        list_to_set !result
    
    (***** CLASSIFIERS *****)

    let gen_cls_adj catgen =
        let adjs = gen_adjs catgen in
        let nump_orders =
            match catgen.nump_orders with
                Some x -> x
              | None -> all_nump_orders in
        list_to_set (List.map
            (fun (nump_order, adj) ->
                match nump_order with
                    ClNum -> make_syncat adj num Fwd catgen.head_cl
                  | NumCl -> make_syncat adj num Bwd catgen.head_cl)
            (cartprod nump_orders adjs))

    let gen_cls_adv catgen =
        let advs = gen_advs catgen in
        let nump_orders =
            match catgen.nump_orders with
                Some x -> x
              | None -> all_nump_orders in
        list_to_set (List.map
            (fun (nump_order, adv) ->
                match nump_order with
                    ClNum -> make_syncat adv num Fwd catgen.head_cl
                  | NumCl -> make_syncat adv num Bwd catgen.head_cl)
            (cartprod nump_orders advs))

    let gen_cls_nmod catgen =
        let nmods = gen_nmods catgen in
        let nump_orders =
            match catgen.nump_orders with
                Some x -> x
              | None -> all_nump_orders in
        list_to_set (List.map
            (fun (nump_order, nmod) ->
                match nump_order with
                    ClNum -> make_syncat nmod num Fwd catgen.head_cl
                  | NumCl -> make_syncat nmod num Bwd catgen.head_cl)
            (cartprod nump_orders nmods))

    let gen_cls_vmod ?(is_infl=false) catgen =
        let vmods = gen_vmods catgen in
        let nump_orders =
            match catgen.nump_orders with
                Some x -> x
              | None -> all_nump_orders 
        in
        (* let avm = gen_avm "$" "-NONE-" "$" "$" "-NONE-" ~is_infl:is_infl in *)
        list_to_set (List.map
            (fun (nump_order, vmod) ->
                match nump_order with
                    ClNum -> make_syncat vmod num Fwd catgen.head_cl
                  | NumCl -> make_syncat vmod num Bwd catgen.head_cl)
            (cartprod nump_orders vmods))

    let gen_cls ?(is_infl=false) catgen =
        let result = ref [] in
        let nump_positions =
            match catgen.nump_positions with
                Some x -> x
              | None -> all_nump_positions in
        List.iter
            (fun nump_position ->
                match nump_position with
                    NumpAsAdj -> result := union (gen_cls_adj catgen) !result
                  | NumpAsAdv -> result := union (gen_cls_adv catgen) !result
                  | NumpAsNmod -> result := union (gen_cls_nmod catgen) !result
                  | NumpAsVmod -> result := union (gen_cls_vmod catgen ~is_infl:is_infl) !result)
            nump_positions;
        !result

    (***** UNARY DERIVATIONS *****)

    let gen_np_to_varg catgen =
        if catgen.is_freewordorder then begin
            let is_infl_by_subj = ref false
            and is_infl_by_obj = ref false
            and is_infl_by_iobj = ref false
            and is_agr_by_gender = ref false
            and is_agr_by_number = ref false
            and is_agr_by_person = ref false in
            let verb_decls =
                match catgen.verb_decls with
                    Some x -> x
                  | None -> all_verb_decls in
            let noun_verb_agrs =
                match catgen.noun_verb_agrs with
                    Some x -> x
                  | None -> all_noun_verb_agrs in
            List.iter
                (fun x ->
                    match x with
                        VDecl_by_subj -> is_infl_by_subj := true
                      | VDecl_by_obj -> is_infl_by_obj := true
                      | VDecl_by_iobj -> is_infl_by_iobj := true)
                verb_decls;
            List.iter
                (fun x ->
                    match x with
                        NVAgr_by_gender -> is_agr_by_gender := true
                      | NVAgr_by_number -> is_agr_by_number := true
                      | NVAgr_by_person -> is_agr_by_person := true)
                noun_verb_agrs;
            let genders = if !is_agr_by_gender then catgen.genders
                          else ["-NONE-"] in
            let persons = if !is_agr_by_person then catgen.persons
                          else ["-NONE-"] in
            let numbers = if !is_agr_by_number then catgen.numbers
                          else ["-NONE-"] in
            List.iter
                (fun gender ->
                    List.iter
                        (fun person ->
                            List.iter
                                (fun number ->
                                    List.iter
                                        (fun case ->
                                            (* let avm = gen_avm gender person number case "-NONE-" ~is_infl:catgen.is_infl
                                            and avm' = gen_avm gender person number "-NONE-" "$" ~is_infl:catgen.is_infl in *)
                                            let np' = Syncat.create_atom "np" (* ~ftrs:avm *) in
                                            (* 
                                            let s1 = Syncat.create_fwd s s Syncat.Right ~ftrs:avm'
                                            and s2 = Syncat.create_bwd s s Syncat.Left ~ftrs:avm' in
                                            *)
                                            let s1 = make_syncat s s Fwd false
                                            and s2 = make_syncat s s Bwd false in
                                            Dict.add_unarydrv catgen.dict np' s1;
                                            Dict.add_unarydrv catgen.dict np' s2)
                                        catgen.cases_arg)
                                numbers)
                        persons)
                genders
        end else ()

    let gen_np_to_adj catgen =
        if catgen.is_infl then begin
            List.iter
                (fun case ->
                    (* let avm = gen_avm "-NONE-" "-NONE-" "-NONE-" case "-NONE-" ~is_infl:catgen.is_infl
                    and avm' = gen_avm "$" "$" "$" "$" "$" ~is_infl:catgen.is_infl in *)
                    let adj_orders =
                        match catgen.adj_orders with
                            Some x -> x
                          | None -> all_adj_orders in
                    let np' = Syncat.create_atom "np" (* ~ftrs:avm *) in
                    List.iter
                        (fun adj_order ->
                            let adj = 
                                match adj_order with
                                    NounAdj -> (* Syncat.create_bwd np np Syncat.Left ~ftrs:avm' *)
                                        make_syncat np np Bwd false
                                  | AdjNoun -> (* Syncat.create_fwd np np Syncat.Right ~ftrs:avm' *) 
                                        make_syncat np np Fwd false
                            in
                            Dict.add_unarydrv catgen.dict np' adj)
                        adj_orders)
                catgen.cases_adj
        end else ()

    (***** MAIN FUNCTION *****)

    let catset_to_string catset =
        String.concat "\n" (List.map Syncat.to_string catset)

    let init_empty_ftrs catgen =
        if List.length catgen.genders = 0 then
            catgen.genders <- ["-NONE-"]
        else ();
        if List.length catgen.persons = 0 then
            catgen.persons <- ["-NONE-"]
        else ();
        if List.length catgen.numbers = 0 then
            catgen.numbers <- ["-NONE-"]
        else ();
        if List.length catgen.cases_arg = 0 then
            catgen.cases_arg <- ["-NONE-"]
        else ();
        if List.length catgen.cases_apposarg = 0 then
            catgen.cases_apposarg <- ["-NONE-"]
        else ();
        if List.length catgen.cases_adj = 0 then
            catgen.cases_adj <- ["-NONE-"]
        else ();
        if List.length catgen.cases_all = 0 then
            catgen.cases_all <- ["-NONE-"]
        else ()

    let test catgen =
        init_empty_ftrs catgen;
        printf "Catgen Testing\n\n";
        printf ">> adjs <<\n%s\n\n" (catset_to_string (gen_adjs catgen));
        printf ">> nmods <<\n%s\n\n" (catset_to_string (gen_nmods catgen));
        printf ">> nmods_for_gerund <<\n%s\n\n" (catset_to_string (gen_nmods_for_gerund catgen));
        printf ">> nmods_for_relpro <<\n%s\n\n" (catset_to_string (gen_nmods_for_relpro catgen));
        printf ">> verbs <<\n%s\n\n" (catset_to_string (gen_verbs catgen));
        printf ">> vis <<\n%s\n\n" (catset_to_string (gen_verbs_vis catgen));
        printf ">> vts <<\n%s\n\n" (catset_to_string (gen_verbs_vts catgen));
        printf ">> vds <<\n%s\n\n" (catset_to_string (gen_verbs_vds catgen));
        printf ">> vicomps <<\n%s\n\n" (catset_to_string (gen_verbs_vicomps catgen));
        printf ">> vtcomps <<\n%s\n\n" (catset_to_string (gen_verbs_vtcomps catgen));
        printf ">> vcomps <<\n%s\n\n" (catset_to_string (gen_verbs_vcomps catgen));
        printf ">> vps <<\n%s\n\n" (catset_to_string (gen_vps catgen));
        printf ">> vmods <<\n%s\n\n" (catset_to_string (gen_vmods catgen));
        printf ">> gmods <<\n%s\n\n" (catset_to_string (gen_gmods catgen));
        printf ">> vmods_for_gerund <<\n%s\n\n" (catset_to_string (gen_vmods_for_gerund catgen));
        printf ">> smods <<\n%s\n\n" (catset_to_string (gen_smods catgen));
        printf ">> advs <<\n%s\n\n" (catset_to_string (gen_advs catgen));
        printf ">> modals <<\n%s\n\n" (catset_to_string (gen_modals catgen));
        printf ">> gerund_nouns <<\n%s\n\n" (catset_to_string (gen_gerund_nouns catgen));
        printf ">> gerund_nmods <<\n%s\n\n" (catset_to_string (gen_gerund_nmods catgen));
        printf ">> gerund_vmods <<\n%s\n\n" (catset_to_string (gen_gerund_vmods catgen));
        printf ">> gerunds <<\n%s\n\n" (catset_to_string (gen_gerunds catgen));
        printf ">> pps <<\n%s\n\n" (catset_to_string (gen_pps catgen));
        printf ">> adpositions (or prepositions) <<\n%s\n\n" (catset_to_string (gen_adpositions catgen));
        printf ">> relpros <<\n%s\n\n" (catset_to_string (gen_relpros catgen));
        printf ">> copulas <<\n%s\n\n" (catset_to_string (gen_copulas catgen));
        printf ">> parts <<\n%s\n\n" (catset_to_string (gen_parts catgen));
        printf ">> subconjs <<\n%s\n\n" (catset_to_string (gen_subconjs catgen));
        printf ">> posses <<\n%s\n\n" (catset_to_string (gen_posses catgen));
        printf ">> infs <<\n%s\n\n" (catset_to_string (gen_infs catgen));
        printf ">> npnoms <<\n%s\n\n" (catset_to_string (gen_npnoms catgen));
        printf ">> vpnoms <<\n%s\n\n" (catset_to_string (gen_vpnoms catgen));
        printf ">> cls_adj <<\n%s\n\n" (catset_to_string (gen_cls_adj catgen));
        printf ">> cls_adv <<\n%s\n\n" (catset_to_string (gen_cls_adv catgen));
        printf ">> cls_nmod <<\n%s\n\n" (catset_to_string (gen_cls_nmod catgen));
        printf ">> cls_vmod <<\n%s\n\n" (catset_to_string (gen_cls_vmod catgen));
        printf ">> cls <<\n%s\n\n" (catset_to_string (gen_cls catgen));
        printf ">> negs <<\n%s\n\n" (catset_to_string (gen_negs catgen));
        printf ">> smods_final <<\n%s\n\n" (catset_to_string (gen_smods_final catgen))

    let run catgen =
        init_empty_ftrs catgen;
        let grpmap = [
            ("[n]",           [np]);
            ("[nmod]",        gen_nmods catgen);
            ("[conj]",        match catgen.depscheme with
                                  HeadOutwardScheme -> [conj]
                                | DepbankScheme -> [conj_depbank]);
            ("[conj_collins]", [conj]);
            ("[conj_depbank]", [conj_depbank]);
            ("[conj_truedep]", [conj_truedep]);
            ("[conj_rev]",     [conj_rev]);
            ("[v]",           gen_verbs catgen);
            ("[vi]",          gen_verbs_vis catgen);
            ("[vt]",          gen_verbs_vts catgen);
            ("[vd]",          gen_verbs_vds catgen);
            ("[vicomp]",      gen_verbs_vicomps catgen);
            ("[vtcomp]",      gen_verbs_vtcomps catgen);
            ("[vcomp]",       gen_verbs_vcomps catgen);
            ("[adj]",         gen_adjs catgen);
            ("[adv]",         gen_advs catgen);
            ("[modal]",       gen_modals catgen);
            ("[adposition]",  gen_adpositions catgen);
            ("[prep]",        gen_prepositions catgen);
            ("[post]",        gen_postpositions catgen);
            ("[prep]",        gen_adpositions catgen);
            ("[relpro]",      gen_relpros catgen);
            ("[copula]",      gen_copulas catgen);
            ("[part]",        gen_parts catgen);
            ("[gerund]",      gen_gerunds catgen);
            ("[npgerund]",    gen_gerund_nouns catgen);
            ("[nmodgerund]",  gen_gerund_nmods catgen);
            ("[vmodgerund]",  gen_gerund_vmods catgen);
            ("[subconj]",     gen_subconjs catgen);
            ("[vmod]",        gen_vmods catgen);
            ("[gmod]",        gen_gmods catgen);
            ("[smod]",        gen_smods catgen);
            ("[cl]",          gen_cls catgen);
            ("[adjcl]",       gen_cls_adj catgen);
            ("[advcl]",       gen_cls_adv catgen);
            ("[nmodcl]",      gen_cls_nmod catgen);
            ("[vmodcl]",      gen_cls_vmod catgen);
            ("[poss]",        gen_posses catgen);
            ("[inf]",         gen_infs catgen);
            ("[npnom]",       gen_vpnoms catgen);
            ("[vpnom]",       gen_vpnoms catgen);
            ("[neg]",         gen_negs catgen);
            ("[s]",           [s]);
            ("[np]",          [np]);
            ("[num]",         [num]);
            ("[vp]",          gen_vps catgen);
            ("[pp]",          gen_pps catgen)] in
        List.iter
            (fun (grp, syncats) ->
                MacroExpander.add_cats catgen.macroexp grp syncats;
                let tag = String.sub grp 1 (String.length grp - 2) in
                let h = Syncat.create_atom tag in
                List.iter
                    (fun syncat ->
                        Dict.add_unarydrv catgen.dict h syncat)
                    syncats
            )
            grpmap;
        MacroExpander.add_cats catgen.macroexp "[smod]" (gen_smods_final catgen);
        List.iter
            (fun (_, syncats) ->
                MacroExpander.add_cats catgen.macroexp "[$all$]" syncats)
            grpmap;
        gen_np_to_varg catgen;
        gen_np_to_adj catgen
    
end

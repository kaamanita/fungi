(** Hash Database. *)

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

(** Stored Item. *)
module type ITEM_TYPE =
sig

    (** Key. *)
    type key_t

    (** Value. *)
    type value_t

    (** [equal k1 k2] checks if two keys [k1] and [k2] are equal. *)
    val equal : key_t -> key_t -> bool

    (** [hash k] computes the hash value of a key [k]. *)
    val hash : key_t -> int

end

(** Hash Database. *)
module HDB :
functor (Item : ITEM_TYPE) -> sig

    (** Key. *)
    type key_t

    (** Value. *)
    type value_t

    (** Hash database. *)
    type t

    (** [create n] creates a hash database with an initial number of
        entries [n]. *)
    val create : int -> t

    (** [add hdb k v] adds an entry of key [k] and value [v] into a hash
        database [hdb]. If the key [k] is already in the database, the
        new value is stored in a new entry. *)
    val add : t -> key_t -> value_t -> unit

    (** [copy hdb] copies a hash database. *)
    val copy : t -> t

    (** [find hdb k] finds the value of a key [k] in the hash database 
        [hdb]. If the key [k] is not in the database, the exception
        [Not_found] will be thrown. *)
    val find : t -> key_t -> value_t

    (** [findall hdb k] finds all values of a key [k] in the hash
        database [hdb]. *)
    val find_all : t -> key_t -> value_t list

    (** [mem hdb k] checks if a key [k] is in the hash database [hdb]. *)
    val mem : t -> key_t -> bool

    (** [remove hdb k] removes all entries of a key [k] in a hash
        database [hdb]. *)
    val remove : t -> key_t -> unit

    (** [replace hdb k v] replaces the entry of a key [k] with a value
        [v] on a hash database [hdb]. *)
    val replace : t -> key_t -> value_t -> unit

    (** [iter fn hdb] iterates a function [fn] on a hash database
        [hdb]. *)
    val iter : (key_t -> value_t -> unit) -> t -> unit

    (** [fold fn hdb x] folds a function [fn] over a hash database 
        [hdb] with an initial input [x]. *)
    val fold : (key_t -> value_t -> 'a -> 'a) -> t -> 'a -> 'a

    (** [length hdb] finds the length of a hash database [hdb]. *)
    val length : t -> int

    (** [from_file fname] loads a hash database from a file [fname]. *)
    val from_file : string -> t

    (** [to_file hdb fname] saves a hash database [hdb] to a file 
        [fname]. *)
    val to_file : t -> string -> unit

end

(** Sequential Hashed Database. *)
module SeqHDB :
functor (
    Item : sig
        type key_t = int
        type value_t
        val equal : key_t -> key_t -> bool
        val hash : key_t -> int
    end
) -> sig

    (** Key. *)
    type key_t = Item.key_t

    (** Value. *)
    type value_t = Item.value_t

    (** Sequential hash database. *)
    type t

    (** [create dbname] creates a sequential hash database stored in a
        file [fname]. *)
    val create : string -> t

    (** [add hdb v] appends a value [v] to a sequential hash database
        [hdb]. *)
    val add : t -> value_t -> unit

    (** [copy hdb] copies a sequential hash database [hdb]. *)
    val copy : t -> t

    (** [mem hdb i] checks if an index [i] is a member of a sequential
        hash database [hdb]. *)
    val mem : t -> key_t -> bool

    (** [find hdb i] finds the value of an index [i] in a sequential
        hash database [hdb]. *)
    val find : t -> key_t -> value_t

    (** [find_all hdb i] finds all values of an index [i] in a sequential
        hash database [hdb]. *)
    val find_all : t -> key_t -> value_t list

    (** [remove hdb i] removes all entries of an index [i] in a
        sequential hash database [hdb]. *)
    val remove : t -> key_t -> unit

    (** [replace hdb i v] replaces the entry of an index [i] in a
        sequential hash database [hdb]. *)
    val replace : t -> key_t -> value_t -> unit

    (** [iter fn hdb] iterates over a sequential hash database [hdb]
        with a function [fn]. *)
    val iter : (key_t -> value_t -> unit) -> t -> unit

    (** [fold fn hdb x] folds a function [fn] over a sequential hash
        database [hdb] with an initial input [x]. *)
    val fold : (key_t -> value_t -> 'a -> 'a) -> t -> 'a -> 'a

    (** [length hdb] finds the length of a sequential hash database
        [hdb]. *)
    val length : t -> int

    (** [from_file fname] loads a sequential hash database from a file
        [fname]. *)
    val from_file : string -> t

    (** [to_file hdb fname] saves a sequential hash database [hdb] to a
        file [fname]. *)
    val to_file : t -> string -> unit
end

(** Sequential Hashed Database Wrapper. *)
module SeqHDBWrapper :
functor (
    Item : sig
        type key_t = int
        type value_t
        val equal : key_t -> key_t -> bool
        val hash : key_t -> int
    end
) -> sig

    (** Key. *)
    type key_t = Item.key_t

    (** Value. *)
    type value_t = Item.value_t

    (** Internal sequential hash database. *)
    type seqhdb_t = SeqHDB(Item).t

    (** Sequential hash database wrapper. *)
    type t

    (** [create hdbs] creates a sequential hash database wrapper from a
        list of sequential hash databases [hdbs]. *)
    val create : seqhdb_t list -> t

    (** [from_files fnames] loads a sequential hash database wrapper from
        a list of sequential hash database files [fnames]. *)
    val from_files : string list -> t

    (** [get_nodbs hdbw] finds the number of sequential hash databases
        in the wrapper [hdbw]. *)
    val get_nodbs : t -> int

    (** [get_noitems hdbw] finds the total number of entries in the entire
        wrapper [hdbw]. *)
    val get_noitems : t -> int

    (** [nth hdbw i] finds the [i]-th item of a sequential hash database
        wrapper [hdbw]. *)
    val nth : t -> int -> seqhdb_t

    (** [calc_relidx hdbw i] calculates a relative index of the [i]-th
        item of a sequential hash database [hdbw]. The results is of the
        form (index of sequential database, relative index in such 
        database). *)
    val calc_relidx : t -> int -> int * int

    (** [mem hdbw i] checks if an index [i] is in a sequential hash
        database [hdbw]. *)
    val mem : t -> int -> bool

    (** [find hdbw i] finds the [i]-th item of a sequential hash database 
        wrapper [hdbw]. *)
    val find : t -> int -> value_t

    (** [find_all hdbw i] finds all items at the [i]-th index of a
        sequential hash database wrapper [hdbw]. *)
    val find_all : t -> int -> value_t list

    (** [length hdbw] finds the total number of items in a sequential
        hash database wrapper [hdbw]. *)
    val length : t -> int

    (** [replace hdbw i v] replaces the [i]-th item of a sequential hash
        database wrapper [hdbw] with a value [v]. *)
    val replace : t -> int -> value_t -> unit

    (** [iter fn hdbw] iterates a function [fn] over a sequential hash
        database wrapper [hdbw]. *)
    val iter : (key_t -> value_t -> unit) -> t -> unit
end

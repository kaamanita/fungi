open Cg
open Chart
open Prob
open Deriv
open Models
open Inout
open Utils
open Printf

module ScoreInst = struct

    type syncat_t = Syncat.t
    type word_t = string

    type t = {
        syncat : syncat_t;
        head   : word_t;
    }

    let create syncat head = {
        syncat = syncat;
        head   = head;
    }

    let get_syncat inst =
        inst.syncat

    let get_head inst =
        inst.head

    let equal inst1 inst2 =
        Syncat.equal inst1.syncat inst2.syncat
        && inst1.head = inst2.head

    let hash inst =
        Syncat.hash inst.syncat + Hashtbl.hash inst.head

    let to_string inst =
        sprintf "%s{%s}" (Syncat.to_string inst.syncat) inst.head

end

module ScoreTbl = ProbTbl(ScoreInst)

module type EXPCALC_TYPE = sig
    val calc_prob : float -> float -> float
end

let uniform_scale_prob prob _ = prob

let uniform_localscore _ _ = 1.0

module EStep(ExpCalc : EXPCALC_TYPE) = struct

    type syncat_t = Syncat.t

    type word_t = string

    type chart_t = Chart.t

    type scoreinst_t = ScoreInst.t

    type scoretbl_t = ScoreTbl.t

    module IOScoreInst = Inout.ScoreInst

    module IOScoreTbl = Inout.ScoreTbl

    type ioscoretbl_t = IOScoreTbl.t

    type genmodel_t = DerivGenModel.t

    type deriv_t = Deriv.t

    type fn_scale_prob_t = float -> deriv_t -> float

    type fn_localscore_t = int -> int -> float

    type t = {
        epsilon         : float;
        chart           : chart_t;
        ruleprob        : genmodel_t;
        ruleprior       : genmodel_t;
        insctbl         : ioscoretbl_t;
        outsctbl        : ioscoretbl_t;
        ruleexp         : genmodel_t;
        catexp          : scoretbl_t;
        start           : syncat_t;
        endpos          : int;
        scale_prob      : fn_scale_prob_t;
        localscore      : fn_localscore_t;
        mutable sentexp : float;
    }

    let create 
            ?(scale_prob = uniform_scale_prob) 
            ?(localscore = uniform_localscore)
            n epsilon ruleprob ruleprior chart start
            ruleexp catexp = 
        let (insctbl, outsctbl) = 
            Inout.compute n epsilon ruleprob chart start in
        (* let ruleexp = create_pcfg n epsilon
        and catexp = ScoreTbl.create n epsilon in *)
        {
            epsilon    = epsilon;
            chart      = chart;
            ruleprob   = ruleprob;
            ruleprior  = ruleprior;
            insctbl    = insctbl;
            outsctbl   = outsctbl;
            ruleexp    = ruleexp;
            catexp     = catexp;
            start      = start;
            endpos     = List.length (Chart.get_sentence chart) - 1;
            scale_prob = scale_prob;
            localscore = localscore;
            sentexp    = 0.0;
        }

    let to_string inout =
        sprintf "<Rule Expectation>\n%s\n\n<Category Expectation>\n%s\n\n<Rule Prob>\n%s"
            (DerivGenModel.to_string inout.ruleexp)
            (ScoreTbl.to_string inout.catexp)
            (DerivGenModel.to_string inout.ruleprob)

    let get_sentexp inout =
        inout.sentexp

    let calc_lexexp inout syncat span headpos word =
        Scale.inc ();
        Scale.display ();
        let inst = ScoreInst.create syncat word 
        and ioinst = Inout.ScoreInst.create span syncat word in
        let deriv = conv_word_to_deriv syncat word in
        (* printf ">> Calculating the expectation of %s\n\n" 
            (Deriv.to_string deriv); *)
        let prior = DerivGenModel.get_smooth_prob inout.ruleprior deriv in
        let ruleprob =
            DerivGenModel.get_smooth_prob inout.ruleprob deriv in
        let outsc = IOScoreTbl.get_smooth_prob inout.outsctbl ioinst in
        (* printf "Prior = %e\nRule prob = %e\nOutside score = %e\n"
            prior ruleprob outsc; *)
        let subtotal = prior +. ruleprob *. outsc in
        (* printf "Subtotal = %e\n" subtotal;
        printf "RuleExp before %e -> " (DerivGenModel.get_prob inout.ruleexp deriv); *)
        DerivGenModel.acc_prob inout.ruleexp deriv subtotal;
        (* printf "after %e\n" (DerivGenModel.get_prob inout.ruleexp deriv);
        printf "CatExp before %e -> " (ScoreTbl.get_prob inout.catexp inst); *)
        ScoreTbl.acc_prob inout.catexp inst subtotal (*;
        printf "after %e\n\n" (ScoreTbl.get_prob inout.catexp inst) *)

    let calc_dtrsexp inout syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let inst = ScoreInst.create syncat word 
        and ioinst = IOScoreInst.create span syncat word in
        let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
        (* printf ">> Calculating the expectation of %s\n\n" 
            (Deriv.to_string deriv); *)
        let prior = DerivGenModel.get_smooth_prob inout.ruleprior deriv in
        let ruleprob =
            DerivGenModel.get_smooth_prob inout.ruleprob deriv in
        let outsc = IOScoreTbl.get_smooth_prob inout.outsctbl ioinst in
        (* printf "Prior = %e\nRule prob = %e\nOutside score = %e\n"
            prior ruleprob outsc; *)
        let subtotal = List.fold_left
            (fun v (syncat', span', headpos', word') ->
                let ioinst' = IOScoreInst.create span' syncat' word' in
                v *. IOScoreTbl.get_smooth_prob inout.insctbl ioinst'
                    *. inout.localscore headpos headpos')
            (prior +. ruleprob *. outsc) dtrargs in
        (* printf "Subtotal = %e\n" subtotal;
        printf "RuleExp before %e -> " (DerivGenModel.get_prob inout.ruleexp deriv); *)
        DerivGenModel.acc_prob inout.ruleexp deriv subtotal;
        (* printf "after %e\n" (DerivGenModel.get_prob inout.ruleexp deriv);
        printf "CatExp before %e -> " (ScoreTbl.get_prob inout.catexp inst); *)
        ScoreTbl.acc_prob inout.catexp inst subtotal (*;
        printf "after %e\n\n" (ScoreTbl.get_prob inout.catexp inst) *)

    let compute_e_step inout =
        Scale.add 
            (* (Scale.Number (Chart.get_derivcnt inout.chart)) *)
            (Scale.WindMill 1500)
            "E-Step  " "";
        Chart.iter_derivs inout.chart
            Chart.TopDown
            (calc_lexexp inout) (calc_dtrsexp inout)
            (0, inout.endpos);
        Scale.discard ()

    let compute_sentexp_lex inout syncat span headpos word =
        if Syncat.equal_ign_attrs syncat inout.start then
            let ioinst = IOScoreInst.create span syncat word in
            let subtotal = 
                IOScoreTbl.get_smooth_prob inout.insctbl ioinst in
            inout.sentexp <- inout.sentexp +. subtotal
        else ()

    let compute_sentexp_dtrs inout 
            syncat span headpos word depdir dtrargs =
        if Syncat.equal_ign_attrs syncat inout.start then
            let ioinst = IOScoreInst.create span syncat word in
            let subtotal = 
                IOScoreTbl.get_smooth_prob inout.insctbl ioinst in
            inout.sentexp <- inout.sentexp +. subtotal
        else ()

    let compute_sentexp inout =
        Chart.iter_derivs_onelevel inout.chart
            (compute_sentexp_lex inout) (compute_sentexp_dtrs inout)
            (0, inout.endpos)

    let compute 
            ?(scale_prob = uniform_scale_prob) 
            ?(localscore = uniform_localscore) 
            n epsilon ruleprob ruleprior chart start
            ruleexp catexp =
        let inout = create 
            ~scale_prob:scale_prob ~localscore:localscore
            n epsilon ruleprob ruleprior chart start
            ruleexp catexp in
        compute_e_step inout

    let compute_sentexp_from_chart 
            ?(scale_prob = uniform_scale_prob) 
            ?(localscore = uniform_localscore) 
            n epsilon ruleprob ruleprior chart start
            ruleexp catexp =
        let inout = create 
            ~scale_prob:scale_prob ~localscore:localscore
            n epsilon ruleprob ruleprior chart start
            ruleexp catexp in
        compute_sentexp inout;
        get_sentexp inout

end

module MStep(ExpCalc : EXPCALC_TYPE) = struct

    type syncat_t = Syncat.t

    type word_t = string

    type scoreinst_t = ScoreInst.t

    type scoretbl_t = ScoreTbl.t

    type genmodel_t = DerivGenModel.t

    type deriv_t = Deriv.t

    type fn_scale_prob_t = float -> deriv_t -> float

    type fn_localscore_t = int -> int -> float

    type t = {
        epsilon         : float;
        ruleprob        : genmodel_t;
        ruleprior       : genmodel_t;
        ruleexp         : genmodel_t;
        catexp          : scoretbl_t;
        start           : syncat_t;
        scale_prob      : fn_scale_prob_t;
        localscore      : fn_localscore_t;
        mutable sentexp : float;
    }

    let create 
            ?(scale_prob = uniform_scale_prob) 
            ?(localscore = uniform_localscore)
            n epsilon ruleprob ruleprior start
            ruleexp catexp = 
        (* let ruleexp = create_pcfg n epsilon
        and catexp = ScoreTbl.create n epsilon in *)
        {
            epsilon    = epsilon;
            ruleprob   = ruleprob;
            ruleprior  = ruleprior;
            ruleexp    = ruleexp;
            catexp     = catexp;
            start      = start;
            scale_prob = scale_prob;
            localscore = localscore;
            sentexp    = 0.0;
        }

    let calc_ruleprob inout newtbl deriv oldruleprob =
        Scale.inc ();
        Scale.display ();
        (* printf ">> Calculating the rule probability of %s\n\n"
            (Deriv.to_string deriv); *)
        let prems = Deriv.get_prems deriv in
        let syncat = Deriv.get_syncat prems
        and head = Deriv.get_head prems in
        let inst = ScoreInst.create syncat head in
        let ruleexp = DerivGenModel.get_smooth_prob inout.ruleexp deriv
        and catexp = ScoreTbl.get_smooth_prob inout.catexp inst in
        (* printf "Rule expectation = %e\nCategory expectation = %e\n"
            ruleexp catexp; *)
        let ruleprob' = ExpCalc.calc_prob ruleexp catexp in
        let ruleprob = inout.scale_prob ruleprob' deriv in
        (* printf "Rule probability = %e\n\n" ruleprob; *)
        DerivGenModel.set_prob newtbl deriv ruleprob


    let compute_m_step inout =
        Scale.add 
            (* (Scale.Number (Chart.get_derivcnt inout.chart)) *)
            (Scale.WindMill 1500)
            "M-Step  " "";
        let newtbl = DerivGenModel.create 100 inout.epsilon [] in
        DerivGenModel.iter inout.ruleexp (calc_ruleprob inout newtbl);
        DerivGenModel.iter inout.ruleprob
            (fun inst p ->
                if not (DerivGenModel.mem newtbl inst) then
                    DerivGenModel.acc_prob newtbl inst p
                else ());
        DerivGenModel.iter newtbl
            (fun inst _ -> 
                DerivGenModel.set_prob inout.ruleprob inst 0.0);
        DerivGenModel.iter newtbl
            (fun inst p -> 
                DerivGenModel.acc_prob inout.ruleprob inst p);
        Scale.discard ()


    let compute 
            ?(scale_prob = uniform_scale_prob) 
            ?(localscore = uniform_localscore) 
            n epsilon ruleprob ruleprior start
            ruleexp catexp =
        let inout = create 
            ~scale_prob:scale_prob ~localscore:localscore
            n epsilon ruleprob ruleprior start 
            ruleexp catexp in
        compute_m_step inout

end

module InOutAlgo (ExpCalc : EXPCALC_TYPE) = struct

    type syncat_t = Syncat.t

    type word_t = string

    type chart_t = Chart.t

    type genmodel_t = DerivGenModel.t

    type scoreinst_t = ScoreInst.t

    type scoretbl_t = ScoreTbl.t

    module EStep = EStep (ExpCalc)
    
    module MStep = MStep (ExpCalc)

    type fn_get_chart_t = int -> chart_t

    type deriv_t = Deriv.t
    
    type fn_scale_prob_t = float -> deriv_t -> float

    type fn_localscore_t = int -> int -> float

    type t = {
        noentries   : int;
        diffthr     : float;
        rndthr      : int;
        epsilon     : float;
        ruleprob    : genmodel_t;
        ruleprior   : genmodel_t;
        start       : syncat_t;
        get_chart   : fn_get_chart_t;
        scale_prob  : fn_scale_prob_t;
        localscore  : fn_localscore_t;
        nocharts    : int;
        sentexps    : float array;
        mutable rnd : int;
    }

    let create 
            ?(scale_prob = uniform_scale_prob) 
            ?(localscore = uniform_localscore) 
            n diffthr rndthr 
            epsilon ruleprob ruleprior start get_chart nocharts =
        {
            noentries  = n;
            diffthr    = diffthr;
            rndthr     = rndthr;
            epsilon    = epsilon;
            ruleprob   = ruleprob;
            ruleprior  = ruleprior;
            start      = start;
            get_chart  = get_chart;
            scale_prob = scale_prob;
            localscore = localscore;
            nocharts   = nocharts;
            sentexps   = Array.make nocharts 0.0;
            rnd        = 0;
        }

    let get_round inout =
        inout.rnd

    let reset_round inout =
        inout.rnd <- 0

    (* let calc_dkl inout exps1 exps2 =
        let result = ref 0.0 in
        let div1 = ref 0.0 
        and div2 = ref 0.0 in
        for ind = 0 to inout.nocharts - 1 do
            div1 := !div1 +. exps1.(ind);
            div2 := !div2 +. exps2.(ind)
        done;
        if !div1 = 0.0 then result := Stdlib.infinity
        else begin
            if !div2 = 0.0 then div2 := 1.0
            else ();
            for ind = 0 to inout.nocharts - 1 do
                let p = exps1.(ind) /. !div1
                and q = exps2.(ind) /. !div2 in
                if q > 0.0 then begin
                    let r = q *. log (q /. p) in
                    result := !result +. r
                end else ()
            done
        end;
        if !result >= 0.0 then !result
        else -1.0 *. !result *)

    let calc_dkl inout genmodel1 genmodel2 =
        let result = ref 0.0 
        and divp = ref 0.0 
        and divq = ref 0.0 in
        DerivGenModel.iter
            genmodel1
            (fun inst p -> divp := !divp +. p);
        DerivGenModel.iter
            genmodel2
            (fun inst q -> divq := !divq +. q);
        if !divp = 0.0 || !divq = 0.0 then infinity
        else begin
            DerivGenModel.iter
                genmodel1
                (fun inst p ->
                    let q = DerivGenModel.get_prob genmodel2 inst in
                    let p' = p /. !divp
                    and q' = q /. !divq in
                    if (p' > 0.0) && (q' > 0.0) then begin
                        let r = q' *. (log q' -. log p') in
                        result := !result +. r
                    end else ());
            !result
        end
        (* if !result >= 0.0 then !result
        else -1.0 *. !result *)

    (* let compute_sentexps inout =
        Scale.add (Scale.Number inout.nocharts) "sentexp " " > ";
        for ind = 0 to inout.nocharts - 1 do
            Scale.inc ();
            Scale.display ();
            let chart = inout.get_chart ind in
            inout.sentexps.(ind) <- 
                EStep.compute_sentexp_from_chart 
                    inout.noentries inout.epsilon 
                    inout.ruleprob inout.ruleprior
                    chart inout.start
                    inout.ruleexp inout.catexp
        done;
        Scale.discard () *)

    let compute_onernd inout =
        Scale.add (Scale.Number inout.nocharts) "chart " " > ";
        (* let oldsentexps = Array.copy inout.sentexps in *)
        let oldruleprob = DerivGenModel.create 100 inout.epsilon [] in
        DerivGenModel.iter inout.ruleprob
            (fun inst prob ->
                DerivGenModel.acc_prob oldruleprob inst prob);
        let ruleexp = create_pcfg inout.noentries inout.epsilon
        and catexp = ScoreTbl.create inout.noentries inout.epsilon in
        for ind = 0 to inout.nocharts - 1 do
            Scale.inc ();
            Scale.display ();
            let chart = inout.get_chart ind in
            if not (Chart.is_filledup chart) then
                EStep.compute
                    inout.noentries inout.epsilon 
                    inout.ruleprob inout.ruleprior
                    chart inout.start
                    ruleexp catexp 
            else ()
            (*;
            MStep.compute
                inout.noentries inout.epsilon 
                inout.ruleprob inout.ruleprior
                inout.start
                ruleexp catexp *)
        done;
        Scale.discard ();
        MStep.compute
            inout.noentries inout.epsilon 
            inout.ruleprob inout.ruleprior
            inout.start
            ruleexp catexp;
        (* compute_sentexps inout; *)
        Utils.run_save_prob ();
        calc_dkl inout oldruleprob inout.ruleprob

    let compute_inout inout =
        Scale.clearline ();
        Scale.print ">> Training with Inside/Outside Algorithm\n";
        Scale.redisplay ();
        (* let starttime = Unix.time () in
        compute_sentexps inout;
        let endtime = Unix.time () in
        let runtime = (endtime -. starttime) /. 60.0 in
        Scale.clearline ();
        Scale.print (sprintf "     Initial sentexp computation done (runtime = %.2f mins)\n" runtime);
        Scale.redisplay (); *)
        Scale.add Scale.NumberNoMax "EM (iter " ") > ";
        let absdiff = ref infinity 
        and olddkl = ref 0.0 in
        while !absdiff > inout.diffthr && inout.rnd < inout.rndthr do
            Scale.inc ();
            Scale.display ();
            let starttime = Unix.time () in
            let dkl = compute_onernd inout in
            let endtime = Unix.time () in
            absdiff := dkl -. !olddkl;
            if !absdiff < 0.0 then absdiff := -1.0 *. !absdiff
            else ();
            olddkl := dkl;
            inout.rnd <- inout.rnd + 1;
            Scale.clearline ();
            let runtime = (endtime -. starttime) /. 60.0 in
            Scale.print (sprintf "     Iteration %d: D_KL = %e (runtime = %.2f mins)\n" inout.rnd dkl runtime);
            Scale.redisplay ()
        done;
        Scale.discard ()

    let compute 
            ?(scale_prob = uniform_scale_prob) 
            ?(localscore = uniform_localscore) 
            n diffthr rndthr 
            epsilon ruleprob ruleprior start get_chart nocharts =
        let inout = create 
            ~scale_prob:scale_prob ~localscore:localscore
            n diffthr rndthr 
            epsilon ruleprob ruleprior start get_chart nocharts in
        compute_inout inout

end

module HashedInt = struct

    type t = int

    let hash = Hashtbl.hash

    let equal = Stdlib.(=)

end

let leapfrogthr = ref 15

module BabystepInOutAlgo (ExpCalc : EXPCALC_TYPE) = struct

    type syncat_t = Syncat.t

    type word_t = string

    type chart_t = Chart.t

    type genmodel_t = DerivGenModel.t

    module InOutAlgo = InOutAlgo(ExpCalc)

    type inoutalgo_t = InOutAlgo.t

    module StepTbl = Hashtbl.Make(HashedInt)

    type steptbl_t = (int list) StepTbl.t

    type fn_get_chart_t = int -> chart_t

    type deriv_t = Deriv.t
    
    type fn_scale_prob_t = float -> deriv_t -> float

    type fn_localscore_t = int -> int -> float

    type t = {
        noentries     : int;
        diffthr       : float;
        rndthr        : int;
        epsilon       : float;
        ruleprob      : genmodel_t;
        ruleprior     : genmodel_t;
        start         : syncat_t;
        get_chart     : fn_get_chart_t;
        scale_prob    : fn_scale_prob_t;
        localscore    : fn_localscore_t;
        nocharts      : int;
        sentexps      : float array;
        steptbl       : steptbl_t;
        mutable steps : int list ref;
        mutable inout : inoutalgo_t option;
        mutable rnd   : int;
    }

    let create 
        ?(scale_prob = uniform_scale_prob) 
        ?(localscore = uniform_localscore) 
        n diffthr rndthr 
        epsilon ruleprob ruleprior start get_chart nocharts =
    {
        noentries   = n;
        diffthr     = diffthr;
        rndthr      = rndthr;
        epsilon     = epsilon;
        ruleprob    = ruleprob;
        ruleprior   = ruleprior;
        start       = start;
        get_chart   = get_chart;
        scale_prob  = scale_prob;
        localscore  = localscore;
        nocharts    = nocharts;
        sentexps    = Array.make nocharts 0.0;
        steptbl     = StepTbl.create 100;
        steps       = ref [];
        inout       = None;
        rnd         = 0;
    }

    let get_round babystep =
        babystep.rnd

    let reset_round babystep =
        babystep.rnd <- 0

    let assg_step babystep sentlen =
        if sentlen < !leapfrogthr then sentlen
        else int_of_float (5.0 *. ceil (float_of_int (sentlen - 1) /. 5.0))

    let build_steptbl babystep =
        Scale.add Scale.StaticInfix "Building steps ..." "";
        Scale.display ();
        for i = 0 to babystep.nocharts - 1 do begin
            let chart = babystep.get_chart i in
            let sentlen = assg_step babystep (List.length (Chart.get_sentence chart)) + 1 in
            if not (StepTbl.mem babystep.steptbl sentlen) then
                StepTbl.replace babystep.steptbl sentlen []
            else ();
            let entry = StepTbl.find babystep.steptbl sentlen in
            StepTbl.replace babystep.steptbl sentlen (entry @ [i]);
            if not (List.mem sentlen !(babystep.steps)) then
                babystep.steps := (sentlen :: !(babystep.steps))
            else ()
        end done;
        babystep.steps := List.sort Stdlib.compare !(babystep.steps);
        (* 
        for i = 0 to (List.length !(babystep.steps) - 2) do
            let sentlen = List.nth !(babystep.steps) i in
            let nextsentlen = List.nth !(babystep.steps) (i + 1) in
            let entry = StepTbl.find babystep.steptbl sentlen
            and nextentry = StepTbl.find babystep.steptbl nextsentlen in
            StepTbl.replace babystep.steptbl nextsentlen (entry @ nextentry)
        done;
        *)
        Scale.discard ()

    let get_chart babystep sentlen i =
        let entry = StepTbl.find babystep.steptbl sentlen in
        let sentid = List.nth entry i in
        babystep.get_chart sentid

    let compute_babystep babystep =
        for i = 0 to (List.length !(babystep.steps) - 1) do
            let sentlen = List.nth !(babystep.steps) i in
            Scale.clearline ();
            if sentlen <= !leapfrogthr then
                Scale.print (Printf.sprintf ">> Running Babystep (sentlen=%d)\n" (sentlen - 1))
            else
                Scale.print (Printf.sprintf ">> Running Leapfrog (sentlen=%d)\n" (sentlen - 1));
            Scale.redisplay ();
            if sentlen <= !leapfrogthr then
                Scale.add Scale.StaticInfix 
                    (Printf.sprintf "Babystep (sentlen=%d" (sentlen - 1))
                    ") > "
            else
                Scale.add Scale.StaticInfix 
                    (Printf.sprintf "Leapfrog (sentlen=%d" (sentlen - 1))
                    ") > ";
            Scale.display ();
            let entry = StepTbl.find babystep.steptbl sentlen in
            let get_chart' = get_chart babystep sentlen 
            and nocharts = List.length entry in
            InOutAlgo.compute
                ~scale_prob:babystep.scale_prob ~localscore:babystep.localscore
                babystep.noentries babystep.diffthr babystep.rndthr 
                babystep.epsilon babystep.ruleprob babystep.ruleprior 
                babystep.start get_chart' nocharts;
            Scale.discard ()
        done

    let compute 
            ?(scale_prob = uniform_scale_prob) 
            ?(localscore = uniform_localscore) 
            n diffthr rndthr 
            epsilon ruleprob ruleprior start get_chart nocharts =
        let babystep = create 
            ~scale_prob:scale_prob ~localscore:localscore
            n diffthr rndthr 
            epsilon ruleprob ruleprior start get_chart nocharts in
        build_steptbl babystep;
        compute_babystep babystep

end

module StandardInOutExpCalc = struct

    let calc_prob ruleexp catexp =
        if ruleexp > 0.0 && catexp > 0.0 then 
            ruleexp /. catexp
        else 0.0

end

module VBInOutExpCalc = struct

    let calc_prob ruleexp catexp =
        if ruleexp > 0.0 && catexp > 0.0 then 
            exp (digamma ruleexp -. digamma catexp)
        else 0.0

end

module StandardInOutAlgo = InOutAlgo(StandardInOutExpCalc)

module VBInOutAlgo = InOutAlgo(VBInOutExpCalc)

module StandardBabystepInOutAlgo = BabystepInOutAlgo(StandardInOutExpCalc)

module VBBabystepInOutAlgo = BabystepInOutAlgo(VBInOutExpCalc)

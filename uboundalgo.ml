open Cg
open Chart
open Prob
open Deriv
open Tree
open Dep
open Utils

module Inst = struct

    type span_t = int * int
    
    type syncat_t = Syncat.t
    
    type word_t = string
    
    type t = span_t * syncat_t * word_t

    let create span syncat head = (span, syncat, head)

    let get_span inst =
        let (span, _, _) = inst in
        span

    let get_syncat inst =
        let (_, syncat, _) = inst in
        syncat

    let get_head inst =
        let (_, _, head) = inst in
        head
    
    let hash inst = 
        let (span, cat, head) = inst in
        Hashtbl.hash span + Syncat.hash cat + Hashtbl.hash head
    
    let equal inst1 inst2 =
        let (span1, cat1, head1) = inst1
        and (span2, cat2, head2) = inst2 in
        span1 = span2 && Syncat.equal cat1 cat2 && head1 = head2

    let to_string inst =
        let ((begpos, endpos), syncat, word) = inst in
        Printf.sprintf "(%d, %d, %s, %s)" begpos endpos (Syncat.to_string syncat) word

end

module InstTbl = Hashtbl.Make(Inst)

module Viterbi = struct

    type syncat_t = Syncat.t

    type depstruct_t = DepStruct.t

    type word_t = string

    type chart_t = Chart.t

    type tree_t = Tree.t

    type span_t = int * int

    type inst_t = Inst.t

    type insttbl_t = (tree_t * int) InstTbl.t

    type t = {
        insttbl         : insttbl_t;
        depstr          : depstruct_t;
        chart           : chart_t;
        endpos          : int;
        mutable maxroot : (inst_t * int) option;
    }

    let create depstr chart =
        (* Printf.printf "(viterbi.create           ) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let sentlen = List.length (Chart.get_sentence chart) in
        {   
            insttbl  = InstTbl.create (sentlen * sentlen);
            depstr   = depstr;
            chart    = chart;
            endpos   = sentlen - 1;
            maxroot  = None;
        }

    let mem_inst viterbi inst =
        InstTbl.mem viterbi.insttbl inst

    let get_inst viterbi inst =
        if mem_inst viterbi inst then
            InstTbl.find viterbi.insttbl inst
        else raise Not_found

    let get_tree viterbi inst =
        let (tree, _) = get_inst viterbi inst in
        tree

    let get_score viterbi inst =
        let (_, score) = get_inst viterbi inst in
        score

    let set_inst viterbi inst tree score =
        if not (mem_inst viterbi inst) 
                || get_score viterbi inst < score then
            InstTbl.replace viterbi.insttbl inst (tree, score)
        else ()

    let set_maxroot viterbi inst score =
        if Inst.get_span inst = (0, viterbi.endpos) then
            match viterbi.maxroot with
                None ->
                    viterbi.maxroot <- Some (inst, score)
              | Some (inst', score') ->
                begin
                    if score > score' then
                        viterbi.maxroot <- Some (inst, score)
                    else ()
                end
        else () 

    let build_lex_tree viterbi syncat span headpos word =
        Scale.inc ();
        Scale.display ();
        let inst = Inst.create span syncat word in
        let lexnode = Tree.create_term word span in
        let tree = Tree.create_nonterm 
            syncat word [lexnode] span headpos Syncat.Unary in
        let score = 0 in
        set_inst viterbi inst tree score;
        set_maxroot viterbi inst score

    let build_dtrs_tree viterbi syncat span headpos word depdir dtrargs =
        Scale.inc ();
        Scale.display ();
        let inst = Inst.create span syncat word in
        let deriv = conv_dtrargs_to_deriv syncat word depdir dtrargs in
        let depdir = Deriv.get_depdir (Deriv.get_consq deriv) in
        let dtrs = ref [] in
        let score_val =
            if List.length dtrargs = 3 then begin
                let (left, mid, right) = (List.nth dtrargs 0, List.nth dtrargs 1, List.nth dtrargs 2) in
                let (_, _, lpos, _) = left
                and (mcat, _, mpos, _) = mid
                and (_, _, rpos, _) = right in
                if Syncat.equal_ign_attrs mcat Syncat.conn_cat 
                        && DepStruct.mem viterbi.depstr lpos mpos 
                        && DepStruct.mem viterbi.depstr mpos rpos 
                then 2
                else if Syncat.equal_ign_attrs mcat Syncat.conn_cat_depbank 
                        && DepStruct.mem viterbi.depstr mpos lpos
                        && DepStruct.mem viterbi.depstr mpos rpos
                then 2
                else if Syncat.equal_ign_attrs mcat Syncat.conn_cat_truedep 
                        && DepStruct.mem viterbi.depstr lpos rpos
                        && DepStruct.mem viterbi.depstr rpos mpos
                then 2
                else if Syncat.equal_ign_attrs mcat Syncat.conn_cat_rev
                        && DepStruct.mem viterbi.depstr mpos lpos
                        && DepStruct.mem viterbi.depstr rpos mpos
                then 2
                else 0
            end else if List.length dtrargs = 2 then begin
                let (left, right) = (List.nth dtrargs 0, List.nth dtrargs 1) in
                let (_, _, lpos, _) = left
                and (_, _, rpos, _) = right in
                match depdir with
                    Syncat.Left | Syncat.RightSerial ->
                        if DepStruct.mem viterbi.depstr lpos rpos then 1
                        else 0
                  | Syncat.Right | Syncat.LeftSerial ->
                        if DepStruct.mem viterbi.depstr rpos lpos then 1
                        else 0
                  | Syncat.Coor | Syncat.Unary ->
                        failwith "ubound:build_dtrs_tree: unexpected dtrargs length (neither 1, 2, or 3)"
            end else if List.length dtrargs = 1 then 0
            else failwith "ubound.build_dtrs_tree: unexpected dtrargs length (neither 1, 2, or 3)"
        in
        let score = ref score_val in
        List.iter
            (fun (syncat', span', headpos', word') ->
                let inst' = Inst.create span' syncat' word' in
                let (tree', score') = get_inst viterbi inst' in
                dtrs := !dtrs @ [tree'];
                score := !score + score')
            dtrargs;
        let tree = Tree.create_nonterm
            syncat word !dtrs span headpos depdir in
        set_inst viterbi inst tree !score;
        set_maxroot viterbi inst !score

    let compute_viterbi viterbi =
        Scale.add (Scale.WindMill 5000) " " "";
        Chart.iter_derivs viterbi.chart
            Chart.BottomUp
            (build_lex_tree viterbi) (build_dtrs_tree viterbi)
            (0, viterbi.endpos);
        Scale.discard ()

    let decapsulate_tree tree =
        if Tree.is_nonterm tree then begin
            let dtrs = Tree.get_dtrs tree in
            let depdir = Tree.get_depdir tree in
            match depdir with
                Syncat.Left | Syncat.RightSerial -> List.nth dtrs 0
              | Syncat.Right | Syncat.LeftSerial -> List.nth dtrs 1
              | Syncat.Coor -> List.nth dtrs 0
              | Syncat.Unary -> List.nth dtrs 0
        end else tree

    let find_max viterbi =
        let (tree, score) = match viterbi.maxroot with
            None -> raise Not_found
          | Some (inst, score) -> begin
                (* Printf.printf "Inst = %s\n" (Inst.to_string inst); *)
                get_inst viterbi inst
            end in
        (* Printf.printf "tree      = %s\n" (Tree.to_string tree); *)
        let decaptree = decapsulate_tree tree in
        (* Printf.printf "decaptree = %s\n" (Tree.to_string decaptree); *)
        (decaptree, score)

    let compute depstr chart =
        (* Printf.printf "(viterbi.compute          ) Sentence = [%s]\n" (String.concat " " (Chart.get_sentence chart)); *)
        let viterbi = create depstr chart in
        compute_viterbi viterbi;
        find_max viterbi

end

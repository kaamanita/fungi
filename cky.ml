open Cg
open Chart
open Utils
open Dict
open Printf

module CKY = struct

    type syncat_t = CG.syncat_t
    type depdir_t = CG.depdir_t
    type item_t = Chart.item_t
    type id_t = Chart.id_t
    type span_t = Chart.span_t
    type prod_t = Chart.prod_t
    type chart_t = Chart.t
    type word_t = Dict.word_t
    type dict_t = Dict.t
    type transf_t = CG.transf_t

    type t = {
        chart          : chart_t;
        dict           : dict_t;
        sent           : word_t list;
        words          : word_t list;
        sentlen        : int;
        start          : syncat_t;
        mutable transf : transf_t ref;
    }

    (* It automatically adds the ending mark to the end of the sentence. *)
    let add_period sent =
        let len = List.length sent in
        if List.nth sent (len - 1) = ending_mark then sent
        else sent @ [ending_mark]

    let rec sep_words_attrs dict sent =
        match sent with
            [] -> []
          | s :: rest ->
              try begin
                  let idx = String.rindex s '@' in
                  let word = String.sub s 0 idx in
                  word :: sep_words_attrs dict rest
              end with Not_found ->
                  s :: sep_words_attrs dict rest

    let create sent words dict start = 
        let sent = add_period sent in 
        let newsent = sep_words_attrs dict sent in
        {
            chart   = Chart.create newsent words;
            dict    = dict;
            sent    = newsent;
            words   = words;
            sentlen = List.length newsent;
            start   = start;
            transf  = ref [];
        }

    let change_transf cky transfrules =
        cky.transf := transfrules

    let to_string cky =
        sprintf "<Chart>\n%s" (Chart.to_string cky.chart)

    let unary_deriv cky span =
        (* let (begpos, endpos) = span in *)
        (* printf "unary_deriv (%d, %d)\n" begpos endpos;
        flush stdout; *)
        let newcats = ref [] in
        List.iter
            (fun item ->
                let syncat = Item.get_syncat item in
                newcats := (syncat, Item.get_headpos_of_item item) :: !newcats)
            (Chart.get_items cky.chart span);
        (* printf "existing categories = [%s]\n"
            (String.concat ", " (List.map Syncat.to_string !newcats)); *)
        (* List.iter
            (fun (cat, newcats) ->
                printf "Tranf %s -> " (Syncat.to_string cat);
                List.iter
                    (fun cat -> printf "%s, " (Syncat.to_string cat))
                    newcats;
                printf "\n";
                flush stdout)
            !(cky.transf); *)
        List.iter
            (fun item ->
                let syncat = Item.get_syncat item in
                (* printf "found %s\n" (Syncat.to_string syncat);
                flush stdout; *)
                List.iter
                    (fun newcat -> 
                        let h = Item.get_headpos_of_item item in
                        if not (List.mem (newcat, h) !newcats) then begin
                            newcats := (newcat, h) :: !newcats;
                            (* printf "add %s -> %s\n" (Syncat.to_string syncat) (Syncat.to_string newcat);
                            flush stdout; *)
                            List.iter
                                (fun prod ->
                                    let headpos = Item.get_headpos prod in
                                    Chart.add_deriv cky.chart span newcat headpos prod)
                                (Item.get_prods item)
                        end else ())
                    (CG.transform !(cky.transf) syncat))
            (Chart.get_items cky.chart span)

    let rec init_chart_by_sent cky sent index =
        match sent with
            [] -> ()
          | word :: rest ->
            begin
                List.iter 
                    (fun cat -> 
                        let span = (index, index) in
                        let prod = Item.create_prod_lex word index in
                        Chart.add_deriv cky.chart span cat index prod;
                        unary_deriv cky span)
                    (Dict.get_cats cky.dict word);
                init_chart_by_sent cky rest (index + 1)
            end

    let init_chart cky =
        init_chart_by_sent cky cky.sent 0

    let combine_items cky span item1 item2 =
        let syncat1 = Item.get_syncat item1
        and syncat2 = Item.get_syncat item2 
        and word1 = List.nth cky.sent (Item.get_headpos_of_item item1) 
        and word2 = List.nth cky.sent (Item.get_headpos_of_item item2) in
        List.iter
            (fun (rescat, depdir') ->
                (* Override the dependency direction *)
                let depdir =
                    if Dict.is_allowed_univdep cky.dict word1 word2 
                        && Dict.is_allowed_univdep cky.dict word2 word1 then depdir'
                    else if Dict.is_allowed_univdep cky.dict word1 word2 then Syncat.Left
                    else if Dict.is_allowed_univdep cky.dict word2 word1 then Syncat.Right
                    else depdir'
                in
                let headitem =
                    match depdir with
                        Syncat.Left | Syncat.RightSerial -> item1
                      | Syncat.Right | Syncat.LeftSerial -> item2
                      | Syncat.Unary -> failwith "combine_items"
                      | Syncat.Coor -> item1 in
                List.iter 
                    (fun prod ->
                        let headpos = Item.get_headpos prod in
                        let newprod = Item.create_prod_ids 
                            [Item.get_id item1; Item.get_id item2] headpos depdir in
                        Chart.add_deriv cky.chart span rescat headpos newprod) 
                    (Item.get_prods headitem))
            (CG.funcapp syncat1 syncat2)

    let left_serialize_items cky span item1 item2 =
        let syncat1 = Item.get_syncat item1
        and syncat2 = Item.get_syncat item2 in
        if Syncat.equal_ign_attrs syncat1 syncat2 && Dict.is_leftser cky.dict syncat1 then
            let rescat = CG.left_serialize syncat1 syncat2 in
            let depdir = Syncat.LeftSerial in
            let headitem = item2 in
            List.iter
                (fun prod ->
                    let headpos = Item.get_headpos prod in
                    let newprod = Item.create_prod_ids 
                        [Item.get_id item1; Item.get_id item2] headpos depdir in
                    Chart.add_deriv cky.chart span rescat headpos newprod
                )
                (Item.get_prods headitem)
        else ()

    let right_serialize_items cky span item1 item2 =
        let syncat1 = Item.get_syncat item1
        and syncat2 = Item.get_syncat item2 in
        if Syncat.equal_ign_attrs syncat1 syncat2 && Dict.is_rightser cky.dict syncat2 then
            let rescat = CG.right_serialize syncat1 syncat2 in
            let depdir = Syncat.RightSerial in
            let headitem = item1 in
            List.iter
                (fun prod ->
                    let headpos = Item.get_headpos prod in
                    let newprod = Item.create_prod_ids 
                        [Item.get_id item1; Item.get_id item2] headpos depdir in
                    Chart.add_deriv cky.chart span rescat headpos newprod
                )
                (Item.get_prods headitem)
        else ()

    let combine_edges cky span =
        let (begpos, endpos) = span in
        for immpos = begpos to endpos - 1 do
            let span1 = (begpos, immpos)
            and span2 = (immpos + 1, endpos) in
            if Chart.mem cky.chart span1 && Chart.mem cky.chart span2 then
                let items1 = Chart.get_items cky.chart span1
                and items2 = Chart.get_items cky.chart span2 in
                cartiter_noerr 
                    (fun item1 item2 -> combine_items cky span item1 item2) 
                    items1 items2;
                cartiter_noerr 
                    (fun item1 item2 -> left_serialize_items cky span item1 item2) 
                    items1 items2;
                cartiter_noerr 
                    (fun item1 item2 -> right_serialize_items cky span item1 item2) 
                    items1 items2
            else ()
        done

    let conjoin_items cky span item1 item2 item3 =
        let syncat1 = Item.get_syncat item1
        and id2 = Item.get_id item2
        and syncat3 = Item.get_syncat item3 in
        let rescat = CG.conjoin_opt syncat1 syncat3 in
        let depdir = Syncat.Coor in
        let headitem = item1 in
        List.iter
            (fun prod ->
                let headpos = Item.get_headpos prod in
                let newprod = Item.create_prod_ids 
                    [Item.get_id item1; id2; Item.get_id item3] 
                    headpos depdir in
                Chart.add_deriv cky.chart span rescat headpos newprod) 
            (Item.get_prods headitem)

    let conjoin_items_depbank cky span item1 item2 item3 =
        let syncat1 = Item.get_syncat item1
        and id2 = Item.get_id item2
        and syncat3 = Item.get_syncat item3 in
        let rescat = CG.conjoin_opt syncat1 syncat3 in
        let depdir = Syncat.Coor in
        let headitem = item2 in
        List.iter
            (fun prod ->
                let headpos = Item.get_headpos prod in
                let newprod = Item.create_prod_ids 
                    [Item.get_id item1; id2; Item.get_id item3] 
                    headpos depdir in
                Chart.add_deriv cky.chart span rescat headpos newprod) 
            (Item.get_prods headitem)

    let conjoin_items_truedep cky span item1 item2 item3 =
        let syncat1 = Item.get_syncat item1
        and id2 = Item.get_id item2
        and syncat3 = Item.get_syncat item3 in
        let rescat = CG.conjoin_opt syncat1 syncat3 in
        let depdir = Syncat.Coor in
        let headitem = item2 in
        List.iter
            (fun prod ->
                let headpos = Item.get_headpos prod in
                let newprod = Item.create_prod_ids 
                    [Item.get_id item1; id2; Item.get_id item3] 
                    headpos depdir in
                Chart.add_deriv cky.chart span rescat headpos newprod) 
            (Item.get_prods headitem)

    let conjoin_items_rev cky span item1 item2 item3 =
        let syncat1 = Item.get_syncat item1
        and id2 = Item.get_id item2
        and syncat3 = Item.get_syncat item3 in
        let rescat = CG.conjoin_opt syncat1 syncat3 in
        let depdir = Syncat.Coor in
        let headitem = item2 in
        List.iter
            (fun prod ->
                let headpos = Item.get_headpos prod in
                let newprod = Item.create_prod_ids 
                    [Item.get_id item1; id2; Item.get_id item3] 
                    headpos depdir in
                Chart.add_deriv cky.chart span rescat headpos newprod) 
            (Item.get_prods headitem)

    let conjoin_edges cky span =
        let (begpos, endpos) = span in
        for immpos = begpos to endpos - 2 do
            let span1 = (begpos, immpos)
            and span2 = (immpos + 1, immpos + 1)
            and span3 = (immpos + 2, endpos) in
            if Chart.mem cky.chart span1 && Chart.mem cky.chart span2 
                    && Chart.mem cky.chart span3 then
                try begin
                    let item2 = Chart.find_syncat_in_span 
                        cky.chart span2 Syncat.conn_cat (immpos + 1) in
                    let items1 = Chart.get_items cky.chart span1
                    and items3 = Chart.get_items cky.chart span3 in
                    cartiter_noerr
                        (fun item1 item3 -> 
                            conjoin_items cky span item1 item2 item3)
                        items1 items3
                end with Not_found -> ();
                try begin
                    let item2 = Chart.find_syncat_in_span 
                        cky.chart span2 Syncat.conn_cat_depbank (immpos + 1) in
                    let items1 = Chart.get_items cky.chart span1
                    and items3 = Chart.get_items cky.chart span3 in
                    cartiter_noerr
                        (fun item1 item3 -> 
                            conjoin_items_depbank cky span item1 item2 item3)
                        items1 items3
                end with Not_found -> ();
                try begin
                    let item2 = Chart.find_syncat_in_span 
                        cky.chart span2 Syncat.conn_cat_truedep (immpos + 1) in
                    let items1 = Chart.get_items cky.chart span1
                    and items3 = Chart.get_items cky.chart span3 in
                    cartiter_noerr
                        (fun item1 item3 -> 
                            conjoin_items_truedep cky span item1 item2 item3)
                        items1 items3
                end with Not_found -> ();
                try begin
                    let item2 = Chart.find_syncat_in_span 
                        cky.chart span2 Syncat.conn_cat_rev (immpos + 1) in
                    let items1 = Chart.get_items cky.chart span1
                    and items3 = Chart.get_items cky.chart span3 in
                    cartiter_noerr
                        (fun item1 item3 -> 
                            conjoin_items_rev cky span item1 item2 item3)
                        items1 items3
                end with Not_found -> ()
            else ()
        done

    let build_chart cky =
        for edgelen = 2 to cky.sentlen do
            for begpos = 0 to cky.sentlen - edgelen do
                let endpos = begpos + edgelen - 1 in
                let span = (begpos, endpos) in
                combine_edges cky span;
                conjoin_edges cky span;
                unary_deriv cky span
            done
        done

    let rec make_bo_prods cky prods =
        match prods with
            [] -> []
          | prod :: rest ->
            begin
                try
                    let ids = Item.get_ids prod in
                    let flag = ref true in
                    List.iter
                        (fun id ->
                            let item = Chart.get_item cky.chart id in
                            let syncat = Item.get_syncat item in
                            if not (Syncat.is_bo_cat syncat) then
                                flag := false
                            else ())
                        ids;
                    if not !flag then 
                        prod :: make_bo_prods cky rest
                    else make_bo_prods cky rest
                with _ -> make_bo_prods cky rest
            end

    let fillup_items cky span item1 item2 =
        let syncat1 = Item.get_syncat item1
        and syncat2 = Item.get_syncat item2
        and word1 = List.nth cky.sent (Item.get_headpos_of_item item1) 
        and word2 = List.nth cky.sent (Item.get_headpos_of_item item2) in
        List.iter
            (fun (rescat, depdir') ->
                let depdir =
                    if Dict.is_allowed_univdep cky.dict word1 word2 
                        && Dict.is_allowed_univdep cky.dict word2 word1 then depdir'
                    else if Dict.is_allowed_univdep cky.dict word1 word2 then Syncat.Left
                    else if Dict.is_allowed_univdep cky.dict word2 word1 then Syncat.Right
                    else depdir'
                in
                let headitem =
                    match depdir with
                        Syncat.Left | Syncat.RightSerial -> item1
                      | Syncat.Right | Syncat.LeftSerial -> item2
                      | Syncat.Unary -> failwith "fillup_items"
                      | Syncat.Coor -> item1 in
                let headprods = Item.get_prods headitem in
                List.iter 
                    (fun prod ->
                        let headpos = Item.get_headpos prod in
                        let newprod = Item.create_prod_ids 
                            [Item.get_id item1; Item.get_id item2] headpos depdir in
                        Chart.add_deriv cky.chart span rescat headpos newprod) 
                    headprods)
            (CG.funcapp syncat1 syncat2)

    let fillup_edges cky span =
        let (begpos, endpos) = span in
        if not (Chart.mem cky.chart span) then
            if begpos = endpos then
                let word = List.nth cky.sent begpos in
                let prod = Item.create_prod_lex word begpos in
                Chart.add_deriv cky.chart span (Syncat.bo_cat Syncat.NoDep) begpos prod
            else
                for immpos = begpos to endpos - 1 do
                    let span1 = (begpos, immpos)
                    and span2 = (immpos + 1, endpos) in
                    let items1 = Chart.get_items cky.chart span1
                    and items2 = Chart.get_items cky.chart span2 in
                    cartiter_noerr
                        (fun item1 item2 -> fillup_items cky span item1 item2)
                        items1 items2
                done
        else if begpos = endpos then begin
            let items = Chart.get_items cky.chart span in
            List.iter
                (fun item ->
                    let syncat = Item.get_syncat item in
                    let word = List.nth cky.sent begpos in
                    let prod = Item.create_prod_lex word begpos in
                    if Syncat.is_leftpunc_cat syncat then
                        Chart.add_deriv cky.chart span (Syncat.rightpunc_cat Syncat.NoDep) begpos prod
                    else if Syncat.is_rightpunc_cat syncat then
                        Chart.add_deriv cky.chart span (Syncat.leftpunc_cat Syncat.NoDep) begpos prod
                    else ())
                items
        end

    let hypothesize_fillup_edges cky span flag =
        List.iter
            (fun item ->
                let syncat = Item.get_syncat item in
                let prods = Item.get_prods item in
                let deptag = Syncat.get_deptag syncat in
                List.iter
                    (fun prod ->
                        let headpos = Item.get_headpos prod in
                        flag := true;
                        Chart.add_deriv cky.chart span 
                            (Syncat.bo_cat deptag) headpos prod)
                    prods)
            (Chart.get_items cky.chart span)

    let rec is_to_traverse_down history span =
        match history with
            [] -> true
          | span' :: rest ->
                let (b, e) = span in
                let (b', e') = span' in
                if b' <= b && e <= e' then false
                else is_to_traverse_down rest span

    let rec is_to_merge_up history span =
        match history with
            [] -> false
          | span' :: rest ->
                let (b, e) = span in
                let (b', e') = span' in
                if b < b' && e >= e' then true
                else if b <= b' && e > e' then true
                else is_to_merge_up rest span

    let fillup_chart cky =
        for begpos = 0 to cky.sentlen - 1 do
            let endpos = begpos in
            let span = (begpos, endpos) in
            fillup_edges cky span
        done;
        let history = ref [] in
        let is_hypot = ref false in
        for edgelen = cky.sentlen - 1 downto 2 do
            for begpos = 0 to cky.sentlen - edgelen - 1 do
                let endpos = begpos + edgelen - 1 in
                let span = (begpos, endpos) in
                if Chart.mem cky.chart span && is_to_traverse_down !history span then begin
                    (* printf "hypothesize at (%d, %d)\n" begpos endpos; *)
                    hypothesize_fillup_edges cky span is_hypot;
                    history := span :: !history
                end else ()
            done
        done;
        if not !is_hypot then
            let edgelen = 1 in
            for begpos = 0 to cky.sentlen - edgelen - 1 do
                let endpos = begpos + edgelen - 1 in
                let span = (begpos, endpos) in
                if Chart.mem cky.chart span && is_to_traverse_down !history span then begin
                    (* printf "hypothesize at (%d, %d)\n" begpos endpos; *)
                    hypothesize_fillup_edges cky span is_hypot;
                    history := span :: !history
                end else ()
            done
        else ();
        (* printf "%s\n" (Chart.to_string cky.chart); *)
        for edgelen = 2 to cky.sentlen do
            for begpos = 0 to cky.sentlen - edgelen do
                let endpos = begpos + edgelen - 1 in
                let span = (begpos, endpos) in
                if not (Chart.mem cky.chart span) && is_to_merge_up !history span then begin
                    (* printf "fillup at (%d, %d)\n" begpos endpos; *)
                    fillup_edges cky span
                end else ()
            done
        done (* ;
        printf "%s\n" (Chart.to_string cky.chart) *)

    let clean_fillup_edges cky span =
        let items = Chart.get_items cky.chart span in
        List.iter
            (fun item ->
                let newprods = make_bo_prods cky (Item.get_prods item) in
                if List.length newprods <> 0 then
                    Item.set_prods item newprods
                else ())
            items

    let clean_fillup cky =
        for edgelen = 1 to cky.sentlen do
            for begpos = 0 to cky.sentlen - edgelen do
                let endpos = begpos + edgelen - 1 in
                let span = (begpos, endpos) in
                clean_fillup_edges cky span
            done
        done

    let parse ?(transf=[]) sent words dict start =
        let cky = create sent words dict start in
        change_transf cky transf;
        let enum = ref false in
        init_chart cky;
        build_chart cky;
        if not (Chart.is_success cky.chart) then begin
            Chart.set_filledup cky.chart;
            fillup_chart cky;
            enum := true
        end else ();
        Chart.elim_excess cky.chart cky.start;
        if not !enum then
            Chart.set_enumlevel cky.chart (Dict.get_enumlevel dict)
        else
            Chart.set_enumlevel cky.chart Dict.EnumOnAll;
        Chart.count_derivs cky.chart;
        cky.chart

end

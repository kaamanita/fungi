open Printf

open Avm
open Cg
open Dep
open Chart
open Dict
open Utils

module Tree = struct

    type syncat_t = CG.syncat_t
    type depdir_t = CG.depdir_t
    type span_t = Chart.span_t
    type word_t = Dict.word_t
    type depstruct_t = DepStruct.t

    type t =
        Term of word_t * span_t
      | Nonterm of syncat_t * word_t * t list * span_t * int * depdir_t

    let create_term word span =
        Term (word, span)

    let create_nonterm syncat word dtrs span headpos depdir =
        Nonterm (syncat, word, dtrs, span, headpos, depdir)

    let is_term tree =
        match tree with
            Term _ -> true
          | Nonterm _ -> false

    let is_nonterm tree =
        match tree with
            Term _ -> false
          | Nonterm _ -> true

    let get_word tree =
        match tree with
            Term (word, _) -> word
          | Nonterm (_, word, _, _, _, _) -> word

    let get_span tree =
        match tree with
            Term (_, span) -> span
          | Nonterm (_, _, _, span, _, _) -> span

    let get_syncat tree =
        match tree with
            Term _ -> failwith "get_syncat"
          | Nonterm (syncat, _, _, _, _, _) -> syncat

    let get_dtrs tree =
        match tree with
            Term _ -> failwith "get_dtrs"
          | Nonterm (_, _, dtrs, _, _, _) -> dtrs

    let get_headpos tree =
        match tree with
            Term (_, (headpos, _)) -> headpos
          | Nonterm (_, _, _, _, headpos, _) -> headpos

    let get_depdir tree =
        match tree with
            Term _ -> failwith "get_depdir"
          | Nonterm (_, _, _, _, _, depdir) -> depdir

    let rec to_string tree =
        match tree with
            Term (word, _) -> word
          | Nonterm (_, word, dtrs, _, _, _) ->
                sprintf "(%s %s)" word 
                    (String.concat " " (List.map to_string dtrs))

    let score_branching tree lbrconst rbrconst =
        match tree with
            Term _ -> 0.0
          | Nonterm (_, _, dtrs, _, _, _) ->
                let n = List.length dtrs in
                if n > 1 then
                    let left = List.nth dtrs 0 
                    and right = List.nth dtrs (n - 1) in
                    let (x_l, y_l) = get_span left
                    and (x_r, y_r) = get_span right in
                    let pref_left = (float_of_int (y_l - x_l + 1)) *. Stdlib.log lbrconst
                    and pref_right = (float_of_int (y_r - x_r + 1)) *. Stdlib.log rbrconst in
                    pref_left +. pref_right
                else 0.0

    let rec print_spaces indent =
        let result = ref "" in
        for i = 1 to indent do
            result := " " ^ !result;
        done;
        !result

    let rec pp ?(indent=0) tree =
        match tree with
            Term (word, _) -> word
          | Nonterm (_, word, dtrs, _, _, depdir) ->
                let newindent = indent + (String.length word) + 4 in
                let leading = sprintf "\n%s" (print_spaces newindent) in
                sprintf "(%s %s %s)" word
                    (Syncat.depdir_to_string (get_depdir tree))
                    (String.concat leading
                        (List.map 
                            (fun dtr -> pp dtr ~indent:newindent) dtrs))

    let rec pp_syncat ?(indent=0) tree =
        match tree with
            Term (word, _) -> word
          | Nonterm (syncat, word, dtrs, _, _, depdir) ->
                let newindent = indent + (String.length (Syncat.to_string syncat)) + (String.length word) + 5 in
                let leading = sprintf "\n%s" (print_spaces newindent) in
                sprintf "(%s:%s %s %s)" 
                    (Syncat.to_string syncat) word
                    (Syncat.depdir_to_string (get_depdir tree))
                    (String.concat leading
                        (List.map 
                            (fun dtr -> pp_syncat dtr ~indent:newindent) dtrs))

    let get_rootpos tree =
        match tree with
            Term (_, (begpos, _)) -> begpos
          | Nonterm (_, _, _, _, headpos, _) -> headpos

    let rec tree_to_sent tree =
        match tree with
            Term (word, _) -> [word]
          | Nonterm (_, _, dtrs, _, _, _) ->
                List.concat (List.map tree_to_sent dtrs)

    let rec tree_to_catseq tree =
        match tree with
            Term _ -> []
          | Nonterm (syncat, _, dtrs, _, _, _) -> begin
                match dtrs with
                    [Term (word, _)] -> [syncat]
                  | _ -> List.concat (List.map tree_to_catseq dtrs)
            end

    let rec tree_to_wcatseq tree =
        match tree with
            Term _ -> []
          | Nonterm (syncat, _, dtrs, _, _, _) -> begin
                match dtrs with
                    [Term (word, _)] -> [syncat]
                  | _ -> List.concat (List.map tree_to_wcatseq dtrs)
            end

    let rec tree_to_depstr tree depstr =
        match tree with
            Term (_, _) -> ()
          | Nonterm (syncat, _, dtrs, _, headpos, depdir) ->
            begin
                if List.length dtrs = 3 then begin
                    let (left, mid, right) = (List.nth dtrs 0, List.nth dtrs 1, List.nth dtrs 2) in
                    if is_nonterm mid then begin
                        let cat = get_syncat mid in
                        if Syncat.equal_ign_attrs cat Syncat.conn_cat then begin
                            DepStruct.add depstr (get_rootpos left) (get_rootpos mid) Syncat.Left;
                            DepStruct.add depstr (get_rootpos mid) (get_rootpos right) Syncat.Left
                        end else if Syncat.equal_ign_attrs cat Syncat.conn_cat_depbank then begin
                            DepStruct.add depstr (get_rootpos mid) (get_rootpos left) Syncat.Right;
                            DepStruct.add depstr (get_rootpos mid) (get_rootpos right) Syncat.Left
                        end else if Syncat.equal_ign_attrs cat Syncat.conn_cat_truedep then begin
                            DepStruct.add depstr (get_rootpos left) (get_rootpos right) Syncat.Left;
                            DepStruct.add depstr (get_rootpos right) (get_rootpos mid) Syncat.Right
                        end else if Syncat.equal_ign_attrs cat Syncat.conn_cat_rev then begin
                            DepStruct.add depstr (get_rootpos mid) (get_rootpos left) Syncat.Right;
                            DepStruct.add depstr (get_rootpos right) (get_rootpos mid) Syncat.Left
                        end else begin
                            DepStruct.add depstr (get_rootpos left) (get_rootpos mid) Syncat.Left;
                            DepStruct.add depstr (get_rootpos mid) (get_rootpos right) Syncat.Left
                        end
                    end else
                        List.iter
                            (fun dtr -> 
                                let dtrpos = get_rootpos dtr in
                                let depdir =
                                    if headpos < dtrpos then Syncat.Left
                                    else Syncat.Right
                                in
                                DepStruct.add depstr headpos dtrpos depdir)
                            dtrs
                end else if List.length dtrs = 2 then begin
                    let lpos = get_rootpos (List.nth dtrs 0)
                    and rpos = get_rootpos (List.nth dtrs 1) in
                    match depdir with
                        Syncat.Left | Syncat.RightSerial ->
                            DepStruct.add depstr lpos rpos depdir
                      | Syncat.Right | Syncat.LeftSerial -> 
                            DepStruct.add depstr rpos lpos depdir
                      | Syncat.Coor | Syncat.Unary -> 
                            failwith "tree_to_depstr: unexpected depdir (either Coor or Unary)"
                end else if List.length dtrs = 1 then
                    let hpos = get_rootpos (List.nth dtrs 0) in
                    DepStruct.set_syncat depstr hpos syncat
                else failwith "tree_to_depstr: unexpected dtrs length (neither 1, 2, or 3)";
                List.iter
                    (fun dtr -> tree_to_depstr dtr depstr)
                    dtrs
            end

    let to_depstr tree =
        let sent = tree_to_sent tree in
        let rootpos = get_rootpos tree in
        let depstr = DepStruct.create (List.length sent) sent rootpos in
        tree_to_depstr tree depstr;
        depstr

    let rec to_ubrackets' tree : span_t list =
        match tree with
            Term (_, span) -> 
                [span]
          | Nonterm (_, _, dtrs, span, _, _) ->
                union [span] (List.flatten (List.map to_ubrackets' dtrs))

    let rec to_ubrackets tree : span_t list =
        List.filter
            (fun (lidx, ridx) -> lidx != ridx)
            (to_ubrackets' tree)

    let rec to_brackets tree : (span_t * word_t) list =
        match tree with
            Term (head, span) ->
                [(span, head)]
          | Nonterm (_, head, dtrs, span, _, _) ->
                union [(span, head)] (List.flatten (List.map to_brackets dtrs))

    let rec to_catwordlist tree : (syncat_t * word_t) list =
        match tree with
            Term _ -> []
          | Nonterm (syncat, headword, dtrs, span, headpos, depdir) -> begin
                match depdir with
                    Syncat.Left | Syncat.Right | Syncat.Coor | Syncat.LeftSerial | Syncat.RightSerial ->
                        List.concat (List.map to_catwordlist dtrs)
                  | Syncat.Unary -> [(syncat, headword)]
            end

    let rec conv_to_chart chart tree =
        match tree with
            Term _ -> 0
          | Nonterm (syncat, headword, dtrs, span, headpos, depdir) -> begin
                match dtrs with
                    [] -> failwith "conv_to_chart"
                  | [dtr] -> begin
                        match dtr with
                            Term (_, _) ->
                                let prod = Item.create_prod_lex headword headpos in
                                let item = Chart.add_deriv_ret_item 
                                    chart span syncat headpos prod in
                                Item.get_id item
                          | Nonterm _ -> failwith "conv_to_chart"
                    end
                  | _ -> begin
                        let dtrids = List.map (conv_to_chart chart) dtrs in
                        let prod = Item.create_prod_ids dtrids headpos depdir in
                        let item = Chart.add_deriv_ret_item
                            chart span syncat headpos prod in
                        Item.get_id item
                    end
            end

    let to_chart tree words : Chart.t =
        let sent = tree_to_sent tree in
        (* let avms = List.map (fun _ -> [Avm.create []]) sent in *)
        let chart = Chart.create sent words in
        ignore (conv_to_chart chart tree);
        chart

    let to_conll tree words =
        let sent = tree_to_sent tree in
        (*
        let wcatseq = tree_to_wcatseq tree in
        let catseq = tree_to_catseq tree in
        *)
        let rootpos = get_rootpos tree in
        let depstr = DepStruct.create (List.length sent) sent rootpos in
        tree_to_depstr tree depstr;
        let lines = ref [] in
        for i = 0 to List.length sent - 1 do
            let id = i + 1 in
            let form = List.nth words i (* List.nth sent i *) in
            let lemma = List.nth words i (* List.nth sent i *) in
            let cpostag = List.nth sent i (* Syncat.to_string (List.nth wcatseq i) *) in
            let postag = List.nth sent i (* Syncat.to_string (List.nth catseq i) *) in
            let ftrs = "_" in
            let head =
                try (DepStruct.find_head depstr i) + 1
                with Not_found -> 0 in
            let deprel = 
                if head = 0 then "ROOT" 
                else (* "HEAD" *)
                    try
                        match (DepStruct.get_depdir depstr (head - 1) i) with
                            Syncat.Left | Syncat.Right -> "HEAD"
                          | Syncat.LeftSerial | Syncat.RightSerial -> "SERIAL"
                          | Syncat.Coor -> "CONJ"
                          | Syncat.Unary -> "UNARY"
                    with Not_found -> "HEAD"
            in
            let phead = "_" in
            let pdeprel = DepStruct.get_syncat depstr i in
            let line = Printf.sprintf
                "%d\t%s\t%s\t%s\t%s\t%s\t%d\t%s\t%s\t%s"
                id form lemma cpostag postag ftrs head deprel phead pdeprel
            in
            lines := !lines @ [line]
        done;
        String.concat "\n" !lines

    let rec head (tree : t) : word_t * span_t =
        match tree with
            Term (w, sp) -> (w, sp)
          | Nonterm (_, _, dtrs, _, _, dir) ->
            begin
                match dir with
                    Syncat.Left | Syncat.LeftSerial ->
                        head (List.nth dtrs 0)
                  | Syncat.Right | Syncat.RightSerial ->
                        head (List.nth dtrs (List.length dtrs - 1))
                  | Syncat.Coor -> head (List.nth dtrs 0)
                  | Syncat.Unary -> head (List.nth dtrs 0)
            end

    let fillup_height ?(highlight : bool = false) (m, len, h', lines) height =
        if h' < height then
            let result = ref lines in
            let vbar =
                if highlight then "[34m[1m|[0m"
                else "|"
            in
            let top =
                Printf.sprintf "%s%s%s"
                    (String.make (m - 1) ' ') vbar (String.make (len - m) ' ')
            in
            for i = 1 to height - h' do
                result := top :: !result
            done;
            (m, len, height, !result)
        else (m, len, h', lines)

    let center_text (txt : string) (len : int) : string =
        if Utils.unicode_length txt < len then begin
            let area = len - Utils.unicode_length txt in
            let leftspc = area / 2 in
            let rightspc = area - leftspc in
            (String.make leftspc ' ') ^ txt ^ (String.make rightspc ' ')
        end else txt

    let set_highlight (txt : string) (highlight : bool) : string =
        if highlight then
            "[34m[1m[4m" ^ txt ^ "[0m"
        else txt

    let rec pp_depstruct' 
        ?(highlight : bool = false) ?(showcat : bool = false) ?(headcat : syncat_t = Syncat.create_atom "word") (tree : t) (words : string list) 
    : int * int * int * string list =
        match tree with
            Term (tag, (x, _)) ->
            begin
                let infoline1 = Syncat.to_string headcat in
                let word = List.nth words x in
                let infoline2 = word in
                let infoline3 = tag in
                let len = 
                    if showcat then
                        max 
                            (max (String.length infoline1) (Utils.unicode_length infoline2)) 
                            (String.length infoline3)
                    else
                        max (Utils.unicode_length infoline2) (String.length infoline3)
                in
                let midpos = (len + 1) / 2 in
                let cinfo1 = set_highlight (center_text infoline1 len) highlight in
                let cinfo2 = set_highlight (center_text infoline2 len) highlight in
                let cinfo3 = set_highlight (center_text infoline3 len) highlight in
                if showcat then
                    (midpos, len, 0, [cinfo1; cinfo2; cinfo3])
                else
                    (midpos, len, 0, [cinfo2; cinfo3])
            end
          | Nonterm (syncat, word, dtrs, _, headpos, depdir) ->
            begin
                let infos' =
                    List.map
                        (fun tree' ->
                            if headpos = get_headpos tree' then
                                (true, pp_depstruct' tree' words ~highlight:highlight ~showcat:showcat ~headcat:syncat)
                            else
                                (false, pp_depstruct' tree' words ~highlight:false ~showcat:showcat ~headcat:syncat)
                        )
                        dtrs
                in
                let h_max =
                    List.fold_left 
                        (fun r (_, (_, _, h, _)) -> if h > r then h else r) 
                        0 infos'
                in
                let infos =
                    List.map
                        (fun (is_head, (m, len, h, lines)) ->
                            if is_head then 
                                fillup_height (m, len, h, lines) h_max ~highlight:(highlight && is_head)
                            else
                                fillup_height (m, len, h, lines) h_max ~highlight:false
                        )
                        infos'
                in
                let dep = set_highlight (
                    match depdir with
                        Syncat.Left -> ">"
                      | Syncat.Right -> "<"
                      | Syncat.Coor -> "&"
                      | Syncat.Unary -> "!"
                      | Syncat.LeftSerial -> "L"
                      | Syncat.RightSerial -> "R"
                ) highlight in
                let vbar = "|"
                and hvbar = set_highlight "|" highlight in
                let (star, dot, amp) =
                    if highlight then (
                        set_highlight "*" highlight, ".", set_highlight "&" highlight
                    ) else ("*", ".", "&")
                in
                match depdir with
                    Syncat.Left | Syncat.Right | Syncat.LeftSerial | Syncat.RightSerial -> begin
                        let (m1, len1, h1, lines1) = List.nth infos 0
                        and (m2, len2, h2, lines2) = List.nth infos 1 in
                        let dashcnt = len1 + m2 - m1 + 1 in
                        let dashcnt1 =
                            if dashcnt mod 2 = 0 then dashcnt / 2 - 1
                            else dashcnt / 2
                        and dashcnt2 = dashcnt / 2 in
                        let vbar1 =
                            if highlight && (depdir = Syncat.Left || depdir = Syncat.RightSerial) then hvbar
                            else vbar
                        and vbar2 =
                            if highlight && (depdir = Syncat.Right || depdir = Syncat.LeftSerial) then hvbar
                            else vbar
                        in
                        let top1 =
                            Printf.sprintf "%s%s%s  %s%s%s"
                                (String.make (m1 - 1) ' ') vbar1 (String.make (len1 - m1) ' ')
                                (String.make (m2 - 1) ' ') vbar2 (String.make (len2 - m2) ' ')
                        in
                        let top2 =
                            if depdir = Syncat.Left || depdir = Syncat.RightSerial then
                                Printf.sprintf "%s%s%s%s%s%s%s"
                                    (String.make (m1 - 1) ' ') star (String.make (dashcnt1) '-')
                                    dep (String.make (dashcnt2) '-') dot (String.make (len2 - m2) ' ')
                            else
                                Printf.sprintf "%s%s%s%s%s%s%s"
                                    (String.make (m1 - 1) ' ') dot (String.make (dashcnt1) '-')
                                    dep (String.make (dashcnt2) '-') star (String.make (len2 - m2) ' ')
                        in
                        let result = top2 :: top1 :: (
                            List.map2
                                (fun line1 line2 ->
                                    line1 ^ "  " ^ line2
                                )
                                lines1 lines2
                        ) in
                        let newm = 
                            if depdir = Syncat.Left || depdir = Syncat.RightSerial then m1 
                            else len1 + 2 + m2 
                        in
                        let newlen = len1 + len2 + 2 in
                        let newh = 2 + max h1 h2 in
                        (newm, newlen, newh, result)
                    end
                  | Syncat.Coor -> begin
                        let (m1, len1, h1, lines1) = List.nth infos 0
                        and (m2, len2, h2, lines2) = List.nth infos 1
                        and (m3, len3, h3, lines3) = List.nth infos 2 in
                        let (ishd1, _) = List.nth infos' 0
                        and (ishd2, _) = List.nth infos' 1
                        and (ishd3, _) = List.nth infos' 2 in
                        let vbar1 = if ishd1 && highlight then hvbar else vbar
                        and vbar2 = if ishd2 && highlight then hvbar else vbar
                        and vbar3 = if ishd3 && highlight then hvbar else vbar in
                        let sign1 = if ishd1 then amp else dot
                        and sign2 = if ishd2 then amp else dot
                        and sign3 = if ishd3 then amp else dot in
                        let top1 =
                            Printf.sprintf "%s%s%s  %s%s%s  %s%s%s"
                                (String.make (m1 - 1) ' ') vbar1 (String.make (len1 - m1) ' ')
                                (String.make (m2 - 1) ' ') vbar2 (String.make (len2 - m2) ' ')
                                (String.make (m3 - 1) ' ') vbar3 (String.make (len3 - m3) ' ')
                        in
                        let top2 =
                            Printf.sprintf "%s%s%s--%s%s%s--%s%s%s"
                                (String.make (m1 - 1) ' ') sign1 (String.make (len1 - m1) '-')
                                (String.make (m2 - 1) '-') sign2 (String.make (len2 - m2) '-')
                                (String.make (m3 - 1) '-') sign3 (String.make (len3 - m3) ' ')
                        in
                        let result = top2 :: top1 :: (
                            List.map2
                                (fun line1 line2 -> line1 ^ "  " ^ line2)
                                (List.map2
                                    (fun line1 line2 -> line1 ^ "  " ^ line2) 
                                    lines1 lines2) 
                                lines3
                        ) in
                        let newm =
                            if ishd1 then m1
                            else if ishd2 then len1 + 2 + m2
                            else len1 + 2 + len2 + 2 + m3
                        in
                        let newlen = len1 + len2 + len3 + 4 in
                        let newh = 2 + max (max h1 h2) h3 in
                        (newm, newlen, newh, result)
                    end
                  | Syncat.Unary -> List.nth infos 0
            end

    let pp_depstruct ?(highlight : bool = false) ?(showcat : bool = false) ?(comment : bool = false) (tree : t) (words : string list) : string =
        let (m, len, height, lines) = pp_depstruct' tree words ~highlight:highlight ~showcat:showcat ~headcat:(Syncat.create_atom "word") in
        let lines = 
            if comment then List.map (fun x -> "# " ^ x) lines 
            else lines
        in
        String.concat "\n" lines

end

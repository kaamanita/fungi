module ParserRunner :
  sig
    type syncat_t = Cg.Syncat.t
    type t
    val create : string -> string -> string list -> syncat_t -> t
    val tryparse_elt :
      Cky.CKY.dict_t ->
      Cky.CKY.word_t list -> Cky.CKY.word_t list -> 
      Dict.Dict.enumlevel_t -> syncat_t -> Cky.CKY.chart_t * bool
    val tryparse :
      Cky.CKY.dict_t ->
      Cky.CKY.word_t list -> Cky.CKY.word_t list ->
      Dict.Dict.enumlevel_t list -> syncat_t ->
      Cky.CKY.chart_t option * Dict.Dict.enumlevel_t * bool ->
      Cky.CKY.chart_t option * Dict.Dict.enumlevel_t * bool
    val list_enumlevels : Dict.Dict.enumlevel_t -> Dict.Dict.enumlevel_t list
    val parse :
      Cky.CKY.dict_t ->
      Chartdb.ChartDb.t ->
      Cky.CKY.word_t list -> Cky.CKY.word_t list -> 
      syncat_t -> bool * Dict.Dict.enumlevel_t * Chart.Chart.t
    val parse_file : Cky.CKY.dict_t -> Chartdb.ChartDb.t -> syncat_t -> string -> unit
    val run : t -> unit
    val compute : string -> string -> string list -> syncat_t -> unit
  end

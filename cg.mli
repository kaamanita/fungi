(** Categorial Grammar. *)

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

(** Syntactic Category. *)
module Syncat :
sig

    (** Attribute value matrix. *)
    type avm_t = Avm.Avm.t
    
    (** Dependency direction. This direction is drawn from the dependent 
        to the head of the phrase. *)
    type depdir_t = 
        Left
      | Unary
      | Coor
      | Right
      | LeftSerial
      | RightSerial
    
    (** Valence dependency tag. It corresponds to the valence tag of the
        the split-head Dependency Model with Valence. *)
    type deptag_t = NoDep | LeftDep | RightDep 
                  | LeftToRightDep | RightToLeftDep

    (** Serialization tag. *)
    type sertag_t = NoSer | LeftSer | RightSer

    (** Syntactic category. *)
    type t

    (** [conn_cat] creates a connective category [&]. This corresponds to 
        the conjunction category of Categorial Grammar. However, the 
        dependency structure for the tag sequence A & A is (A < (& < A)), 
        where < points to the head of a phrase. *)
    val conn_cat : t

    (** [conn_cat_depbank] creates a connective category [&&]. This also 
        corresponds to the conjunction category of Categorial Grammar. 
        However, the dependency structure for the tag sequence A && A is 
        (A > (& < A)), where < and > point to the head of a phrase. *)
    val conn_cat_depbank : t
    
    (** [conn_cat_truedep] creates a connective category [&&&]. This also 
        corresponds to the conjunction category of Categorial Grammar. 
        However, the dependency structure for the tag sequence A && A is 
        (A < (& > A)), where < and > point to the head of a phrase. *)
    val conn_cat_truedep : t
    
    (** [conn_cat_rev] creates a connective category [&&&&]. This also 
        corresponds to the conjunction category of Categorial Grammar. 
        However, the dependency structure for the tag sequence A && A is 
        (A > (& > A)), where < and > point to the head of a phrase. *)
    val conn_cat_rev : t

    (** [bo_cat d] creates a backoff category having a valence dependency 
        tag [d], which always has the label [*]. This type of categories
        is used for the linear interpolation. *)
    val bo_cat : deptag_t -> t

    (** [leftpunc_cat d] creates a left punctuation category having a
        valence dependency tag [d], which always has the label [<*]. 
        This category combines with a consecutive syntactic category 
        on the right hand side, yielding such syntactic category. *)
    val leftpunc_cat : deptag_t -> t

    (** [leftpunc_cat d] creates a right punctuation category having a
        valence dependency tag [d], which always has the label [*>]. 
        This category combines with a consecutive syntactic category 
        on the left hand side, yielding such syntactic category. *)
    val rightpunc_cat : deptag_t -> t

    (** [is_leftpunc_cat c] checks if the category [c] is a left 
        punctuation category. *)
    val is_leftpunc_cat : t -> bool

    (** [is_rightpunc_cat c] checks if a category [c] is a right
        punctuation category. *)
    val is_rightpunc_cat : t -> bool

    (** [is_rightpunc_cat c] checks if a category [c] is a left or
        right punctuation category. *)
    val is_punc_cat : t -> bool
    
    (** [is_bo_cat c] checks if a category [c] is a backoff category. *)
    val is_bo_cat : t -> bool

    (** [root_cat] creates a root symbol, having the label [ROOT]. *)
    val root_cat : t

    (** [create_atom l ~deptag:d ~isconj:j ~ftrs:a] creates an atomic
        category having the label [l], a valence dependency tag [d], 
        an attribute value matrix [a]. If [is_conj] is set to [true],
        the category is a result of coordination. *)
    val create_atom : 
        ?is_conj:bool -> ?deptag:deptag_t -> ?ftrs:avm_t -> string -> t
    
    (** [create_bwd l r ~deptag:d ~is_conj:j ~ftrs:a] creates a
        complex category [l \ r] having a valence dependency tag [d],
        and an attribute value matrix [a]. If [is_conj] is set to [true],
        the category is a result of coordination. *)
    val create_bwd : 
        ?is_conj:bool -> ?deptag:deptag_t -> ?ftrs:avm_t 
        -> t -> t -> depdir_t -> t
    
    (** [create_bwd l r ~deptag:d ~is_conj:j ~ftrs:a] creates a
        complex category [l / r] having a valence dependency tag [d],
        and an attribute value matrix [a]. If [is_conj] is set to [true],
        the category is a result of coordination. *)
    val create_fwd : 
        ?is_conj:bool -> ?deptag:deptag_t -> ?ftrs:avm_t 
        -> t -> t -> depdir_t -> t
    
    val copy : ?ftrs:avm_t -> t -> t

    (** [is_conj c] checks if a category [c] is a result of 
        coordination. *)
    val is_conj : t -> bool

    val is_serial : t -> bool

    (** [get_deptag c] returns the valence dependency tag of a 
        syntactic category [c]. *)
    val get_deptag : t -> deptag_t

    (** [get_serial c] returns the serialization tag of a
        syntactic category [c]. *)
    val get_serial : t -> sertag_t

    (** [get_ftrs c] returns the attribute value matrix of a
        syntactic category [c]. *)
    val get_ftrs : t -> avm_t

    (** [is_atom c] checks if [c] is an atomic category. *)
    val is_atom : t -> bool

    (** [is_fwd c] checks if [c] is a forward slash category. *)
    val is_fwd : t -> bool

    (** [is_bwd c] checks if [c] is a backward slash category. *)
    val is_bwd : t -> bool

    (** [get_value c] returns the label of an atomic category [c]. If
        [c] is not atomic, a failure exception will be thrown. *)
    val get_value : t -> string
    
    (** [get_consq c] returns the [l] part of the complex categories 
        [l \ r] and [l / r]. If [c] is not complex, a failure exception
        will be thrown. *)
    val get_consq : t -> t
    
    (** [get_consq c] returns the [r] part of the complex categories 
        [l \ r] and [l / r]. If [c] is not complex, a failure exception
        will be thrown. *)
    val get_prems : t -> t
    
    (** [get_depdir c] returns the dependency direction of the slash
        in a complex category [c]. If [c] is not complex, a failure
        exception will be thrown. *)
    val get_depdir : t -> depdir_t
    
    (** [depdir_to_string d] converts a dependency direction [d] into
        a string represention. *)
    val depdir_to_string : depdir_t -> string

    (** [deptag_to_string d] converts a valence dependency tag [d] into
        a string represention. *)
    val deptag_to_string : deptag_t -> string
    
    (** [to_string c] converts a syntactic category [c] into a string
        representation. *)
    val to_string : t -> string

    (** [equal c1 c2] checks if two syntactic categories [c1] and [c2]
        are equal. *)
    val equal : t -> t -> bool
    
    (** [equal c1 c2] checks if two syntactic categories [c1] and [c2]
        are equal, regardless their flags, dependency tags, and 
        attribute value matrices. *)
    val equal_ign_attrs : t -> t -> bool
    
    (** [hash c] calculates the hash value for a syntactic category [c]. *)
    val hash : t -> int
    
    (** [compare c1 c2] compares two syntactic categories [c1] and [c2]
        and produces +1, 0, and -1 for [c1 > c2], [c1 = c2], and 
        [c1 < c2], respectively. *)
    val compare : t -> t -> int

    val slashno : t -> float

    (** [score p c ~depth:d] computes the penalty score of a syntactic
        category [c] with the penalty ratio [p] and the initial depth [d].
        The initial depth is not necessary to be specified as it is set
        to the default value 0. For a category of [n] slashes, the
        penalty score is [p ** n]. *)
    val score : ?depth:float -> float -> t -> float

    (** [penalize_category p c] computes the penalty score of a
        syntactic category [c] with the penalty ratio [10 ** p]. *)
    val penalize_category : float -> t -> float
    
    (** [cat_prior p c ~depth:d] computes the prior hyperparameter for
        a syntactic category [c] with a penalty ratio [p] and an initial 
        depth [d]. *)
    val cat_prior : ?depth:float -> float -> t -> float
end

(** Categorial Grammar. *)
module CG :
sig

    (** Syntactic category. *)
    type syncat_t = Syncat.t

    (** Dependency direction. *)
    type depdir_t = Syncat.depdir_t

    (** Transformation rules of syntactic categories. *)
    type transf_t = (syncat_t * syncat_t list) list
   
    (** This exception is thrown when category combination fails. *)
    exception Combination_failure
 
    (** [enable_split_head ()] turns on the split-head mode. In this
        mode, each word consequently generates dependents on the right
        and then ones on the left. *)
    val enable_split_head : unit -> unit

    (** [disable_split_head ()] turns off the split-head mode. *)
    val disable_split_head : unit -> unit

    (** [transform rs c] transforms a syntactic category [c] into all
        possible syntactic categories with respect to a list of 
        transformation rules [rs]. *)
    val transform : transf_t -> syncat_t -> syncat_t list
    
    (** [funcapp c1 c2] applies functional application to two syntactic
        categories [c1] and [c2]. *)
    val funcapp : 
        Syncat.t -> Syncat.t -> (Syncat.t * Syncat.depdir_t) list
    
    (** [conjoin c1 c2 c3] applies coordination to three syntactic
        categories [c1], [c2], and [c3]. [c2] must only be a connective
        category. *)
    val conjoin : Syncat.t -> Syncat.t -> Syncat.t -> Syncat.t
    
    (** [conjoin_opt c1 c2] applies coordination to two syntactic
        categories [c1] and [c2], given that [c1] and [c2] are left
        and right conjuncts of a connective category. The dependency
        direction is (c1 < (& < c2)). *)
    val conjoin_opt : Syncat.t -> Syncat.t -> Syncat.t

    (** [conjoin_opt_depbank c1 c2] applies coordination to two syntactic
        categories [c1] and [c2], given that [c1] and [c2] are left
        and right conjuncts of a connective category. The dependency
        direction is (c1 > (& < c2)). *)
    val conjoin_opt_depbank : Syncat.t -> Syncat.t -> Syncat.t

    val left_serialize : Syncat.t -> Syncat.t -> Syncat.t

    val right_serialize : Syncat.t -> Syncat.t -> Syncat.t
end

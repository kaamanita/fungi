(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

module ConllDep :
sig
    type entry_t
    type entries_t = entry_t list * string list * int list * int
    type content_t = entries_t list
    val add_prohtag : string -> unit
    val add_mwutag : string -> unit
    val add_tagmap : string -> string -> string -> unit
    val compile : string -> content_t
    val compile_many : string list -> content_t
    val elim_longsents : bool -> int option -> content_t -> content_t
    val entries_to_conll : entries_t -> string
    val entries_to_tags : entries_t -> string
    val entries_to_sents : entries_t -> string
    val read_postags_deprels : content_t -> string list * string list * string list * string list
    val conv_to_depstrs : content_t -> Dep.DepStruct.t list
    val from_file : string -> Dep.DepStruct.t list
    val from_files : string list -> Dep.DepStruct.t list
end

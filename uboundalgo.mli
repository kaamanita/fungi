module Viterbi :
sig
    type syncat_t = Cg.Syncat.t
    type depstruct_t = Dep.DepStruct.t
    type chart_t = Chart.Chart.t
    type tree_t = Tree.Tree.t
    type t
    val create : depstruct_t -> chart_t -> t
    val compute_viterbi : t -> unit
    val find_max : t -> tree_t * int
    val compute : depstruct_t -> chart_t -> tree_t * int
end

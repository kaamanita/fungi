(** Probability Tables and Generative Model *)

(** Instance Type Signature *)
module type INSTANCE_TYPE =
sig 
    type t 
    val equal : t -> t -> bool 
    val hash : t -> int 
    val to_string : t -> string
end

(** Conditional Instance Type Signature *)
module type COND_INSTANCE_TYPE =
sig
    include INSTANCE_TYPE

    (** Premise *)
    type prems_t

    (** Consequence *)
    type consq_t

    (** Create an instance from the premise and the consequence. *)
    val create : prems_t -> consq_t -> t

    (** Get the premise. *)
    val get_prems : t -> prems_t

    (** Get the consequence. *)
    val get_consq : t -> consq_t

    (** Check if two premises equal. *)
    val equal_prems : prems_t -> prems_t -> bool

    (** Calculate the hash value of the premise. *)
    val hash_prems : prems_t -> int

    val prems_to_string : prems_t -> string
end

(** Backoff Instance Type Signature *)
module type BACKOFF_INSTANCE_TYPE = sig
    include COND_INSTANCE_TYPE
    type data_t
    val backoff : data_t -> prems_t * consq_t
end

(** Probability Table *)
module ProbTbl :
functor (Inst : INSTANCE_TYPE) ->
sig
    (** Instance *)
    type inst_t = Inst.t

    (** Probability table *)
    type t

    (** Create a probability table with the size and the default value. *)
    val create : int -> float -> t

    (** Get the default value. *)
    val get_epsilon : t -> float

    (** Check if an instance is a member of the table. *)
    val mem : t -> inst_t -> bool

    val length : t -> int

    (** Get the probability of the instance. *)
    val get_prob : t -> inst_t -> float

    (** Get the smoothened probability of the instance. *)
    val get_smooth_prob : t -> inst_t -> float

    (** Set the probability of the instance. *)
    val set_prob : t -> inst_t -> float -> unit

    (** Accumulate the probability of the instance. *)
    val acc_prob : t -> inst_t -> float -> unit

    (** Iterate all entries with the function. *)
    val iter : t -> (inst_t -> float -> unit) -> unit

    val to_string : t -> string
end

(** Conditional Probability Table *)
module CondProbTbl :
functor (Inst : COND_INSTANCE_TYPE) ->
sig
    (** Premise *)
    type prems_t = Inst.prems_t

    (** Consequence *)
    type consq_t = Inst.consq_t

    (** Instance *)
    type inst_t = Inst.t

    (** Conditional probability table *)
    type t

    (** Create a conditional probability table with the size and 
        the default value. *)
    val create : int -> float -> t

    (** Get the default value. *)
    val get_epsilon : t -> float

    (** Check if the instance is a member of the table. *)
    val mem : t -> inst_t -> bool

    val length : t -> int

    (** Get the probability of the instance. *)
    val get_prob : t -> inst_t -> float
    
    (** Get the smoothened probability of the instance. *)
    val get_smooth_prob : t -> inst_t -> float

    (** Set the probability of the instance. *)
    val set_prob : t -> inst_t -> float -> unit

    (** Accumulate the probability of the instance. *)
    val acc_prob : t -> inst_t -> float -> unit

    (** Iterate all entries with the function. *)
    val iter : t -> (inst_t -> float -> unit) -> unit
    
    (** Iterate all premises with the function. *)
    val iter_prems : t -> (prems_t -> float -> unit) -> unit

    val to_string : t -> string
end

(** Generative Model *)
module GenModel :
functor (Inst : COND_INSTANCE_TYPE) ->
sig
    (** Instance *)
    type inst_t = Inst.t

    (** Function for accumulating the probability of an instance *)
    type fn_acc_prob_t = inst_t -> float -> unit
    
    (** Function for calculating the probability (optionally 
        smoothened) of an instance *)
    type fn_get_prob_t = inst_t -> float
    
    (** Function for setting the probability of an instance *)
    type fn_set_prob_t = inst_t -> float -> unit
   
    (** Feature probability table *)
    type probftr_t
   
    (** Create a feature probability table from the 
        four functions: get_prob, get_smooth_prob, set_prob, 
        and acc_prob. *)
    val create_probftr :
        fn_get_prob_t -> fn_get_prob_t -> fn_set_prob_t -> fn_acc_prob_t 
        -> probftr_t
   
    type probtbl_t = ProbTbl(Inst).t

    (** Generative model *)
    type t
    
    (** Create a generative model with the size, the default value, 
        and the list of generative probability tables. *)
    val create : int -> float -> probftr_t list -> t

    val get_epsilon : t -> float

    val length : t -> int

    val mem : t -> inst_t -> bool

    val get_ftrprob : t -> inst_t -> float

    val get_smooth_ftrprob : t -> inst_t -> float

    val get_prob_noftr : t -> inst_t -> float
    
    (** Get the probability of the instance. *)
    val get_prob : t -> inst_t -> float

    val get_smooth_prob_noftr : t -> inst_t -> float
    
    (** Get the smoothened probability of the instance. *)
    val get_smooth_prob : t -> inst_t -> float
    
    (** Set the probability of the instance. *)
    val set_prob : t -> inst_t -> float -> unit
    
    (** Accumulate the probability of the instance. *)
    val acc_prob : t -> inst_t -> float -> unit

    (** Iterate all entries with the function. *)
    val iter : t -> (inst_t -> float -> unit) -> unit

    (** Read a generative model from the file. *)
    val from_file : string -> probftr_t list -> t
    
    (** Write the generative model from a file. *)
    val to_file : t -> string -> unit

    val to_string : t -> string

    val print : t -> unit

    val dkl : t -> t -> float 

end

(** Generative Model with Backoff *)
module GenModelWithBackoff :
functor (Inst : COND_INSTANCE_TYPE) ->
functor (BackoffInst : BACKOFF_INSTANCE_TYPE with type data_t = Inst.t and type prems_t = Inst.t) ->
sig
    (** Instance *)
    type inst_t = Inst.t

    (** Function for accumulating the probability of an instance *)
    type fn_acc_prob_t = inst_t -> float -> unit
    
    (** Function for calculating the probability (optionally 
        smoothened) of an instance *)
    type fn_get_prob_t = inst_t -> float
    
    (** Function for setting the probability of an instance *)
    type fn_set_prob_t = inst_t -> float -> unit
   
    (** Create a feature probability table from the 
        four functions: get_prob, get_smooth_prob, set_prob, 
        and acc_prob. *)
    val create_probftr :
        GenModel(Inst).fn_get_prob_t -> 
        GenModel(Inst).fn_get_prob_t -> 
        GenModel(Inst).fn_set_prob_t -> 
        GenModel(Inst).fn_acc_prob_t -> 
        GenModel(Inst).probftr_t
   
    type probftr_t = GenModel(Inst).probftr_t
    
    type probtbl_t = ProbTbl(Inst).t

    (** Generative model *)
    type t
    
    (** Create a generative model with the size, the default value, 
        and the list of generative probability tables. *)
    val create : int -> float -> probftr_t list -> t

    val get_epsilon : t -> float

    val length : t -> int

    val get_prob_noftr : t -> inst_t -> float
    
    (** Get the probability of the instance. *)
    val get_prob : t -> inst_t -> float

    val get_smooth_prob_noftr : t -> inst_t -> float
    
    (** Get the smoothened probability of the instance. *)
    val get_smooth_prob : t -> inst_t -> float
    
    (** Set the probability of the instance. *)
    val set_prob : t -> inst_t -> float -> unit
    
    (** Accumulate the probability of the instance. *)
    val acc_prob : t -> inst_t -> float -> unit

    (** Iterate all entries with the function. *)
    val iter : t -> (inst_t -> float -> unit) -> unit

    (** Read a generative model from the file. *)
    val from_file : string -> probftr_t list -> t
    
    (** Write the generative model from a file. *)
    val to_file : t -> string -> unit

    val to_string : t -> string

    val print : t -> unit
end

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Cg
open Dict
open Macro
open Catgen
open Depgrms
open Ruletypes
open Utils
open Printf

module DictLoader = struct

    type macroexp_t = MacroExpander.t
    type catgen_t = Catgen.t
    type dmv_t = Dmv.t
    type evg_t = Evg.t
    type ghg_t = Ghg.t
    type dict_t = Dict.t

    type t = {
        dict                : dict_t;
        macroexp            : macroexp_t;
        catgen              : catgen_t;
        dmv                 : dmv_t;
        evg                 : evg_t;
        ghg                 : ghg_t;
        is_test_catgen      : bool;
        mutable is_typology : bool;
    }

    let create n is_test_catgen dict = 
        let macroexp = MacroExpander.create n in
        MacroExpander.add_cats macroexp "[conj_ho]" [Syncat.conn_cat];
        MacroExpander.add_cats macroexp "[conj_db]" [Syncat.conn_cat_depbank];
        Dict.add_accept dict (Syncat.bo_cat Syncat.NoDep);
        {
            dict = dict;
            macroexp = macroexp;
            catgen = Catgen.create macroexp dict;
            dmv = Dmv.create macroexp dict;
            evg = Evg.create macroexp dict;
            ghg = Ghg.create macroexp dict;
            is_test_catgen = is_test_catgen;
            is_typology = false;
        }

    let to_string dload =
        MacroExpander.to_string dload.macroexp

    let interpret_depscheme dload value =
        if Utils.get_seedlvl () > 0 then
            match value with
                "head-outward" -> Catgen.change_depscheme dload.catgen Catgen.HeadOutwardScheme
              | "depbank" -> Catgen.change_depscheme dload.catgen Catgen.DepbankScheme
              | _ -> failwith (sprintf "interpret_depscheme: unknown value '%s'" value)
        else ()

    let interpret_wordorder dload value =
        if Utils.get_seedlvl () > 1 then
            match value with
                "fixed" -> Catgen.set_freewordorder dload.catgen false
              | "free" -> Catgen.set_freewordorder dload.catgen true
              | _ -> failwith (sprintf "wordorder: unknown value '%s'" value)
        else ()

    let interpret_heads dload flag =
        match flag with
            "+adj" -> Catgen.set_head_adj dload.catgen true
          | "-adj" -> Catgen.set_head_adj dload.catgen false
          | "+nmod" -> Catgen.set_head_nmod dload.catgen true
          | "-nmod" -> Catgen.set_head_nmod dload.catgen false
          | "+verb" -> Catgen.set_head_verb dload.catgen true
          | "-verb" -> Catgen.set_head_verb dload.catgen false
          | "+copula" -> Catgen.set_head_copula dload.catgen true
          | "-copula" -> Catgen.set_head_copula dload.catgen false
          | "+vmod" -> Catgen.set_head_vmod dload.catgen true
          | "-vmod" -> Catgen.set_head_vmod dload.catgen false
          | "+gmod" -> Catgen.set_head_gmod dload.catgen true
          | "-gmod" -> Catgen.set_head_gmod dload.catgen false
          | "+smod" -> Catgen.set_head_smod dload.catgen true
          | "-smod" -> Catgen.set_head_smod dload.catgen false
          | "+adv" -> Catgen.set_head_adv dload.catgen true
          | "-adv" -> Catgen.set_head_adv dload.catgen false
          | "+neg" -> Catgen.set_head_neg dload.catgen true
          | "-neg" -> Catgen.set_head_neg dload.catgen false
          | "+modal" -> Catgen.set_head_modal dload.catgen true
          | "-modal" -> Catgen.set_head_modal dload.catgen false
          | "+adpos" -> Catgen.set_head_adpos dload.catgen true
          | "-adpos" -> Catgen.set_head_adpos dload.catgen false
          | "+relpro" -> Catgen.set_head_relpro dload.catgen true
          | "-relpro" -> Catgen.set_head_relpro dload.catgen false
          | "+part" -> Catgen.set_head_part dload.catgen true
          | "-part" -> Catgen.set_head_part dload.catgen false
          | "+subconj_smod" -> Catgen.set_head_subconj_smod dload.catgen true
          | "-subconj_smod" -> Catgen.set_head_subconj_smod dload.catgen false
          | "+subconj_phr" -> Catgen.set_head_subconj_phr dload.catgen true
          | "-subconj_phr" -> Catgen.set_head_subconj_phr dload.catgen false
          | "+poss_nmod" -> Catgen.set_head_poss_nmod dload.catgen true
          | "-poss_nmod" -> Catgen.set_head_poss_nmod dload.catgen false
          | "+poss_phr" -> Catgen.set_head_poss_phr dload.catgen true
          | "-poss_phr" -> Catgen.set_head_poss_phr dload.catgen false
          | "+inf" -> Catgen.set_head_inf dload.catgen true
          | "-inf" -> Catgen.set_head_inf dload.catgen false
          | "+npnom" -> Catgen.set_head_npnom dload.catgen true
          | "-npnom" -> Catgen.set_head_npnom dload.catgen false
          | "+vpnom" -> Catgen.set_head_vpnom dload.catgen true
          | "-vpnom" -> Catgen.set_head_vpnom dload.catgen false
          | "+cl" -> Catgen.set_head_cl dload.catgen true
          | "-cl" -> Catgen.set_head_cl dload.catgen false
          | _ -> failwith (sprintf "heads: unknown value '%s'" flag)

    let interpret_inflection dload value =
        if Utils.get_seedlvl () > 2 then
            match value with
                "no" -> begin
                    Catgen.set_infl dload.catgen false;
                    Utils.set_infl false
                end
              | "yes" -> begin
                    Catgen.set_infl dload.catgen true;
                    Utils.set_infl true
                end
              | _ -> failwith (sprintf "inflection: unknown value '%s'" value)
        else ()

    let interpret_verb_decls dload value =
        if Utils.get_seedlvl () > 3 then
            match value with
                "subj" -> Catgen.add_verb_decl dload.catgen Catgen.VDecl_by_subj
              | "obj" -> Catgen.add_verb_decl dload.catgen Catgen.VDecl_by_obj
              | "iobj" -> Catgen.add_verb_decl dload.catgen Catgen.VDecl_by_iobj
              | _ -> failwith (sprintf "interpret_verb_decls: unknown value '%s'" value)
        else ()

    let interpret_noun_verb_agrs dload value =
        if Utils.get_seedlvl () > 4 then
            match value with
                "gender" -> Catgen.add_noun_verb_agr dload.catgen Catgen.NVAgr_by_gender
              | "number" -> Catgen.add_noun_verb_agr dload.catgen Catgen.NVAgr_by_number
              | "person" -> Catgen.add_noun_verb_agr dload.catgen Catgen.NVAgr_by_person
              | _ -> failwith (sprintf "interpret_noun_verb_agrs: unknown value '%s'" value)
        else ()

    let interpret_adj_noun_agrs dload value =
        if Utils.get_seedlvl () > 5 then
            match value with
                "gender" -> Catgen.add_adj_noun_agr dload.catgen Catgen.ANAgr_by_gender
              | "number" -> Catgen.add_adj_noun_agr dload.catgen Catgen.ANAgr_by_number
              | "case" -> Catgen.add_adj_noun_agr dload.catgen Catgen.ANAgr_by_case
              | _ -> failwith (sprintf "interpret_adj_noun_agrs: unknown value '%s'" value)
        else ()

    let interpret_word_order dload value =
        if Utils.get_seedlvl () > 6 then
            match value with
                "SVOI" -> Catgen.add_word_order dload.catgen Catgen.SVOI
              | "SVIO" -> Catgen.add_word_order dload.catgen Catgen.SVIO
              | "SIVO" -> Catgen.add_word_order dload.catgen Catgen.SIVO
              | "ISVO" -> Catgen.add_word_order dload.catgen Catgen.ISVO
              | "SOVI" -> Catgen.add_word_order dload.catgen Catgen.SOVI
              | "SOIV" -> Catgen.add_word_order dload.catgen Catgen.SOIV
              | "SIOV" -> Catgen.add_word_order dload.catgen Catgen.SIOV
              | "ISOV" -> Catgen.add_word_order dload.catgen Catgen.ISOV
              | "VSOI" -> Catgen.add_word_order dload.catgen Catgen.VSOI
              | "VSIO" -> Catgen.add_word_order dload.catgen Catgen.VSIO
              | "VISO" -> Catgen.add_word_order dload.catgen Catgen.VISO
              | "IVSO" -> Catgen.add_word_order dload.catgen Catgen.IVSO
              | "OVSI" -> Catgen.add_word_order dload.catgen Catgen.OVSI
              | "OVIS" -> Catgen.add_word_order dload.catgen Catgen.OVIS
              | "OIVS" -> Catgen.add_word_order dload.catgen Catgen.OIVS
              | "IOVS" -> Catgen.add_word_order dload.catgen Catgen.IOVS
              | "OSVI" -> Catgen.add_word_order dload.catgen Catgen.OSVI
              | "OSIV" -> Catgen.add_word_order dload.catgen Catgen.OSIV
              | "OISV" -> Catgen.add_word_order dload.catgen Catgen.OISV
              | "IOSV" -> Catgen.add_word_order dload.catgen Catgen.IOSV
              | "VOSI" -> Catgen.add_word_order dload.catgen Catgen.VOSI
              | "VOIS" -> Catgen.add_word_order dload.catgen Catgen.VOIS
              | "VIOS" -> Catgen.add_word_order dload.catgen Catgen.VIOS
              | "IVOS" -> Catgen.add_word_order dload.catgen Catgen.IVOS
              | _ -> failwith (sprintf "interpret_word_order: unknown value '%s'" value)
        else ()

    let interpret_vicomp_order dload value =
        if Utils.get_seedlvl () > 7 then
            match value with
                "SVC" -> Catgen.add_vicomp_order dload.catgen Catgen.SVC
              | "SCV" -> Catgen.add_vicomp_order dload.catgen Catgen.SCV
              | "CSV" -> Catgen.add_vicomp_order dload.catgen Catgen.CSV
              | "VSC" -> Catgen.add_vicomp_order dload.catgen Catgen.VSC
              | "VCS" -> Catgen.add_vicomp_order dload.catgen Catgen.VCS
              | "CVS" -> Catgen.add_vicomp_order dload.catgen Catgen.CVS
              | _ -> failwith (sprintf "interpret_vicomp_order: unknown value '%s'" value)
        else ()

    let interpret_vtcomp_order dload value =
        if Utils.get_seedlvl () > 8 then
            match value with
                "SVOC" -> Catgen.add_vtcomp_order dload.catgen Catgen.SVOC
              | "SVCO" -> Catgen.add_vtcomp_order dload.catgen Catgen.SVCO
              | "SCVO" -> Catgen.add_vtcomp_order dload.catgen Catgen.SCVO
              | "CSVO" -> Catgen.add_vtcomp_order dload.catgen Catgen.CSVO
              | "SOVC" -> Catgen.add_vtcomp_order dload.catgen Catgen.SOVC
              | "SOCV" -> Catgen.add_vtcomp_order dload.catgen Catgen.SOCV
              | "SCOV" -> Catgen.add_vtcomp_order dload.catgen Catgen.SCOV
              | "CSOV" -> Catgen.add_vtcomp_order dload.catgen Catgen.CSOV
              | "VSOC" -> Catgen.add_vtcomp_order dload.catgen Catgen.VSOC
              | "VSCO" -> Catgen.add_vtcomp_order dload.catgen Catgen.VSCO
              | "VCSO" -> Catgen.add_vtcomp_order dload.catgen Catgen.VCSO
              | "CVSO" -> Catgen.add_vtcomp_order dload.catgen Catgen.CVSO
              | "OVSC" -> Catgen.add_vtcomp_order dload.catgen Catgen.OVSC
              | "OVCS" -> Catgen.add_vtcomp_order dload.catgen Catgen.OVCS
              | "OCVS" -> Catgen.add_vtcomp_order dload.catgen Catgen.OCVS
              | "COVS" -> Catgen.add_vtcomp_order dload.catgen Catgen.COVS
              | "OSVC" -> Catgen.add_vtcomp_order dload.catgen Catgen.OSVC
              | "OSCV" -> Catgen.add_vtcomp_order dload.catgen Catgen.OSCV
              | "OCSV" -> Catgen.add_vtcomp_order dload.catgen Catgen.OCSV
              | "COSV" -> Catgen.add_vtcomp_order dload.catgen Catgen.COSV
              | "VOSC" -> Catgen.add_vtcomp_order dload.catgen Catgen.VOSC
              | "VOCS" -> Catgen.add_vtcomp_order dload.catgen Catgen.VOCS
              | "VCOS" -> Catgen.add_vtcomp_order dload.catgen Catgen.VCOS
              | "CVOS" -> Catgen.add_vtcomp_order dload.catgen Catgen.CVOS
              | _ -> failwith (sprintf "interpret_vtcomp_order: unknown value '%s'" value)
        else ()

    let interpret_adj_order dload value =
        if Utils.get_seedlvl () > 9 then
            match value with
                "noun+adj" -> Catgen.add_adj_order dload.catgen Catgen.NounAdj
              | "adj+noun" -> Catgen.add_adj_order dload.catgen Catgen.AdjNoun
              | _ -> failwith (sprintf "interpret_adj_order: unknown value '%s'" value)
        else ()

    let interpret_adv_order dload value =
        if Utils.get_seedlvl () > 10 then
            match value with
                "vp+adv" -> Catgen.add_adv_order dload.catgen Catgen.VpAdv
              | "adv+vp" -> Catgen.add_adv_order dload.catgen Catgen.AdvVp
              | _ -> failwith (sprintf "interpret_adv_order: unknown value '%s'" value)
        else ()

    let interpret_advadj_order dload value =
        if Utils.get_seedlvl () > 11 then
            match value with
                "adv+adj" -> Catgen.add_advadj_order dload.catgen Catgen.AdvAdj
              | "adj+adv" -> Catgen.add_advadj_order dload.catgen Catgen.AdjAdv
              | _ -> failwith (sprintf "interpret_advadj_order: unknown value '%s'" value)
        else ()

    let interpret_modal_order dload value =
        if Utils.get_seedlvl () > 12 then
            match value with
                "modal+vp" -> Catgen.add_modal_order dload.catgen Catgen.ModalVp
              | "vp+modal" -> Catgen.add_modal_order dload.catgen Catgen.VpModal
              | _ -> failwith (sprintf "interpret_modal_order: unknown value '%s'" value)
        else ()

    let interpret_modal_infl dload value =
        if Utils.get_seedlvl () > 12 then
            match value with
                "inflected" -> Catgen.add_modal_infl dload.catgen Catgen.Modal_infl
              | "req_infv" -> Catgen.add_modal_infl dload.catgen Catgen.Modal_req_infv
              | _ -> failwith (sprintf "interpret_modal_infl: unknown value '%s'" value)
        else ()

    let interpret_adposition dload value =
        if Utils.get_seedlvl () > 13 then
            match value with
                "prep+np" -> Catgen.add_adposition dload.catgen Catgen.Preposition
              | "np+post" -> Catgen.add_adposition dload.catgen Catgen.Postposition
              | _ -> failwith (sprintf "interpret_adposition: unknown value '%s'" value)
        else ()

    let interpret_nmod_order dload value =
        if Utils.get_seedlvl () > 14 then
            match value with
                "np+nmod" -> Catgen.add_nmod_order dload.catgen Catgen.NpNmod
              | "nmod+np" -> Catgen.add_nmod_order dload.catgen Catgen.NmodNp
              | _ -> failwith (sprintf "interpret_nmod_order: unknown value '%s'" value)
        else ()

    let interpret_vmod_order dload value =
        if Utils.get_seedlvl () > 15 then
            match value with
                "vp+vmod" -> Catgen.add_vmod_order dload.catgen Catgen.VpVmod
              | "vmod+vp" -> Catgen.add_vmod_order dload.catgen Catgen.VmodVp
              | _ -> failwith (sprintf "interpret_vmod_order: unknown value '%s'" value)
        else ()

    let interpret_poss_order dload value =
        if Utils.get_seedlvl () > 16 then
            match value with
                "owner+poss+ownee" -> Catgen.add_poss_order dload.catgen Catgen.OwnerPossOwnee
              | "ownee+poss+owner" -> Catgen.add_poss_order dload.catgen Catgen.OwneePossOwner
              | "owner+ownee+poss" -> Catgen.add_poss_order dload.catgen Catgen.OwnerOwneePoss
              | "ownee+owner+poss" -> Catgen.add_poss_order dload.catgen Catgen.OwneeOwnerPoss
              | "poss+owner+ownee" -> Catgen.add_poss_order dload.catgen Catgen.PossOwnerOwnee
              | "poss+ownee+owner" -> Catgen.add_poss_order dload.catgen Catgen.PossOwneeOwner
              | _ -> failwith (sprintf "interpret_poss_order: unknown value '%s'" value)
        else ()

    let interpret_relpro_order dload value =
        if Utils.get_seedlvl () > 17 then
            match value with
                "relpro+comp" -> Catgen.add_relpro_order dload.catgen Catgen.RelproComp
              | "comp+relpro" -> Catgen.add_relpro_order dload.catgen Catgen.CompRelpro
              | _ -> failwith (sprintf "interpret_relpro_order: unknown value '%s'" value)
        else ()

    let interpret_relpro_infl dload value =
        if Utils.get_seedlvl () > 18 then
            match value with
                "gender" -> Catgen.add_relpro_infl dload.catgen Catgen.ANAgr_by_gender
              | "number" -> Catgen.add_relpro_infl dload.catgen Catgen.ANAgr_by_number
              | "case" -> Catgen.add_relpro_infl dload.catgen Catgen.ANAgr_by_case
              | _ -> failwith (sprintf "interpret_relpro_infl: unknown value '%s'" value)
        else ()

    let interpret_relcls_order dload value =
        if Utils.get_seedlvl () > 19 then
            match value with
                "np+relcls" -> Catgen.add_relcls_order dload.catgen Catgen.NpRelcls
              | "relcls+np" -> Catgen.add_relcls_order dload.catgen Catgen.RelclsNp
              | _ -> failwith (sprintf "interpret_relcls_order: unknown value '%s'" value)
        else ()

    let interpret_subcls_order dload value =
        if Utils.get_seedlvl () > 20 then
            match value with
                "main+conj+subcls" -> Catgen.add_subcls_order dload.catgen Catgen.MainConjSubcls
              | "subcls+conj+main" -> Catgen.add_subcls_order dload.catgen Catgen.SubclsConjMain
              | "main+subcls+conj" -> Catgen.add_subcls_order dload.catgen Catgen.MainSubclsConj
              | "subcls+main+conj" -> Catgen.add_subcls_order dload.catgen Catgen.SubclsMainConj
              | "conj+main+subcls" -> Catgen.add_subcls_order dload.catgen Catgen.ConjMainSubcls
              | "conj+subcls+main" -> Catgen.add_subcls_order dload.catgen Catgen.ConjSubclsMain
              | _ -> failwith (sprintf "interpret_subcls_order: unknown value '%s'" value)
        else ()

    let interpret_npgerund_order dload value =
        if Utils.get_seedlvl () > 21 then
            match value with
                "np+gerund" -> Catgen.add_npgerund_order dload.catgen Catgen.NpGerund
              | "gerund+np" -> Catgen.add_npgerund_order dload.catgen Catgen.GerundNp
              | _ -> failwith (sprintf "interpret_npgerund_order: unknown value '%s'" value)
        else ()

    let interpret_npgerund_infl dload value =
        if Utils.get_seedlvl () > 22 then
            match value with
                "gender" -> Catgen.add_npgerund_infl dload.catgen Catgen.ANAgr_by_gender
              | "number" -> Catgen.add_npgerund_infl dload.catgen Catgen.ANAgr_by_number
              | "case" -> Catgen.add_npgerund_infl dload.catgen Catgen.ANAgr_by_case
              | _ -> failwith (sprintf "interpret_npgerund_infl: unknown value '%s'" value)
        else ()

    let interpret_vpgerund_order dload value =
        if Utils.get_seedlvl () > 23 then
            match value with
                "vp+gerund" -> Catgen.add_vpgerund_order dload.catgen Catgen.VpGerund
              | "gerund+vp" -> Catgen.add_vpgerund_order dload.catgen Catgen.GerundVp
              | _ -> failwith (sprintf "interpret_vpgerund_order: unknown value '%s'" value)
        else ()

    let interpret_infvp_order dload value =
        if Utils.get_seedlvl () > 24 then
            match value with
                "inf+vp" -> Catgen.add_infvp_order dload.catgen Catgen.InfVp
              | "vp+inf" -> Catgen.add_infvp_order dload.catgen Catgen.VpInf
              | _ -> failwith (sprintf "interpret_infvp_order: unknown value '%s'" value)
        else ()

    let interpret_npnom_order dload value =
        if Utils.get_seedlvl () > 25 then
            match value with
                "nom+np" -> Catgen.add_npnom_order dload.catgen Catgen.NomNp
              | "np+nom" -> Catgen.add_npnom_order dload.catgen Catgen.NpNom
              | _ -> failwith (sprintf "interpret_npnom_order: unknown value '%s'" value)
        else ()

    let interpret_vpnom_order dload value =
        if Utils.get_seedlvl () > 26 then
            match value with
                "nom+vp" -> Catgen.add_vpnom_order dload.catgen Catgen.NomVp
              | "vp+nom" -> Catgen.add_vpnom_order dload.catgen Catgen.VpNom
              | _ -> failwith (sprintf "interpret_vpnom_order: unknown value '%s'" value)
        else ()

    let interpret_smod_order dload value =
        if Utils.get_seedlvl () > 27 then
            match value with
                "smod+sent" -> Catgen.add_smod_order dload.catgen Catgen.SmodSent
              | "sent+smod" -> Catgen.add_smod_order dload.catgen Catgen.SentSmod
              | _ -> failwith (sprintf "interpret_smod_order: unknown value '%s'" value)
        else ()

    let interpret_part_order dload value =
        if Utils.get_seedlvl () > 28 then
            match value with
                "part+vp" -> Catgen.add_part_order dload.catgen Catgen.PartVp
              | "vp+part" -> Catgen.add_part_order dload.catgen Catgen.VpPart
              | _ -> failwith (sprintf "interpret_part_order: unknown value '%s'" value)
        else ()

    let interpret_nump_order dload value =
        if Utils.get_seedlvl () > 29 then
            match value with
                "num+cl" -> Catgen.add_nump_order dload.catgen Catgen.NumCl
              | "cl+num" -> Catgen.add_nump_order dload.catgen Catgen.ClNum
              | _ -> failwith (sprintf "interpret_nump_order: unknown value '%s'" value)
        else ()

    let interpret_nump_position dload value =
        if Utils.get_seedlvl () > 30 then
            match value with
                "adj" -> Catgen.add_nump_position dload.catgen Catgen.NumpAsAdj
              | "adv" -> Catgen.add_nump_position dload.catgen Catgen.NumpAsAdv
              | "nmod" -> Catgen.add_nump_position dload.catgen Catgen.NumpAsNmod
              | "vmod" -> Catgen.add_nump_position dload.catgen Catgen.NumpAsVmod
              | _ -> failwith (sprintf "interpret_nump_position: unknown value '%s'" value)
        else ()

    let interpret_nump_infl dload value =
        if Utils.get_seedlvl () > 30 then
            match value with
                "gender" -> Catgen.add_nump_infl dload.catgen Catgen.ANAgr_by_gender
              | "number" -> Catgen.add_nump_infl dload.catgen Catgen.ANAgr_by_number
              | "case" -> Catgen.add_nump_infl dload.catgen Catgen.ANAgr_by_case
              | _ -> failwith (sprintf "interpret_nump_infl: unknown value '%s'" value)
        else ()

    let interpret_bool_value values flagname =
        let value = List.nth values (List.length values - 1) in
        match value with
            "true" -> true
          | "false" -> false
          | _ -> failwith (sprintf "interpret_%s_value: unknown value '%s'" flagname value)

    let interpret_copula dload values =
        if Utils.get_seedlvl () > 31 then
            if (interpret_bool_value values "copula") then
                Catgen.enable_copula dload.catgen
            else
                Catgen.disable_copula dload.catgen
        else ()

    let interpret_gerund_as_np dload values =
        if Utils.get_seedlvl () > 32 then
            if (interpret_bool_value values "gerund") then
                Catgen.enable_gerund_as_np dload.catgen
            else
                Catgen.disable_gerund_as_np dload.catgen
        else ()

    let interpret_gerund_as_nmod dload values =
        if Utils.get_seedlvl () > 33 then
            if (interpret_bool_value values "gerund_as_nmod") then
                Catgen.enable_gerund_as_nmod dload.catgen
            else
                Catgen.disable_gerund_as_nmod dload.catgen
        else ()

    let interpret_gerund_as_vmod dload values =
        if Utils.get_seedlvl () > 34 then
            if (interpret_bool_value values "gerund_as_vmod") then
                Catgen.enable_gerund_as_vmod dload.catgen
            else
                Catgen.disable_gerund_as_vmod dload.catgen
        else ()

    let interpret_shift dload values =
        if Utils.get_seedlvl () > 35 then
            if (interpret_bool_value values "shift") then
                Catgen.enable_shift dload.catgen
            else
                Catgen.disable_shift dload.catgen
        else ()

    let interpret_neg dload value =
        if Utils.get_seedlvl () > 36 then
            match value with
                "neg+v" -> Catgen.add_neg_order dload.catgen Catgen.NegV
              | "v+neg" -> Catgen.add_neg_order dload.catgen Catgen.VNeg
              | "neg+adj" -> Catgen.add_neg_order dload.catgen Catgen.NegAdj
              | "adj+neg" -> Catgen.add_neg_order dload.catgen Catgen.AdjNeg
              | "neg+adv" -> Catgen.add_neg_order dload.catgen Catgen.NegAdv
              | "adv+neg" -> Catgen.add_neg_order dload.catgen Catgen.AdvNeg
              | "neg+nmod" -> Catgen.add_neg_order dload.catgen Catgen.NegNmod
              | "nmod+neg" -> Catgen.add_neg_order dload.catgen Catgen.NmodNeg
              | "neg+vmod" -> Catgen.add_neg_order dload.catgen Catgen.NegVmod
              | "vmod+neg" -> Catgen.add_neg_order dload.catgen Catgen.VmodNeg
              | _ -> failwith (sprintf "interpret_neg: unknown value '%s'" value)
        else ()

    let interpret_drop dload value =
        if Utils.get_seedlvl () > 37 then
            match value with
                "subj" -> Catgen.add_drop dload.catgen Catgen.SubjDrop
              | "obj" -> Catgen.add_drop dload.catgen Catgen.ObjDrop
              | "iobj" -> Catgen.add_drop dload.catgen Catgen.IobjDrop
              | "none" -> Catgen.disable_drops dload.catgen
              | _ -> failwith (sprintf "interpret_drop: unknown value '%s'" value)
        else ()

    let interpret_typology dload flag values =
        match flag with
            "depscheme" -> List.iter (interpret_depscheme dload) values (* > 0 *)
          | "wordorder" -> List.iter (interpret_wordorder dload) values (* > 1 *)
          | "inflection" -> List.iter (interpret_inflection dload) values (* > 2 *)
          | "verbdecl" -> List.iter (interpret_verb_decls dload) values (* > 3 *)
          | "noun_verb_agr" -> List.iter (interpret_noun_verb_agrs dload) values (* > 4 *)
          | "adj_noun_agr" -> List.iter (interpret_adj_noun_agrs dload) values (* > 5 *)
          | "genders" -> List.iter (Catgen.add_gender dload.catgen) values
          | "persons" -> List.iter (Catgen.add_person dload.catgen) values
          | "numbers" -> List.iter (Catgen.add_number dload.catgen) values
          | "argcases" -> List.iter (Catgen.add_case_arg dload.catgen) values
          | "appos_argcases" -> List.iter (Catgen.add_case_apposarg dload.catgen) values
          | "adjcases" -> List.iter (Catgen.add_case_adj dload.catgen) values
          | "sentence" -> List.iter (interpret_word_order dload) values (* > 6 *)
          | "vicomp" -> List.iter (interpret_vicomp_order dload) values (* > 7 *)
          | "vtcomp" -> List.iter (interpret_vtcomp_order dload) values (* > 8 *)
          | "adj" -> List.iter (interpret_adj_order dload) values (* > 9 *)
          | "adv" -> List.iter (interpret_adv_order dload) values (* > 10 *)
          | "advadj" -> List.iter (interpret_advadj_order dload) values (* > 11 *)
          | "modal" -> List.iter (interpret_modal_order dload) values (* > 12 *)
          | "modal_infl" -> List.iter (interpret_modal_infl dload) values (* > 12 *)
          | "adposition" -> List.iter (interpret_adposition dload) values (* > 13 *)
          | "nmod" -> List.iter (interpret_nmod_order dload) values (* > 14 *)
          | "vmod" -> List.iter (interpret_vmod_order dload) values (* > 15 *)
          | "poss" -> List.iter (interpret_poss_order dload) values (* > 16 *)
          | "relpro" -> List.iter (interpret_relpro_order dload) values (* > 17 *)
          | "relpro_infl" -> List.iter (interpret_relpro_infl dload) values (* > 18 *)
          | "relcls" -> List.iter (interpret_relcls_order dload) values (* > 19 *)
          | "subcls" -> List.iter (interpret_subcls_order dload) values (* > 20 *)
          | "npgerund" -> List.iter (interpret_npgerund_order dload) values (* > 21 *)
          | "npgerund_infl" -> List.iter (interpret_npgerund_infl dload) values (* > 22 *)
          | "vpgerund" -> List.iter (interpret_vpgerund_order dload) values (* > 23 *)
          | "infvp" -> List.iter (interpret_infvp_order dload) values (* > 24 *)
          | "npnom" -> List.iter (interpret_npnom_order dload) values (* > 25 *)
          | "vpnom" -> List.iter (interpret_vpnom_order dload) values (* > 26 *)
          | "smod" -> List.iter (interpret_smod_order dload) values (* > 27 *)
          | "part" -> List.iter (interpret_part_order dload) values (* > 28 *)
          | "nump" -> List.iter (interpret_nump_order dload) values (* > 29 *)
          | "nump_position" -> List.iter (interpret_nump_position dload) values (* > 30 *)
          | "nump_infl" -> List.iter (interpret_nump_infl dload) values (* > 30 *)
          | "copula" -> interpret_copula dload values (* > 31 *)
          | "gerund_as_np" -> interpret_gerund_as_np dload values (* > 32 *)
          | "gerund_as_nmod" -> interpret_gerund_as_nmod dload values (* > 33 *)
          | "gerund_as_vmod" -> interpret_gerund_as_vmod dload values (* > 34 *)
          | "shift" -> interpret_shift dload values (* > 35 *)
          | "neg" -> List.iter (interpret_neg dload) values (* > 36 *)
          | "drop" -> List.iter (interpret_drop dload) values (* > 37 *)
          | "cats_aux" -> List.iter (Dict.add_aux dload.dict) values
          | "cats_v" -> List.iter (Dict.add_v dload.dict) values
          | "cats_n" -> List.iter (Dict.add_n dload.dict) values
          | "cats_adj" -> List.iter (Dict.add_adj dload.dict) values
          | "cats_adv" -> List.iter (Dict.add_adv dload.dict) values
          | "cats_adpos" -> List.iter (Dict.add_prep dload.dict) values
          | "cats_num" -> List.iter (Dict.add_num dload.dict) values
          | "cats_conj" -> List.iter (Dict.add_conj dload.dict) values
          | "heads" -> List.iter (interpret_heads dload) values
          | _ -> failwith (sprintf "interpret_typology: unknown flag '%s'" flag)

    let interpret_ghg_value value =
        match value with
            "left_then_right" -> Ghg.LeftThenRight
          | "right_then_left" -> Ghg.RightThenLeft
          | "left_only" -> Ghg.LeftOnly
          | "right_only" -> Ghg.RightOnly
          | "no_fixed_order" | "none" -> Ghg.NoFixedOrder
          | _ -> failwith (sprintf "interpret_ghg_value: unknown value '%s'" value)

    let interpret_ghg_typology dload flag values =
        match flag with
            "no_args" -> 
                if List.length values > 0 then
                    Ghg.set_noargs dload.ghg (int_of_string (List.nth values 0))
                else
                    Ghg.set_noargs dload.ghg 3
          | "verb" -> 
                List.iter 
                    (fun value ->
                        let v = interpret_ghg_value value in
                        Ghg.set_varg dload.ghg v) 
                    values
          | "causative" -> 
                List.iter 
                    (fun value ->
                        let v = interpret_ghg_value value in
                        Ghg.set_causearg dload.ghg v) 
                    values
          | "adjective" ->
                List.iter 
                    (fun value ->
                        let v = interpret_ghg_value value in
                        Ghg.set_adjarg dload.ghg v) 
                    values
          | "adverb" ->
                List.iter 
                    (fun value ->
                        let v = interpret_ghg_value value in
                        Ghg.set_advarg dload.ghg v) 
                    values
          | "link" ->
                List.iter 
                    (fun value ->
                        let v = interpret_ghg_value value in
                        Ghg.set_linkarg dload.ghg v) 
                    values
          | "transformer" ->
                List.iter 
                    (fun value ->
                        let v = interpret_ghg_value value in
                        Ghg.set_transfarg dload.ghg v) 
                    values
          | "series" ->
                List.iter 
                    (fun value ->
                        let v = interpret_ghg_value value in
                        Ghg.set_seriesarg dload.ghg v) 
                    values
          | "files" ->
                Ghg.read_lex dload.ghg values
          | _ -> failwith (sprintf "interpret_ghg_typology: unknown flag '%s'" flag)

    let interpret dload cmd dict =
        match cmd with
            LexEntry (word, cats) -> begin
                List.iter 
                    (Dict.add_entry dict word) 
                    (List.concat 
                        (List.map 
                            (MacroExpander.expand dload.macroexp) 
                            cats));
                List.iter
                    (fun cat ->
                        if MacroSyncat.is_atom cat then
                            let label = (MacroSyncat.get_label cat) in
                            let label' = String.sub label 1 (String.length label - 2) in
                            Dict.add_catdict dict word label'
                        else ())
                    cats
            end
          | MacroEntry (macro, cats) ->
                MacroExpander.add dload.macroexp macro cats
          | TransfEntry (cat, cats) ->
                let syncats = MacroExpander.expand dload.macroexp cat
                and newsyncats = List.flatten 
                    (List.map (MacroExpander.expand dload.macroexp) cats) in
                cartiter
                    (fun syncat newsyncat ->
                        Dict.add_unarydrv dict syncat newsyncat)
                    syncats newsyncats
          | UncEntry words ->
                List.iter (Dict.add_uncertain dict) words
          | EnumEntry enumlevel ->
                Dict.set_enumlevel dict enumlevel
          | AddAttr (attr, avms) ->
                List.iter 
                    (fun a -> Dict.add_attr dict attr a) 
                    avms
          | AddCats cats ->
                List.iter 
                    (fun cat ->
                        let expcats = 
                            MacroExpander.expand dload.macroexp cat in
                        List.iter (Dict.add_cat dict) expcats)
                    cats
          | AddLeftPuncs words ->
                List.iter 
                    (fun word ->
                        Dict.add_entry dict word 
                            (Cg.Syncat.leftpunc_cat Cg.Syncat.NoDep))
                    words
          | AddRightPuncs words ->
                List.iter 
                    (fun word ->
                        Dict.add_entry dict word 
                            (Cg.Syncat.rightpunc_cat Cg.Syncat.NoDep))
                    words
          | AddLeftSer cats ->
                List.iter
                    (fun cat ->
                        let expcats = MacroExpander.expand dload.macroexp cat in
                        List.iter (Dict.add_leftser dict) expcats)
                    cats
          | AddRightSer cats ->
                List.iter
                    (fun cat ->
                        let expcats = MacroExpander.expand dload.macroexp cat in
                        List.iter (Dict.add_rightser dict) expcats)
                    cats
          | Accept cats ->
            begin
                List.iter
                    (fun cat ->
                        let expcats =
                            MacroExpander.expand dload.macroexp cat in
                        List.iter 
                            (fun cat' -> 
                                Dict.add_accept dict cat')
                            expcats)
                    cats;
                if dload.is_typology then
                    Catgen.run dload.catgen
                else ()
            end
          | Typology typoflags ->
            begin
                dload.is_typology <- true;
                List.iter
                    (fun (flag, value) ->
                        interpret_typology dload flag value)
                    typoflags;
                if dload.is_test_catgen then
                    Catgen.test dload.catgen
                else ();
                Catgen.run dload.catgen
            end
          | DMV fnames -> Dmv.run dload.dmv fnames
          | EVG fnames -> Evg.run dload.evg fnames
          | GHG typoflags -> begin
                List.iter
                    (fun (flag, value) ->
                        interpret_ghg_typology dload flag value)
                    typoflags;
                Ghg.run dload.ghg
            end
          | EnableSplitHead -> CG.enable_split_head ()
          | DisableSplitHead -> CG.disable_split_head ()

    let load fname is_test_catgen =
        let lexbuf = Lexing.from_channel (open_in fname) in
        let dict = Dict.create 100 Dict.NoEnum in
        let dload = create 100 is_test_catgen dict in
        let flag = ref true in
        while !flag do
            try
                let cmd = Ruleparser.dict_entry Rulelexer.token lexbuf in
                interpret dload cmd dict
            with
                Rulelexer.Eof -> flag := false
              | e -> raise e
        done;
        if dload.is_test_catgen then
            printf "%s\n\n" (to_string dload)
        else ();
        dict

end

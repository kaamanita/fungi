(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Dep
open Hdb
open Tree
open Utils
open Printf

module MarshTree = struct

    type tree_t = Tree.t

    type depstruct_t = DepStruct.t

    type t = (tree_t * float * depstruct_t * string list * bool)  option

    type key_t = int

    type value_t = t

    let create tree prob depstr words enum : t =
        Some (tree, prob, depstr, words, enum)

    let equal = Stdlib.(=)

    let hash = Hashtbl.hash

    let get_tree (mtree : t) =
        match mtree with
            Some (tree, _, _, _, _) -> tree
          | None -> failwith "get_tree"

    let get_prob (mtree : t) =
        match mtree with
            Some (_, prob, _, _, _) -> prob
          | None -> failwith "get_prob"

    let get_depstr (mtree : t) =
        match mtree with
            Some (_, _, depstr, _, _) -> depstr
          | None -> failwith "get_depstr"

    let get_words (mtree : t) =
        match mtree with
            Some (_, _, _, words, _) -> words
          | None -> failwith "get_words"

    let get_enum (mtree : t) =
        match mtree with
            Some (_, _, _, _, enum) -> enum
          | None -> failwith "get_words"

    let to_string (mtree : t) =
        match mtree with
            Some (tree, prob, depstr, words, enum) ->
                sprintf "%s\nProbability = %e\n<Dependency>\n%s<Enum=%s>"
                    (Tree.to_string tree) prob (DepStruct.to_string depstr) (string_of_bool enum)
          | None -> "None"

end

module TreeDb = SeqHDB (MarshTree)

module TreeDbWrapper = SeqHDBWrapper (MarshTree)

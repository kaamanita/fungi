(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

open Cg
open Utils
open Printf
open Avm

module MacroSyncat = struct

    type avm_t = Avm.t

    type syncat_t = Syncat.t

    type depdir_t = Syncat.depdir_t

    type t =
        Atom of string
      | Syncat of syncat_t
      | Bwd of t * t * depdir_t * avm_t
      | Fwd of t * t * depdir_t * avm_t

    let create_atom name =
        Atom name

    let create_syncat syncat =
        Syncat syncat

    let create_bwd elt1 elt2 depdir avm =
        Bwd (elt1, elt2, depdir, avm)

    let create_fwd elt1 elt2 depdir avm =
        Fwd (elt1, elt2, depdir, avm)

    let is_atom macrosyncat =
        match macrosyncat with
            Atom _ -> true
          | Syncat _ | Bwd _ | Fwd _ -> false

    let get_label macrosyncat =
        match macrosyncat with
            Atom l -> l
          | Syncat _ | Bwd _ | Fwd _ -> failwith "macrosyncat.get_label"

    let rec to_string macrosyncat =
        match macrosyncat with
            Atom value -> value
          | Syncat syncat -> Syncat.to_string syncat
          | Bwd (elt1, elt2, depdir, avm) ->
                sprintf "%s\\%s%s"
                    (to_string elt1) 
                    (Syncat.depdir_to_string depdir) 
                    (to_string_paren elt2)
          | Fwd (elt1, elt2, depdir, avm) ->
                sprintf "%s/%s%s"
                    (to_string elt1) 
                    (Syncat.depdir_to_string depdir) 
                    (to_string_paren elt2)

    and to_string_paren macrosyncat =
        match macrosyncat with
            Bwd _ | Fwd _ ->
                sprintf "(%s)" (to_string macrosyncat)
          | Atom _ | Syncat _ ->
                to_string macrosyncat

    let equal = Stdlib.(=)

end

module HashedString = struct
    type t = string
    let hash = Hashtbl.hash
    let equal = Stdlib.(=)
end

module MacroTbl = Hashtbl.Make(HashedString)

module MacroExpander = struct

    type macroname_t = string

    type syncat_t = Syncat.t

    type macrosyncat_t = MacroSyncat.t

    type t = syncat_t list MacroTbl.t

    let create n : t =
        MacroTbl.create n

    let to_string macroexp =
        let lines = ref ["<Macro Table>"] in
        let macros = ref [] in
        MacroTbl.iter
            (fun macroname _ ->
                macros := macroname :: !macros)
            macroexp;
        macros := List.sort Stdlib.compare !macros;
        List.iter
            (fun macroname ->
                let syncats = MacroTbl.find macroexp macroname in
                let line = sprintf "Macro %s :- %s"
                    macroname 
                    (string_of_list ~bracket:false 
                        Syncat.to_string syncats) in
                lines := !lines @ [line])
            !macros;
        String.concat "\n" !lines

    let mem macroexp macroname =
        MacroTbl.mem macroexp macroname

    let find macroexp macroname =
        if mem macroexp macroname then
            MacroTbl.find macroexp macroname
        else []

    let add_cats macroexp macroname syncats =
        let oldmembers = find macroexp macroname in
        let newmembers = union oldmembers syncats in
        MacroTbl.replace macroexp macroname newmembers

    let rec expand macroexp macrosyncat =
        Scale.inc ();
        Scale.display ();
        match macrosyncat with
            MacroSyncat.Atom name -> find macroexp name
          | MacroSyncat.Syncat syncat -> [syncat]
          | MacroSyncat.Fwd (elt1, elt2, depdir, avm) ->
                cartmap 
                    (fun e1 e2 -> 
                        Syncat.create_fwd e1 e2 depdir ~ftrs:avm) 
                    (expand macroexp elt1) (expand macroexp elt2)
          | MacroSyncat.Bwd (elt1, elt2, depdir, avm) ->
                cartmap 
                    (fun e1 e2 -> 
                        Syncat.create_bwd e1 e2 depdir ~ftrs:avm) 
                    (expand macroexp elt1) (expand macroexp elt2)

    let add macroexp macroname macrosyncats =
        Scale.add (Scale.WindMill 1) 
            (sprintf "expanding macro %s " macroname) "";
        List.iter
            (fun macrosyncat ->
                let syncats = expand macroexp macrosyncat in
                add_cats macroexp macroname syncats)
            macrosyncats;
        Scale.discard ()

end

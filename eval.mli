(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

module HashedDirDep :
  sig
    type t = Dep.DepPair.t
    val equal : 'a -> 'a -> bool
    val hash : 'a -> int
  end
module DirDepTbl :
  sig
    type key = HashedDirDep.t
    type 'a t = 'a Hashtbl.Make(HashedDirDep).t
    val create : int -> 'a t
    val clear : 'a t -> unit
    val copy : 'a t -> 'a t
    val add : 'a t -> key -> 'a -> unit
    val remove : 'a t -> key -> unit
    val find : 'a t -> key -> 'a
    val find_all : 'a t -> key -> 'a list
    val replace : 'a t -> key -> 'a -> unit
    val mem : 'a t -> key -> bool
    val iter : (key -> 'a -> unit) -> 'a t -> unit
    val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val length : 'a t -> int
  end
module type DATA_TYPE =
  sig
    type tree_t
    type count_t
    type t
    type storage_t
    val extract : tree_t -> t list
    val extract_words : tree_t -> t list
    val get_headpos : tree_t -> int
    val tree_to_string : tree_t -> string
    val to_string : t -> string
    val create_storage : unit -> storage_t
    val store_correct : storage_t -> t -> unit
    val store_gcorrect : storage_t -> t -> unit
    val count_correct : storage_t -> t -> count_t
    val store_overgen : storage_t -> t -> unit
    val store_undergen : storage_t -> t -> unit
    val store_total : storage_t -> t -> unit
    val top_history : storage_t -> int -> (count_t * t) list
  end
module Metric :
  functor (GoldData : DATA_TYPE) ->
  functor (TestData : DATA_TYPE with
    type t = GoldData.t
    and type count_t = GoldData.count_t
    and type storage_t = GoldData.storage_t) ->
sig
      type goldtree_t = GoldData.tree_t
      type testtree_t = TestData.tree_t
      type count_t = TestData.count_t
      type data_t = GoldData.t
      type storage_t = GoldData.storage_t
      type comp_recall_t = data_t list -> data_t list -> data_t list
      type t
      val create : comp_recall_t -> comp_recall_t -> t
      val create_from_history : 
        comp_recall_t -> comp_recall_t -> storage_t * storage_t * storage_t * storage_t * storage_t -> t
      val get_history : t -> storage_t * storage_t * storage_t * storage_t * storage_t
      val store_history :
        t -> GoldData.t list -> GoldData.t list -> GoldData.t list -> GoldData.t list -> unit
      val process : ?printout:bool -> t -> GoldData.tree_t -> TestData.tree_t -> unit
      val get_precision : t -> float
      val get_recall : t -> float
      val get_f1 : t -> float
      val top_corrects : t -> int -> (count_t * GoldData.t) list
      val top_overgens : t -> int -> (count_t * GoldData.t) list
      val top_undergens : t -> int -> (count_t * GoldData.t) list
      val top_totals : t -> int -> (count_t * count_t * GoldData.t) list
end
module UBracket :
  sig
    type tree_t = Tree.Tree.t
    type count_t = int
    type t = int * int
    type storage_t = t option
    val extract : Tree.Tree.t -> Tree.Tree.span_t list
    val extract_words : Tree.Tree.t -> Tree.Tree.span_t list
    val get_headpos : tree_t -> int
    val tree_to_string : Tree.Tree.t -> string
    val to_string : int * int -> string
    val create_storage : 'a -> 'b option
    val store_correct : 'a -> 'b -> unit
    val store_gcorrect : 'a -> 'b -> unit
    val count_correct : 'a -> 'b -> count_t
    val store_overgen : 'a -> 'b -> unit
    val store_undergen : 'a -> 'b -> unit
    val top_history : 'a -> 'b -> 'c list
  end
module UndirDep :
  sig
    type tree_t = Tree.Tree.t
    type count_t = int
    type t = Dep.DepPair.t
    type storage_t = t option
    val extract : Tree.Tree.t -> Dep.DepStruct.deppair_t list
    val extract_words : Tree.Tree.t -> Dep.DepStruct.deppair_t list
    val get_headpos : tree_t -> int
    val tree_to_string : Tree.Tree.t -> string
    val to_string : Dep.DepPair.t -> string
    val create_storage : 'a -> 'b option
    val store_correct : 'a -> 'b -> unit
    val store_gcorrect : 'a -> 'b -> unit
    val count_correct : 'a -> 'b -> count_t
    val store_overgen : 'a -> 'b -> unit
    val store_undergen : 'a -> 'b -> unit
    val top_history : 'a -> 'b -> 'c list
  end
module DirDep :
  sig
    type tree_t = Tree.Tree.t
    type count_t = int
    type t = Dep.DepPair.t
    type storage_t = count_t DirDepTbl.t
    val extract : Tree.Tree.t -> Dep.DepStruct.deppair_t list
    val extract_words : Tree.Tree.t -> Dep.DepStruct.deppair_t list
    val get_headpos : tree_t -> int
    val tree_to_string : Tree.Tree.t -> string
    val to_string : Dep.DepPair.t -> string
    val create_storage : 'a -> 'b DirDepTbl.t
    val store_history : storage_t -> t -> unit
    val store_correct : storage_t -> t -> unit
    val store_gcorrect : storage_t -> t -> unit
    val count_correct : storage_t -> t -> count_t
    val store_overgen : storage_t -> t -> unit
    val store_undergen : storage_t -> t -> unit
    val store_total : storage_t -> t -> unit
    val sort_history : storage_t -> (count_t * t) list
    val top_history : storage_t -> int -> (count_t * t) list
  end
module ConllDirDep :
  sig
    type tree_t = Dep.DepStruct.t
    type count_t = int
    type t = Dep.DepPair.t
    type storage_t = count_t DirDepTbl.t
    val extract : Dep.DepStruct.t -> Dep.DepStruct.deppair_t list
    val extract_words : Dep.DepStruct.t -> Dep.DepStruct.deppair_t list
    val get_headpos : tree_t -> int
    val tree_to_string : Dep.DepStruct.t -> string
    val to_string : Dep.DepPair.t -> string
    val create_storage : 'a -> 'b DirDepTbl.t
    val store_history : storage_t -> t -> unit
    val store_correct : storage_t -> t -> unit
    val store_gcorrect : storage_t -> t -> unit
    val count_correct : storage_t -> t -> count_t
    val store_overgen : storage_t -> t -> unit
    val store_undergen : storage_t -> t -> unit
    val store_total : storage_t -> t -> unit
    val sort_history : storage_t -> (count_t * t) list
    val top_history : storage_t -> int -> (count_t * t) list
  end
module HeadBracket :
  sig
    type tree_t = Tree.Tree.t
    type count_t = int
    type span_t = int * int
    type word_t = string
    type t = span_t * word_t
    type storage_t = t option
    val extract : Tree.Tree.t -> (Tree.Tree.span_t * Tree.Tree.word_t) list
    val extract_words : Tree.Tree.t -> (Tree.Tree.span_t * Tree.Tree.word_t) list
    val get_headpos : tree_t -> int
    val tree_to_string : Tree.Tree.t -> string
    val to_string : (int * int) * string -> string
    val create_storage : 'a -> 'b option
    val store_correct : 'a -> 'b -> unit
    val store_gcorrect : 'a -> 'b -> unit
    val count_correct : 'a -> 'b -> count_t
    val store_overgen : 'a -> 'b -> unit
    val store_undergen : 'a -> 'b -> unit
    val top_history : 'a -> 'b -> 'c list
  end
module Eval :
  sig
    type tree_t = Tree.Tree.t
    type depstr_t = Dep.DepStruct.t
    type storage_t = int DirDepTbl.t
    type dirdepstorage_t = Metric(DirDep)(DirDep).storage_t
    type t
    val create : tree_t list -> tree_t list -> t
    val get_history :
      t ->
      dirdepstorage_t * dirdepstorage_t * dirdepstorage_t * dirdepstorage_t * dirdepstorage_t
    val create_from_history :
      tree_t list ->
      tree_t list ->
      dirdepstorage_t * dirdepstorage_t * dirdepstorage_t * dirdepstorage_t * dirdepstorage_t -> t
    val run_brk : t -> unit
    val run_dep : t -> unit
    val get_ubrkscores : t -> float * float * float
    val get_udepscores : t -> float * float * float
    val get_ddepscores : t -> float * float * float
    val get_hbrkscores : t -> float * float * float
    val top_dirdep_corrects : t -> int -> (int * DirDep.t) list
    val top_dirdep_overgens : t -> int -> (int * DirDep.t) list
    val top_dirdep_undergens : t -> int -> (int * DirDep.t) list
    val top_dirdep_totals : t -> int -> (int * int * DirDep.t) list
  end
module ConllEval :
sig
    type tree_t = Tree.Tree.t
    type count_t = int
    type depstr_t = Dep.DepStruct.t
    type storage_t = int DirDepTbl.t
    type dirdepstorage_t = Metric(ConllDirDep)(DirDep).storage_t
    type t
    val create : depstr_t list -> tree_t list -> t
    val get_history :
      t ->
      storage_t * storage_t * storage_t * storage_t * storage_t
    val create_from_history :
      depstr_t list ->
      tree_t list ->
      storage_t * storage_t * storage_t * storage_t * storage_t -> t
    val run : t -> unit
    val get_ddepscores : t -> float * float * float
    val top_dirdep_corrects : t -> int -> (int * ConllDirDep.t) list
    val top_dirdep_overgens : t -> int -> (int * ConllDirDep.t) list
    val top_dirdep_undergens : t -> int -> (int * ConllDirDep.t) list
    val top_dirdep_totals : t -> int -> (int * int * ConllDirDep.t) list
end
module Evaluator :
  sig
    type tree_t = Tree.Tree.t
    type eval_t = Eval.t
    type storage_t = int DirDepTbl.t
    type dirdepstorage_t = Eval.dirdepstorage_t
    type t
    val create :
      string -> int -> string list -> string list -> string list -> t
    val get_history :
      t ->
      dirdepstorage_t * dirdepstorage_t * dirdepstorage_t * dirdepstorage_t * dirdepstorage_t
    val create_from_history :
      string ->
      int ->
      string list ->
      string list ->
      string list ->
      dirdepstorage_t * dirdepstorage_t * dirdepstorage_t * dirdepstorage_t * dirdepstorage_t -> t
    val run : t -> unit
    val compute :
      string -> int -> string list -> string list -> string list -> unit
    val get_ubrkscores : t -> float * float * float
    val get_udepscores : t -> float * float * float
    val get_ddepscores : t -> float * float * float
    val get_hbrkscores : t -> float * float * float
    val scores_to_string : t -> string
    val tophist_to_string : (int * Dep.DepPair.t) list -> string
    val history_to_string : t -> string
    val top_corrects : t -> (int * DirDep.t) list
    val top_overgens : t -> (int * DirDep.t) list
    val top_undergens : t -> (int * DirDep.t) list
    val top_totals : t -> (int * int * DirDep.t) list
  end
module ConllEvaluator :
  sig
    type tree_t = Tree.Tree.t
    type eval_t = ConllEval.t
    type storage_t = int DirDepTbl.t
    type dirdepstorage_t = ConllEval.dirdepstorage_t
    type t
    val create : string -> int -> string list -> string list -> t
    val get_history :
      t -> storage_t * storage_t * storage_t * storage_t * storage_t
    val create_from_history :
      string ->
      int ->
      string list ->
      string list ->
      storage_t * storage_t * storage_t * storage_t * storage_t -> t
    val run : t -> unit
    val compute : string -> int -> string list -> string list -> unit
    val get_ddepscores : t -> float * float * float
    val scores_to_string : t -> string
    val tophist_to_string : (int * Dep.DepPair.t) list -> string
    val history_to_string : t -> string
    val top_corrects : t -> (int * ConllDirDep.t) list
    val top_overgens : t -> (int * ConllDirDep.t) list
    val top_undergens : t -> (int * ConllDirDep.t) list
    val top_totals : t -> (int * int * DirDep.t) list
  end


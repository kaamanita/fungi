(** Dictionary. *)

(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

(** Dictionary. *)
module Dict :
sig
    (** Lexical item. *)
    type word_t = string

    (** Syntactic category. *)
    type syncat_t = Cg.CG.syncat_t

    (** Attribute-value matrix. *)
    type avm_t = Avm.Avm.t
    
    (** Enumeration level. *)
    type enumlevel_t = 
        NoEnum
      | EnumOnUncertain
      | EnumOnAll

    (** [enumlevel_to_string e] returns a string representation of an
        enumeration level [e]. *)
    val enumlevel_to_string : enumlevel_t -> string
    
    (** Dictionary. *)
    type t
    
    (** [create n e] creates a dictionary with a number of entries [n]
        and an enumeration level [e]. *)
    val create : int -> enumlevel_t -> t

    val add_aux : t -> word_t -> unit
    val add_v : t -> word_t -> unit
    val add_n : t -> word_t -> unit
    val add_adj : t -> word_t -> unit
    val add_adv : t -> word_t -> unit
    val add_prep : t -> word_t -> unit
    val add_num : t -> word_t -> unit
    val add_conj : t -> word_t -> unit

    (** [to_string d] represents a dictionary [d] with a string. *)
    val to_string : t -> string

    (** [get_enumlevel d] finds the enumeration level of a dictionary 
        [d]. *)
    val get_enumlevel : t -> enumlevel_t

    (** [set_enumlevel d e] sets the enumeration level of the 
        dictionary [d] with an enumeration level [e]. *)
    val set_enumlevel : t -> enumlevel_t -> unit
    
    (** [mem d w] checks if a word [w] is in a dictionary [d]. *)
    val mem : t -> word_t -> bool
    
    (** [find d w] finds all categories of a word [w] from a dictionary 
        [d]. It raises Not_found if the word is not found. *)
    val find : t -> word_t -> syncat_t list
    
    (** [is_uncertain d w] checks if a word [w] is in the uncertain 
        list. *)
    val is_uncertain : t -> word_t -> bool

    (** [add_cat d c] adds a syntactic category [c] to the list of known
        categories of a dictionary [d]. *)
    val add_cat : t -> syncat_t -> unit

    (** [add_cat d c] adds a syntactic category [c] to the list of
        accepted categories of a dictionary [d]. *)
    val add_accept : t -> syncat_t -> unit

    val is_leftser : t -> syncat_t -> bool
    val is_rightser : t -> syncat_t -> bool
    val add_leftser : t -> syncat_t -> unit
    val add_rightser : t -> syncat_t -> unit
    
    (** [add_entry d w c] adds a lexicon entry of word [w] and syntactic
        category [c] into a dictionary [d]. *)
    val add_entry : t -> word_t -> syncat_t -> unit

    val add_attr : t -> string -> avm_t -> unit
    
    (** [add_uncertain d w] adds an uncertain word [w] into a dictionary
        [w]. *)
    val add_uncertain : t -> word_t -> unit

    (** [add_unarydrv d c1 c2] adds a unary transformation [c1 => c2] 
        to a dictionary [d]. *)
    val add_unarydrv : t -> syncat_t -> syncat_t -> unit
    
    (** [get_cats d w] finds all categories of a word [w] in a dictionary
        [d]. Dependending on the enumeration level, all categories may be 
        returned if the word is in the uncertainty list. *)
    val get_cats : t -> word_t -> syncat_t list

    val get_avms : t -> word_t -> avm_t list

    val get_allcats : t -> syncat_t list

    (** [get_accepts d] finds the list of accepted categories of the
        dictionary [d]. *)
    val get_accepts : t -> syncat_t list

    (** [get_unarydrv d] finds the list of unary transformation rules of
        a dictionary [d]. *)
    val get_unarydrv : t -> Cg.CG.transf_t

    val is_allowed_univdep : t -> word_t -> word_t -> bool

    val add_catdict : t -> word_t -> word_t -> unit

    val iter_catdict : t -> (word_t -> word_t list -> unit) -> unit

    val get_catdict : t -> word_t -> word_t list
end

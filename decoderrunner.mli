type decoding_t = ViterbiDecoding | MEVDecoding | MBRDecoding
module DecoderRunner :
  sig
    type t
    val create :
      decoding_t -> Cg.Syncat.t -> 
      Models.probflag_t list -> float -> string -> float -> float -> float -> string list -> string -> bool -> t
    val parse_chart :
      decoding_t ->
      Deriv.DerivGenModel.t ->
      Chart.Chart.t ->
      Cg.Syncat.t -> float -> float -> float ->
      Tree.Tree.t * float * Tree.Tree.depstruct_t * bool
    val parse_wrapper :
      decoding_t ->
      Deriv.DerivGenModel.t ->
      Cg.Syncat.t ->
      Chartdb.ChartDbWrapper.t -> Treedb.TreeDb.t -> float -> float -> float -> bool -> unit
    val run : t -> unit
    val compute :
      decoding_t ->
      Cg.Syncat.t ->
      Models.probflag_t list -> float -> string -> float -> float -> float -> string list -> string -> bool -> unit
  end
val set_probflags : Models.probflag_t list ref -> string -> unit
val set_decoding : decoding_t ref -> string -> unit
val set_start : Cg.Syncat.t ref -> string -> unit

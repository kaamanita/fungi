(***********************************************************
*                                                          *
*      FUNGI - Fast UNsupervised Grammar Induction         *
*                                                          *
*   Originator: Prachya Boonkwan [1], [2]                  *
*               [1] School of Informatics,                 *
*                   University of Edinburgh, UK            *
*               [2] National Electronics and Computer      *
*                   Technology Center (NECTEC), Thailand   *
*                                                          *
*   Copyright (C) 2011 Prachya Boonkwan                    *
*   <p.boonkwan@sms.ed.ac.uk, kaamanita@hotmail.com>       *
*                                                          *
*   This library is free software; you can redistribute    *
*   it and/or modify it under the terms of the GNU         *
*   Lesser General Public License as published by the      *
*   Free Software Foundation; either version 2.1 of the    *
*   License, or (at your option) any later version.        *
*                                                          *
*   This library is distributed in the hope that it will   *
*   be useful, but WITHOUT ANY WARRANTY; without even      *
*   the implied warranty of MERCHANTABILITY or FITNESS     *
*   FOR A PARTICULAR PURPOSE.  See the GNU Lesser          *
*   General Public License for more details.               *
*                                                          *
*   You should have received a copy of the GNU Lesser      *
*   General Public License along with this library; if     *
*   not, write to the Free Software Foundation, Inc.,      *
*   59 Temple Place, Suite 330, Boston, MA  02111-1307     *
*   USA.                                                   *
*                                                          *
***********************************************************)

module Avm = struct

    type t = (string * string) list

    let create content = 
        List.sort Stdlib.compare (Utils.elim_dups content)

    let create_empty () = []

    let is_empty avm =
        avm = []

    let rec subsumes avm1 avm2 =
        match avm1 with
            [] -> true
          | (k, v) :: rest ->
                if not (List.mem_assoc k avm2) then 
                    subsumes rest avm2
                else
                    let v' = List.assoc k avm2 in
                    let lv' = String.length v' in
                    let avm2' = List.remove_assoc k avm2 in
                    if v = v' then
                        subsumes rest avm2'
                    else if v' = "$" || v = "$" then
                        subsumes rest avm2'
                    else if lv' > 1 && v'.[0] = '$' 
                            && String.sub v' 1 (lv' - 1) = v then
                        subsumes rest avm2'
                    else false

    let rec subsumes_attrs avm1 avm2 =
        match avm1 with
            [] -> true
          | (k, _) :: rest ->
                if List.mem_assoc k avm2 then
                    subsumes_attrs rest avm2
                else false

    let rec combine avm1 avm2 = 
        match avm1 with
            [] -> avm2
          | (k, v) :: rest ->
                let rest = List.remove_assoc k avm1 in
                if not (List.mem_assoc k avm2) then
                    (k, v) :: (combine rest avm2)
                else
                    let v' = List.assoc k avm2 in
                    let lv = String.length v in
                    let lv' = String.length v' in
                    let avm2' = List.remove_assoc k avm2 in
                    if v = v' then
                        (k, v) :: (combine rest avm2')
                    else if v' = "$" then
                        combine rest avm2'
                    else if v = "$" then
                        (k, v) :: (combine rest avm2')
                    else if lv' > 1 && v'.[0] = '$' 
                            && String.sub v' 1 (lv' - 1) = v then
                        combine rest avm2'
                    else if lv > 1 && v.[0] = '$' 
                            && String.sub v 1 (lv - 1) = v then
                        combine rest avm2'
                    else
                        combine rest avm2'

    let rec replace_inner avm1 avm2 =
        match avm1 with
            [] -> avm2
          | (k, v) :: rest ->
                if (List.mem_assoc k avm2) then begin
                    let v' = List.assoc k avm2 in
                    let lv' = String.length v' in
                    let avm2' = List.remove_assoc k avm2 in
                    if v' = "$" then
                        replace_inner rest avm2'
                    else if lv' > 1 && v'.[0] = '$' 
                            && String.sub v' 1 (lv' - 1) = v then
                        replace_inner rest avm2'
                    else (k, v') :: replace_inner rest avm2'
                end else (k, v) :: replace_inner rest avm2

    let replace avm1 avm2 =
        List.sort Stdlib.compare (replace_inner avm1 avm2)

    let to_string avm =
        Printf.sprintf "{%s}" 
            (String.concat ", " 
                (List.map 
                    (fun (k, v) -> 
                        Printf.sprintf "%s:%s" k v) 
                    avm))

    let to_string_cat avm =
        match avm with
            [] -> ""
          | _ -> to_string avm

    let equal = Stdlib.(=)

    let hash = Hashtbl.hash

end

(** Syntactic Derivation *)

(** Syntactic Derivation Module *)
module Deriv :
sig
    (** Syntactic category *)
    type syncat_t = Cg.CG.syncat_t

    (** Dependency direction *)
    type depdir_t = Cg.CG.depdir_t

    (** Lexical item *)
    type word_t = string

    (** Premise *)
    type prems_t

    (** Create a premise from a syntactic category and 
        a lexical item. *)
    val create_prems : syncat_t -> word_t -> prems_t

    (** Get the syntactic category from the premise. *)
    val get_syncat : prems_t -> syncat_t

    (** Get the head lexicon from the premise. *)
    val get_head : prems_t -> word_t
    
    (** Consequence *)
    type consq_t

    (** Create a consequence from a list of symbols and a 
        dependency direction. *)
    val create_consq_dtrs : (syncat_t * word_t) list -> depdir_t -> consq_t

    (** Create a consequence from a lexical item. *)
    val create_consq_lex : word_t -> consq_t

    (** Get the daughter list from the consequence. *)
    val get_dtrs : consq_t -> (syncat_t * word_t) list

    (** Get the dependency direction from the consequence. *)
    val get_depdir : consq_t -> depdir_t

    (** Get the lexical item from the consequence. *)
    val get_word : consq_t -> word_t

    (** Check if the consequence is a production. *)
    val is_consq_dtrs : consq_t -> bool

    (** Check if the consequence is a lexical item. *)
    val is_consq_lex : consq_t -> bool
    
    (** Syntactic derivation *)
    type t
    
    (** Create a syntactic derivation from its premise 
        and consequence. This function is for constructing the
        module ProbTbl, not for practical use. *)
    val create : prems_t -> consq_t -> t

    (** Create a syntactic derivation from its head symbol, head word,
        daughter symbols, and dependency direction. This function
        is for practical use. *)
    val create_deriv_dtrs : 
        syncat_t -> word_t -> (syncat_t * word_t) list -> depdir_t -> t

    (** Create a syntactic derivation from its head symbol
        and lexical item. This function is for practical use. *)
    val create_deriv_lex : syncat_t -> word_t -> t
    
    (** Get the premise. *)
    val get_prems : t -> prems_t
    
    (** Get the consequence. *)
    val get_consq : t -> consq_t

    (** Check if two syntactic derivations equal. *)
    val equal : t -> t -> bool
    
    (** Calculate the hash value of the syntactic derivation. *)
    val hash : t -> int
    
    (** Check if two premises equal. *)
    val equal_prems : prems_t -> prems_t -> bool
    
    (** Calculate the hash value of the premise. *)
    val hash_prems : prems_t -> int

    val to_string : t -> string

    val prems_to_string : prems_t -> string

    val backoff : t -> t * word_t
end

module DerivBackoff :
sig
    type deriv_t = Deriv.t
    type word_t = string
    type data_t = deriv_t
    type t = { deriv : deriv_t; word : word_t; }
    val equal : t -> t -> bool
    val hash : t -> int
    val to_string : t -> string
    type prems_t = deriv_t
    type consq_t = word_t
    val create : deriv_t -> word_t -> t
    val get_prems : t -> deriv_t
    val get_consq : t -> word_t
    val equal_prems : Deriv.t -> Deriv.t -> bool
    val hash_prems : Deriv.t -> int
    val prems_to_string : Deriv.t -> string
    val backoff : Deriv.t -> Deriv.t * Deriv.word_t
end

(** Syntactic derivation *)
type deriv_t = Deriv.t

module DerivProbTbl :
sig
    type inst_t = Deriv.t
    type t
    val create : int -> float -> t
    val get_epsilon : t -> float
    val mem : t -> inst_t -> bool
    val get_prob : t -> inst_t -> float
    val get_smooth_prob : t -> inst_t -> float
    val set_prob : t -> inst_t -> float -> unit
    val acc_prob : t -> inst_t -> float -> unit
    val iter : t -> (inst_t -> float -> unit) -> unit
    val to_string : t -> string
end

(** Generative Model of Syntactic Derivation Module *)
module DerivGenModel :
sig
    (** Instance *)
    type inst_t = deriv_t

    (** Function for accumulating the probability of an instance *)
    type fn_acc_prob_t = inst_t -> float -> unit
    
    (** Function for calculating the probability (optionally 
        smoothened) of an instance *)
    type fn_get_prob_t = inst_t -> float
    
    (** Function for setting the probability of an instance *)
    type fn_set_prob_t = inst_t -> float -> unit
    
    (** Feature probability table *)
    type probftr_t (* = Prob.GenModel(Deriv).probftr_t *)
    
    (** Create a feature probability table from the 
        four functions: get_prob, get_smooth_prob, set_prob, 
        and acc_prob. *)
    val create_probftr :
        fn_get_prob_t -> fn_get_prob_t 
        -> fn_set_prob_t -> fn_acc_prob_t 
        -> probftr_t

    type probtbl_t = DerivProbTbl.t
    
    (** Generative model *)
    type t
    
    (** Create a generative model with the size, the default value, 
        and the list of generative probability tables. *)
    val create : int -> float -> probftr_t list -> t

    val length : t -> int
    (** Get the probability of the instance. *)
    val get_prob : t -> inst_t -> float

    val mem : t -> inst_t -> bool
    
    (** Get the smoothened probability of the instance. *)
    val get_smooth_prob : t -> inst_t -> float
    
    (** Set the probability of the instance. *)
    val set_prob : t -> inst_t -> float -> unit
    
    (** Accumulate the probability of the instance. *)
    val acc_prob : t -> inst_t -> float -> unit

    (** Iterate all entries with the function. *)
    val iter : t -> (inst_t -> float -> unit) -> unit
    
    (** Read a generative model from the file. *)
    val from_file : string -> probftr_t list -> t
    
    (** Write the generative model from a file. *)
    val to_file : t -> string -> unit

    val to_string : t -> string

    val print : t -> unit

    val dkl : t -> t -> float
end

(*
(** Generative Model of Syntactic Derivation Module with Backoff *)
module DerivGenModelWithBackoff :
sig
    (** Instance *)
    type inst_t = deriv_t

    (** Feature probability table *)
    type probftr_t (* = Prob.GenModel(Deriv).probftr_t *)

    (** Create a feature probability table from the 
        four functions: get_prob, get_smooth_prob, set_prob, 
        and acc_prob. *)
    val create_probftr :
        DerivGenModel.fn_get_prob_t -> 
        DerivGenModel.fn_get_prob_t -> 
        DerivGenModel.fn_set_prob_t -> 
        DerivGenModel.fn_acc_prob_t -> 
        probftr_t

    (** Generative model *)
    type t
    
    (** Create a generative model with the size, the default value, 
        and the list of generative probability tables. *)
    val create : int -> float -> probftr_t list -> t
    
    (** Get the probability of the instance. *)
    val get_prob : t -> inst_t -> float
    
    (** Get the smoothened probability of the instance. *)
    val get_smooth_prob : t -> inst_t -> float
    
    (** Set the probability of the instance. *)
    val set_prob : t -> inst_t -> float -> unit
    
    (** Accumulate the probability of the instance. *)
    val acc_prob : t -> inst_t -> float -> unit

    (** Iterate all entries with the function. *)
    val iter : t -> (inst_t -> float -> unit) -> unit
    
    (** Read a generative model from the file. *)
    val from_file : string -> probftr_t list -> t
    
    (** Write the generative model from a file. *)
    val to_file : t -> string -> unit

    val to_string : t -> string
end
*)

(** Convert a word entry into a syntactic derivation. *)
val conv_word_to_deriv : Deriv.syncat_t -> Deriv.word_t -> Deriv.t

(** Convert a derivation entry into a syntactic derivation. *)
val conv_dtrargs_to_deriv :
    Deriv.syncat_t -> Deriv.word_t -> Deriv.depdir_t -> 
    (Deriv.syncat_t * 'a * 'b * Deriv.word_t) list -> Deriv.t

(** Feature Extraction Module *)
module type FTR_TYPE =
sig
    (** Syntactic category *)
    type prems_t

    (** Feature *)
    type t

    (** Check if two premises equal. *)
    val equal_prems : prems_t -> prems_t -> bool

    (** Calculate the hash value of the premise. *)
    val hash_prems : prems_t -> int

    (** Check if two feature values equal. *)
    val equal : t -> t -> bool

    (** Calculate the hash value of the feature value. *)
    val hash : t -> int

    (** Extract the feature value(s) of the parent 
        syntactic category. *)
    val extract_mtrftrs : deriv_t -> (prems_t * t) list

    (** Extract the feature value(s) of the daughter
        syntactic categories. *)
    val extract_dtrftrs : deriv_t -> (prems_t * t) list

    val to_string : t -> string

    val prems_to_string : prems_t -> string
end

(** Feature Extraction Module for Syntactic Derivation *)
module DerivFtr :
functor (Ftr : FTR_TYPE) ->
sig
    (** Feature probability table *)
    type probftr_t = DerivGenModel.probftr_t

    (** Create a feature probability table for being embedded in 
        a generative model. *)
    val create : int -> float -> probftr_t
end
